﻿using HMTool.Network.Login;
using HMTool.Network.Servers;
using Sfs2X.Core;
using Sfs2X.Entities.Data;

namespace HMTool.Network.SFS
{
    public class GmSFS : BaseSFS
    {
        public GmSFS(ServerInfo serverInfo, LoginInfo loginInfo) : base(serverInfo, loginInfo)
        {
            
        }

        public override string IP => ServerInfo.GmServerIP;
        public override int Port => ServerInfo.GmServerPort;
        public override string ResIP => ServerInfo.GmServerResourceIP;
        public override int ResPort => ServerInfo.GameServerResourcePort;

        public override ISFSObject GetLoginParameters(CustomSfs2x customSfs2X)
        {
            GMLoginInfo gmLoginInfo = LoginInfo as GMLoginInfo;

            ISFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutUtfString("pass", gmLoginInfo.Password);
            sfsObj.PutInt("version", gmLoginInfo.version);
            return sfsObj;
        }

        public override void ConnectionEvent(BaseEvent baseEvent)
        {

        }

        public override void ConnectionLostEvent(BaseEvent baseEvent)
        {

        }

        public override void LoginEvent(BaseEvent baseEvent)
        {

        }

        public override void LoginErrorEvent(BaseEvent baseEvent)
        {

        }

        public override void ExtensionResponseEvent(BaseEvent baseEvent)
        {
            string msg = string.Empty;
            msg = GetErrorMsg(baseEvent);
            
            bool isSuccess = string.IsNullOrEmpty(msg);

            var type = this.GetType();
            var key = (string)baseEvent.Params[RESPONSE.Key.CMD];
            var resObj = baseEvent.Params[RESPONSE.Key.DATA] as SFSObject;

            var findProtocol = Protocol.GetProtocol(type, key);
            findProtocol.ResEvent?.Invoke(isSuccess, resObj);

            if (!isSuccess)
                CustomMessageBox.Show(msg);
        }
    }
}
