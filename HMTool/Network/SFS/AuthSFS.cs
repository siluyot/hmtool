﻿using HMTool.Latale;
using HMTool.Network.Login;
using HMTool.Network.Servers;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Sfs2X.Util;

namespace HMTool.Network.SFS
{
    public class AuthSFS : BaseSFS
    {
        public AuthSFS(ServerInfo serverInfo, LoginInfo loginInfo) : base(serverInfo, loginInfo)
        {

        }

        public override string IP => ServerInfo.AuthServerIP;
        public override int Port => ServerInfo.AuthServerPort;
        public override string ResIP => string.Empty;
        public override int ResPort => 0;

        public override ISFSObject GetLoginParameters(CustomSfs2x customSfs2X)
        {
            ISFSObject sfsObj = SFSObject.NewInstance();

            sfsObj.PutInt("g", ServerInfo.GameServerSeq);
            //sfsObj.PutInt("g", 0);            
            sfsObj.PutInt("1", 12);                         //앱버전...임시로 12
            sfsObj.PutByteArray("2", new ByteArray());      //기기고유값
            sfsObj.PutByte("3", 0);
            sfsObj.PutByte("4", 1);
            sfsObj.PutByteArray("6", Utills.GetEncrypt("b8ba95d19a8a7c9000f7527fd155fe7a", customSfs2X.Sfs2x));

            return sfsObj;
        }

        public override void ConnectionEvent(BaseEvent baseEvent)
        {

        }

        public override void ConnectionLostEvent(BaseEvent baseEvent)
        {

        }

        public override void LoginEvent(BaseEvent baseEvent)
        {

        }

        public override void LoginErrorEvent(BaseEvent baseEvent)
        {

        }

        public override void ExtensionResponseEvent(BaseEvent baseEvent)
        {

        }

    }
}
