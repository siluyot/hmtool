﻿using HMTool.Latale;
using HMTool.Network.Login;
using HMTool.Network.Servers;
using Sfs2X.Core;
using Sfs2X.Entities.Data;

namespace HMTool.Network.SFS
{
    public class GameSFS : BaseSFS
    {
        private GameInfo gameInfo;
        private string accountPass;

        public GameSFS(ServerInfo serverInfo, LoginInfo loginInfo) : base(serverInfo, loginInfo)
        {

        }

        public override string IP => gameInfo?.gameIP;
        public override int Port => gameInfo.port;
        public override string ResIP => string.Empty;
        public override int ResPort => 0;

        public void SetInfo(GameInfo gameInfo, string accountPass)
        {
            this.gameInfo = gameInfo;
            this.accountPass = accountPass;
        }

        public override ISFSObject GetLoginParameters(CustomSfs2x customSfs2X)
        {
            if (gameInfo == null) return null;

            ISFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutInt("g", ServerInfo.GameServerSeq);
            sfsObj.PutByteArray("1", Utills.GetEncrypt(accountPass, customSfs2X.Sfs2x));
            sfsObj.PutInt("2", gameInfo.updateKey);
            sfsObj.PutInt("4", 12); //앱버전...
            sfsObj.PutByte("5", 0);
            sfsObj.PutByteArray("6", Utills.GetEncrypt("66619086a9beba7048e9", customSfs2X.Sfs2x));
            sfsObj.PutByteArray("8", Utills.GetEncrypt("9%29.png", customSfs2X.Sfs2x));
            sfsObj.PutByte("9", 0); //새로운계정이면 0 아니면 1
            sfsObj.PutByteArray("10", Utills.GetEncrypt("4v4c3fubyd", customSfs2X.Sfs2x)); //기기고유값
            sfsObj.PutByte("11", gameInfo.IsTossServer ? (byte)1 : (byte)0); //토스서버면 1 아니면 0
            sfsObj.PutInt("12", gameInfo.userSessionKey); //서버를 바꾸는거면 -1 아니면 0
            sfsObj.PutUtfString("13", string.Empty);

            return sfsObj;
        }

        public override void ConnectionEvent(BaseEvent baseEvent)
        {

        }

        public override void ConnectionLostEvent(BaseEvent baseEvent)
        {

        }

        public override void LoginEvent(BaseEvent baseEvent)
        {

        }

        public override void LoginErrorEvent(BaseEvent baseEvent)
        {

        }

        public override void ExtensionResponseEvent(BaseEvent baseEvent)
        {

        }

    }

    public class GameInfo
    {
        public string gameIP;
        public int port;
        public string resIP;
        public string appVer;
        public int updateKey;
        public int zoneKey;
        public bool IsTossServer;
        public int userSessionKey;

        public GameInfo(ISFSObject resObj)
        {
            if (resObj != null)
            {
                ISFSObject gameServerInfo = resObj.GetSFSObject("1");

                gameIP = gameServerInfo.ContainsKey("1") ? gameServerInfo.GetUtfString("1") : string.Empty;
                port = gameServerInfo.ContainsKey("2") ? gameServerInfo.GetInt("2") : -1;
                resIP = gameServerInfo.ContainsKey("c") ? gameServerInfo.GetUtfString("c") : string.Empty;
                appVer = gameServerInfo.ContainsKey("a") ? gameServerInfo.GetUtfString("a") : string.Empty;
                updateKey = gameServerInfo.ContainsKey("3") ? gameServerInfo.GetInt("3") : -1;
                zoneKey = gameServerInfo.ContainsKey("4") ? gameServerInfo.GetInt("4") : -1;
                IsTossServer = (resObj.ContainsKey("2") && resObj.GetByte("2") == 1) ? true : false;
                userSessionKey = resObj.ContainsKey("3") ? resObj.GetInt("3") : -1;
            }
        }
    }
}
