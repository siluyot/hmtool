﻿using HMTool.Latale;
using HMTool.Network.Login;
using HMTool.Network.Servers;
using Sfs2X.Core;
using Sfs2X.Entities.Data;

namespace HMTool.Network.SFS
{
    public abstract class BaseSFS
    {
        public ServerInfo ServerInfo { get; protected set; }
        public LoginInfo LoginInfo { get; protected set; }

        public BaseSFS(ServerInfo serverInfo, LoginInfo loginInfo)
        {
            this.ServerInfo = serverInfo;
            this.LoginInfo = loginInfo;
        }

        public abstract string IP { get; }
        public abstract int Port { get; }

        public abstract string ResIP { get; }
        public abstract int ResPort { get; }

        public abstract ISFSObject GetLoginParameters(CustomSfs2x customSfs2X);

        public abstract void ConnectionEvent(BaseEvent baseEvent);
        public abstract void ConnectionLostEvent(BaseEvent baseEvent);
        public abstract void ExtensionResponseEvent(BaseEvent baseEvent);
        public abstract void LoginEvent(BaseEvent baseEvent);
        public abstract void LoginErrorEvent(BaseEvent baseEvent);

        protected string GetErrorMsg(BaseEvent baseEvent)
        {
            string msg = string.Empty;

            if (baseEvent.Params.Contains(RESPONSE.Key.ERROR_MESSAGE))
            {
                msg += (string)baseEvent.Params[RESPONSE.Key.ERROR_MESSAGE] + "\n";
            }

            if (!baseEvent.Params.Contains(RESPONSE.Key.CMD))
            {
                return msg += string.Format("알 수 없는 응답을 받음 ");
            }

            var type = this.GetType();
            var key = (string)baseEvent.Params[RESPONSE.Key.CMD];

            var findProtocol = Protocol.GetProtocol(type, key);
            if (findProtocol == null) 
            {
                return msg += string.Format("[{0}] 처리되지 않은 요청을 받음 ", key);
            }

            if (!baseEvent.Params.Contains(RESPONSE.Key.DATA))
            {
                return msg += string.Format("[{0}] RESPONSE DATA가 없습니다. ", key);
            }

            var resObj = baseEvent.Params[RESPONSE.Key.DATA] as SFSObject;
            if (!resObj.ContainsKey(RESPONSE.Key.RESULT))
            {
                return msg += string.Format("[{0}] RCode가 없습니다. ", key);
            }

            var rCode = resObj.GetByte(RESPONSE.Key.RESULT);
            if (rCode != ResultCode.SUCCESS.Key)
            {
                string context = findProtocol == null ? key : findProtocol.context;
                return msg += string.Format("[{0}]\n요청 실패 Rcode {1}\n{2}", context, rCode, ResultCode.GetByKey(rCode).GetMsg());
            }

            return msg;
        }
    }
    
    public class RESPONSE
    {
        public class Key
        {
            public const string CMD = "cmd";
            public const string DATA = "params";
            public const string RESULT = "0";
            public const string ERROR_MESSAGE = "errorMessage";
        }
    }

    public class LOGIN
    {
        public class Key
        {
            public const string SUCCESS = "success";
            public const string CODE = "errorCode";
            public const string MESSAGE = "errorMessage";
            public const string DATA = "data";
            public const string USER = "user";
            public const string ZONE = "zone";
        }
    }
}
