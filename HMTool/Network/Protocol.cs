﻿using HMTool.Network.SFS;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;

namespace HMTool.Network
{
    public abstract class Protocol
    {
        protected abstract Type SFSType { get; }
        protected static Dictionary<Tuple<Type, string>, Protocol> protocolDic = new Dictionary<Tuple<Type, string>, Protocol>();
        
        public Action<bool, SFSObject> ResEvent { get; protected set; }

        public string key;
        public string context;
        public bool notice;
        public string parentKey = string.Empty;

        public Protocol(string key, string context, string parentKey = null, bool notice = false)
        {
            this.key = key;
            this.context = context;
            this.notice = notice;
            this.parentKey = parentKey;

            var pKey = new Tuple<Type, string>(SFSType, key);
            protocolDic.Add(pKey, this);
        }

        public static Protocol GetProtocol(Type sfsType, string key)
        {
            var protocolKey = new Tuple<Type, string>(sfsType, key);

            if (protocolDic.ContainsKey(protocolKey))
            {
                return protocolDic[protocolKey];
            }
            else return null;
        }

        public void SetResEvent(Action<bool, SFSObject> resEvent)
        {
            this.ResEvent = resEvent;
        }
    }

    //지엠서버 프로토콜 (GM툴)
    public class GMProtocol : Protocol
    {
        public GMProtocol(string key, string context, string parentKey = null, bool notice = false) : base(key, context, parentKey, notice)
        {
        }

        protected override Type SFSType => typeof(GmSFS);

        public static Protocol GMLogIn = new GMProtocol("1", "지엠유저 로그인 정보 조회");
        public static Protocol SearchUser = new GMProtocol("16", "유저 검색", GMLogIn.key);
        public static Protocol SearchCharacter = new GMProtocol("17", "캐릭터 조회", SearchUser.key);
        public static Protocol RequestMailList = new GMProtocol("23", "메일 조회", SearchUser.key);
        public static Protocol SendMail = new GMProtocol("24", "메일 보내기", SearchUser.key, true);
        public static Protocol DeleteMail = new GMProtocol("25", "메일 삭제", SearchUser.key, true);
        public static Protocol DeleteItemInven = new GMProtocol("35", "캐릭터 아이템 인벤 삭제", SearchUser.key, true);
        public static Protocol AddItemInven = new GMProtocol("36", "캐릭터 아이템 인벤 추가", SearchUser.key, true);
        public static Protocol TutorialEdit = new GMProtocol("45", "튜토리얼 진행 수정", SearchUser.key, true);
        public static Protocol KickUser = new GMProtocol("49", "유저 강퇴", SearchUser.key, true);
        public static Protocol BlockUser = new GMProtocol("50", "유저 블럭", SearchUser.key, true);
        public static Protocol UpdataUserData = new GMProtocol("54", "유저, 캐릭터 데이터 갱신", SearchUser.key, true);
        public static Protocol RequestItemInvenList = new GMProtocol("65", "캐릭터 아이템 인벤 조회", SearchUser.key);
        public static Protocol RequestPetInvenList = new GMProtocol("95", "캐릭터 펫 인벤 조회", SearchUser.key);
        public static Protocol AddPetInven = new GMProtocol("96", "캐릭터 펫 인벤 추가", SearchUser.key, true);
        public static Protocol DeletePetInven = new GMProtocol("97", "캐릭터 펫 인벤 삭제", SearchUser.key, true);
        public static Protocol Ping = new GMProtocol("158", "핑퐁");
        public static Protocol EditItem = new GMProtocol("176", "캐릭터 아이템 수정", SearchUser.key, true);        
        public static Protocol EditPet = new GMProtocol("179", "유저 펫 수정", SearchUser.key, true);
        public static Protocol ResetSkill = new GMProtocol("180", "스킬 초기화", SearchUser.key, true);
    }

    //인증서버 프로토콜 (게임 인증서버)
    public class AuthProtocol : Protocol
    {
        public AuthProtocol(string key, string context, string parentKey = null, bool notice = false) : base(key, context, parentKey, notice)
        {
        }

        protected override Type SFSType => typeof(AuthSFS);

        public static Protocol LoginServerInfo = new AuthProtocol("1", "로그인을 시도한 서버 정보");
    }

    //게임서버 프로토콜 (게임서버[라테일 클라이언트])
    public class GameProtocol : Protocol
    {
        public GameProtocol(string key, string context, string parentKey = null, bool notice = false) : base(key, context, parentKey, notice)
        {
        }

        protected override Type SFSType => typeof(GameSFS);

        public static Protocol UserInfo = new GameProtocol("11", "유저 정보 요청");
        public static Protocol JoinGameMap = new GameProtocol("16", "게임맵 입장");
        public static Protocol ResetSkillTree = new GameProtocol("522", "스킬트리 초기화", JoinGameMap.key);
        public static Protocol LeaveGameMap = new GameProtocol("775", "필드맵 퇴장", JoinGameMap.key);
    }
}
