﻿namespace HMTool.Network.Login
{
    public abstract class LoginInfo
    {
        public string ID { get; protected set; }
        public string Password { get; protected set; }

        public abstract string ZoneName { get; }        
    }

    public class GMLoginInfo : LoginInfo
    {        
        public int version;

        public GMLoginInfo(string id, string password, int version)
        {
            this.ID = id;
            this.Password = password;

            this.version = version;
        }

        public override string ZoneName => "latale_gm";
    }

    public class AuthLogInInfo : LoginInfo
    {
        private int gameServerSeq;

        public AuthLogInInfo(string id, string password, int gameServerSeq)
        {
            this.ID = id;
            this.Password = password;
            this.gameServerSeq = gameServerSeq;
        }

        public override string ZoneName => "latale_auth";
    }

    public class GameLoginInfo : LoginInfo
    {
        public GameLoginInfo(string id, string password)
        {
            this.ID = id;
            this.Password = password;
        }

        public override string ZoneName => "latale";
    }
}
