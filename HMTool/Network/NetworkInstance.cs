﻿using HMTool.Network.Login;
using HMTool.Network.SFS;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HMTool.Network
{
    public class NetworkInstance : ISFSEventListener
    {        
        public string Owner { get; private set; }

        public BaseSFS BaseSFS { get; private set; }
        public CustomSfs2x CustomSfs2X { get; private set; }
        
        private Action loginSuccess;
        private Action<string> loginError;
        private Action<string> connectError;

        private Timer pingTimer;

        public NetworkInstance()
        {
            CustomSfs2X = new CustomSfs2x();
        }
        
        public void Init(object owner, BaseSFS baseSFS, Action loginSuccess, Action<string> loginError, Action<string> connectError)
        {
            this.Owner = owner.GetType().Name;
            this.BaseSFS = baseSFS;

            this.loginSuccess = loginSuccess;
            this.loginError = loginError;
            this.connectError = connectError;

            CustomSfs2X.AddEventListener(this);
        }

        public void ChangeOwner(object newOwner, Action<string> connectError = null)
        {
            //이미 연결된 네트워크라서 ConnectError만 처리
            this.Owner = newOwner.GetType().Name;

            this.loginSuccess = null;
            this.loginError = null;
            this.connectError = connectError;
        }

        public void Connect()
        {
            CustomSfs2X.Connect(BaseSFS.IP, BaseSFS.Port);
        }

        public void LogIn()
        {
            var parameters = BaseSFS.GetLoginParameters(CustomSfs2X);
            if (parameters == null) return;

            LoginInfo LoginInfo = BaseSFS.LoginInfo;
            CustomSfs2X.Login(LoginInfo.ID, LoginInfo.Password, LoginInfo.ZoneName, parameters);
        }

        public void Disconnect()
        {
            DisposeTimer();

            CustomSfs2X.Disconnect();
            CustomSfs2X.RemoveAllEventListener();
        }

        public bool Send(Protocol protocol, SFSObject sfsObject)
        {
            sfsObject = sfsObject ?? SFSObject.NewInstance();
            return CustomSfs2X.Send(protocol, sfsObject);
        }

        private void StartPingPong()
        {           
            TimerCallback timerCallback = (state) =>
            {
                if (CustomSfs2X == null || CustomSfs2X.Sfs2x == null || !CustomSfs2X.Sfs2x.IsConnected) 
                {
                    DisposeTimer();
                }
                else Send(GMProtocol.Ping, null);
            };

            pingTimer = new Timer(timerCallback, null, 0, 60000);
        }

        private void DisposeTimer()
        {
            if (pingTimer == null)
                return;

            pingTimer.Change(Timeout.Infinite, Timeout.Infinite);
            pingTimer.Dispose();            
        }

        /////////////////////
        //ISFSEventListener//
        /////////////////////
        public void ConnectionEvent(BaseEvent baseEvent)
        {
            BaseSFS.ConnectionEvent(baseEvent);

            //연결에 성공하면 바로 로그인 시도
            LogIn();
        }
        
        public void LoginEvent(BaseEvent baseEvent)
        {
            StartPingPong();

            BaseSFS.LoginEvent(baseEvent);

            loginSuccess?.Invoke();
        }

        public void ConnectionLostEvent(BaseEvent baseEvent)
        {
            BaseSFS.ConnectionLostEvent(baseEvent);

            string errorMsg = string.Empty;
            if (baseEvent.Params.Contains(RESPONSE.Key.ERROR_MESSAGE))
                errorMsg = baseEvent.Params[RESPONSE.Key.ERROR_MESSAGE].ToString();

            connectError?.Invoke(errorMsg);
        }

        public void LoginErrorEvent(BaseEvent baseEvent)
        {
            BaseSFS.LoginErrorEvent(baseEvent);

            string errorMsg = string.Empty;
            if (baseEvent.Params.Contains(RESPONSE.Key.ERROR_MESSAGE))
                errorMsg = baseEvent.Params[RESPONSE.Key.ERROR_MESSAGE].ToString();
            
            loginError?.Invoke(errorMsg);
        }

        public void ExtensionResponseEvent(BaseEvent baseEvent)
        {            
            BaseSFS.ExtensionResponseEvent(baseEvent);
        }        
    }
    
}
