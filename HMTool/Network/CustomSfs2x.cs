﻿using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;

namespace HMTool.Network
{
    public class CustomSfs2x
    {
        public SmartFox Sfs2x { get; protected set; }

        public CustomSfs2x()
        {
            Sfs2x = new SmartFox(true);
        }

        public void AddEventListener(ISFSEventListener eventListener)
        {
            Sfs2x.AddEventListener(SFSEvent.CONNECTION, eventListener.ConnectionEvent);
            Sfs2x.AddEventListener(SFSEvent.CONNECTION_LOST, eventListener.ConnectionLostEvent);
            Sfs2x.AddEventListener(SFSEvent.LOGIN, eventListener.LoginEvent);
            Sfs2x.AddEventListener(SFSEvent.LOGIN_ERROR, eventListener.LoginErrorEvent);
            Sfs2x.AddEventListener(SFSEvent.EXTENSION_RESPONSE, eventListener.ExtensionResponseEvent);            
        }

        public void RemoveAllEventListener()
        {
            Sfs2x.RemoveAllEventListeners();
        }

        public void Connect(string ip, int port)
        {
            Sfs2x.Connect(ip, port);
        }

        public void Login(string id, string pw, string zone, ISFSObject loginParameters)
        {
            Sfs2x.Send(new LoginRequest(id, pw, zone, loginParameters));
        }

        public bool Send(Protocol protocol, SFSObject sfsObj)
        {
            string[] keys = sfsObj.GetKeys();
            for (int i = 0; i < keys.Length; i++)
            {
                if (sfsObj.GetData(keys[i]).Data == null)
                    return false;
            }
            Sfs2x.Send(new ExtensionRequest(protocol.key, sfsObj));

            return true;
        }

        public void Disconnect()
        {
            if (Sfs2x.IsConnected)
                Sfs2x.Disconnect();
        }
    }
}
