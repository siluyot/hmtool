﻿using System.Collections.Generic;

namespace HMTool.Network.Servers
{
    public abstract class ServerInfo
    {
        public static List<ServerInfo> List = new List<ServerInfo>()
        {
            new DevServer(),
            new TestServer(),
            new LocalServer(),
            new IOSTestServer(),
            new Taiwan_TestServer(),
            new Taiwan_BetaServer(),
            new Thai_TestServer(),
            new Thai_BetaServer(),
            new Global_TestServer(),
            new Japan_TestServer(),

            new KIMServer(),
            new PARKServer(),
            new YEOServer(),

            new Kor_1Server(),
            new Kor_2Server(),

            new Taiwan_1Server(),
            new Taiwan_2Server(),
            new Taiwan_3Server(),
            new Taiwan_4Server(),
            new Taiwan_5Server(),
            new Taiwan_6Server(),
            new Taiwan_7Server(),

            new Thai_1Server(),
            new Thai_2Server(),
            new Thai_3Server(),
            new Thai_4Server(),
        };

        public string TypeName { get { return this.GetType().Name; } }

        public abstract bool IsTestServer { get; }
        public abstract string PathName { get; }

        //지엠
        public abstract string GmServerIP { get; }
        public abstract int GmServerPort { get; }

        public abstract string GmServerResourceIP { get; }
        public abstract int GmServerResourcePort { get; }

        //게임 - 인증
        public abstract string AuthServerIP { get; }
        public abstract int AuthServerPort { get; }

        //게임 - 게임
        public abstract string GameServerResourceIP { get; }
        public abstract int GameServerResourcePort { get; }

        public abstract int GameServerSeq { get; }

        public string GetDBDownloadPath(string dbName)
        {
            return string.Format("http://{0}:{1}/resources/{2}", GmServerResourceIP, GmServerResourcePort, dbName);
        }

        public string GetSecretDBDownloadPath(string dbName)
        {
            return string.Format("http://{0}:{1}/noupload/{2}", GmServerResourceIP, GmServerResourcePort, dbName);
        }

        public string GetServerListPath()
        {
            if (GameServerResourcePort == 0)
            {
                return string.Format("http://{0}/resources/server_list.txt", GameServerResourceIP);
            }
            else return string.Format("http://{0}:{1}/resources/server_list.txt", GameServerResourceIP, GameServerResourcePort);
        }
    }
}
