﻿namespace HMTool.Network.Servers
{
    public class IOSTestServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "ios";

        public override string GmServerIP => "211.239.162.27";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "211.239.162.27";
        public override int GmServerResourcePort => 8080;
        
        public override string AuthServerIP => "211.239.162.35";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "cdn.convtf.com/latale/cbt_10_12";
        public override int GameServerResourcePort => 0;

        public override int GameServerSeq => 0;
    }
}