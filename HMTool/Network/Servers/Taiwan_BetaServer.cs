﻿namespace HMTool.Network.Servers
{
    public class Taiwan_BetaServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "taiwanBeta";

        public override string GmServerIP => "27.105.88.245";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "27.105.88.245";
        public override int GmServerResourcePort => 8080;
        
        public override string AuthServerIP => "chtwbeta-auth.gamedreamer.com";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "chtwcdn.gamedreamer.com/beta";
        public override int GameServerResourcePort => 0;

        public override int GameServerSeq => 198;
    }
}