﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMTool.Network.Servers
{
    public class Thai_BetaServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "thaiBeta";

        public override string GmServerIP => "54.255.251.177";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "54.255.251.177";
        public override int GmServerResourcePort => 8080;

        public override string AuthServerIP => "0"; //확인해서 넣어야함
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "0"; //확인해서 넣어야함
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 0; //확인해서 넣어야함
    }
}
