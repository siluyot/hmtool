﻿namespace HMTool.Network.Servers
{
    public class DevServer : ServerInfo
    {        
        public override bool IsTestServer => true;
        public override string PathName => "dev";

        public override string GmServerIP => "211.239.162.21";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "211.239.162.21";
        public override int GmServerResourcePort => 8080;
        
        public override string AuthServerIP => "211.239.162.21";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "211.239.162.21";
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 0;
    }
}