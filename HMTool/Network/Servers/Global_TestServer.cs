﻿namespace HMTool.Network.Servers
{
    public class Global_TestServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "globalTest";

        public override string GmServerIP => "18.210.131.92";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "18.210.131.92";
        public override int GmServerResourcePort => 8080;

        public override string AuthServerIP => "0"; //확인해서 넣어야함
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "18.210.131.92";
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 0; //확인해서 넣어야함
    }
}
