﻿namespace HMTool.Network.Servers
{
    class Japan_TestServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "japanTest";

        public override string GmServerIP => "52.198.108.54";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "52.198.108.54";
        public override int GmServerResourcePort => 8080;

        public override string AuthServerIP => "0"; //확인해서 넣어야함
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "0"; //확인해서 넣어야함
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 0;
    }
}
