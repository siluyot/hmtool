﻿namespace HMTool.Network.Servers
{
    public class Thai_TestServer : ServerInfo
    {       
        public override bool IsTestServer => true;
        public override string PathName => "thaiTest";

        public override string GmServerIP => "159.65.8.192";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "159.65.8.192";
        public override int GmServerResourcePort => 8080;

        public override string AuthServerIP => "0"; //확인해서 넣어야함
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "159.65.8.192";
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 0; //확인해서 넣어야함

        //DEV("Dev-Server", "211.239.162.21", 9933, "211.239.162.21:8080", "dev", new GMMode[0]),  TEST("Test-Server", "211.239.162.31", 9933, "211.239.162.31:8080", "test", new GMMode[0]),  IOS("IOS ����", "211.239.162.27", 9933, "211.239.162.27:8080", "ios", new GMMode[0]),  EC2_1("1.������", "52.78.144.172", 9933, "52.78.144.172:8080", "1.������", new GMMode[0]),  EC2_2("2.������", "13.124.111.78", 9933, "52.78.144.172:8080", "1.������", new GMMode[0]),  TAIWAN_TEST("TAIWAN_TEST", "27.105.88.231", 9933, "27.105.88.231:8080", "taiwanTest", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_BETA("TAIWAN_BETA", "27.105.88.245", 9933, "27.105.88.245:8080", "taiwanBeta", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_1("TAIWAN1", "13.250.194.74", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_2("TAIWAN2", "52.221.11.173", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_3("TAIWAN3", "52.76.80.19", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_4("TAIWAN4", "13.250.228.10", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_5("TAIWAN5", "52.221.123.242", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_6("TAIWAN6", "52.77.102.67", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  TAIWAN_7("TAIWAN7", "13.250.216.137", 9933, "13.250.194.74:8080", "taiwan", new GMMode[] { GMMode.TAIWAN }),  GLOBAL_TEST("GLOBAL_TEST", "18.210.131.92", 9933, "18.210.131.92:8080", "globalTest", new GMMode[] { GMMode.GLOBAL }),  
        //THAI_TEST("THAI_TEST", "159.65.8.192", 9933, "159.65.8.192:8080", "thaiTest", new GMMode[] { GMMode.THAI }),  
        //THAI_BETA("THAI_BETA", "54.255.251.177", 9933, "54.255.251.177:8080", "thaiBeta", new GMMode[] { GMMode.THAI }),  
        //THAI_1("THAI1", "13.229.1.53", 9933, "13.229.1.53:8080", "thai", new GMMode[] { GMMode.THAI }),  
        //THAI_2("THAI2", "13.251.48.187", 9933, "13.229.1.53:8080", "thai", new GMMode[] { GMMode.THAI }),  
        //THAI_3("THAI3", "52.220.210.67", 9933, "13.229.1.53:8080", "thai", new GMMode[] { GMMode.THAI }),  
        //THAI_4("THAI4", "3.0.3.93", 9933, "13.229.1.53:8080", "thai", new GMMode[] { GMMode.THAI }),  
        //PARK("PARK-Server", "203.84.246.247", 9933, "211.239.162.21:8080", "park", new GMMode[0]),  YEO("YEO-Server", "203.84.246.248", 9933, "203.84.246.248:8080", "yeo", new GMMode[0]),  KIM("KIM-Server", "192.168.2.155", 9933, "192.168.2.155:8080", "kim", new GMMode[0]);
    }
}
