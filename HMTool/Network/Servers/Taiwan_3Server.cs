﻿namespace HMTool.Network.Servers
{
    public class Taiwan_3Server : ServerInfo
    {
        public override bool IsTestServer => false;
        public override string PathName => "taiwan";

        public override string GmServerIP => "52.76.80.19";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "13.250.194.74";
        public override int GmServerResourcePort => 8080;
        
        public override string AuthServerIP => "chtwbeta-auth.gamedreamer.com";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "chtwcdn.gamedreamer.com/service";
        public override int GameServerResourcePort => 0;

        public override int GameServerSeq => 3;

    }
}