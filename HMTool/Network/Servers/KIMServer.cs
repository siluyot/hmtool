﻿namespace HMTool.Network.Servers
{
    public class KIMServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "kim";

        public override string GmServerIP => "192.168.2.155";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "192.168.2.155";
        public override int GmServerResourcePort => 8080;
    
        public override string AuthServerIP => "192.168.2.155";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "192.168.2.155";
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 0;
    }
}