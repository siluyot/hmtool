﻿namespace HMTool.Network.Servers
{
    public class Kor_2Server : ServerInfo
    {
        public override bool IsTestServer => false;       
        //public override string PathName => "1.이리스";
        public override string PathName => "2.카즈노";

        public override string GmServerIP => "13.124.111.78";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "52.78.144.172";
        public override int GmServerResourcePort => 8080;
        
        public override string AuthServerIP => "latale.login.funipoll.co.kr";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "cdn.convtf.com/latale/service";
        public override int GameServerResourcePort => 0;

        public override int GameServerSeq => 1;
    }
}