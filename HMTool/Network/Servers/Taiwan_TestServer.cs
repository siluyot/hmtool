﻿namespace HMTool.Network.Servers
{
    public class Taiwan_TestServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "taiwanTest";
        
        public override string GmServerIP => "27.105.88.231";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "27.105.88.231";
        public override int GmServerResourcePort => 8080;
        
        public override string AuthServerIP => "27.105.88.228";
        public override int AuthServerPort => 9933;

        public override string GameServerResourceIP => "27.105.88.231";
        public override int GameServerResourcePort => 8080;

        public override int GameServerSeq => 199;
    }
}