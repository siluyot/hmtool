﻿namespace HMTool.Network.Servers
{
    public class YEOServer : ServerInfo
    {
        public override bool IsTestServer => true;
        public override string PathName => "yeo";

        public override string GmServerIP => "203.84.246.248";
        public override int GmServerPort => 9933;

        public override string GmServerResourceIP => "203.84.246.248";
        public override int GmServerResourcePort => 8080;

        public override string AuthServerIP => string.Empty;
        public override int AuthServerPort => 0;

        public override string GameServerResourceIP => string.Empty;
        public override int GameServerResourcePort => 0;

        public override int GameServerSeq => 0;
    }
}