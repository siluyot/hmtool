﻿using HMTool.Data;
using HMTool.Network.Servers;
using HMTool.Network.SFS;
using System;
using System.Collections.Generic;

namespace HMTool.Network
{
    public class NetworkManager
    {
        private static NetworkManager instance = null;
        public static NetworkManager Instance
        {
            get { return instance = instance ?? new NetworkManager(); }
        }

        private List<NetworkInstance> networkList = new List<NetworkInstance>();

        public void TryConnect(object owner, BaseSFS baseSFS, Action loginSuccess = null, Action<string> loginError = null, Action<string> connectError = null)
        {
            //owner = 해당 ViewModel의 GetType().Name 을 가진다.
            NetworkInstance networkInstance = GetNetwork(owner);

            if (networkInstance == null)
            {
                networkInstance = new NetworkInstance();
                networkList.Add(networkInstance);
            }
            else networkInstance.Disconnect();

            networkInstance.Init(owner, baseSFS, loginSuccess, loginError, connectError);
            networkInstance.Connect();
        }

        public bool SendProtocol(object owner, RequestData requestData, bool notice = true)
        {
            var protocol = requestData.protocol;
            var callback = requestData.callback;
            var sfsObject = requestData.sfsObject;

            protocol.SetResEvent(callback);

            var network = GetNetwork(owner);
            if (network == null)
            {
                CustomMessageBox.Show("Owner : " + owner + "\n해당 네트워크 인스턴스가 없습니다.");
                return false;
            }

            if (notice && !network.BaseSFS.ServerInfo.IsTestServer && protocol.notice)
            {
                var result = CustomMessageBox.Show(string.Format("요청할 서버가 테스트서버가 아닙니다.\n계속 진행하시겠습니까?\n\n진행 할 요청 : [{0}]", protocol.context), string.Empty, System.Windows.MessageBoxButton.YesNo);
                if (result == System.Windows.MessageBoxResult.No)
                    return false;
            }

            if (!network.Send(protocol, sfsObject))
            {
                Protocol parentProtocol = Protocol.GetProtocol(network.BaseSFS.GetType(), protocol.parentKey);
                CustomMessageBox.Show(string.Format("{0} 실패\n{1}(이)가 필요합니다.", protocol.context, parentProtocol.context));
                return false;
            }
            return true;
        }

        public void Disconnect(object owner)
        {
            NetworkInstance networkInstance = GetNetwork(owner);
            if (networkInstance == null) return;

            networkInstance.Disconnect();
        }

        public void DeleteNetwork(object owner)
        {
            NetworkInstance networkInstance = GetNetwork(owner);
            if (networkInstance == null) return;

            networkList.Remove(networkInstance);

            networkInstance.Disconnect();            
            networkInstance = null;
        }

        public void ChangeOwner(object oldOwner, object newOwner)
        {
            NetworkInstance networkInstance = GetNetwork(oldOwner);
            if (networkInstance == null) return;

            networkInstance.ChangeOwner(newOwner);
        }

        public NetworkInstance GetNetwork(object owner)
        {
            return networkList.Find(v => v.Owner == owner.GetType().Name);
        }
    }
}
