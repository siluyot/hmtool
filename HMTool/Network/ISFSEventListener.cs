﻿using Sfs2X.Core;

namespace HMTool.Network
{
    public interface ISFSEventListener
    {
        void ConnectionEvent(BaseEvent baseEvent);
        void ConnectionLostEvent(BaseEvent baseEvent);

        void LoginEvent(BaseEvent baseEvent);        
        void LoginErrorEvent(BaseEvent baseEvent);

        void ExtensionResponseEvent(BaseEvent baseEvent);
    }
}
