﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;

namespace HMTool.Custom
{
    public abstract class ColumnDetail : Attribute
    {
        public string key { get; private set; }

        public ColumnDetail(string key)
        {
            this.key = key;
        }

        public abstract IViewData[] GetDataArray(object obj, Dictionary<string, object> parameter);
    }

    public class ColumnDetailParameter : Attribute
    {
        public string key { get; private set; }
        public string FieldName { get; private set; }

        public ColumnDetailParameter(string key, string FieldName)
        {
            this.key = key;
            this.FieldName = FieldName;
        }
    }

    public class BaseDetail : ColumnDetail
    {
        protected DataBaseType type = DataBaseType.None;
        protected IDataBase BaseDataBase { get => DataBaseManager.Instance.GetDataBase(type); }

        public BaseDetail(DataBaseType type, string key) : base(key)
        {
            this.type = type;
        }

        public override IViewData[] GetDataArray(object obj, Dictionary<string, object> parameter)
        {
            var list = new List<IViewData>();

            if (type != DataBaseType.None)
            {                
                var findData = BaseDataBase.GetData(obj) as IViewData;
                list.Add(findData);
            }           
            return list.ToArray();
        }
    }

    public class DropListDetail : ColumnDetail
    {
        public DropListDetail(string key) : base(key)
        {

        }

        public override IViewData[] GetDataArray(object obj, Dictionary<string, object> parameter)
        {
            List<object> dropListID = new List<object>();
            List<IViewData> dataList = new List<IViewData>();

            if (parameter != null)
            {                
                if (parameter.ContainsKey("item_type"))
                {
                    var itemDB = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM);
                    var petDB = DataBaseManager.Instance.GetDataBase(DataBaseType.PET_BOOK);
                    var titleDB = DataBaseManager.Instance.GetDataBase(DataBaseType.TITLE_NAME);

                    var dropDataList = DataBaseManager.Instance.GetDataList(DataBaseType.MONSTER_DROP_LIST);
                    var jobDataList = DataBaseManager.Instance.GetDataList(DataBaseType.JOB_DROP);
                    var makeDataList = DataBaseManager.Instance.GetDataList(DataBaseType.MAKE);
                    
                    //item_type = 59(랜덤아이템) -> 드랍리스트 또는 직업드랍
                    //item_type = 60(패키지상자) -> 제작데이터
                    //item_type = 35(칭호) -> 칭호데이터
                    var type = ItemType.GetByKey((byte)parameter["item_type"]);
                    if (type == ItemType.RAND_ITEM)
                    {
                        if (parameter.ContainsKey("item_value_max"))
                        {
                            //item_value_max = 0 이면 바로 드랍리스트
                            //item_value_max = 1 이면 직업드랍 들렀다가 드랍리스트
                            int isJobDrop = (int)parameter["item_value_max"];
                            if (isJobDrop == 1)
                            {
                                var findDataList = jobDataList.FindAll(v => obj.Equals(v.ID));
                                foreach (var data in findDataList)
                                {
                                    var jobDropData = data as JobDropData;
                                    foreach (var id in jobDropData.Drop_list_id)
                                    {
                                        dropListID.Add(id);
                                    }
                                }
                            }
                            else dropListID.Add(obj);

                            foreach (var dropId in dropListID)
                            {
                                var findDropList = dropDataList.FindAll(v => dropId.Equals(v.ID));
                                foreach (var data in findDropList)
                                {                                    
                                    var dropData = data as MonsterDropListData;
                                    var dropType = (DropType)dropData.object_type;

                                    ICustomData findDrop = null;
                                    DataBoxItem[] dataBoxArray = null;

                                    if (dropType == DropType.ITEM_ID)
                                    {
                                        findDrop = itemDB.GetData(dropData.object_id);

                                        var item = findDrop as ItemData;
                                        dataBoxArray = item.GetDataBoxItem();
                                    }
                                    else if (dropType == DropType.PET_ID)
                                    {
                                        findDrop = petDB.GetData(dropData.object_id);

                                        var pet = findDrop as PetBookData;
                                        dataBoxArray = pet.GetDataBoxItem();
                                    }
                                    else
                                    {
                                        findDrop = new MailData(MailTab.None, 0, dropType.ToString(), 0, GiftType.NONE, dropData.object_id);

                                        var mail = findDrop as MailData;
                                        dataBoxArray = mail.GetDataBoxItem();
                                    }

                                    foreach (var item in dataBoxArray)
                                    {
                                        dataList.Add(item);
                                    }
                                }
                            }
                        }
                        else dataList.Add(new ViewData() { name = "item_value_max를 찾을 수 없습니다." });
                    }
                    else if (type == ItemType.PACK_BOX_ITEM)
                    {
                        var findData = makeDataList.Find(v => obj.Equals(v.ID));
                        if (findData != null)
                        {
                            var makeData = findData as MakeData;
                            foreach (var material in makeData.MakeMaterials)
                            {
                                if (material == null) continue;
                                var materialType = material.giftType;

                                ICustomData findMaterial = null;
                                DataBoxItem[] dataBoxArray = null;

                                if (materialType == GiftType.ITEM)
                                {
                                    findMaterial = itemDB.GetData(material.itemID);

                                    var item = findMaterial as ItemData;
                                    dataBoxArray = item.GetDataBoxItem();
                                }
                                else if (materialType == GiftType.PET)
                                {
                                    findMaterial = petDB.GetData(material.itemID);

                                    var pet = findMaterial as PetBookData;
                                    dataBoxArray = pet.GetDataBoxItem();
                                }
                                else
                                {
                                    findMaterial = new MailData(MailTab.None, 0, materialType.Value, 0, materialType, material.requiredCount);

                                    var mail = findMaterial as MailData;
                                    dataBoxArray = mail.GetDataBoxItem();
                                }

                                foreach (var item in dataBoxArray)
                                {
                                    dataList.Add(item);
                                }
                            }
                        }
                    }
                    else if (type == ItemType.TITLE_ITEM)
                    {
                        var findTitle = titleDB.GetData(obj);

                        var title = findTitle as TitleData;
                        var dataBoxArray = title.GetDataBoxItem();

                        foreach (var item in dataBoxArray)
                        {
                            dataList.Add(item);
                        }
                    }
                }
                else dataList.Add(new ViewData() { name = "item_type을 찾을 수 없습니다." });
            }
            else dropListID.Add(obj);

            if (dataList.Count <= 0)
                dataList.Add(new ViewData() { name = "데이터 없음" });

            return dataList.ToArray();
        }
    }

    public class SetTableDetail : ColumnDetail
    {
        public SetTableDetail(string key) : base(key)
        {

        }

        public override IViewData[] GetDataArray(object obj, Dictionary<string, object> parameter)
        {
            var itemDB = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM) as ItemDataBase;

            var findList = itemDB.GetSetItemList((int)obj);

            if (findList.Count <= 0)
                findList.Add(new ItemData() { name = "데이터 없음" });

            return findList.ToArray();
        }
    }

    public class MapInfoDetail : ColumnDetail
    {
        public MapInfoDetail(string key) : base(key)
        {

        }

        public override IViewData[] GetDataArray(object obj, Dictionary<string, object> parameter)
        {
            var mapInfoDB = DataBaseManager.Instance.GetDataBase(DataBaseType.MAP_INFO) as MapInfoDataBase;
            
            var findData = mapInfoDB.GetDataList().Find(v =>
            {
                var mapData = v as MapInfoData;
                return mapData.Map.Equals(obj);
            });

            if (findData == null)
            {
                string msg = string.Empty;

                if (mapInfoDB.GachaShopId.Equals(obj))
                {
                    msg = "뽑기상점에서 획득가능";
                }
                else msg = Utills.GetSpecialDropMapText(obj);

                if (msg == string.Empty)
                    msg = "데이터 없음";

                findData = new MapInfoData()
                {
                    Name = msg,
                };
            }

            return new IViewData[] { findData as IViewData };
        }
    }

    public class ItemRandOptionDetail : ColumnDetail
    {
        public ItemRandOptionDetail(string key) : base(key)
        {

        }

        public override IViewData[] GetDataArray(object obj, Dictionary<string, object> parameter)
        {
            var optionList = new List<OptionData>();

            var optionDB = DataBaseManager.Instance.GetDataBase(DataBaseType.OPTION_DATA) as OptionDataBase;
            var itemRandomOptionDB = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM_RAND_OPTION) as ItemRandOptionDataBase;

            var dataList = itemRandomOptionDB.GetOptionDataList(obj);
            if (dataList != null)
            {
                foreach (var data in dataList)
                {
                    var findData = optionDB.GetData(data.option_id) as OptionData;
                    findData = findData ?? new OptionData() { desc = "데이터 없음" };

                    optionList.Add(findData);
                }
            }
            else optionList.Add(new OptionData() { desc = "데이터 없음" });

            return optionList.ToArray();
        }
    }
}
