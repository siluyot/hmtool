﻿using HMTool.WindowView;
using HMTool.WindowView.ViewModel.Base;
using System.Windows;
using System.Windows.Threading;

namespace HMTool
{
    public class CustomMessageBox
    {
        public static MessageBoxResult Show(string msg, string caption = "", MessageBoxButton button = MessageBoxButton.OK)
        {
            Dispatcher dispatcher = null;
            var window = WindowManager.Instance.GetLastAddedWindow();

            if (window != null) dispatcher = window.Dispatcher;
            else dispatcher = WindowManager.Instance.CurDispatcher;

            MessageBoxResult result = MessageBoxResult.None;
            dispatcher?.Invoke(() =>
            {
                result = MessageBox.Show(window, msg, caption, button);
            });

            return result;
        }

        public static MessageBoxResult Show(WindowViewModel windowViewModel, string msg, string caption = "", MessageBoxButton button = MessageBoxButton.OK)
        {
            Dispatcher dispatcher = null;
            var window = WindowManager.Instance.FindWindow(windowViewModel);

            if (window != null) dispatcher = window.Dispatcher;
            else dispatcher = WindowManager.Instance.CurDispatcher;

            MessageBoxResult result = MessageBoxResult.None;
            dispatcher?.Invoke(() =>
            {
                result = MessageBox.Show(window, msg, caption, button);
            });

            return result;
        }


        public static MessageBoxResult Show(Window window, string msg, string caption = "", MessageBoxButton button = MessageBoxButton.OK)
        {
            MessageBoxResult result = MessageBoxResult.None;

            if (window == null) return result;

            window.Dispatcher?.Invoke(() =>
            {
                result = MessageBox.Show(window, msg, caption, button);
            });

            return result;
        }
    }
}
