﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace HMTool.DB
{
    /// <summary>
    /// 데이터 추가
    /// 1. 해당 데이터의 컬럼을 가지는 클래스 작성 (HMTool/Data/Base, ReadViewData 상속)
    /// 2. 해당 데이터의 DataBase 클래스 작성 (HMTool/DataBase, DataBase<T> 상속)
    /// 3. DataBaseManager의 Process에 추가
    /// </summary>
    public interface IDataBase
    {  
        string GetMsg();
        string GetDataName();

        object CreateNewData();

        List<ICustomData> GetDataList();
        ICustomData GetData(object obj);        

        DataBaseType GetDBType();
        void AddData(SQLiteDataReader rdr);
        void ReadData();
        void CombinedData();
    }

    public abstract class DataBase<T> : IDataBase where T : ICustomData, new()
    {
        private List<ICustomData> dataList = new List<ICustomData>();
        protected StringBuilder msgSB = new StringBuilder();

        public string GetMsg() => msgSB?.ToString();
        public string GetDataName() => typeof(T).Name;

        public object CreateNewData() => new T();

        public List<ICustomData> GetDataList() => dataList;
        public virtual ICustomData GetData(object obj)
        {
            var findData = GetDataList().Find(v =>
            {
                var convertObj = Convert.ChangeType(obj, v.ID.GetType());
                return convertObj.Equals(v.ID);
            });

            if (findData == null)
            {
                findData = CreateNewData() as ICustomData;
            }
            return findData;
        }

        public abstract DataBaseType GetDBType();
        //DB에서 읽어온 데이터를 리스트에 추가 -> DataBase 생성 완료
        public virtual void AddData(SQLiteDataReader rdr)
        {
            IReadData data = CreateNewData() as IReadData;
            T newData = data.GetReadData<T>(rdr, msgSB);

            dataList.Add(newData);
        }
        //읽어온 데이터로 추가 작업이 필요한 경우 -> DataBase 추가 완료
        public abstract void ReadData();
        //그 추가 작업이 다른 디비의 참조가 필요한 경우
        public abstract void CombinedData();
    }

    public class BasisDataBase : DataBase<BasisData>
    {
        public override DataBaseType GetDBType() => DataBaseType.BASIS;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    public class ItemDataBase : DataBase<ItemData>
    {
        private Dictionary<int, List<ItemData>> setItemDic = new Dictionary<int, List<ItemData>>();

        public override DataBaseType GetDBType() => DataBaseType.ITEM;

        public override void ReadData()
        {
            //세트 아이템
            foreach (var data in GetDataList())
            {
                var itemData = data as ItemData;
                if (itemData.set_table_id == 0) continue;

                if (setItemDic.ContainsKey(itemData.set_table_id))
                {
                    setItemDic[itemData.set_table_id].Add(itemData);
                }
                else
                {
                    setItemDic[itemData.set_table_id] = new List<ItemData>();
                    setItemDic[itemData.set_table_id].Add(itemData);
                }
            }            
        }

        public override void CombinedData() { }

        public List<ItemData> GetSetItemList(int setid)
        {
            if (setItemDic.ContainsKey(setid))
            {
                return setItemDic[setid];
            }
            else return new List<ItemData>();
        }
    }

    public class PetDataBase : DataBase<PetBookData>
    {
        public override DataBaseType GetDBType() => DataBaseType.PET_BOOK;

        public override void ReadData() { }        
        public override void CombinedData() { }
    }

    public class NpcDataBase : DataBase<NpcData>
    {
        public override DataBaseType GetDBType() => DataBaseType.NPC;

        public override void ReadData() { }
        public override void CombinedData() { }        
    }

    public class MakeDataBase : DataBase<MakeData>
    {
        public override DataBaseType GetDBType() => DataBaseType.MAKE;
        
        public override void ReadData()
        {
            //재료중에 아이템이 아닌것들도 들어가있음...
            //if (newData.make_type == 2 || newData.make_type == 3 || newData.make_type == 4)
            //{
            //    /*
            //     * make_type
            //     * 0 : 일반 제작에 사용되는 타입입니다. NPC에서 제작을 할 경우 사용.
            //     * 1 : 아이템 승급 시 특정 재료가 필요할 경우 값을 1로 설정하여 재료를 세팅합니다.
            //     * 2 : 패키지 상품 구성 시 값을 2로 세팅하고 넣을 아이템을 package_type_0~4에 세팅합니다.
            //     * 3 : 라인 친구초대 보상입력
            //     * 4 : 페이스북 공유 보상 입력
            //     * 5 : 재료풀에 들어가는 재료
            //     */
            //    return;
            //}
            for (int i = 0; i < GetDataList().Count; i++)
            {
                MakeData makeData = GetDataList()[i] as MakeData;

                MaterialInfo[] infoes = new MaterialInfo[]
                {
                    new MaterialInfo(makeData.item_id_0, makeData.item_size_0, makeData.package_type_0),
                    new MaterialInfo(makeData.item_id_1, makeData.item_size_1, makeData.package_type_1),
                    new MaterialInfo(makeData.item_id_2, makeData.item_size_2, makeData.package_type_2),
                    new MaterialInfo(makeData.item_id_3, makeData.item_size_3, makeData.package_type_3),
                    new MaterialInfo(makeData.item_id_4, makeData.item_size_4, makeData.package_type_4),
                };

                MakeMaterialData[] makeMaterials = new MakeMaterialData[infoes.Length];
                for (int j = 0; j < infoes.Length; j++)
                {
                    var Info = infoes[j];
                    if (Info.IsColumnValid == false)
                        continue;

                    //재료풀인 경우  make_type = 5 , package_type = 1, quest_complete_id = itemID....
                    if (makeData.make_type == 5 && Info.UseGroupMaterialPool)
                    {
                        int groupID = Info.itemID;
                        int nameID = 0;

                        Predicate<ICustomData> predicate = new Predicate<ICustomData>((v) =>
                        {
                            var make = v as MakeData;
                            return make.make_type == 5 && make.quest_complete_id == groupID;
                        });
                        List<ICustomData> materialPool = GetDataList().FindAll(predicate);

                        if (materialPool.Count > 0)
                            nameID = ((MakeData)materialPool[0]).make_name_id;

                        List<int> idList = new List<int>();

                        for (int k = 0; k < materialPool.Count; k++)
                            idList.Add(((MakeData)materialPool[k]).item_id_0);

                        makeMaterials[j] = new MakeMaterialData(idList.ToArray(), 1, Info.size, nameID);
                    }
                    //패키지상품 make_type = 2
                    else if (makeData.make_type == 2)
                    {
                        int itemID = Info.itemID;
                        int requiredCount = Info.size;
                        GiftType giftType = GiftType.GetGiftType(Info.package_type);
                        makeMaterials[j] = new MakeMaterialData(itemID, requiredCount, makeData.make_name_id, giftType);
                    }
                    else
                    {
                        int itemID = Info.itemID;
                        int requiredCount = Info.size;
                        makeMaterials[j] = new MakeMaterialData(itemID, requiredCount, makeData.make_name_id, GiftType.NONE);
                    }
                }
                makeData.MakeMaterials = makeMaterials;
            }
        }

        public override void CombinedData() { }       
    }

    public class StatusDataBase : DataBase<CharStatusData>
    {
        public override DataBaseType GetDBType() => DataBaseType.CHAR_STATUS;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    public class CashShopDataBase : DataBase<CashShopData>
    {
        public override DataBaseType GetDBType() => DataBaseType.CASH_SHOP;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    public class JobDropDataBase : DataBase<JobDropData>
    {
        public override DataBaseType GetDBType() => DataBaseType.JOB_DROP;

        public override void ReadData() { }
        public override void CombinedData(){ }
    }

    public class MonsterDropDataBase : DataBase<MonsterDropData>
    {
        public override DataBaseType GetDBType() => DataBaseType.MONSTER_DROP;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    public class MonsterDropListDataBase : DataBase<MonsterDropListData>
    {
        public override DataBaseType GetDBType() => DataBaseType.MONSTER_DROP_LIST;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    public class GachaDropDataBase : DataBase<GachaDropData>
    {
        public override DataBaseType GetDBType() => DataBaseType.GACHA_DROP_LIST;
        
        public override void AddData(SQLiteDataReader rdr) { }
        public override void ReadData() { }

        public override void CombinedData()
        {
            //GachaDropDataBase -> 캐시샵데이터를 이용해서 생성
            var dataList = DataBaseManager.Instance.GetDataList(DataBaseType.CASH_SHOP);
            if (dataList == null)
                return;

            foreach (var data in dataList)
            {
                var cashShopData = data as CashShopData;
                if (cashShopData.IsGachaData())
                {
                    GachaDropData gachaDropData = new GachaDropData(cashShopData);
                    GetDataList().Add(gachaDropData);
                }
            }
        }
    }

    public class BinggoPanDataBase : DataBase<BinggoPanData>
    {
        public override DataBaseType GetDBType() => DataBaseType.BINGGOPAN;

        public override void ReadData() { }
        public override void CombinedData()
        {
            // itemDB 참조가 필요함
            var itemDataBase = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM);
            
            // 이름, 아이템 타입, id 설정
            for (int i = 0; i < GetDataList().Count; i++)
            {
                BinggoPanData data = GetDataList()[i] as BinggoPanData;
                if (data != null && itemDataBase != null)
                {
                    data.itemType = data.collect_id != 0 ? BinggoPanData.ItemType.Collect : BinggoPanData.ItemType.Reward;
                    data.id = data.ID;

                    // reward일 경우 아이템 또는 다이아 같은 재화인지 구분
                    if (data.itemType == BinggoPanData.ItemType.Collect)
                    {
                        data.name = itemDataBase.GetData(data.ID).NAME;
                        data.size = data.collect_size;
                    }
                    else
                    {
                        data.name = data.GetGiftType().Value;
                        data.size = data.reward_size;
                    }
                }
            }
        }
    }

    public class LoginBonusDataBase : DataBase<LoginBonusData>
    {
        public override DataBaseType GetDBType() => DataBaseType.LOGIN_BONUS;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //OptionData
    public class OptionDataBase : DataBase<OptionData>
    {
        public override DataBaseType GetDBType() => DataBaseType.OPTION_DATA;

        public override void AddData(SQLiteDataReader rdr)
        {
            IReadData data = CreateNewData() as IReadData;
            OptionData newData = data.GetReadData<OptionData>(rdr, msgSB);
            newData.Init();

            GetDataList().Add(newData);
        }
        
        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //PetExpData
    public class PetExpDataBase : DataBase<PetExpData>
    {
        public override DataBaseType GetDBType() => DataBaseType.PET_EXP;

        private Dictionary<int, List<PetExpData>> petExpDataDic = new Dictionary<int, List<PetExpData>>();

        public List<PetExpData> GetPetExpList(int level)
        {
            if (level <= 0) level = 1;

            List<PetExpData> findDataList = new List<PetExpData>();
            petExpDataDic?.TryGetValue(level, out findDataList);

            return findDataList;
        }

        public PetExpData GetPetExpData(int level, int exp_id)
        {
            var findList = GetPetExpList(level);
            var findData = findList.Find(v => v.pet_group_id == exp_id);

            return findData;
        }

        public int GetPetLevelByExp(int exp, int exp_id)
        {
            var findList = GetDataList().FindAll(v => 
            {
                var data = v as PetExpData;
                return data.pet_group_id == exp_id && data.exppoint <= exp;
            });

            if (findList == null || findList.Count == 0) return 0;

            findList?.Sort((a, b) => 
            {
                return (b as PetExpData).pet_level.CompareTo((a as PetExpData).pet_level);
            });

            return (findList.First() as PetExpData).pet_level;
        }

        public override void ReadData()
        {
            var dataList = GetDataList();            
            for (int i = 0; i < dataList.Count; i++)
            {
                var data = dataList[i] as PetExpData;
                if (!petExpDataDic.ContainsKey(data.pet_level))
                {
                    petExpDataDic.Add(data.pet_level, new List<PetExpData>());
                }                    
                petExpDataDic[data.pet_level].Add(data);
            }
        }

        public override void CombinedData() { }
    }

    //LanguageData
    public class LanguageDataBase : DataBase<LanguageData>
    {
        public override DataBaseType GetDBType() => DataBaseType.LANGUAGE;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //SetTableData
    public class SetTableDataBase : DataBase<SetTableData>
    {        
        public override DataBaseType GetDBType() => DataBaseType.SET_TABLE;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //MapInfoData
    public class MapInfoDataBase : DataBase<MapInfoData>
    {
        //뽑기상점에서 획득 가능합니다. -> 문구ID
        public readonly int GachaShopId = 50027;

        public override DataBaseType GetDBType() => DataBaseType.MAP_INFO;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //ItemRandOptionData
    public class ItemRandOptionDataBase : DataBase<ItemRandOptionData>
    {
        private Dictionary<object, List<ItemRandOptionData>> randomOptionGroupDic = new Dictionary<object, List<ItemRandOptionData>>();

        public override DataBaseType GetDBType() => DataBaseType.ITEM_RAND_OPTION;

        public override void ReadData()
        {
            int curOptionID = -1;
            List<ItemRandOptionData> curOptionGroup = null; // Dictionary 찾는 과정을 최소화하기 위해 가장 최근 리스트는 잡아둡니다.

            foreach (var data in GetDataList())
            {
                var each = data as ItemRandOptionData;

                if (curOptionID == each.item_rand_option_id)
                    curOptionGroup.Add(each);
                else
                {
                    if (randomOptionGroupDic.ContainsKey(each.item_rand_option_id))
                    {
                        curOptionGroup = randomOptionGroupDic[each.item_rand_option_id];
                    }
                    else
                    {
                        curOptionGroup = new List<ItemRandOptionData>();
                        randomOptionGroupDic.Add(each.item_rand_option_id, curOptionGroup);
                    }

                    curOptionID = each.item_rand_option_id;
                    curOptionGroup.Add(each);
                }
            }
        }
        public override void CombinedData() { }

        public List<ItemRandOptionData> GetOptionDataList(object id)
        {
            var list = new List<ItemRandOptionData>();
            randomOptionGroupDic.TryGetValue(id, out list);

            return list;
        }
    }

    //TitleData
    public class TitleDataBase : DataBase<TitleData>
    {
        public override DataBaseType GetDBType() => DataBaseType.TITLE_NAME;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //SkillData
    public class SkillDataBase : DataBase<SkillData>
    {
        public override DataBaseType GetDBType() => DataBaseType.SKILL;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //SkillExtraData
    public class SkillExtraDataBase : DataBase<SkillExtraData>
    {
        public override DataBaseType GetDBType() => DataBaseType.SKILL_EXTRA;

        public override void ReadData() { }
        public override void CombinedData() { }
    }

    //QuestData
    public class QuestDataBase : DataBase<QuestData>
    {
        public override DataBaseType GetDBType() => DataBaseType.QUEST;

        public override void ReadData() { }
        public override void CombinedData() { }
    }
}


