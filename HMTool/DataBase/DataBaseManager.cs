﻿using HMTool.Data.Base;
using HMTool.Data;
using HMTool.Latale;
using HMTool.Network.Servers;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HMTool.WindowView.ViewModel.Base;

namespace HMTool.DB
{
    public class DataBaseManager
    {
        private static DataBaseManager instance = null;
        public static DataBaseManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new DataBaseManager();
                return instance;
            }
        }

        private StringBuilder sb = null;
        public string Msg
        {
            get
            {
                string msg = sb?.ToString();
                sb.Clear();

                return msg;
            }
        }

        public LAVersion laVersion;
        public ServerInfo serverInfo;

        private Dictionary<DataBaseType, DBVersionData> dataVersionDic;
        private Dictionary<DataBaseType, IDataBase> databaseDIc = new Dictionary<DataBaseType, IDataBase>();

        private WebClient webClient = null;

        public void Init(LAVersion laVersion, ServerInfo serverInfo, Dictionary<DataBaseType, DBVersionData> dataVersionDic)
        {
            sb = new StringBuilder();
            webClient = new WebClient();

            this.laVersion = laVersion;
            this.serverInfo = serverInfo;
            this.dataVersionDic = dataVersionDic;

            this.databaseDIc.Clear();
        }

        public DataBaseType[] GetAddedDataBaseTypes()
        {
            if (databaseDIc.Keys == null) return null;
            return databaseDIc.Keys.ToArray();
        }

        public IDataBase GetDataBase(DataBaseType dataType)
        {
            if (!databaseDIc.ContainsKey(dataType)) return null;
            return databaseDIc[dataType];
        }

        public List<ICustomData> GetDataList(DataBaseType dataType)
        {
            if (!databaseDIc.ContainsKey(dataType)) return null;
            return databaseDIc[dataType].GetDataList();
        }

        /// <summary>
        /// Load -> Read -> Combined
        /// </summary>
        public Task<bool> Process(WindowViewModel viewModel)
        {
            IDataBase[] database = new IDataBase[]
            {
                new ItemDataBase(),
                new PetDataBase(),
                new NpcDataBase(),
                new MakeDataBase(),
                new StatusDataBase(),
                new BasisDataBase(),
                new CashShopDataBase(),
                new JobDropDataBase(),
                new MonsterDropDataBase(),
                new MonsterDropListDataBase(),
                new GachaDropDataBase(),
                new BinggoPanDataBase(),
                new LoginBonusDataBase(),
                new OptionDataBase(),
                new PetExpDataBase(),
                new LanguageDataBase(),
                new SetTableDataBase(),
                new MapInfoDataBase(),
                new ItemRandOptionDataBase(),
                new TitleDataBase(),
                new SkillDataBase(),
                new SkillExtraDataBase(),
                new QuestDataBase(),
            };

            return LoadDataBase(viewModel, database);
        }

        private async Task<bool> LoadDataBase(WindowViewModel owner, IDataBase[] database)
        {
            bool isComplete = await LoadingViewModel.Show(owner, database, new LoadDatabase(this));

            if (isComplete)
            {
                //데이터 로드 끝
                foreach (var db in database)
                {
                    string msg = db.GetMsg();

                    if (msg != string.Empty)
                        sb.Append(msg);
                }
                return await ReadDataBase(owner, database);
            }
            else return false;
        }

        private async Task<bool> ReadDataBase(WindowViewModel owner, IDataBase[] database)
        {
            bool isComplete = await LoadingViewModel.Show(owner, database, new ReadDataBase());

            if (isComplete)
            {
                //데이터 리드 끝
                foreach (var db in database)
                {
                    databaseDIc.Add(db.GetDBType(), db);
                }
                return await CombinedDataBase(owner);
            }
            else return false;
        }

        private async Task<bool> CombinedDataBase(WindowViewModel owner)
        {
            IDataBase[] database = databaseDIc.Values.ToArray();
            
            return await LoadingViewModel.Show(owner, database, new CombineDataBase()); 
            //데이터 합성 끝
        }

        public void LoadDataBaseFile(IDataBase dataBase)
        {
            DataBaseType type = dataBase.GetDBType();
            string dataclassName = dataBase.GetDataName();

            string resourcesDirectoryName = "resources";
            string directoryPath = resourcesDirectoryName + "/" + serverInfo.PathName;

            bool isSecret = false;

            string fileName = string.Empty;
            string filePath = string.Empty;

            ExploreDirectory(directoryPath, resourcesDirectoryName);

            if (type == DataBaseType.DEFENCE_VALUE ||
                type == DataBaseType.MONSTER_DROP ||
                type == DataBaseType.MONSTER_DROP_LIST ||
                type == DataBaseType.SERVER_MAP_INFO)
            {
                //시크릿파일로 구분된 디비는 앞에 이름이 없다.
                isSecret = true;
                fileName = string.Format("DB_{0}_", (int)type);
            }
            else fileName = string.Format("{0}DB_", dataclassName);


            if (dataVersionDic.ContainsKey(type))
            {
                short version = dataVersionDic[type].data_version;

                fileName = fileName + version;
                filePath = directoryPath + "/" + fileName;
            }
            else
            {
                var dbFiles = Directory.GetFiles(directoryPath);
                var findDBFiles = dbFiles.ToList().FindAll(v => v.Contains(fileName));

                filePath = findDBFiles.Max();

                if (filePath == null)
                    return;
            }

            //if (dataVersionDic == null)
            //{
            //    var dbFiles = Directory.GetFiles(directoryPath);
            //    var findDBFiles = dbFiles.ToList().FindAll(v => v.Contains(fileName));

            //    filePath = findDBFiles.Max();

            //    if (filePath == null)
            //        return;
            //}
            //else
            //{
            //    if (!dataVersionDic.ContainsKey(type))
            //        return;

            //    short version = dataVersionDic[type].data_version;

            //    fileName = fileName + version;
            //    filePath = directoryPath + "/" + fileName;
            //}

            FileInfo file = new FileInfo(filePath);
            if (!file.Exists)
            {
                if (!DownLoadFile(isSecret, fileName, filePath))
                    return;
            }

            SQLiteConnection conn = new SQLiteConnection("Data Source=" + file.FullName + ";Version=3;");
            conn.Open();

            string query = "SELECT * FROM " + dataclassName;

            SQLiteCommand cmd = new SQLiteCommand(query, conn);            
            SQLiteDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                dataBase.AddData(rdr);
            }

            rdr.Close();
        }
        
        private void ExploreDirectory(string directoryPath, string resourcesDirectoryName)
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            
            DirectoryInfo directoryInfo = new DirectoryInfo(currentDirectory + "/" + resourcesDirectoryName);
            if (!directoryInfo.Exists)
                directoryInfo.Create();

            directoryInfo = new DirectoryInfo(currentDirectory + "/" + directoryPath);
            if (!directoryInfo.Exists)
                directoryInfo.Create();
        }

        private bool DownLoadFile(bool isSecret, string fileName, string filePath)
        {
            string downloadAddress = isSecret ? serverInfo.GetSecretDBDownloadPath(fileName) : serverInfo.GetDBDownloadPath(fileName);

            try
            {
                byte[] dbBuffer = webClient.DownloadData(downloadAddress);

                AesEncrypter decoder = new AesEncrypter(isSecret ? laVersion.aes_secret_key : laVersion.aes_iv);
                byte[] decodedBuffer = decoder.Decrypt(dbBuffer);

                using (FileStream stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Write, 4096, true))
                    stream.Write(decodedBuffer, 0, decodedBuffer.Length);

                return true;
            }
            catch (Exception e)
            {
                CustomMessageBox.Show(string.Format("{0}\n{1}", fileName, e.Message));
                return false;
            }                      
        }
    }
}
