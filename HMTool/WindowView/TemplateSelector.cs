﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HMTool.WindowView
{
    public class TemplateSelector : DataTemplateSelector
    {
        public DataTemplate TextTemplate { get; set; }
        public DataTemplate ComboTemplate { get; set; }
        
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null) return null;

            FrameworkElement frameworkElement = container as FrameworkElement;
            if (frameworkElement == null) return null;

            if (item is TextTemplateBox)
            {
                TextTemplate = frameworkElement.FindResource("text") as DataTemplate;
                return TextTemplate;
            }
            else if (item is ComboTemplateBox)
            {
                ComboTemplate = frameworkElement.FindResource("combo") as DataTemplate;
                return ComboTemplate;
            }

            return null;
        }
    }

    public sealed class InterfaceTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement containerElement = container as FrameworkElement;

            if (null == item || null == containerElement)
                return base.SelectTemplate(item, container);

            Type itemType = item.GetType();

            IEnumerable<Type> dataTypes
                = Enumerable.Repeat(itemType, 1).Concat(itemType.GetInterfaces());

            DataTemplate template
                = dataTypes.Select(t => new DataTemplateKey(t))
                    .Select(containerElement.TryFindResource)
                    .OfType<DataTemplate>()
                    .FirstOrDefault();

            //return template ?? base.SelectTemplate(item, container);
            return template ?? new DataTemplate();
        }
    }

    public abstract class TemplateBox
    {
        public string Name { get; set; }
        public object Value { get; set; }

        public abstract void Init(string name, object value);
    }

    public class TextTemplateBox : TemplateBox
    {
        public override void Init(string name, object value)
        {
            this.Name = name;
            this.Value = value;
        }
    }

    public class ComboTemplateBox : TemplateBox
    {
        public ObservableCollection<object> List { get; set; }       
        
        public ComboTemplateBox(object[] array)
        {
            List = new ObservableCollection<object>();

            foreach (var data in array)
            {
                List.Add(data);
            }            
        }

        public override void Init(string name, object value)
        {
            this.Name = name;
            this.Value = List.FirstOrDefault();
        }
    }
}
