﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Base
{
    public class WindowViewModel : BaseViewModel
    {
        public event EventHandler<RoutedEventArgs> LoadEvent;
        public event EventHandler<KeyEventArgs> KeyDownEvent;

        public event EventHandler<EventArgs> CloseEvent;

        public event Action Hide;

        private bool isClosed = false;
        
        public virtual void OnLoaded(object sender, RoutedEventArgs e)
        {
            //창이 로드 됐을때 호출된다.
            LoadEvent?.Invoke(sender, e);
        }

        public virtual void KeyDown(object sender, KeyEventArgs e)
        {
            //키를 눌렀을때 호출된다.
            KeyDownEvent?.Invoke(sender, e);
        }

        public virtual void Closing(object sender, CancelEventArgs e)
        {
            //창이 닫히려고 할때 호출된다.
            isClosed = true;
        }

        public virtual void Closed(object sender, EventArgs e)
        {
            //창이 닫히고 호출된다.
        }

        public void CloseWindow()
        {
            if (isClosed) return;

            //호출하면 창을 닫는다.
            CloseEvent?.Invoke(this, EventArgs.Empty);
        }

        public void HideWindow()
        {
            //호출하면 창을 숨긴다.
            Hide?.Invoke();
        }
    }
}
