﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Base
{
    public abstract class UserControlViewModel : BaseViewModel
    {
        public object ParentObject { get; private set; }

        public UserControlViewModel(object parentObject)
        {
            this.ParentObject = parentObject;

            if (ParentObject is WindowViewModel)
            {
                (ParentObject as WindowViewModel).KeyDownEvent += KeyDown;
            }
        }

        public virtual void KeyDown(object sender, KeyEventArgs e)
        {

        }

        public abstract UserControl GetNewView();

        public abstract string GetTabName { get; }

        public virtual void Dispose()
        {
            if (ParentObject is WindowViewModel)
            {
                (ParentObject as WindowViewModel).KeyDownEvent -= KeyDown;
            }
        }
    }
}
