﻿using System.ComponentModel;
using System.Data;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Base
{
    public abstract class Storage : IStorage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public object ParentObject { get; }

        public ICommand DoubleClickCmd { get; set; }
        public ICommand ReqStorageCmd { get; set; }
        public ICommand DeleteStorageItemesCmd { get; set; }
        public ICommand DataBoxCmd { get; set; }
        
        private DataTable mainDataTable = new DataTable();
        public DataTable MainDataTable { get { return mainDataTable; }
            set
            {
                mainDataTable = value;
                OnPropertyChanged("MainDataTable");
            }
        }

        public abstract string Title { get; }

        public Storage(object parentObject)
        {
            this.ParentObject = parentObject;

            DoubleClickCmd = new Command(DoubleClick);
            ReqStorageCmd = new Command(ReqStorage);
            DeleteStorageItemesCmd = new Command(DeleteStorageItemes);
            DataBoxCmd = new Command(DataBoxAction);
        }

        public void OnPropertyChanged(string propertyName)
        {
            //view에서(.xaml) 바인딩한 propertyChanged 이벤트 
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public abstract void DoubleClick(object parameter);
        public abstract void ReqStorage(object parameter);
        public abstract void DeleteStorageItemes(object parameter);
        public abstract void DataBoxAction(object parameter);
    }
}
