﻿using HMTool.Latale;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Base
{
    public interface IStorage
    {
        ICommand DoubleClickCmd { get; set; }
        ICommand ReqStorageCmd { get; set; }
        ICommand DeleteStorageItemesCmd { get; set; }
        ICommand DataBoxCmd { get; set; }

        DataTable MainDataTable { get; set; }
    }

    public interface IStorageViewModel
    {
        IStorage Storage { get; }
        IStorage StandByStorage { get; }
    }

    public interface IStorageEditMode
    {
        ICommand EditModeCmd { get; set; }
        ICommand ApplyEditCmd { get; set; }
        ObservableCollection<TemplateBox> EditBoxList { get; set; }
    }

    public interface IStorageAddGiftData
    {
        ICommand AddGiftDataCmd { get; set; }

        int InputCount { get; set; }
        GiftType CurrentGiftType { get; set; }
        ObservableCollection<GiftType> GiftTypeList { get; set; }
    }
}
