﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace HMTool.WindowView.ViewModel.Base
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void SetDataContext(Window window)
        {
            window.DataContext = this;            
        }

        public void SetDataContext(UserControl userControl)
        {
            userControl.DataContext = this;
        }

        public void OnPropertyChanged(string propertyName)
        {
            //view에서(.xaml) 바인딩한 propertyChanged 이벤트 
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
