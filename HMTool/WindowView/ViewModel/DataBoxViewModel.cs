﻿using HMTool.Custom;
using HMTool.Data;
using HMTool.Data.Base;
using HMTool.WindowView.ViewModel.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel
{
    public class DataBoxViewModel : WindowViewModel
    {        
        private WindowViewModel parentObject = null;
        private List<DataBoxItem> dataBoxList = new List<DataBoxItem>();

        private DataTable dt1 = new DataTable();
        public DataTable Dt1
        {
            get { return dt1; }
            set
            {
                dt1 = value;
                OnPropertyChanged("Dt1");
            }
        }
        
        private DataTable dt2 = new DataTable();
        public DataTable Dt2
        {
            get { return dt2; }
            set
            {
                dt2 = value;
                OnPropertyChanged("Dt2");
            }
        }

        private DataTable dt3 = new DataTable();
        public DataTable Dt3
        {
            get { return dt3; }
            set
            {
                dt3 = value;
                OnPropertyChanged("Dt3");
            }
        }

        public bool IsShowAllColumns { get; set; }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return (deleteCommand) ?? (deleteCommand = new Command(DeleteItems));
            }
        }

        private ICommand dataInfoCommand;
        public ICommand DataInfoCommand
        {
            get
            {
                return (dataInfoCommand) ?? (dataInfoCommand = new Command(ShowTargetData));
            }
        }

        private ICommand dbCommand;
        public ICommand DBCommand
        {
            get
            {
                return (dbCommand) ?? (dbCommand = new Command(ShowTargetDB));
            }
        }

        private ICommand presetSaveCommand;
        public ICommand PresetSaveCommand
        {
            get
            {
                return (presetSaveCommand) ?? (presetSaveCommand = new Command(CreatePreset));
            }
        }

        private ICommand presetLoadCommand;
        public ICommand PresetLoadCommand
        {
            get
            {
                return (presetLoadCommand) ?? (presetLoadCommand = new Command(LoadPreset));
            }
        }

        private string currentField;
        public string CurrentField
        {
            get => currentField;
            set
            {
                currentField = value;
                OnPropertyChanged("CurrentField");
            }
        }

        public ObservableCollection<string> DBTypeList { get; set; }
        
        private Dictionary<string, List<Tuple<object, ColumnDetail>>> AttributeDic { get; set; }
        private Dictionary<string, Dictionary<string, object>> ParameterDic { get; set; }

        public DataBoxViewModel(WindowViewModel parentObject)
        {            
            this.parentObject = parentObject;

            DBTypeList = new ObservableCollection<string>();

            AttributeDic = new Dictionary<string, List<Tuple<object, ColumnDetail>>>();
            ParameterDic = new Dictionary<string, Dictionary<string, object>>();         
        }

        public async void AddData(ICustomData[] dataArray)
        {
            foreach (var data in dataArray)
            {
                if (data is IViewData)
                {
                    var dataBoxItem = (data as IViewData).GetDataBoxItem();
                    foreach (var boxItem in dataBoxItem)
                    {
                        dataBoxList.Add(boxItem);                                            
                    }                    
                }   
            }            

            var task = new LoadDataTable(LoadDataTableType.All, new DataTable(), false);
            await LoadingViewModel.Show(this, dataBoxList.ToArray(), task);

            Dt1 = (task as LoadDataTable).dt;
        }

        private void DeleteItems(object parameter)
        {
            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in rowViewArray)
            {
                DataBoxItem item = FindDataBoxItem(rowView.Row);

                Dt1.Rows.Remove(rowView.Row);
                dataBoxList.Remove(item);
            }
        }

        private async void ShowTargetData(object parameter)
        {
            if (parameter == null) return;

            DBTypeList.Clear();
            AttributeDic.Clear();
            ParameterDic.Clear();

            DataRowView rowView = parameter as DataRowView;
            DataBoxItem item = FindDataBoxItem(rowView.Row);

            ICustomData targetData = item.originalData;

            var fields = targetData.GetType().GetFields();
            foreach (var field in fields)
            {
                var atts = field.GetCustomAttributes(false);
                foreach (var att in atts)
                {
                    if (att is ColumnDetail)
                    {
                        object obj = field.GetValue(item.originalData);

                        var detail = att as ColumnDetail;
                        var key = detail.key;

                        if (!AttributeDic.ContainsKey(key))
                        {
                            AttributeDic.Add(key, new List<Tuple<object, ColumnDetail>>());                            
                        }
                        AttributeDic[key].Add(new Tuple<object, ColumnDetail>(obj, detail));

                        if (!DBTypeList.Contains(key))
                        {
                            DBTypeList.Add(key);
                        }
                    }
                    else if (att is ColumnDetailParameter)
                    {
                        object obj = field.GetValue(item.originalData);

                        var customParameter = att as ColumnDetailParameter;

                        var key = customParameter.key;
                        var fieldName = customParameter.FieldName;

                        if (!ParameterDic.ContainsKey(key))
                        {
                            ParameterDic.Add(key, new Dictionary<string, object>());                            
                        }
                        ParameterDic[key].Add(fieldName, obj);
                    }
                }
            }

            CurrentField = DBTypeList.FirstOrDefault();

            var task = new LoadDataTable(LoadDataTableType.All, new DataTable(), IsShowAllColumns);
            await LoadingViewModel.Show(this, new object[] { item.originalData }, task);

            Dt2 = (task as LoadDataTable).dt;

            //바로 조회
            ShowTargetDB(null);
        }

        private async void ShowTargetDB(object parameter)
        {
            if (CurrentField == null || CurrentField == string.Empty) return;

            var dataList = new List<ICustomData>();

            if (AttributeDic.ContainsKey(CurrentField))
            {                
                var typeList = AttributeDic[CurrentField];
                foreach (var tuple in typeList)
                {
                    var obj = tuple.Item1;
                    var att = tuple.Item2;

                    Dictionary<string, object> parameterDic = null;
                    if (ParameterDic.ContainsKey(CurrentField))
                    {
                        parameterDic = ParameterDic[CurrentField];          
                    }

                    var array = att.GetDataArray(obj, parameterDic);
                    dataList.AddRange(array);
                }
            }

            var task = new LoadDataTable(LoadDataTableType.All, new DataTable(), IsShowAllColumns);
            await LoadingViewModel.Show(this, dataList.ToArray(), task);

            Dt3 = (task as LoadDataTable).dt;
        }

        private void CreatePreset(object parameter)
        {
            Directory.CreateDirectory("preset");

            SaveFileDialog dialog = new SaveFileDialog();
            {
                dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\preset";
                dialog.Filter = "Text Files |*.txt;";
                dialog.Title = "Save Preset";
                dialog.CheckPathExists = true;
            };
            dialog.ShowDialog();

            if (dialog.FileName != "")
            {
                List<ICustomData> list = new List<ICustomData>();
                foreach (var data in dataBoxList)
                {
                    list.Add(data.originalData);
                }

                string serializedJson = JsonConvert.SerializeObject(list, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
                });
                
                File.WriteAllText(dialog.FileName, serializedJson);
            }            
        }

        private void LoadPreset(object parameter)
        {
            Directory.CreateDirectory("preset");

            OpenFileDialog dialog = new OpenFileDialog();
            {
                dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\preset";
                dialog.Filter = "Text Files |*.txt;";
                dialog.Title = "Load Preset";
                dialog.CheckFileExists = true;
                dialog.CheckPathExists = true;
            }
            dialog.ShowDialog();

            if (dialog.FileName != "")
            {                
                string json = File.ReadAllText(dialog.FileName);

                var list = JsonConvert.DeserializeObject<List<ICustomData>>(json, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                });

                if (list == null)
                {
                    CustomMessageBox.Show(this, "잘못된 파일입니다.");
                }
                else AddData(list.ToArray());
            }
        }

        public DataBoxItem[] GetDataArray()
        {
            return dataBoxList.ToArray();
        }

        private DataBoxItem FindDataBoxItem(DataRow row)
        {         
            int id = row.Field<int>("id");
            string name = row.Field<string>("name");
            int count = row.Field<int>("count");

            return dataBoxList.Find(v => v.id == id && v.name == name);
        }        
    }

    public class DataBoxItem : ViewData
    {                
        public ICustomData originalData;

        //public int id;
        //public string name;
        public int grade;
        public int count;
        
        public DataBoxItem()
        {

        }

        public DataBoxItem(ICustomData data, int id, string name, int grade = 1, int count = 1)
        {
            this.originalData = data;

            this.id = id;
            this.name = name;
            this.grade = grade;
            this.count = count;
        }

        public DataBoxItem(DataBoxItem oldData)
        {
            this.originalData = oldData.originalData;

            this.id = oldData.id;
            this.name = oldData.name;
            this.grade = oldData.grade;
            this.count = oldData.count;
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "grade", typeof(int)),
                new Tuple<string, Type>( "count", typeof(int)),                
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");            

            return list.Find(v => v.ID == id);
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                this,
            };
        }
    }
}
