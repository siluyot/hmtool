﻿using HMTool.WindowView.ViewModel.Base;

namespace HMTool.WindowView.ViewModel.Inven.Item
{
    public class ItemInvenViewModel : InvenViewModel
    {
        public override IStorage Storage { get; set; }
        public override IStorage StandByStorage { get; set; }

        public ItemInvenViewModel(object parentObject) : base(parentObject)
        {
            Storage = new ItemInvenStorage(parentObject);
            StandByStorage = new ItemInvenStandByStorage(parentObject);
        }

        public override string GetTabName => "Item";        
    }
}
