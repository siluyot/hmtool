﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.Latale;
using HMTool.Network;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Inven.Item
{
    public class ItemInvenStorage : Storage, IStorageEditMode
    {
        public List<CharItemData> dataList = new List<CharItemData>();

        private CharDetailInfo lastCharData = null;
        public CharDetailInfo CurrnetCharData
        {
            get
            {
                ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
                return toolBoxViewModel.FindViewModel<UserInfoViewModel>()?.CharDetailInfo;
            }
        }

        public ICommand EditModeCmd { get; set; }
        public ICommand ApplyEditCmd { get; set; }
        public ObservableCollection<TemplateBox> EditBoxList { get; set; }

        public ItemInvenStorage(object parentObject) : base(parentObject)
        {
            EditModeCmd = new Command(EditModeItem);
            ApplyEditCmd = new Command(ReqEditItem);
            EditBoxList = new ObservableCollection<TemplateBox>();
        }

        public override void DoubleClick(object parameter)
        {
            
        }

        private async void ResStorage(bool isSuccess, SFSObject resObj)
        {
            if (isSuccess)
            {
                dataList.Clear();

                if (resObj.ContainsKey("2"))
                {
                    ISFSArray array = resObj.GetSFSArray("3");
                    int uid = resObj.GetInt("1");
                    int cid = resObj.GetInt("2");

                    for (int i = 0; i < array.Count; i++)
                    {
                        ISFSObject sfsObj = array.GetSFSObject(i);

                        CharItemData charItemData = new CharItemData();
                        charItemData.SetCharItemData(uid, cid, sfsObj);

                        switch (charItemData.item_pos?.itemPosGroup)
                        {
                            case ItemPosGroup.EQUIP:
                            case ItemPosGroup.COS_EQUIP:
                            case ItemPosGroup.INVEN:
                            case ItemPosGroup.COS_INVEN:
                                dataList.Add(charItemData);
                                break;
                        }
                    }

                    if (dataList.Count <= 0)
                    {
                        CharItemData data = new CharItemData
                        {
                            item_no = 0,
                            pos = "없음",
                            type = "없음",
                            item_id = 0,
                            name = "없음",
                            grade = 0,
                            size = 0,
                        };
                        dataList.Add(data);
                    }

                    MainDataTable = await UpdateTable(dataList.ToArray());
                }
            }
        }

        private void EditModeItem(object parameter)
        {
            var data = new CharItemData();
            var fields = data.GetType().GetFields();

            EditBoxList.Clear();
            foreach (var column in data.GetEditColumns())
            {
                var name = column.Item1;
                var template = column.Item2;

                template.Init(name, 0);

                EditBoxList.Add(template);
            }
        }

        public void ReqEditItem(object parameter)
        {
            if (parameter == null)
                return;

            if (lastCharData != CurrnetCharData)
            {
                CustomMessageBox.Show("해당 캐릭터의 인벤조회가 필요합니다.");
                return;
            }

            IList list = (IList)parameter;
            if (list.Count <= 0)
            {
                CustomMessageBox.Show("변경하려는 항목을 선택해 주세요");
                return;
            }

            var editList = new List<CharItemData>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in dataRowViewes)
            {
                var findData = new CharItemData().DataRowToData(customDataList, rowView.Row) as CharItemData;
                if (findData == null) continue;

                var fields = findData.GetType().GetFields();
                foreach (var info in EditBoxList)
                {
                    var findField = Array.Find(fields, v => v.Name == info.Name);
                    try
                    {
                        var value = Convert.ChangeType(info.Value, findField.FieldType);
                        findField.SetValue(findData, value);
                    }
                    catch
                    {
                        FieldInfo fInfo = findField.FieldType.GetField("MaxValue");
                        if (fInfo != null && fInfo.IsLiteral && !fInfo.IsInitOnly)
                        {
                            object maxValue = fInfo.GetRawConstantValue();
                            findField.SetValue(findData, maxValue);
                        }
                    }
                }
                findData.ApplyChange();
                editList.Add(findData);
            }

            if (editList.Count > 0)
            {
                List<RequestData> requestList = new List<RequestData>();
                foreach (var pet in editList)
                {
                    SFSObject sfsObj = SFSObject.NewInstance();
                    sfsObj.PutInt("1", CurrnetCharData.uid);
                    sfsObj.PutInt("2", CurrnetCharData.cid);
                    sfsObj.PutSFSObject("3", pet.ToSFSObject());

                    requestList.Add(new RequestData(GMProtocol.EditItem, sfsObj));
                }
                RequestLoadingView(requestList, () => ReqStorage(null));
            }
        }

        private async Task<DataTable> UpdateTable(CharItemData[] dataArray)
        {
            DataTable newTable = new DataTable();

            var columns = new CharItemData().GetColumns();
            foreach (var column in columns)
            {
                DataColumn newColumn = new DataColumn(column.Item1, column.Item2);
                newTable.Columns.Add(newColumn);
            }

            var task = new LoadDataTable(LoadDataTableType.Rows, newTable, false);
            await LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, task);

            return (task as LoadDataTable).dt;
        }

        public override void ReqStorage(object parameter)
        {
            lastCharData = CurrnetCharData;

            SFSObject sfsObj = SFSObject.NewInstance();
            if (CurrnetCharData == null) sfsObj.PutNull("2");
            else sfsObj.PutInt("2", CurrnetCharData.uid);

            if (CurrnetCharData == null) sfsObj.PutNull("3");
            else sfsObj.PutInt("3", CurrnetCharData.cid);

            var data = new RequestData(GMProtocol.RequestItemInvenList, sfsObj, ResStorage);
            NetworkManager.Instance.SendProtocol(ParentObject, data);
        }

        public override void DeleteStorageItemes(object parameter)
        {
            if (lastCharData != CurrnetCharData)
            {
                CustomMessageBox.Show("해당 캐릭터의 인벤조회가 필요합니다.");
                return;
            }

            var deleteItemList = new List<long>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in rowViewArray)
            {
                CharItemData item = new CharItemData().DataRowToData(customDataList, rowView.Row) as CharItemData;

                if (!(item.item_no == 0 && item.item_id == 0))
                    deleteItemList.Add(item.item_no);

                MainDataTable.Rows.Remove(rowView.Row);
                dataList.Remove(item);
            }

            if (deleteItemList.Count > 0)
            {
                int uid = CurrnetCharData.uid;
                int cid = CurrnetCharData.cid;

                SFSObject sfsObj = SFSObject.NewInstance();
                sfsObj.PutInt("1", uid);
                sfsObj.PutInt("2", cid);
                sfsObj.PutLongArray("3", deleteItemList.ToArray());

                List<RequestData> requestData = new List<RequestData>()
                {
                    new RequestData(GMProtocol.DeleteItemInven, sfsObj),
                };
                RequestLoadingView(requestData);
            }
        }

        private void RequestLoadingView(List<RequestData> requestList, Action callback = null)
        {
            ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
            UserInfoViewModel userInfoViewModel = toolBoxViewModel.FindViewModel<UserInfoViewModel>();

            List<RequestData> blockList = new List<RequestData>();

            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(true));
            blockList.AddRange(requestList);
            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(false));

            LoadingViewModel.Show(ParentObject as WindowViewModel, blockList.ToArray(), new RequestWait(ParentObject as BaseViewModel), (task) =>
            {
                callback?.Invoke();
                CustomMessageBox.Show("완료!");
            });
        }

        public override void DataBoxAction(object parameter)
        {
            List<CharItemData> selectedList = new List<CharItemData>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in rowViewArray)
            {
                var findData = new CharItemData().DataRowToData(customDataList, rowView.Row) as CharItemData;
                selectedList.Add(findData);
            }

            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            toolBoxVM.OpenDataBox().AddData(selectedList.ToArray());
        }

        public override string Title => "아이템";
    }
}
