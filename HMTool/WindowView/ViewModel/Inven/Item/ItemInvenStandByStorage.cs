﻿using HMTool.Data;
using HMTool.Latale;
using HMTool.Network;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Inven.Item
{
    public class ItemInvenStandByStorage : Storage, IStorageEditMode
    {
        public List<CharItemData> dataList = new List<CharItemData>();
        
        public CharDetailInfo CurrnetCharData
        {
            get
            {
                ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
                return toolBoxViewModel.FindViewModel<UserInfoViewModel>()?.CharDetailInfo;
            }
        }

        public ICommand EditModeCmd { get; set; }
        public ICommand ApplyEditCmd { get; set; }
        public ObservableCollection<TemplateBox> EditBoxList { get; set; }

        public ItemInvenStandByStorage(object parentObject) : base(parentObject)
        {
            EditModeCmd = new Command(EditModeItem);
            ApplyEditCmd = new Command(ApplyEditItem);
            EditBoxList = new ObservableCollection<TemplateBox>();
        }

        private void EditModeItem(object parameter)
        {
            CharItemData item = new CharItemData();
            var fields = item.GetType().GetFields();

            EditBoxList.Clear();
            foreach (var column in item.GetEditColumns())
            {
                var name = column.Item1;
                var template = column.Item2;

                template.Init(name, 0);

                EditBoxList.Add(template);
            }
        }

        private async void ApplyEditItem(object parameter)
        {
            if (parameter == null) return;

            System.Collections.IList list = (System.Collections.IList)parameter;
            if (list.Count <= 0)
            {
                CustomMessageBox.Show("변경하려는 항목을 선택해 주세요");
                return;
            }

            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();            
            foreach (var rowView in dataRowViewes)
            {
                var findData = FindCharWaitItem(rowView.Row);
                if (findData == null) continue;

                var fields = findData.GetType().GetFields();
                foreach (var info in EditBoxList)
                {
                    var findField = Array.Find(fields, v => v.Name == info.Name);
                    try
                    {
                        var value = Convert.ChangeType(info.Value, findField.FieldType);
                        findField.SetValue(findData, value);
                    }
                    catch
                    {
                        FieldInfo fInfo = findField.FieldType.GetField("MaxValue");
                        if (fInfo != null && fInfo.IsLiteral && !fInfo.IsInitOnly)
                        {
                            object maxValue = fInfo.GetRawConstantValue();
                            findField.SetValue(findData, maxValue);
                        }
                    }
                }
                findData.ApplyChange();
            }
            MainDataTable = await SetItemTable(dataList.ToArray());
        }

        public override void DoubleClick(object parameter)
        {
            EditModeItem(parameter);
        }

        public override void ReqStorage(object parameter)
        {
            List<RequestData> requestList = new List<RequestData>();

            foreach (var item in dataList)
            {
                SFSObject sfsObj = SFSObject.NewInstance();
                if (CurrnetCharData == null) sfsObj.PutNull("1");
                else sfsObj.PutInt("1", CurrnetCharData.uid);

                if (CurrnetCharData == null) sfsObj.PutNull("2");
                else sfsObj.PutInt("2", CurrnetCharData.cid);

                sfsObj.PutSFSObject("3", item.ToSFSObject());

                requestList.Add(new RequestData(GMProtocol.AddItemInven, sfsObj));
            }

            if (requestList.Count <= 0)
            {
                CustomMessageBox.Show("추가할 데이터가 없습니다.");
                return;
            }

            RequestLoadingView(requestList);
        }

        private void RequestLoadingView(List<RequestData> requestList)
        {
            ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
            UserInfoViewModel userInfoViewModel = toolBoxViewModel.FindViewModel<UserInfoViewModel>();

            List<RequestData> blockList = new List<RequestData>();

            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(true));
            blockList.AddRange(requestList);
            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(false));

            LoadingViewModel.Show(ParentObject as WindowViewModel, blockList.ToArray(), new RequestWait(ParentObject as BaseViewModel), (task) =>
            {
                CustomMessageBox.Show("완료!");
            });
        }

        public override void DeleteStorageItemes(object parameter)
        {
            System.Collections.IList items = (System.Collections.IList)parameter;
            var collection = items.Cast<DataRowView>().ToArray();

            foreach (var data in collection)
            {
                var findData = FindCharWaitItem(data.Row);
                if (findData == null) continue;

                dataList.Remove(findData);
                MainDataTable.Rows.Remove(data.Row);
            }
        }

        private CharItemData FindCharWaitItem(DataRow row)
        {
            int item_id = row.Field<int>("item_id");
            string name = row.Field<string>("name");
            short item_exppoint = row.Field<short>("item_exppoint");
            short option_index1 = row.Field<short>("option_index1");
            short option_index2 = row.Field<short>("option_index2");
            short option_index3 = row.Field<short>("option_index3");
            short option_index4 = row.Field<short>("option_index4");

            var findData = dataList.Find(v => v.ID == item_id
            && v.NAME == name
            && v.Item_Exppoint == item_exppoint
            && v.Option_Index1 == option_index1
            && v.Option_Index2 == option_index2
            && v.Option_Index3 == option_index3
            && v.Option_Index4 == option_index4);

            return findData;
        }

        public async override void DataBoxAction(object parameter)
        {
            if (CurrnetCharData == null)
            {
                CustomMessageBox.Show("캐릭터 검색이 필요합니다.");
                return;
            }

            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            var dataArray = toolBoxVM.OpenDataBox().GetDataArray();

            foreach (var item in dataArray)
            {
                if (item.originalData is CharItemData)
                {
                    var charItemData = item.originalData as CharItemData;
                    charItemData.item_no = 0;
                    charItemData.cid = CurrnetCharData.cid;
                    charItemData.uid = CurrnetCharData.uid;
                    charItemData.item_pos = ItemPos.INVEN;
                    charItemData.pos = ItemPos.INVEN.itemPosGroup.ToString();

                    dataList.Add(charItemData);
                }
                else if (item.originalData is ItemData)
                {
                    CharItemData charItemData = new CharItemData();
                    charItemData.cid = CurrnetCharData.cid;
                    charItemData.uid = CurrnetCharData.uid;
                    charItemData.settingItem = item.originalData as ItemData;

                    var type = charItemData.settingItem.GetItemType;
                    if (type == ItemType.C_ARMOR ||
                        type == ItemType.C_CAPE ||
                        type == ItemType.C_HELMET ||
                        type == ItemType.C_PANTS)
                    {
                        charItemData.size = 0;
                    }
                    else charItemData.size = (short)item.count;

                    //인벤으로 넣음
                    charItemData.item_pos = ItemPos.INVEN;

                    charItemData.pos = ItemPos.INVEN.itemPosGroup.ToString();
                    charItemData.type = type?.Value.ToString();
                    charItemData.item_id = charItemData.settingItem.item_id;
                    charItemData.grade = charItemData.settingItem.item_grade;
                    charItemData.name = charItemData.settingItem.name;

                    dataList.Add(charItemData);
                }
            }
            MainDataTable = await SetItemTable(dataList.ToArray());
        }

        private async Task<DataTable> SetItemTable(CharItemData[] dataArray)
        {
            DataTable newTable = new DataTable();

            var columns = new CharItemData().GetColumns();
            foreach (var column in columns)
            {
                DataColumn newColumn = new DataColumn(column.Item1, column.Item2);
                newTable.Columns.Add(newColumn);
            }

            var task = new LoadDataTable(LoadDataTableType.Rows, newTable, false);
            await LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, task);

            return (task as LoadDataTable).dt;
        }

        public override string Title => "생성대기함";
    }
}
