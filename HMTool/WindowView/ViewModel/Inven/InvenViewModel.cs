﻿using HMTool.WindowView.View.UC;
using HMTool.WindowView.ViewModel.Base;
using System.Windows.Controls;

namespace HMTool.WindowView.ViewModel.Inven
{
    public abstract class InvenViewModel : UserControlViewModel, IStorageViewModel
    {
        public abstract IStorage Storage { get; set; }
        public abstract IStorage StandByStorage { get; set; }

        public InvenViewModel(object parentObject) : base(parentObject)
        {

        }

        public override UserControl GetNewView()
        {            
            return new Inventory();
        }
    }
}
