﻿using HMTool.Data;
using HMTool.Network;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Inven.Pet
{
    public class PetInvenStandByStorage : Storage, IStorageEditMode
    {
        public List<CharPetData> dataList = new List<CharPetData>();

        public CharDetailInfo CurrnetCharData
        {
            get
            {
                ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
                return toolBoxViewModel.FindViewModel<UserInfoViewModel>()?.CharDetailInfo;
            }
        }
        
        public ICommand EditModeCmd { get; set; }
        public ICommand ApplyEditCmd { get; set; }
        public ObservableCollection<TemplateBox> EditBoxList { get; set; }

        public PetInvenStandByStorage(object parentObject) : base(parentObject)
        {
            EditModeCmd = new Command(EditModeItem);
            ApplyEditCmd = new Command(ApplyEditItem);
            EditBoxList = new ObservableCollection<TemplateBox>();
        }

        private void EditModeItem(object parameter)
        {
            CharPetData data = new CharPetData();
            var fields = data.GetType().GetFields();

            EditBoxList.Clear();
            foreach (var column in data.GetEditColumns())
            {
                var name = column.Item1;
                var template = column.Item2;

                template.Init(name, 0);

                EditBoxList.Add(template);
            }
        }

        private async void ApplyEditItem(object parameter)
        {
            if (parameter == null)
                return;

            System.Collections.IList list = (System.Collections.IList)parameter;
            if (list.Count <= 0)
            {
                CustomMessageBox.Show("변경하려는 항목을 선택해 주세요");
                return;
            }
            
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();
            foreach (var rowView in dataRowViewes)
            {
                var findData = FindCharWaitPet(rowView.Row);
                if (findData == null) continue;

                var fields = findData.GetType().GetFields();
                foreach (var info in EditBoxList)
                {
                    var findField = Array.Find(fields, v => v.Name == info.Name);
                    try
                    {
                        var value = Convert.ChangeType(info.Value, findField.FieldType);
                        findField.SetValue(findData, value);
                    }
                    catch
                    {
                        FieldInfo fInfo = findField.FieldType.GetField("MaxValue");
                        if (fInfo != null && fInfo.IsLiteral && !fInfo.IsInitOnly)
                        {
                            object maxValue = fInfo.GetRawConstantValue();
                            findField.SetValue(findData, maxValue);
                        }
                    }
                }
                findData.ApplyChange();
            }
            MainDataTable = await UpdateTable(dataList.ToArray());
        }


        public override void DoubleClick(object parameter)
        {
            EditModeItem(parameter);
        }

        public override void ReqStorage(object parameter)
        {
            if (dataList.Count <= 0)
            {
                CustomMessageBox.Show("추가할 데이터가 없습니다.");
                return;
            }

            SFSObject sfsObj = SFSObject.NewInstance();

            if (CurrnetCharData == null) sfsObj.PutNull("1");
            else sfsObj.PutInt("1", CurrnetCharData.uid);

            if (CurrnetCharData == null) sfsObj.PutNull("2");
            else sfsObj.PutInt("2", CurrnetCharData.cid);

            ISFSArray sfsArray = SFSArray.NewInstance();
            foreach (var data in dataList)
            {
                sfsArray.AddSFSObject(data.ToSFSObject());
            }
            sfsObj.PutSFSArray("3", sfsArray);

            var requestData = new RequestData(GMProtocol.AddPetInven, sfsObj);
            RequestLoadingView(new List<RequestData>() { requestData });
        }
        
        private void RequestLoadingView(List<RequestData> requestList)
        {
            ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
            UserInfoViewModel userInfoViewModel = toolBoxViewModel.FindViewModel<UserInfoViewModel>();

            List<RequestData> blockList = new List<RequestData>();

            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(true));
            blockList.AddRange(requestList);
            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(false));

            LoadingViewModel.Show(ParentObject as WindowViewModel, blockList.ToArray(), new RequestWait(ParentObject as BaseViewModel), (task) =>
            {
                CustomMessageBox.Show("완료!");
            });
        }

        public override void DeleteStorageItemes(object parameter)
        {
            System.Collections.IList items = (System.Collections.IList)parameter;
            var collection = items.Cast<DataRowView>().ToArray();

            foreach (var data in collection)
            {
                var findData = FindCharWaitPet(data.Row);
                if (findData == null) continue;

                dataList.Remove(findData);
                MainDataTable.Rows.Remove(data.Row);
            }
        }

        private CharPetData FindCharWaitPet(DataRow row)
        {
            short pet_id = row.Field<short>("pet_id");
            string name = row.Field<string>("name");
            short pet_exp = row.Field<short>("pet_exp");
            short skill_id = row.Field<short>("skill_id");
            short skill_exp = row.Field<short>("skill_exp");
            byte skill_dmg_value = row.Field<byte>("skill_dmg_value");

            var findData = dataList.Find(v => v.pet_id == pet_id
            && v.NAME == name
            && v.Pet_Exp == pet_exp
            && v.skill_id == skill_id
            && v.Skill_Exp == skill_exp
            && v.Skill_Dmg_Value == skill_dmg_value);

            return findData;
        }

        public async override void DataBoxAction(object parameter)
        {
            if (CurrnetCharData == null)
            {
                CustomMessageBox.Show("캐릭터 검색이 필요합니다.");
                return;
            }

            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            var dataArray = toolBoxVM.OpenDataBox().GetDataArray();

            foreach (var item in dataArray)
            {
                if (item.originalData is CharPetData)
                {
                    var charPetData = item.originalData as CharPetData;
                    charPetData.owner_cid = 0;

                    charPetData.seq = 0;
                    charPetData.cnt = 1;

                    dataList.Add(charPetData);
                }
                else if (item.originalData is PetBookData)
                {
                    CharPetData charPetData = new CharPetData();
                    charPetData.owner_cid = 0;

                    var petData = item.originalData as PetBookData;
                    charPetData.settingPet = petData;

                    charPetData.pet_id = (short)petData.pet_id;
                    charPetData.skill_id = petData.skill_id;
                    charPetData.cnt = 1;

                    charPetData.grade = charPetData.settingPet.pet_grade;
                    charPetData.name = charPetData.settingPet.pet_name;

                    dataList.Add(charPetData);
                }
            }
            MainDataTable = await UpdateTable(dataList.ToArray());
        }

        private async Task<DataTable> UpdateTable(CharPetData[] dataArray)
        {
            DataTable newTable = new DataTable();

            var columns = new CharPetData().GetColumns();
            foreach (var column in columns)
            {
                DataColumn newColumn = new DataColumn(column.Item1, column.Item2);
                newTable.Columns.Add(newColumn);
            }

            var task = new LoadDataTable(LoadDataTableType.Rows, newTable, false);
            await LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, task);

            return (task as LoadDataTable).dt;
        }

        public override string Title => "생성대기함";
    }
}
