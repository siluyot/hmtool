﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.Network;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Inven.Pet
{
    public class PetInvenStorage : Storage, IStorageEditMode
    {
        public List<CharPetData> dataList = new List<CharPetData>();

        private CharDetailInfo lastCharData = null;
        public CharDetailInfo CurrnetCharData
        {
            get
            {
                ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
                return toolBoxViewModel.FindViewModel<UserInfoViewModel>()?.CharDetailInfo;
            }
        }

        public ICommand EditModeCmd { get; set; }
        public ICommand ApplyEditCmd { get; set; }
        public ObservableCollection<TemplateBox> EditBoxList { get; set; }

        public PetInvenStorage(object parentObject) : base(parentObject)
        {
            EditModeCmd = new Command(EditModeItem);
            ApplyEditCmd = new Command(ReqEditPet);
            EditBoxList = new ObservableCollection<TemplateBox>();
        }
        
        public override void DoubleClick(object parameter)
        {
            
        }

        public override void ReqStorage(object parameter)
        {
            lastCharData = CurrnetCharData;

            SFSObject sfsObj = SFSObject.NewInstance();
            if (CurrnetCharData == null) sfsObj.PutNull("1");
            else sfsObj.PutInt("1", CurrnetCharData.uid);

            if (CurrnetCharData == null) sfsObj.PutNull("2");
            else sfsObj.PutInt("2", CurrnetCharData.cid);

            var data = new RequestData(GMProtocol.RequestPetInvenList, sfsObj, ResStorage);
            NetworkManager.Instance.SendProtocol(ParentObject, data);
        }

        private async void ResStorage(bool isSuccess, SFSObject resObj)
        {
            if (isSuccess)
            {
                dataList.Clear();

                if (resObj.ContainsKey("1"))
                {
                    ISFSArray array = resObj.GetSFSArray("1");
                    for (int i = 0; i < array.Count; i++)
                    {
                        ISFSObject sfsObj = array.GetSFSObject(i);

                        CharPetData charPetData = new CharPetData();
                        charPetData.SetUserPetInvenData(sfsObj);

                        dataList.Add(charPetData);
                    }

                    if (dataList.Count <= 0)
                    {
                        CharPetData data = new CharPetData
                        {
                            seq = 0,
                            pet_id = 0,
                            name = "없음",
                            Pet_Exp = 0,
                            skill_id = 0,
                            Skill_Exp = 0,
                            Skill_Dmg_Value = 0,
                        };
                        dataList.Add(data);
                    }
                    MainDataTable = await SetItemTable(dataList.ToArray());
                }
            }
        }

        private void EditModeItem(object parameter)
        {
            var data = new CharPetData();
            var fields = data.GetType().GetFields();

            EditBoxList.Clear();
            foreach (var column in data.GetEditColumns())
            {
                var name = column.Item1;
                var template = column.Item2;

                template.Init(name, 0);

                EditBoxList.Add(template);
            }
        }

        public void ReqEditPet(object parameter)
        {
            if (parameter == null)
                return;

            if (lastCharData != CurrnetCharData)
            {
                CustomMessageBox.Show("해당 캐릭터의 인벤조회가 필요합니다.");
                return;
            }

            System.Collections.IList list = (System.Collections.IList)parameter;
            if (list.Count <= 0)
            {
                CustomMessageBox.Show("변경하려는 항목을 선택해 주세요");
                return;
            }

            var editList = new List<CharPetData>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();
 
            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in dataRowViewes)
            {
                CharPetData findData = new CharPetData().DataRowToData(customDataList, rowView.Row) as CharPetData;
                if (findData == null) continue;
                
                var fields = findData.GetType().GetFields();
                foreach (var info in EditBoxList)
                {
                    var findField = Array.Find(fields, v => v.Name == info.Name);
                    try
                    {
                        var value = Convert.ChangeType(info.Value, findField.FieldType);
                        findField.SetValue(findData, value);
                    }
                    catch
                    {
                        FieldInfo fInfo = findField.FieldType.GetField("MaxValue");
                        if (fInfo != null && fInfo.IsLiteral && !fInfo.IsInitOnly)
                        {
                            object maxValue = fInfo.GetRawConstantValue();
                            findField.SetValue(findData, maxValue);
                        }
                    }
                }
                findData.ApplyChange();

                editList.Add(findData);
            }

            if (editList.Count > 0)
            {
                List<RequestData> requestList = new List<RequestData>();
                foreach (var pet in editList)
                {
                    SFSObject sfsObj = SFSObject.NewInstance();
                    sfsObj.PutInt("1", CurrnetCharData.uid);
                    sfsObj.PutSFSObject("2", pet.ToSFSObject());

                    requestList.Add(new RequestData(GMProtocol.EditPet, sfsObj));
                }
                RequestLoadingView(requestList, ()=> ReqStorage(null));
            }    
        }

        private async Task<DataTable> SetItemTable(CharPetData[] dataArray)
        {
            DataTable newTable = new DataTable();

            var columns = new CharPetData().GetColumns();
            foreach (var column in columns)
            {
                DataColumn newColumn = new DataColumn(column.Item1, column.Item2);
                newTable.Columns.Add(newColumn);
            }

            var task = new LoadDataTable(LoadDataTableType.Rows, newTable, false);
            await LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, task);

            return (task as LoadDataTable).dt;
        }

        public override void DeleteStorageItemes(object parameter)
        {
            if (lastCharData != CurrnetCharData)
            {
                CustomMessageBox.Show("해당 캐릭터의 인벤조회가 필요합니다.");
                return;
            }

            var deleteList = new List<long>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in rowViewArray)
            {
                CharPetData pet = new CharPetData().DataRowToData(customDataList, rowView.Row) as CharPetData;

                if (!(pet.seq == 0 && pet.pet_id == 0))
                    deleteList.Add(pet.seq);

                MainDataTable.Rows.Remove(rowView.Row);
                dataList.Remove(pet);
            }

            if (deleteList.Count > 0)
            {
                int uid = CurrnetCharData.uid;
                int cid = CurrnetCharData.cid;

                SFSObject sfsObj = SFSObject.NewInstance();
                sfsObj.PutInt("1", uid);
                sfsObj.PutInt("2", cid);
                sfsObj.PutLongArray("3", deleteList.ToArray());

                List<RequestData> requestData = new List<RequestData>()
                {
                    new RequestData(GMProtocol.DeletePetInven, sfsObj),
                };
                RequestLoadingView(requestData);
            }
        }

        private void RequestLoadingView(List<RequestData> requestList, Action callback = null)
        {
            ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
            UserInfoViewModel userInfoViewModel = toolBoxViewModel.FindViewModel<UserInfoViewModel>();

            List<RequestData> blockList = new List<RequestData>();

            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(true));
            blockList.AddRange(requestList);
            blockList.Add(userInfoViewModel.GetUpdataUserBlockReqData(false));

            LoadingViewModel.Show(ParentObject as WindowViewModel, blockList.ToArray(), new RequestWait(ParentObject as BaseViewModel), (task) =>
            {
                callback?.Invoke();
                CustomMessageBox.Show("완료!");                
            });
        }

        public override void DataBoxAction(object parameter)
        {
            var selectedList = new List<CharPetData>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in rowViewArray)
            {
                var findData = new CharPetData().DataRowToData(customDataList, rowView.Row) as CharPetData;
                selectedList.Add(findData);
            }

            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            toolBoxVM.OpenDataBox().AddData(selectedList.ToArray());
        }

        public override string Title => "펫";
    }
}
