﻿using HMTool.WindowView.ViewModel.Base;

namespace HMTool.WindowView.ViewModel.Inven.Pet
{
    public class PetInvenViewModel : InvenViewModel
    {
        public override IStorage Storage { get; set; }
        public override IStorage StandByStorage { get; set; }

        public PetInvenViewModel(object parentObject) : base(parentObject)
        {
            Storage = new PetInvenStorage(parentObject);
            StandByStorage = new PetInvenStandByStorage(parentObject);
        }

        public override string GetTabName => "Pet";
    }
}
