﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Network;
using HMTool.WindowView.View;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;

namespace HMTool.WindowView.ViewModel
{
    public class LoadingViewModel : WindowViewModel
    {
        private double currentProgress;
        public double CurrentProgress
        {
            get { return this.currentProgress; }
            set
            {
                currentProgress = value;
                OnPropertyChanged("CurrentProgress");
            }
        }

        private string label;
        public string Label
        {
            get { return label; }
            set
            {
                label = value;
                OnPropertyChanged("Label");
            }
        }

        private bool isClosed = false;
        private BaseTask task;

        private object[] dataArray = null;
        private Action<BaseTask> completeCallback = null;

        public static Task<bool> Show(WindowViewModel owner, object[] dataArray, BaseTask task, Action<BaseTask> completeCallback = null)
        {
            var viewModel = new LoadingViewModel(dataArray, task, completeCallback);
            var ownerWindow = WindowManager.Instance.FindWindow(owner);

            return (WindowManager.Instance.ShowWindow<Loading>(viewModel, ownerWindow) as LoadingViewModel).Exexute();
        }

        private LoadingViewModel(object[] dataArray, BaseTask task, Action<BaseTask> completeCallback)
        {
            this.dataArray = dataArray;
            this.task = task;
            this.completeCallback = completeCallback;
        }
        
        private async Task<bool> Exexute()
        {
            CurrentProgress = 0;

            //dataArray의 인덱스당 한 Step
            for (int i = 0; i < dataArray.Length && !isClosed; i++)
            {
                Label = task.GetName(dataArray[i]);
                
                await Task.Run(() =>
                {
                    task.Execute(dataArray[i]);
                });

                int value = (int)(((i + 1f) / (float)dataArray?.Length) * 100.0f);
                CurrentProgress = value;
            }
            return EndProcess();
        }

        public bool EndProcess()
        {
            //중간에 창을 닫았으면 또 닫을 필요없음
            //중간에 닫혔으므로 CompleteCallback 무시
            if (isClosed)
                return false;

            CloseWindow();
            completeCallback?.Invoke(task);

            return true;
        }

        public override void Closing(object sender, CancelEventArgs e)
        {
            isClosed = true;
        }
    }

    public abstract class BaseTask
    {
        public abstract string GetName(object data);
        public abstract void Execute(object data);
    }

    //DB 로드
    public class LoadDatabase : BaseTask
    {
        private DataBaseManager dataBaseManager;

        public LoadDatabase(DataBaseManager dataBaseManager)
        {
            this.dataBaseManager = dataBaseManager;
        }

        public override string GetName(object data)
        {
            if (data == null) return "-null data-";
            else return (data as IDataBase).GetDataName();
        }

        public override void Execute(object data)
        {
            if (data == null) return;

            IDataBase dataBase = data as IDataBase;
            dataBaseManager.LoadDataBaseFile(dataBase);
        }
    }

    //DB 리드
    public class ReadDataBase : BaseTask
    {
        public override string GetName(object data)
        {
            if (data == null) return "-null data-";
            else return (data as IDataBase).GetDataName();
        }

        public override void Execute(object data)
        {
            if (data == null) return;

            IDataBase dataBase = data as IDataBase;
            dataBase.ReadData();
        }
    }

    public enum LoadDataTableType
    {
        Columns, Rows, All
    }

    //보여주기 위한 DataGrid를 생성하는 작업
    public class LoadDataTable : BaseTask
    {        
        public LoadDataTableType loadType;
        public DataTable dt;

        private bool isAllColumn;
        private bool isCreatedColumns = false;

        public LoadDataTable(LoadDataTableType loadType, DataTable dt, bool isAllColumn) : base()
        {
            this.loadType = loadType;
            this.dt = dt;
            this.isAllColumn = isAllColumn;
        }

        public override string GetName(object data)
        {
            if (data == null) return "-null data-";
            else return (data as ICustomData).NAME;
        }

        public override void Execute(object data)
        {
            if (data == null) return;

            IViewData viewData = data as IViewData;

            switch (loadType)
            {
                case LoadDataTableType.Columns:
                    CreateDTColumns(viewData);
                    break;
                case LoadDataTableType.Rows:
                    CreateDTRows(viewData);
                    break;
                case LoadDataTableType.All:
                    CreateDTColumns(viewData);
                    CreateDTRows(viewData);
                    break;
            }
        }

        private void CreateDTRows(IViewData viewData)
        {
            if (viewData == null) return;

            var columns = isAllColumn ? viewData.GetAllColumns() : viewData.GetColumns();
            var fields = viewData.GetType().GetFields();

            List<object> list = new List<object>();
            foreach (var name in columns)
            {
                var findData = Array.Find(fields, v => v.Name == name.Item1);

                object addData = "잘못된 데이터";
                if (findData != null)
                    addData = findData.GetValue(viewData);

                list.Add(addData);
            }
            dt.Rows.Add(list.ToArray());
        }

        private void CreateDTColumns(IViewData viewData)
        {
            if (viewData == null)
                return;

            if (isCreatedColumns)
                return;

            var columns = isAllColumn ? viewData.GetAllColumns() : viewData.GetColumns();
            foreach (var column in columns)
            {
                dt.Columns.Add(column.Item1, column.Item2);
            }
            isCreatedColumns = true;
        }
    }

    //DB 합성...
    public class CombineDataBase : BaseTask
    {
        public override string GetName(object data)
        {
            if (data == null) return "-null data-";
            else return (data as IDataBase).GetDataName();
        }

        public override void Execute(object data)
        {
            if (data == null) return;

            IDataBase dataBase = data as IDataBase;
            dataBase.CombinedData();
        }
    }

    /// <summary>
    /// 프로토콜 [요청 -> 응답 -> 요청 -> 응답...]이 연속적으로 필요할 때
    /// </summary>
    public class RequestWait : BaseTask
    {
        public bool msgNotice = true;
        public bool isSendable = true;
        
        public BaseViewModel owner = null;

        public RequestWait(BaseViewModel owner)
        {
            msgNotice = true;
            isSendable = true;            

            this.owner = owner as BaseViewModel;
        }

        public override string GetName(object data)
        {
            if (data == null) return "-null data-";

            var requestData = data as RequestData;
            return requestData.protocol.context;
        }

        public override void Execute(object data)
        {
            if (data == null || !isSendable) return;

            var requestData = data as RequestData;

            var protocol = requestData.protocol;
            var sfsObject = requestData.sfsObject;
            var callback = requestData.callback;
            
            bool isResponse = false;
           
            Action<bool, SFSObject> newCallback = (isSuccess, resObj) =>
            {
                callback?.Invoke(isSuccess, resObj);

                isResponse = true;
                msgNotice = false;
            };

            var newReqData = new RequestData(protocol, sfsObject, newCallback);
            isSendable = NetworkManager.Instance.SendProtocol(owner, newReqData, msgNotice);

            //isSendable = false 이면 바로 종료
            //isResponse = true 가 될때까지 대기
            while (isSendable && !isResponse) { }
        }
    }
}
