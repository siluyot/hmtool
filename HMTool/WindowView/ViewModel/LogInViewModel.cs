﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.Network;
using HMTool.Network.Login;
using HMTool.Network.Servers;
using HMTool.Network.SFS;
using HMTool.WindowView.View;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using Sfs2X.Util;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Windows.Input;
using System.Linq;

namespace HMTool.WindowView.ViewModel
{
    public class LogInViewModel : WindowViewModel
    {
        //기본 버전
        private string version = "50";
        public string Version
        {
            get { return version; }
            set
            {
                version = value;
                OnPropertyChanged("Version");
            }
        }

        private string id = string.Empty;
        public string ID
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged("ID");
            }
        }

        public bool SkipDB { get; set; }

        private SecureString securePassword;

        //서버 리스트 콤보박스
        public ObservableCollection<ServerInfo> ServerList { get; set; }
        //현재 서버 리스트에서 선택된 서버
        public ServerInfo CurrentServer { get; set; }

        //비밀번호 변경 이벤트
        private ICommand passwordChangedCommand;
        public ICommand PasswordChangedCommand
        {
            get
            {
                return (passwordChangedCommand) ?? (passwordChangedCommand = new Command(OnPasswordChanged));
            }
        }

        //연결 버튼
        private ICommand connectCommand;
        public ICommand ConnectCommand
        {
            get
            {
                return (connectCommand) ?? (connectCommand = new Command(TryConnect));
            }
        }

        public LogInViewModel()
        {                       
            ServerList = new ObservableCollection<ServerInfo>(ServerInfo.List);

            if (ServerList.Count > 0)
                CurrentServer = ServerList.FirstOrDefault();
        }

        public override void KeyDown(object sender, KeyEventArgs e)
        {
            //엔터 입력시 연결 시도
            if ((Keyboard.GetKeyStates(Key.Enter) & KeyStates.Down) > 0)
            {
                TryConnect(null);
            }
        }

        private void OnPasswordChanged(object parameter)
        {
            var passwordBox = parameter as System.Windows.Controls.PasswordBox;
            securePassword = passwordBox.SecurePassword;
        }

        private string ConvertPassword(SecureString securePassword)
        {
            if (securePassword == null) return string.Empty;

            var ptr = Marshal.SecureStringToBSTR(securePassword);
            try
            {
                return Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                Marshal.ZeroFreeBSTR(ptr);
            }
        }

        private void TryConnect(object parameter)
        {
            //오프라인모드
            SetDataBase(null, CurrentServer, new Dictionary<DataBaseType, DBVersionData>());
            return;

            //네트워크가 존재한다면 로그인중 이거나 이미 연결됐음
            if (NetworkManager.Instance.GetNetwork(this) != null)
                return;

            string id = this.ID;
            string pw = ConvertPassword(securePassword);
            string inputVersion = this.Version;
            int requestVersion = 0;

            string msg = string.Empty;            
            if (!int.TryParse(inputVersion, out requestVersion))
                msg += "버전 확인이 필요합니다.\n";

            if (id == string.Empty || pw == string.Empty)
                msg += "ID/PW 확인이 필요합니다.\n";

            if (msg != string.Empty)
            {
                CustomMessageBox.Show(msg);
                return;
            }

            ConnectServer(CurrentServer, new GMLoginInfo(id, pw, requestVersion));
        }

        private void ConnectServer(ServerInfo serverInfo, LoginInfo loginInfo)
        {
            GmSFS gmSFS = new GmSFS(serverInfo, loginInfo);
            NetworkManager.Instance.TryConnect(this, gmSFS, LoginSuccess, LoginError, ConnectError);
        }
        
        private void LoginSuccess()
        {
            //SmartFox 로그인 성공 -> 지엠정보 요청

            var data = new RequestData(GMProtocol.GMLogIn, null, GmLogInCallback);
            NetworkManager.Instance.SendProtocol(this, data);
        }

        private void LoginError(string msg)
        {
            //SmartFox 로그인 실패 처리
            CustomMessageBox.Show("Login Error\n" + msg);
            NetworkManager.Instance.DeleteNetwork(this);

            //버전이 안맞을때
            if (msg.Contains("Client API version is obsolete"))
            {
                //msg = "Client API version is obsolete: 47; required version: 48"            
                //47 = 입력된 버전 / 48 = 요구되는 버전
                var reqStr = msg.Split(' ');
                foreach (var str in reqStr)
                {
                    int version;
                    if (int.TryParse(str, out version))
                    {
                        Version = str;
                        TryConnect(null);
                        break;
                    }
                }                
            }           
        }

        private void ConnectError(string msg)
        {
            //SmartFox 연결 실패 처리
            CustomMessageBox.Show("Connect Error\n" + msg);
        }

        private void GmLogInCallback(bool isSuccess, SFSObject resObj)
        {
            //지엠서버 로그인 성공
            if (isSuccess)
            {                
                var network = NetworkManager.Instance.GetNetwork(this);
                var curSFS2x = network.CustomSfs2X.Sfs2x;
                var curServerInfo = network.BaseSFS.ServerInfo;

                LAVersion laVersion = null;
                var versionDic = new Dictionary<DataBaseType, DBVersionData>();

                if (!SkipDB)
                {
                    short dbVersion = resObj.GetShort("1");

                    ByteArray keyBytes = resObj.GetByteArray("2");
                    ByteArray ivBytes = resObj.GetByteArray("3");
                    ByteArray secretBytes = resObj.GetByteArray("4");
                    ISFSArray versionArray = resObj.GetSFSArray("5");

                    laVersion = new LAVersion
                    {
                        data_version = dbVersion,
                        aes_key = Utills.GetDecrypt(keyBytes, curSFS2x),
                        aes_iv = Utills.GetDecrypt(ivBytes, curSFS2x),
                        aes_secret_key = Utills.GetDecrypt(secretBytes, curSFS2x)
                    };

                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < versionArray.Size(); i++)
                    {
                        IReadData data = new DBVersionData();
                        DBVersionData newVersion = data.GetReadData<DBVersionData>(versionArray.GetSFSObject(i), sb);
                        versionDic.Add(newVersion.Data_Type, newVersion);
                    }

                    if (sb.ToString() != string.Empty)
                        CustomMessageBox.Show(sb.ToString());
                }
                
                SetDataBase(laVersion, curServerInfo, versionDic);
            }
        }

        private async void SetDataBase(LAVersion laVersion, ServerInfo serverInfo, Dictionary<DataBaseType, DBVersionData> versionDic)
        {
            DataBaseManager.Instance.Init(laVersion, serverInfo, versionDic);
            bool isComplete = await DataBaseManager.Instance.Process(this);

            if (isComplete)
            {
                //디비 세팅 완료
                string msg = DataBaseManager.Instance.Msg;
                if (msg != string.Empty)
                {
                    CustomMessageBox.Show(msg);
                }
                OpenToolBox();
            }
            else LoginError("사용자의 DB로드 캔슬");
        }

        private void OpenToolBox()
        {
            var curNetwork = NetworkManager.Instance.GetNetwork(this);
            WindowManager.Instance.ShowWindow<ToolBox>(new ToolBoxViewModel(curNetwork));

            HideWindow();
        }

        public override void Closing(object sender, CancelEventArgs e)
        {
            var findViewModel = WindowManager.Instance.FindViewModel(typeof(DataBox));
            findViewModel?.CloseWindow();

            base.Closing(sender, e);
        }
    }
}
