﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.View;
using HMTool.WindowView.View.UC;
using HMTool.WindowView.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel
{
    public class DataBaseViewModel : UserControlViewModel
    {
        public class FilterBox
        {
            public string Name { get; set; }
            public string Text { get; set; }
        }

        public ObservableCollection<DataBaseType> DBTypeList { get; set; }
        public ObservableCollection<FilterBox> FilterList { get; set; }

        public bool IsShowAllColumns { get; set; }
        public DataBaseType SelectedDBType { get; set; }

        private ICommand openDBCommand;
        public ICommand OpenDBCommand
        {
            get
            {
                return (openDBCommand) ?? (openDBCommand = new Command(OpenDataBase));
            }
        }

        private ICommand filterCommand;
        public ICommand FilterCommand
        {
            get
            {
                return (filterCommand) ?? (filterCommand = new Command(ApplyDBFilter));
            }
        }
        
        private ICommand selectCommand;
        public ICommand SelectCommand
        {
            get
            {
                return (selectCommand) ?? (selectCommand = new Command(SelectData));
            }
        }

        private DataTable dataBaseTable = new DataTable();
        public DataTable DataBaseTable
        {
            get { return dataBaseTable; }
            set
            {
                dataBaseTable = value;
                OnPropertyChanged("DataBaseTable");
            }
        }

        private IDataBase curDataBase = null;
        private Tuple<string, Type>[] curColumns = null;

        public DataBaseViewModel(object parentObject) : base(parentObject)
        {
            FilterList = new ObservableCollection<FilterBox>();
            SetDBTypeList();
        }

        public override void KeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.GetKeyStates(Key.Enter) & KeyStates.Down) > 0)
            {
                FilterCommand.Execute(null);
            }                
        }

        private void SetDBTypeList()
        {
            DBTypeList = new ObservableCollection<DataBaseType>();

            var databaseKeys = DataBaseManager.Instance.GetAddedDataBaseTypes();
            foreach (var key in databaseKeys)
            {
                DBTypeList.Add(key);
            }
        }

        private void OpenDataBase(object parameter)
        {
            curDataBase = DataBaseManager.Instance.GetDataBase(SelectedDBType);

            var dataList = curDataBase.GetDataList();

            IViewData curViewData = curDataBase.CreateNewData() as IViewData;
            curColumns = IsShowAllColumns ? curViewData.GetAllColumns() : curViewData.GetColumns();

            SetDataBaseTable(dataList, curColumns);
        }

        private void SetDataBaseTable(List<ICustomData> dataList, Tuple<string, Type>[] columns)
        {
            DataTable newTable = new DataTable();

            FilterList.Clear();
            foreach (var column in columns)
            {
                newTable.Columns.Add(column.Item1, column.Item2);
                FilterList.Add(new FilterBox() { Name = column.Item1, Text = string.Empty });
            }

            LoadingViewModel.Show(ParentObject as WindowViewModel, dataList.ToArray(), new LoadDataTable(LoadDataTableType.Rows, newTable, IsShowAllColumns), (task) =>
            {
                //데이터테이블 생성 끝
                DataBaseTable = (task as LoadDataTable).dt;                
            });
        }

        private void ApplyDBFilter(object parameter)
        {            
            string rowFilterFormat = "Convert({0}, System.String) LIKE '%{1}%'";
            
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < FilterList.Count; i++)
            {
                string value = FilterList[i].Text;
                sb.Append(string.Format(rowFilterFormat, curColumns[i].Item1, value));

                if (i < FilterList.Count - 1) sb.Append(" AND ");
            }
            
            DataBaseTable.DefaultView.RowFilter = sb.ToString();
        }

        private void SelectData(object parameter)
        {
            List<ICustomData> addLIst = new List<ICustomData>();

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var newData = curDataBase?.CreateNewData();
            
            IViewData viewData = newData as IViewData;            
            foreach (var rowView in dataRowViewes)
            {
                ICustomData data = viewData.DataRowToData(curDataBase.GetDataList(), rowView.Row);
                addLIst.Add(data);
            }

            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            toolBoxVM.OpenDataBox().AddData(addLIst.ToArray());
        }

        public override UserControl GetNewView()
        {
            return new DataBaseViewer();
        }

        public override string GetTabName => "DataBase";

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
