﻿using HMTool.WindowView.View.UC;
using HMTool.WindowView.ViewModel.Base;
using System.Windows.Controls;

namespace HMTool.WindowView.ViewModel.Mail
{
    public class MailBoxViewModel : UserControlViewModel, IStorageViewModel
    {
        public IStorage Storage { get; set; }
        public IStorage StandByStorage { get; set; }

        public MailBoxViewModel(object parentObject) : base(parentObject)
        {
            Storage = new MailStorage(parentObject);
            StandByStorage = new MailStandByStorage(parentObject);
        }

        public override string GetTabName => "Mail";

        public override UserControl GetNewView()
        {
            return new Inventory();
        }
    }
}
