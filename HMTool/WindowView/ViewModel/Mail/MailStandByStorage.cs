﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.Latale;
using HMTool.Network;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel.Mail
{
    public class MailStandByStorage : Storage, IStorageAddGiftData
    {
        private List<MailData> dataList = new List<MailData>();

        public ICommand AddGiftDataCmd { get; set; }
        public ICommand ChangeCountCmd { get; set; }

        public int InputCount { get; set; }
        public GiftType CurrentGiftType { get; set; }
        public ObservableCollection<GiftType> GiftTypeList { get; set; }

        public FindUserData CurrnetUserData
        {
            get
            {
                ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
                return toolBoxViewModel?.FindViewModel<UserInfoViewModel>()?.FindUserData;
            }
        }

        public MailStandByStorage(object parentObject) : base(parentObject)
        {
            AddGiftDataCmd = new Command(AddDataNotDB);
            ChangeCountCmd = new Command(ChangeCount);

            GiftTypeList = new ObservableCollection<GiftType>
            {
                GiftType.HP_POTION,
                GiftType.DIA,
                GiftType.MILEAGE,
                GiftType.GOLD,
                GiftType.BUDDY_POINT,
                GiftType.SKILL_ENCHANTPOINT,
                GiftType.BLUE_DIA,
                GiftType.GUILD_COIN,
                GiftType.GUILD_POINT,
                //GiftType.EXP
            };

            InputCount = 1;
            CurrentGiftType = GiftTypeList.FirstOrDefault();
        }

        private void ChangeCount(object parameter)
        {
            if (parameter == null) return;

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            foreach (var rowView in dataRowViewes)
            {
                var customDataList = dataList.Cast<ICustomData>().ToList();
                var item = new MailData().DataRowToData(customDataList, rowView.Row) as MailData;

                int changeCount = InputCount;
                if (item is IMailData)
                {
                    if (changeCount > item.MaxCount)
                        changeCount = item.MaxCount;

                    if (changeCount <= 0)
                        changeCount = 1;
                }
                else changeCount = 1;

                rowView.Row.SetField("count", changeCount);
                item.count = changeCount;
            }
        }

        private async void AddDataNotDB(object parameter)
        {
            int count = InputCount;
            if (count <= 0) count = 1;

            dataList.Add(new MailData(MailTab.None, 0, CurrentGiftType.Value, 1, CurrentGiftType, count));
            
            MainDataTable = await SetItemTable(dataList.ToArray());
        }
        
        private async Task<DataTable> SetItemTable(ICustomData[] dataArray)
        {
            DataTable newTable = new DataTable();

            var columns = new MailData().GetColumns();
            foreach (var column in columns)
            {
                DataColumn newColumn = new DataColumn(column.Item1, column.Item2);
                newTable.Columns.Add(newColumn);
            }

            var task = new LoadDataTable(LoadDataTableType.Rows, newTable, false);
            await LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, task);

            return (task as LoadDataTable).dt;
        }

        public override void DoubleClick(object parameter)
        {
            
        }

        public override void ReqStorage(object parameter)
        {
            var network = NetworkManager.Instance.GetNetwork(ParentObject);
            if (network == null)
            {
                CustomMessageBox.Show("연결된 네트워크를 찾을 수 없습니다.");
                return;
            }

            if (CurrnetUserData == null)
            {
                CustomMessageBox.Show("보낼 대상을 찾을 수 없습니다.");
                return;
            }

            List<RequestData> requestList = new List<RequestData>();

            foreach (var mail in dataList)
            {
                //1시간 뒤
                double ticks = Utills.ConvertToUnixTimestamp(DateTime.UtcNow.AddHours(1));

                string who = "[GM]";
                int contentsId = 0;
                string content = "Mail";
                byte flag = 0; //거래가능 플래그인데 작동안함... 다시봐야 할듯
                int times = 1; //반복할 횟수

                SFSObject sfsObj = SFSObject.NewInstance();
                if (CurrnetUserData == null)
                    sfsObj.PutNull("1");
                else
                    sfsObj.PutInt("1", CurrnetUserData.uid);

                sfsObj.PutByte("2", mail.giftType.Key);
                sfsObj.PutInt("3", mail.id);
                sfsObj.PutInt("4", mail.count);
                sfsObj.PutInt("5", times);

                sfsObj.PutUtfString("6", who);
                sfsObj.PutInt("7", contentsId);
                sfsObj.PutUtfString("8", content);
                sfsObj.PutLong("9", (long)ticks);
                sfsObj.PutByte("10", flag);

                requestList.Add(new RequestData(GMProtocol.SendMail, sfsObj));
            }

            if (requestList.Count <= 0)
            {
                CustomMessageBox.Show("메일발송이 가능한 데이터가 없습니다.");
                return;
            }

            LoadingViewModel.Show(ParentObject as WindowViewModel, requestList.ToArray(), new RequestWait(ParentObject as WindowViewModel), (task) =>
            {
                CustomMessageBox.Show("완료");
            });
        }

        public override void DeleteStorageItemes(object parameter)
        {
            System.Collections.IList items = (System.Collections.IList)parameter;
            var collection = items.Cast<DataRowView>().ToArray();

            foreach (var data in collection)
            {
                int id = data.Row.Field<int>("id");
                string name = data.Row.Field<string>("name");
                int count = data.Row.Field<int>("count");

                var findData = dataList.Find(v => v.id == id && v.name == name && v.count == count);

                dataList.Remove(findData);
                MainDataTable.Rows.Remove(data.Row);
            }
        }

        public async override void DataBoxAction(object parameter)
        {
            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            var dataArray = toolBoxVM.OpenDataBox().GetDataArray();

            foreach (var data in dataArray)
            {
                if (data.originalData is IMailData)
                {
                    var mail = data.originalData as IMailData;
                    MailData newData = new MailData(MailTab.None, data.id, data.name, data.grade, mail.GiftType, data.count, mail.MaxCount);
                    dataList.Add(newData);
                }                
            }
            MainDataTable = await SetItemTable(dataList.ToArray());
        }

        public override string Title => "발송대기함";
    }
}
