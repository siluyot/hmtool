﻿using HMTool.Data;
using HMTool.Data.Base;
using HMTool.Latale;
using HMTool.Network;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace HMTool.WindowView.ViewModel.Mail
{
    public class MailStorage : Storage
    {
        public List<MailData> dataList = new List<MailData>();

        private FindUserData lastUserData = null;
        public FindUserData CurrnetUserData
        {
            get
            {
                ToolBoxViewModel toolBoxViewModel = ParentObject as ToolBoxViewModel;
                return toolBoxViewModel.FindViewModel<UserInfoViewModel>()?.FindUserData;
            }
        }

        public MailStorage(object parentObject) : base(parentObject) { }

        public override void DoubleClick(object parameter)
        {

        }

        private async void ResMyStorageCmd(bool isSuccess, SFSObject resObj)
        {
            if (isSuccess)
            {
                dataList.Clear();

                //아이템
                if (resObj.ContainsKey("1"))
                {
                    ISFSArray array = resObj.GetSFSArray("1");
                    for (int i = array.Count - 1; i >= 0; i--)
                    {
                        GiftData giftData = new GiftData(array.GetSFSObject(i));
                        dataList.Add(new MailData(giftData));
                    }
                }
                //버디
                if (resObj.ContainsKey("2"))
                {
                    ISFSArray array = resObj.GetSFSArray("2");
                    for (int i = array.Count - 1; i >= 0; i--)
                    {
                        BuddyPointGiftData buddyPointGiftData = new BuddyPointGiftData(array.GetSFSObject(i));
                        dataList.Add(new MailData(buddyPointGiftData));
                    }
                }
                //티켓
                if (resObj.ContainsKey("3"))
                {
                    ISFSArray array = resObj.GetSFSArray("3");
                    for (int i = array.Count - 1; i >= 0; i--)
                    {
                        TicketGiftData ticketGiftData = new TicketGiftData(array.GetSFSObject(i));
                        dataList.Add(new MailData(ticketGiftData));
                    }
                }
            }

            if (dataList.Count <= 0)
                dataList.Add(new MailData(MailTab.None, 0, "비어있음", 0, GiftType.NONE, 0, 0));

            MainDataTable = await SetItemTable(dataList.ToArray());
        }

        private async Task<DataTable> SetItemTable(MailData[] dataArray)
        {
            DataTable newTable = new DataTable();

            var columns = new MailData().GetColumns();
            foreach (var column in columns)
            {
                DataColumn newColumn = new DataColumn(column.Item1, column.Item2);
                newTable.Columns.Add(newColumn);
            }

            var task = new LoadDataTable(LoadDataTableType.Rows, newTable, false);
            await LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, task);

            return (task as LoadDataTable).dt;
        }

        public override void ReqStorage(object parameter)
        {
            lastUserData = CurrnetUserData;

            SFSObject sfsObj = SFSObject.NewInstance();
            if (CurrnetUserData == null) sfsObj.PutNull("1");
            else sfsObj.PutInt("1", CurrnetUserData.uid);

            var data = new RequestData(GMProtocol.RequestMailList, sfsObj, ResMyStorageCmd);
            NetworkManager.Instance.SendProtocol(ParentObject, data);
        }

        public override void DeleteStorageItemes(object parameter)
        {
            if (CurrnetUserData == null)
                return;

            if (lastUserData != CurrnetUserData)
            {
                CustomMessageBox.Show("해당 유저의 메일 리스트 조회가 필요합니다.");
                return;
            }

            System.Collections.IList items = (System.Collections.IList)parameter;
            var collection = items.Cast<DataRowView>();

            List<RequestData> requestList = new List<RequestData>();
            foreach (var data in collection)
            {
                long seq = (long)data[0];
                MailTab mailTab = (MailTab)Enum.Parse(typeof(MailTab), (string)data[1]);
                if (mailTab == MailTab.None) continue;

                SFSObject sfsObj = SFSObject.NewInstance();
                sfsObj.PutInt("1", CurrnetUserData.uid);
                sfsObj.PutLong("2", seq);
                sfsObj.PutByte("3", (byte)mailTab);

                requestList.Add(new RequestData(GMProtocol.DeleteMail, sfsObj, null));
            }

            if (requestList.Count == 0)
            {
                CustomMessageBox.Show("삭제 가능한 메일이 없습니다.");
                return;
            }

            LoadingViewModel.Show(ParentObject as WindowViewModel, requestList.ToArray(), new RequestWait(ParentObject as ToolBoxViewModel), (task) =>
            {
                ReqStorage(null);
                CustomMessageBox.Show("완료");
            });
        }

        public override void DataBoxAction(object parameter)
        {
            var selectedList = new List<MailData>();
            var customDataList = dataList.Cast<ICustomData>().ToList();

            System.Collections.IList list = (System.Collections.IList)parameter;
            IEnumerable<DataRowView> dataRowViewes = list.Cast<DataRowView>();

            var rowViewArray = dataRowViewes.ToArray();
            foreach (var rowView in rowViewArray)
            {
                var findData = new MailData().DataRowToData(customDataList, rowView.Row) as MailData;
                selectedList.Add(findData);
            }

            ToolBoxViewModel toolBoxVM = ParentObject as ToolBoxViewModel;
            toolBoxVM.OpenDataBox().AddData(selectedList.ToArray());
        }

        public override string Title => "메일함";
    }
}
