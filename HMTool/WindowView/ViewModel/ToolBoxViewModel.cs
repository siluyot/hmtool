﻿using HMTool.Network;
using HMTool.WindowView.View;
using HMTool.WindowView.ViewModel.Base;
using HMTool.WindowView.ViewModel.Inven.Item;
using HMTool.WindowView.ViewModel.Inven.Pet;
using HMTool.WindowView.ViewModel.Mail;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel
{
    public class ToolBoxViewModel : WindowViewModel
    {        
        public ObservableCollection<TabItem> TabItems { get; set; }
        public TabItem SelectedTab { get; set; }

        private UserControlViewModel[] vmArray;
        private DataBoxViewModel dataBoxVM;

        public ToolBoxViewModel(NetworkInstance networkInstance)
        {
            TabItems = new ObservableCollection<TabItem>();

            vmArray = new UserControlViewModel[]
            {
                new UserInfoViewModel(this),
                new DataBaseViewModel(this),
                new ItemInvenViewModel(this),
                new PetInvenViewModel(this),
                new MailBoxViewModel(this),               
            };

            networkInstance?.ChangeOwner(this, ConnectError);
        }

        public override void OnLoaded(object sender, RoutedEventArgs e)
        {
            SetTabItems();            
        }

        public override void KeyDown(object sender, KeyEventArgs e)
        {
            var findTab = Array.Find(vmArray, v => v.GetTabName == SelectedTab.Header.ToString());
            findTab?.KeyDown(sender, e);
        }

        public DataBoxViewModel OpenDataBox()
        {
            dataBoxVM = WindowManager.Instance.ShowWindow<DataBox>(new DataBoxViewModel(this)) as DataBoxViewModel;
            return dataBoxVM;
        }

        private void SetTabItems()
        {
            foreach (var vm in vmArray)
            {
                var view = vm.GetNewView();
                vm.SetDataContext(view);

                var newTab = new TabItem
                {
                    Header = vm.GetTabName,
                    Content = view,
                };
                TabItems.Add(newTab);
            }

            if (TabItems.Count > 0)
                SelectedTab = TabItems.First();
        }

        public T FindViewModel<T>() where T : UserControlViewModel
        {
            return Array.Find(vmArray, v => v.GetType() == typeof(T)) as T;
        }

        public override void Closing(object sender, CancelEventArgs e)
        {
            base.Closing(sender, e);

            foreach (var tab in vmArray)
            {
                tab.Dispose();
            }

            //기존에 사용하던 연결 삭제
            NetworkManager.Instance.DeleteNetwork(this);

            //툴박스가 닫히면 숨겼던 로그인창 오픈
            WindowManager.Instance.ShowWindow<LogIn>(new LogInViewModel());
        }

        private void ConnectError(string msg)
        {
            //SmartFox 연결 실패 처리
            CustomMessageBox.Show(this, "ConnectionLost\n" + msg);

            CloseWindow();
        }
    }
}
