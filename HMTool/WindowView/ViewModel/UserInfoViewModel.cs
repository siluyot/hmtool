﻿using HMTool.Data;
using HMTool.DB;
using HMTool.Latale;
using HMTool.Network;
using HMTool.WindowView.View.UC;
using HMTool.WindowView.ViewModel.Base;
using Sfs2X.Entities.Data;
using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;

namespace HMTool.WindowView.ViewModel
{
    public class UserInfoViewModel : UserControlViewModel
    {
        private string name = string.Empty;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private string accountID;
        public string AccountID
        {
            get { return accountID; }
            set
            {
                accountID = value;
                OnPropertyChanged("AccountID");
            }
        }

        private string emailPW;
        public string EmailPW
        {
            get { return emailPW; }
            set
            {
                emailPW = value;
                OnPropertyChanged("EmailPW");
            }
        }

        private string accountKey;
        public string AccountKey
        {
            get { return accountKey; }
            set
            {
                accountKey = value;
                OnPropertyChanged("AccountKey");
            }
        }

        private string accountPass;
        public string AccountPass
        {
            get { return accountPass; }
            set
            {
                accountPass = value;
                OnPropertyChanged("AccountPass");
            }
        }

        private int gold;
        public int Gold
        {
            get { return gold; }
            set
            {
                gold = value;
                OnPropertyChanged("Gold");
            }
        }

        public bool IsLevelType_99 { get; set; }

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                return (searchCommand) ?? (searchCommand = new Command(SearchUser));
            }
        }

        private ICommand changeLevelCommand;
        public ICommand ChangeLevelCommand
        {
            get
            {
                return (changeLevelCommand) ?? (changeLevelCommand = new Command(ChangeLevel));
            }
        }

        private ICommand changeJobCommand;
        public ICommand ChangeJobCommand
        {
            get
            {
                return (changeJobCommand) ?? (changeJobCommand = new Command(ChangeJob));
            }
        }

        private ICommand changeTutorialCommand;
        public ICommand ChangeTutorialCommand
        {
            get
            {
                return (changeTutorialCommand) ?? (changeTutorialCommand = new Command(ChangeTutorial));
            }
        }

        public ObservableCollection<short> LevelList { get; set; }
        public ObservableCollection<object> JobList { get; set; }
        public ObservableCollection<object> TutorialList { get; set; }

        private short selectedLevel;
        public short SelectedLevel
        {
            get { return selectedLevel; }
            set
            {
                selectedLevel = value;
                OnPropertyChanged("SelectedLevel");
            }
        }

        private object selectedJob;
        public object SelectedJob
        {
            get { return selectedJob; }
            set
            {
                selectedJob = value;
                OnPropertyChanged("SelectedJob");
            }
        }

        private object selectedTutorial;
        public object SelectedTutorial
        {
            get { return selectedTutorial; }
            set
            {
                selectedTutorial = value;
                OnPropertyChanged("SelectedTutorial");
            }
        }

        public FindUserData FindUserData { get; private set; }
        public CharDetailInfo CharDetailInfo { get; private set; }

        public UserInfoViewModel(object parentObject) : base(parentObject)
        {
            //더미 데이터
            FindUserData = new FindUserData(SFSObject.NewInstance());
            CharDetailInfo = new CharDetailInfo(SFSObject.NewInstance());

            SetLevelList();
            SetJobList();
            SetTutorialList();
        }

        public override void KeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.GetKeyStates(Key.Enter) & KeyStates.Down) > 0)
            {
                SearchCommand.Execute(null);
            }
        }

        private void SetLevelList()
        {
            LevelList = new ObservableCollection<short>();

            var dataList = DataBaseManager.Instance.GetDataList(DataBaseType.CHAR_STATUS);
            if (dataList == null)
                return;

            foreach (var data in dataList)
            {
                short level = (data as CharStatusData).level;

                //직업마다 레벨이 겹치므로 한번 넣은 레벨은 제외
                if (!LevelList.Contains(level))
                {
                    LevelList.Add(level);
                }
            }
        }

        private void SetJobList()
        {
            JobList = new ObservableCollection<object>();

            var jobArray = Enum.GetValues(typeof(JobBaseType));
            foreach (var job in jobArray)
            {
                //캐릭터의 직업을 공용으로 바꿀수는 없으니 제외
                if ((JobBaseType)job == JobBaseType.ALL) continue;
                JobList.Add(job);
            }
        }

        private void SetTutorialList()
        {
            TutorialList = new ObservableCollection<object>();

            var tutorialArray = Enum.GetValues(typeof(TutorialStep));
            foreach (var tutorial in tutorialArray)
            {
                TutorialList.Add(tutorial);
            }
        }

        private void SearchUser(object parameter)
        {
            string searchName = Name;
            if (searchName == string.Empty) return;

            SFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutUtfString("2", searchName);
            sfsObj.PutInt("4", 0);

            var data = new RequestData(GMProtocol.SearchUser, sfsObj, SetUserData);
            NetworkManager.Instance.SendProtocol(ParentObject, data);
        }

        private void SetUserData(bool isSuccess, SFSObject resObj)
        {
            if (isSuccess)
            {
                FindUserData = new FindUserData(resObj);

                if (FindUserData.currentCharacter == null)
                {
                    CustomMessageBox.Show("해당 캐릭터 없음!");
                    return;
                }

                AccountID = FindUserData.Account_ID;
                EmailPW = FindUserData.Email_Password;
                AccountKey = FindUserData.Account_Key;
                AccountPass = FindUserData.Account_Pass;

                //서버에서 255가 오면 튜토리얼 끝난 상태
                TutorialStep tutorialStep = FindUserData.tutorialState == 255 ? 
                    TutorialStep.TutorialEnd : 
                    (TutorialStep)FindUserData.tutorialState;

                SelectedTutorial = tutorialStep;

                SFSObject sfsObj = SFSObject.NewInstance();
                sfsObj.PutInt("1", FindUserData.currentCharacter.uid);
                sfsObj.PutInt("2", FindUserData.currentCharacter.cid);

                var data = new RequestData(GMProtocol.SearchCharacter, sfsObj, SetCharacterData);
                NetworkManager.Instance.SendProtocol(ParentObject, data);
            }
        }

        private void SetCharacterData(bool isSuccess, SFSObject resObj)
        {
            if (isSuccess)
            {
                CharDetailInfo = new CharDetailInfo(resObj.GetSFSObject("1"));

                Gold = CharDetailInfo.gold;
                SelectedLevel = CharDetailInfo.level;
                SelectedJob = (JobBaseType)CharDetailInfo.job;                
            }
            CustomMessageBox.Show("완료!");
        }

        private void ChangeLevel(object parameter)
        {
            if (CharDetailInfo == null)
                return;

            //IsLevelType_99 = false     -> 50렙 0%
            //IsLevelType_99 = true      -> 50렙 99%
            //flag가 true면 99%의 경험치를 만들기 위해 다음레벨 경험치 값에서 -1을 해준다.
            int flag = Convert.ToInt16(IsLevelType_99);

            var dataList = DataBaseManager.Instance.GetDataList(DataBaseType.CHAR_STATUS);

            var findLevel = SelectedLevel + flag;
            var findData = dataList.Find(v => (v as CharStatusData).level == (short)findLevel);

            if (findData == null)
            {
                findLevel = SelectedLevel;
                findData = dataList.Find(v => (v as CharStatusData).level == (short)findLevel);
            }

            int orgExp = CharDetailInfo.exppoint;
            int changeExp = (findData as CharStatusData).exppoint - flag;

            UpdataUserData(UpdataUserDataType.EXP, orgExp, changeExp, (isSuccess, resObj) =>
            {
                if (isSuccess)
                    CharDetailInfo.exppoint = changeExp;
            });
        }

        private void ChangeJob(object parameter)
        {
            if (CharDetailInfo == null)
                return;

            var job = (JobBaseType)SelectedJob;

            byte orgJob = CharDetailInfo.job;            
            byte changeJob = (byte)job;

            UpdataUserData(UpdataUserDataType.JOB, orgJob, changeJob, (isSuccess, resObj) =>
            {
                if (isSuccess)
                {
                    CharDetailInfo.job = changeJob;
                    ResetSkill();
                }                    
            });
        }

        private void ChangeTutorial(object parameter)
        {
            if (FindUserData == null)
                return;

            int uid = FindUserData.uid;
            var step = (TutorialStep)SelectedTutorial;

            SFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutInt("1", uid);
            sfsObj.PutByte("2", (byte)step);

            var changeTutorialReqData = new RequestData(GMProtocol.TutorialEdit, sfsObj);

            RequestData[] dataArray =
            {
                GetUpdataUserBlockReqData(true),
                changeTutorialReqData,
                GetUpdataUserBlockReqData(false)
            };

            LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, new RequestWait(ParentObject as BaseViewModel), (task) =>
            {
                CustomMessageBox.Show("완료!");
            });
        }

        private void ResetSkill()
        {
            if (CharDetailInfo == null)
                return;

            SFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutInt("1", CharDetailInfo.uid);
            sfsObj.PutInt("2", CharDetailInfo.cid);
            sfsObj.PutByte("3", CharDetailInfo.job);

            var data = new RequestData(GMProtocol.ResetSkill, sfsObj);
            NetworkManager.Instance.SendProtocol(ParentObject, data);
        }

        private void UpdataUserData(UpdataUserDataType type, int orgValue, int changeValue, Action<bool, SFSObject> callback)
        {
            //변경되는 값이 아니면 무시
            if (orgValue == changeValue) return;

            var blockUserData = GetUpdataUserBlockReqData(true);
            var updataUserData = GetUpdataUserReqData(type, orgValue, changeValue, callback);
            var unblockUserData = GetUpdataUserBlockReqData(false);

            RequestData[] dataArray =
            {
                blockUserData,
                updataUserData,
                unblockUserData
            };

            LoadingViewModel.Show(ParentObject as WindowViewModel, dataArray, new RequestWait(ParentObject as BaseViewModel), (task) =>
            {
                CustomMessageBox.Show("완료!");
            });
        }

        private RequestData GetUpdataUserReqData(UpdataUserDataType type, int orgValue, int changeValue, Action<bool, SFSObject> callback)
        {
            SFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutByte("1", (byte)type);
            sfsObj.PutInt("2", CharDetailInfo.uid);
            sfsObj.PutInt("3", CharDetailInfo.cid);
            sfsObj.PutInt("4", orgValue);
            sfsObj.PutInt("5", changeValue);
            sfsObj.PutUtfString("6", AccountKey);

            return new RequestData(GMProtocol.UpdataUserData, sfsObj, callback);
        }

        public RequestData GetUpdataUserBlockReqData(bool isBlock)
        {
            byte blockType = isBlock ? (byte)1 : (byte)0;
            double ticks = Utills.ConvertToUnixTimestamp(DateTime.UtcNow.AddMilliseconds(isBlock ? 10 : 0));

            SFSObject sfsObj = SFSObject.NewInstance();
            sfsObj.PutInt("1", FindUserData.uid);
            sfsObj.PutLong("2", (long)ticks);
            sfsObj.PutUtfString("3", " ");
            sfsObj.PutUtfString("4", FindUserData.Account_Key);
            sfsObj.PutByte("5", blockType);   //1이면 블럭 0이면 블럭해제
            sfsObj.PutUtfString("6", FindUserData.searchName);

            return new RequestData(GMProtocol.BlockUser, sfsObj);
        }

        public override UserControl GetNewView()
        {
            return new UserInfo();
        }

        public override string GetTabName => "UserInfo";

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
