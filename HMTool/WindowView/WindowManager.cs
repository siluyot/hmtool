﻿using HMTool.WindowView.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace HMTool.WindowView
{
    public class WindowManager
    {        
        public static WindowManager Instance { get; private set; }
        public Dispatcher CurDispatcher { get { return Application.Current.Dispatcher; } }

        private List<Tuple<Window, WindowViewModel>> windowList = new List<Tuple<Window, WindowViewModel>>();

        public static void CreateInstance(Window window, WindowViewModel viewModel)
        {
            WindowManager newInstance = new WindowManager();

            var newObject = newInstance.CreateWindow(window, viewModel);
            newInstance.AddWindow(newObject);

            Instance = newInstance;
        }

        public WindowViewModel ShowWindow<T>(WindowViewModel newViewModel, Window ownerWindow = null) where T : Window, new()
        {
            var targetWindow = FindWindow(newViewModel);

            //이미 있는 창이면 새로 생성하지 않는다.
            CurDispatcher.Invoke(() => 
            {
                if (targetWindow == null)
                {
                    targetWindow = new T();
                    AddWindow(CreateWindow(targetWindow, newViewModel));
                }
                targetWindow.Owner = ownerWindow;
                
                targetWindow.Show();
                targetWindow.Focus();
            });

            return FindViewModel(targetWindow);
        }

        private Tuple<Window, WindowViewModel> CreateWindow(Window window, WindowViewModel viewModel)
        {
            //DataContext 연결 후 Tuple 생성
            viewModel.SetDataContext(window);            
            return new Tuple<Window, WindowViewModel>(window, viewModel);
        }
        
        private void AddWindow(Tuple<Window, WindowViewModel> newObj)
        {            
            //Window 이벤트 등록, 리스트 추가
            Window window = newObj.Item1 as Window;
            WindowViewModel viewModel = newObj.Item2 as WindowViewModel;

            AddWindowEvent(newObj, window, viewModel);

            windowList.Add(newObj);
        }

        private void AddWindowEvent(Tuple<Window, WindowViewModel> newObj, Window window, WindowViewModel viewModel)
        {
            //창이 로드된 후 호출
            window.Loaded += viewModel.OnLoaded;

            //키입력 이벤트
            window.KeyDown += viewModel.KeyDown;

            //창이 닫히려고 할때 호출
            window.Closing += viewModel.Closing;

            //창이 닫히고 호출
            window.Closed += (sender, e) =>
            {
                viewModel.Closed(sender, e);
                windowList.Remove(newObj);
            };

            //호출하면 창 닫음
            viewModel.CloseEvent += (sender, e) =>
            {                
                CurDispatcher.Invoke(() => window.Close());
            };

            //호출하면 창 숨김
            viewModel.Hide += () =>
            {
                CurDispatcher.Invoke(() => window.Hide());
            };
        }

        public Window FindWindow(WindowViewModel viewModel)
        {
            Type type = viewModel.GetType();
            var findObj = windowList.Find(v => v.Item2.GetType() == type);

            return findObj?.Item1;
        }

        public WindowViewModel FindViewModel(Window window)
        {
            Type type = window.GetType();
            var findObj = windowList.Find(v => v.Item1.GetType() == type);

            return findObj?.Item2;
        }

        public WindowViewModel FindViewModel(Type windowType)
        {
            var findObj = windowList.Find(v => v.Item1.GetType() == windowType);

            return findObj?.Item2;
        }

        /// <summary>
        /// 마지막으로 추가된 창을 가져옴
        /// </summary>
        public Window GetLastAddedWindow()
        {
            if (windowList.Count <= 0)
                return null;

            return windowList.Last().Item1;
        }

        /// <summary>
        /// 해당 View의 창 사이즈를 자동으로 조절하는 방식 설정
        /// </summary>
        /// <param name="viewModel"></param>
        public void SetWindowSizeMode(WindowViewModel viewModel, SizeToContent mode)
        {
            var window = FindWindow(viewModel);
            if (window == null)
                return;

            window.Dispatcher.Invoke(() => window.SizeToContent = mode);            
        }
    }
}
