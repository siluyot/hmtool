﻿using HMTool.Data.Base;
using HMTool.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HMTool.Latale
{
    //라테일 코드들
    public abstract class EnumBaseType<T> where T : EnumBaseType<T>
    {
        protected static List<T> enumValues = new List<T>();

        public readonly int Key;
        public readonly string Value;

        public EnumBaseType(int key, string value)
        {
            Key = key;
            Value = value;
            enumValues.Add((T)this);
        }

        protected static ReadOnlyCollection<T> GetBaseValues()
        {
            return enumValues.AsReadOnly();
        }

        protected static T GetBaseByKey(int key)
        {
            foreach (T t in enumValues)
            {
                if (t.Key == key) return t;
            }            
            return null;
        }

        public override string ToString()
        {
            return string.Concat(Key, ":", Value);
        }
    }
    public abstract class EnumBaseType<T, TKey, TValue>
        where T : EnumBaseType<T, TKey, TValue>
    {
        private static List<T> enumValues = new List<T>();

        public readonly TKey Key;
        public readonly TValue Value;

        public EnumBaseType(TKey key, TValue value)
        {
            Key = key;
            Value = value;
            enumValues.Add((T)this);
        }

        protected static ReadOnlyCollection<T> GetBaseValues()
        {
            return enumValues.AsReadOnly();
        }

        protected static T GetBaseByKey(TKey key)
        {
            foreach (T t in enumValues.Iter())
            {
                if (t.Key.Equals(key)) return t;
            }
            Console.WriteLine(string.Format("Not find key: {0}, Check the static field: {1}", key, typeof(T)));
            return null;
        }
        protected static bool ContainsKey(TKey key)
        {
            foreach (T t in enumValues.Iter())
            {
                if (t.Key.Equals(key)) return true;
            }
            return false;
        }

        public override string ToString()
        {
            return string.Concat(Key, ":", Value);
        }
    }

    public enum MapInfoType
    {
        NONE = -1,
        FIELD,
        SAFETY_ZONE,
        DUNGEON,
        GUILD,
        Single,
        GINDUN,
        HOUSE,
        PVP = 7,
        LOCAL = 8,
        WEDDING = 9,
    }

    public enum DropType
    {
        NONE = 0,
        ITEMS,
        ITEM_ID,
        PETS,
        PET_ID,
        HP_POTION,
        NO_DROP,
        FREE_CASH,
        MILEAGE,
        ELLI,
        BUDDY_POINT,
        SUMMON_PET,
        SUMMON_ITEM,
        FURNI_ITEM,
        HOUSE_SKIN,
        HOUSE_BG_SKIN,
        COSTUME,
        BUFF,
        HOUSE = 25,
        EXP,
        CASH = 28,
        GUILD_POINT,
        EG_TICKET,
        HORNOR,
        REAL_CASH_GOODS,
        REAL_CASH,
        SKILL_POINT,
        EVERYDAY_GOODS = 41,
        PAY_MAIN_QUEST_GOODS,
        PAY_LEVEL_GOODS,
        PAY_INVEN_EXPAND,
        USER_HOT_TIME,
        GUILD_COIN,
        BLUE_DIA,
    }

    public enum MakeTimeType
    {
        NORMAL, EVT_DATE, EVT_HOUR
    }

    public enum NPCEventType
    {
        NONE,
        ITEM_SHOP_BUY,
        ITEM_SHOP_SELL,
        STORAGE,
        RESTORE_HP,
        CRAFT_COSTUME,
        CRAFT_EQUIP,
        CRAFT_FURNI,
        TOWN_WARP,
        FURNI_SHOP_BUY,
        FURNI_SHOP_SELL,
        JOB_ADVANCE,
        HOUSING_RANKING,
        CRAFT_EVENT,
        CRAFT_FOOD,
        CRAFT_GUILD,
    }

    public enum Gift_Type
    {
        NOTHING = -2,
        MAIL = -1,
        NONE = 0,
        ITEMS = 1,
        ITEM = 2,
        PETS = 3,
        PET = 4,
        HP_POTION = 5,
        DIA = 7,
        MILEAGE = 8,
        GOLD = 9,
        BUDDY_POINT = 10,
        PET_TICKET = 11,
        ITEM_TICKET = 12,
        FURNI_ITEM = 13,
        HOUSE_SKIN = 14,
        HOUSE_BG_SKIN = 15,
        COSTUME = 16,
        BUFF = 17,
        USE_ITEM = 18,
        CHOICE_PET = 21,
        CHOICE_ITEM = 22,
        REAL_CASH = 28,
        GUILD_POINT = 29, // UI용 실제 서버랑 통신용으로 사용하지 않음
        SKILL_ENCHANTPOINT = 34,
        EVERYDAY_GOODS = 41,
        PAY_MAIN_QUEST_GOODS = 42,
        PAY_LEVEL_GOODS = 43,
        PAY_INVEN_EXPAND = 44,
        GUILD_COIN = 46,
        BLUE_DIA = 47,
        EXP = 99,
    }

    public enum JobBaseType
    {
        ALL = 0,
        /// <summary>
        /// 양손검, 창
        /// </summary>
        WARRIOR = 1,

        /// <summary>
        /// 한손검, 둔기, 방패
        /// </summary>
        KNIGHT,

        /// <summary>
        /// 지팡이, 단검, 오브
        /// </summary>
        WIZARD,

        /// <summary>
        /// 석궁, 활, 단검
        /// </summary>
        EXPLORER,

        /// <summary>
        /// 양손검, 창
        /// </summary>
        WARLORD,
        /// <summary>
        /// 양손검, 이도
        /// </summary>
        BLADER,

        /// <summary>
        /// 둔기, 한손검
        /// </summary>
        TEMPLEKNIGHT,
        /// <summary>
        /// 둔기, 너클
        /// </summary>
        GUARDIAN,

        /// <summary>
        /// 지팡이, 단검
        /// </summary>
        SORCEROR,
        /// <summary>
        /// 지팡이, 기타
        /// </summary>
        ARTIST,

        /// <summary>
        /// 석궁, 활
        /// </summary>
        TREASUREHUNTER,
        /// <summary>
        /// 석궁, 총
        /// </summary>
        GUNSLINGER,
    }

    public enum DataBaseType
    {
        BASIS = 0,
        DEFENCE_VALUE = 1,
        CHAR_STATUS = 2,
        ITEM = 3,
        OPTION_DATA = 4,
        ITEM_RAND_OPTION = 5,
        BUFF_DATA = 6,
        SKILL = 7,
        MONSTER = 8,
        PET_BOOK = 9,
        MONSTER_DROP = 10,
        MONSTER_DROP_LIST = 11,
        RANDOM_TABLE = 12,
        PET_EXP = 13,
        PET_GRADE = 14,
        WORLD_MAP_GROUP = 15,
        MAP_INFO = 16,
        NPC = 17,
        NPC_TALK = 18,
        QUEST = 19,
        GLOBAL_MOB = 20,
        POWER_VALUE = 21,
        SET_TABLE = 22,
        LOGIN_BONUS = 23,
        ACHIEVE = 24,
        MAIL_CONTENTS = 25,
        LANGUAGE = 26,
        BOX_DROP = 27,
        ITEM_SHOP = 28,
        CASH_SHOP = 29,
        JOB_DROP = 30,
        FIELD_RAID = 31,
        RAID_REWARD = 32,
        MONSTER_AI = 33,
        MONSTER_SKILL = 34,
        PORTAL_LIST = 35,
        SOUND_LIST = 36,
        JOB_BASE_LIST = 37,
        MONSTER_BOOK = 38,
        ACCOUNT_BORAD = 39,
        BINGGOPAN = 40,
        ITEM_PROB_TABLE = 41,
        DUNGEON = 42,
        MAKE = 43,
        COLLECT = 44,
        INFINITE_TOWER = 45,
        INFINITE_SPAWN = 46,
        GUILD_ATTENDANCE_REWARD = 47,
        GUILD_SKILL = 48,
        GUILD_STATUS = 49,
        GUILD_DONATE_REWARD = 50,
        GUILD_QUEST = 51,
        HOUSE_GRADE = 52,
        FURNI_ITEM = 53,
        HOUSE_SHOP = 54,
        HOUSE_OPTION = 55,
        LIKE_SEASON = 56,
        TITLE_NAME = 57,
        SKILL_EXTRA = 58,
        GUILD_SHOP = 59,
        CHARACTERISTIC = 60,
        FARM = 61,
        PROHIBITED_WORDS = 62,
        DEFAULT_ATTACK_ENCHANT = 63,
        CONNECT_REWARD = 64,
        REAL_CASH_GOODS = 65,
        SERVER_MAP_INFO = 66,
        GUILD_CROPS = 67,
        GUILD_CROPS_EXP = 68,
        HEALTH = 69,
        COSTUME_BOOK_REWARD = 70,
        PLAYER_ATTACK_VALUE = 71,

        MAPEVENT = 90,
        EVENTPORTAL = 91,
        MOBARRANGE = 92,
        MAP_TILE = 93,
        LaTaleClientDB = 100,

        //ITEM_OPTION_CATEGORY = 17,
        //ITEM_OPTION_GRANT = 18,
        //ITEM_OPTION_FIX = 19,
        //ITEM_UPGRADE = 20,
        //QUEST_DROP = 23,
        //QUEST_DROP_SIMPLE = 24,
        //MONSTER_STATUS = 25,
        //MONSTER_MANAGE = 28,
        //CHAR_ATTACK_INFO = 21,
        //QUEST_EXT = 23,
        //DUNGEON_SPWAN = 24,
        //VIP_GRADE = 29,
        //VIP_BENEFITS = 30,
        //ITEM_SET_OPTION = 31,

        //임의로 추가        
        GACHA_DROP_LIST,

        None = -1,
    }

    public enum UpdataUserDataType
    {
        DIA = 1,
        BUDDY,
        GOLD,
        HORNOR,
        MILEAGE,
        EXP,
        MAP,
        JOB,
        JOB_EXP,
        COLLECT,
        EG_DUNGEON
    }

    public enum MailTab
    {
        None = -1,
        Gift = 0,
        Ticket = 1,
        Buddy = 2,
    }

    public enum TutorialStep
    {
        None,
        NpcTalk,
        WeaponEquip,
        WeaponEnchantStart,
        WeaponEnchantEnd,
        SkillTree,
        SkillBatch,
        PetGachaStart,
        PetGachaEnd,
        PetEquip,
        PetLevelUpStart,
        PetLevelUpEnd,
        Hunting,
        Fishing,
        TutorialEnd = -1
    }

    public enum ItemPosGroup
    {
        INVEN,
        COS_INVEN,
        PET_INVEN,
        STORAGE,
        EQUIP,
        COS_EQUIP,
        PET_EQUIP,

        None = 255,
    }

    public enum ItemBaseType
    {
        NONE = 0, //없다.
        ONE_HAND_SWORD = 1, //1: 한손검: 10001-20000
        BLUNT_WEAPON,       //2: 둔기: 20001-30000
        KNUCKLES,           //3: 너클: 30001-40000
        TWO_HAND_SWORD,     //4: 양손검: 40001-50000
        SPEAR,              //5: 스피어: 50001-60000
        WAND,               //6: 지팡이: 60001-70000
        SHORT_SWORD,        //7: 단검: 70001-80000
        BOW,                //8: 활: 80001-90000
        CROSS_BOW,          //9: 석궁: 90001-100000
        TWO_SWORD,          //10: 쌍검: 100001-110000
        GUITAR,             //11: 기타: 110001-120000
        GUN,                //12: 총: 120001-130000
        SHIELD,             //13: 방패: 130001-140000
        ASSIST_EQUIP,       //14: 보조장비: 140001-150000
        HELMET,             //15: 투구: 150001-160000
        ARMOR,              //16: 바디: 160001-170000
        GLOVE,              //17: 장갑: 170001-180000
        SHOES,              //18: 신발: 180001-190000
        NECKLACE,           //19: 목걸이: 190001-200000
        RING,               //20: 반지: 200001-210000
        EARRING,            //21: 귀걸이: 210001-220000
        POTION,            //22: HP: 220001-230000
        COSTUME_UPPER,       //23: 코스튬-바디: 230001-240000 
        COSTUME_HELMET,     //24: 코스튬-투구: 240001-250000 
        COSTUME_CLOAK,      //25: 코스튬-망토: 250001-260000 
        PET_RUNE,           //26: 펫-룬 아이템: 260001-270000 
        ITEM_PUZZLE,        //27: 아이템-퍼즐: 270001-280000 
        PET_ATTR,           //28: 펫-속성석: 280001-280500 
        ITEM_ETC,           //29: 잡템: 290001-300000
        CHEST_BOX,          //30: 박스: 300001-310000 
        CHEST_KEY,          //31: 열쇠: 310001-320000
        COSTUME_LOWER,      //32: 코스튬-하의: 320001-330000 
        COSTUME_DYING,      //33: 코스튬-염색약: 28501-28800 
        COSTUME_RANDOM_DYING,   //34: 코스튬-염색약: 28801-29000 
        CALL_NAME,   //35: 칭호 아이템: 28801-29000 
        SOCIAL_MOTION,   //36: 소셜 아이템 37001~38000 
        REVIVE_ITEM,                    //37: 부활 아이템
        BUFF_ITEM,                      //38. 버프 아이템
        RESET_DAY_DUNGEON_ITEM,         //39. 일일 던전 입장 초기화 아이템
        RESET_INF_DUNGEON_ITEM,         //40. 무한의 탑 입장 초기화 아이템
        RESET_GRIDING_ITEM,             //41. 연마 초기화 아이템
        RESET_CHARISTIC_ITEM,           //42. 특성 초기화 아이템
        RAND_HAIR_ITEM,                 //43. 랜덤 헤어 아이템
        RAND_FACE_ITEM,                 //44. 랜덤 얼굴 아이템
        COSTUME_RING,                   //45. 코스튬 반지(목소리 반지)
        MULTIDUNGEON_TICKET,            //46. 멀티 던전 초기화 티켓
        C_ARMOR_NO_DRY,                 //47. 코스튬-바디(염색불가)
        C_HELMET_NO_DRY,                //48. 코스튬-투구(염색불가)
        C_CAPE_NO_DRY,                  //49. 코스튬-망토(염색불가)
        C_PANTS_NO_DRY,                 //50. 코스튬-하의(염색불가)
        SKILL_UPGRADE_TICKET,           //51. 스킬 강화권
        EVENT_GACHA_TICKET,             //52. 이벤트 가챠 티켓
        HOT_TIME_ITEM,                  //53. 핫 타임 아이템
        CHANGE_OPTION_ITEM,             //54. 옵션 변경권 아이템
        CHANGE_SPECIFIC_OPTION,         //55. 지정 옵션 변경권 아이템
        TRADE_ITEM,                     //56. 거래 변경권 아이템
        CHANGE_NAME_ITEM,               //57. 이름 변경권 아이템
        HP_POTION_ITEM,                 //58. HP포션 아이템
        RAND_ITEM,                      //59. 랜덤 아이템
        PACK_BOX_ITEM,                  //60. 패키지 아이템
        REPUTE_ITEM,                    //61. 평판 아이템
        PET_EQUIP_ITEM,                 //62. 펫 장비 아이템
        HONER_POINT_ITEM,               //63. 명예 포인트 아이템
        SEED_ITEM,                      //64. 씨앗 아이템
        FRUIT_ITEM,                     //65. 열매 아이템
        ACCELERANT_ITEM,                //66. 촉진제 아이템
        BLU_DIA_ITEM,                   //67. 블루다이아 획득용 아이템
        COSTUME_MATERIAL = 68,          //68. 코스튬 만능 재료
        FOOD_ITEM,                      //69. 음식 아이템
        NOT_BREAK_GRIDING_ITEM,         //70. 연마 깨짐 방지권
        MARRY_RING,                     //71. 결혼반지
        MARRY_LOVE_POTION,              //72. 사랑의 묘약
        MARRY_OBLIVION_POTION = 73,     //73. 망각의 이슬
        MOTION_ITEM,                    //74. 모션 아이템
        MAIN_CHARACTER_CHANGE_ITEM,     //75. 대표캐릭터 변경 아이템
        GENDER_CHANGE_ITEM,             //76. 성별 변경 아이템
    }

    public enum ItemRandOptionFixedType
    {
        None,
        Random,
        FixedSelect,
        FixedRandom,
    }

    //캐시삽
    public enum GoodsTypes
    {
        PET_1 = 1,
        PET_10_1,
        PET_10_BUDDY,
        ITEM_1,
        RUNE_PUZZLE_1,
        DIA,
        ELLY,
        POTION,
        MONTH_FIX,
        BUFF,
        LOGIN_SHOP,
        CASH_POP_SHOP,
        PACKAGE,
        COSTUME_ITEM,
        BEAUTY_SHOP,
        FURNITURE,
        ITEM_10_1,
        SNS_MOTION,
        SOME_ITEM,
        DRY_ITEM,
        PET_1_JOB,
        PET_10_1_JOB,
        BLUE_DIA,
        COSTUME_ITEM_1,
        COSTUME_ITEM_10,
        COSTUME_ITEM_10_1,
        THIRD_PAY_DIA = 30,
        FREE_PRIMIUM_PET,
        FREE_PRIMIUM_ITEM,
        FREE_PET,
        FREE_ITEM,
        PACKAGE_GOODS = 50
    }

    public enum OptionType
    {
        NONE = 0,          //0없음
        STR = 1,          //1힘
        INT = 2,          //2지능
        DEX = 3,          //3민첩
        LUK = 4,          //4행운
        MAX_HP = 5,          //5MAX HP
        MAX_MP = 6,          //6MAX MP
        MIN_ATK = 7,          //7최소 공격력
        MAX_ATK = 8,          //8최대 공격력
        BAS_ATK = 9,          //9공격력
        ALL_SKL_ATK = 10,         //10모든 스킬 공격력
        DFN = 11,         //11물리 방어력
        MAG_DFN = 12,         //12마법 방어력
        ACR = 13,         //13명중률
        DODGE = 14,         //14회피율
        CRTCAL_PER = 15,         //15치명타 확률증가
        CRTCAL_DMG = 16,         //16치명타 데미지증가
        MOVE_SPD_UP = 17,         //17이동속도 증가
        JUMP_PWR_UP = 18,         //18점프 파워 증가
        HEAL_PWR_UP = 19,         //19치유력 증가
        EXP_ADD = 20,         //20경험치 증가
        GOLD_ADD = 21,         //21골드 증가
        DMG_PER_ASP = 22,         //22데미지{0}% HP로 흡수
        DROP_PER_UP = 23,         //23드랍 확률 증가
        //HON_ADD                = 24,       //24명예 추가 획득
        //RANK_POINT_ADD         = 25,       //25랭킹 포인트 추가획득
        OPTION_ADD = 26,         //26속성 부여
        OPTION_DMG_UP = 27,         //27속성 스킬 데미지 증가
        MON_TYPE_DMG_UP = 28,        //28몬스터 타입 데미지 증가
        SKILL_KNOCK_X = 29,         //29특정 스킬의 넉백 X증가
        //SKILL_KNOCK_Y          = 30,       //30특정 스킬의 넉백 Y증가
        STUN = 31,         //31스턴
        SLEEP = 32,         //32슬립
        MOVE_NON = 33,         //33이동불가
        SILENCE = 34,         //34침묵
        PUT_LMT_LV = 35,         //35착용제한LV감소
        PHYS_IMMUNE = 36,         //36물리 이뮨
        MAGIC_IMMUNE = 37,         //37마법 이뮨
        FULL_IMMUNE = 38,         //38완전 이뮨
        DEBUFF_IMMUNE = 39,         //39상태 이상 이뮨
        DOT = 40,         //40도트
        NEED_MP = 41,         //41MP 소모량 감소
        RECOVERY_MP = 42,         //42MP 회복량 증가
        NEED_FP = 43,         //43FP(분노) 소모량 감소
        RECOVERY_FP = 44,         //44FP(분노) 회복량 감소
        MAX_FP = 45,         //45MAX FP(분노)
        MONSTERTYPE_DEFEN = 46,         //46 특정 몬스터 타입에게 받는 데미지 감소율%
        SKILL_COOLTIME = 47,         //스킬 쿨타임 감소
        ADD_MONSTER_LIMIT = 48,          //타격가능 개체수 증가
        WEAPON_DAMAGE = 49,         //특정 무기 장착 시 공격력 증가
        DEFENCE_BLOCK = 49,         //방패 블록// option_value_extra는 몇퍼센트를 감소 하는가다
        BUFF = 50,         //50버프
        EXEC_OPTION_ITEM_EQUIP = 51, //"[조건]특정 무기 장착시 옵션 발동
        SKILL_ADD = 53,         //특정 스킬 추가
        SKILL_TYPE_DAMAGE = 55,     //특정 타입 몬스터에게 스킬추가 데미지
        SKILL_ATTR_DAMAGE = 56,     //특정 속성 몬스터 스킬 추가데미지
        WEAPON_MAG_DFN = 57,    //특정 무기 장착 시 마법 방어력 증가
        WEAPON_ACCURACY = 58,    //특정 무기 장착 시 명중 증가
        WEAPON_EVASION = 59,    //특정 무기 장착 시 회피 증가
        WEAPON_CRITICAL_CHANCE = 60,    //특정 무기 장착 시 치명타율 증가
        WEAPON_CRITICAL_DAMAGE = 61,    //특정 무기 장착 시 치명타 데미지 증가
        CRI_PER = 62,   //치명타 확률 증가
        GUARDIAN_DEVO = 63,   //가디언-헌신
        BASIC_ATTACK_DMG = 64, //평타 데미지 증가%
        HEAL_MANA = 65,   //힐스킬 마나소모량 감소 
        HEAL_COOL = 66,   //힐스킬 쿨타임 감소
        BUFF_TIME = 67,   //버프지속시간 증가
        BUFF_MANA = 68,   //버프스킬 마나소모량 감소
        BUFF_COOL = 69,   //버프스킬 쿨타임 감소
        DEFAULT_ATK_LIMIT = 70,   //평타 타깃수 증가
        DEFAULT_ATK_DMG_WEAPON = 71,   //평타 데미지 증가 정수로만
        DEFAULT_ATK_DMG_ATTR = 72,   //평타,특정 속성 몬스터에게 추뎀
        DEFAULT_ATK_DMG_TYPE = 73,   //평타,특정 타입 몬스터에게 추뎀
        DEFAULT_ATK_BUFF = 74,   //평타,확률로 디버프걸기(중독,상처)
        DEC_DMG = 75,   //데미지 %감소
        AURORA_OF_COURAGE = 76,   //용기의 오로라(공,방,치명)% 증가
        SHIELD = 77,       //보호막
        BERSERKER = 78,       //버서커(방어력감소%,평타뎀지증가%)
        DEC_DEF = 79,       //방어력 감소
        DEC_DMG_MONSTER = 80,       //몬스터에게 받는 데미지 감소
        ADD_DMG_BOSS = 81,       //보스몬스터에게 추가데미지(스킬,평타)
        SKILL_DMG_PER = 82,       //펫스킬 데미지 증가%(skill.default_dmg 기준)
        ADD_DMG = 83,       //데미지 증가%(최종 데미지의 %)
        ADD_AGGRO = 84, //어그로 추가
        PVP_ADD_ATK_DMG_RATE = 85, //PVP데미지증가%
        PVP_DECREASE_ATK_DMG_RATE = 86,//PVP데미지감소추가%
        ALL_STATS = 87,//올스탯 증가
        ADD_DMG_DODGE_DOWN = 88,//데미지 증가 % 회피율 다운
        AGGRO_NORMAL = 89,//어그로 위협수준 증가 일반
        AGGRO_SKILL = 90, //어그로 위협수준 증가 스킬
        PLUS_SKILL_ALL = 91,     // 모든 스킬 +1
        PLUS_SKILL_JOB = 92,     // 모든 직업 스킬 +1
        PLUS_SKILL_PET = 93,     // 모든 펫 스킬 +1
        PLUS_SKILL_ONE = 94,     // 특정 스킬 +1
        ADD_SKILL_DMG_DODGE_DOWN = 95,     // 스킬 데미지 증가% 회피율 다운
        STEAL_HP_ADD_NORMAL = 96,     // 데미지  HP로 흡수-평타
        STEAL_HP_ADD_SKILL = 97,     // 데미지  HP로 흡수-스킬
        STEAL_HP_ADD_ALL = 98,     // 데미지  HP로 흡수-평타,스킬
        POTION_HP_ADD = 99,     // 포션으로 얻는 HP량 증가
        POTION_USE_ADD = 100,    // 한번에 사용할수 있는 물약갯수 증가
        OPTION_COUNT = 101,    // 옵션의 마지막 항목으로 옵션 타입 갯수를 편하게 관리하기 위해 추가.
    }

    public enum BuffExeType
    {
        SKILL_ETC,     //스킬등 기타 사유로 발동
        ITEM,       //아이템 사용시 발동
        ATTACK,     //일반 공격시 발동
        CRT_ATTACK,   // 크리티컬 공격시 발동
        ATTACK_BY,      //피격 발동
        CRT_ATTACK_BY,  //크리티컬 피격 발동.
    }

    public sealed class ItemType : EnumBaseType<ItemType, byte, string>
    {
        public static readonly ItemType NONE = new ItemType(0, 0, "None", ItemPos.NONE);
        public static readonly ItemType ONEHAND_SWORD = new ItemType(1, 1, "한손검", ItemPos.WEAPON, 120, 360);
        public static readonly ItemType BLUNT = new ItemType(2, 1, "둔기", ItemPos.WEAPON, 80, 350);
        public static readonly ItemType KNUCKLE = new ItemType(3, 1, "너클", ItemPos.WEAPON, 80, 170);
        public static readonly ItemType TWOHAND_SWORD = new ItemType(4, 1, "양손검", ItemPos.WEAPON, 130, 350);
        public static readonly ItemType SPEAR = new ItemType(5, 1, "창", ItemPos.WEAPON, 170, 280);
        public static readonly ItemType STAFF = new ItemType(6, 1, "지팡이", ItemPos.WEAPON, 390, 420);
        public static readonly ItemType DAGGER = new ItemType(7, 1, "단검", ItemPos.WEAPON, 180, 250);
        public static readonly ItemType BOW = new ItemType(8, 1, "활", ItemPos.WEAPON, 390, 350);
        public static readonly ItemType CROSSBOW = new ItemType(9, 1, "석궁", ItemPos.WEAPON, 400, 360);
        public static readonly ItemType DUAL_SWORD = new ItemType(10, 1, "쌍검", ItemPos.WEAPON, 100, 470);
        public static readonly ItemType GUITAR = new ItemType(11, 1, "기타", ItemPos.WEAPON, 220, 410);
        public static readonly ItemType GUN = new ItemType(12, 1, "총", ItemPos.WEAPON, 420, 420);
        public static readonly ItemType SHIELD = new ItemType(13, 2, "방패" ,ItemPos.SHIELD);
        public static readonly ItemType EQUIP_AIDS = new ItemType(14, 2, "보조장비" ,ItemPos.SHIELD);
        public static readonly ItemType HELMET = new ItemType(15, 2, "투구", ItemPos.HELMET);
        public static readonly ItemType ARMOR = new ItemType(16, 2, "바디", ItemPos.ARMOR);
        public static readonly ItemType GLOVES = new ItemType(17, 2, "장갑", ItemPos.GLOVES);
        public static readonly ItemType SHOES = new ItemType(18, 2, "신발", ItemPos.SHOES);
        public static readonly ItemType NECKLACE = new ItemType(19, 2, "목걸이", ItemPos.NECKLACE);
        public static readonly ItemType RING = new ItemType(20, 2, "반지", ItemPos.RING);
        public static readonly ItemType EARRING = new ItemType(21, 2, "귀걸이", ItemPos.EARRING);
        public static readonly ItemType HP = new ItemType(22, 3, "HP", ItemPos.INVEN);
        public static readonly ItemType C_ARMOR = new ItemType(23, 4, "코스튬상의", ItemPos.C_ARMOR);
        public static readonly ItemType C_HELMET = new ItemType(24, 4, "코스튬모자", ItemPos.C_HELMET);
        public static readonly ItemType C_CAPE = new ItemType(25, 4, "코스튬망토", ItemPos.C_CAPE);
        public static readonly ItemType PET_RUNE = new ItemType(26, 5, "펫 룬", ItemPos.INVEN);
        public static readonly ItemType PUZZLE = new ItemType(27, 5, "퍼즐", ItemPos.INVEN);
        public static readonly ItemType ATTR_STONE = new ItemType(28, 5, "펫 속성석", ItemPos.INVEN);
        public static readonly ItemType ETC_ITEM = new ItemType(29, 6, "기타아이템", ItemPos.INVEN);
        public static readonly ItemType BOX_ITEM = new ItemType(30, 7, "박스아이템", ItemPos.INVEN);
        public static readonly ItemType KEY_ITEM = new ItemType(31, 7, "박스키아이템", ItemPos.INVEN);
        public static readonly ItemType C_PANTS = new ItemType(32, 4, "코스튬하의", ItemPos.C_PANTS);
        public static readonly ItemType DRY_ITEM = new ItemType(33, 7, "염색아이템", ItemPos.INVEN);
        public static readonly ItemType DRY_ITEM_RAND = new ItemType(34, 7, "랜덤염색아이템", ItemPos.INVEN);
        public static readonly ItemType TITLE_ITEM = new ItemType(35, 7, "칭호", ItemPos.INVEN);
        public static readonly ItemType SNS_ITEM = new ItemType(36, 7, "소셜모션", ItemPos.INVEN);
        public static readonly ItemType REVIVE_ITEM = new ItemType(37, 7, "부활권", ItemPos.INVEN);
        public static readonly ItemType BUFF_ITEM = new ItemType(38, 7, "버프아이템", ItemPos.INVEN);
        public static readonly ItemType RESET_DAY_DUNGEON_ITEM = new ItemType(39, 7, "요일던전리셋", ItemPos.INVEN);
        public static readonly ItemType RESET_INF_DUNGEON_ITEM = new ItemType(40, 7, "무한던전리셋", ItemPos.INVEN);
        public static readonly ItemType RESET_GRIDING_ITEM = new ItemType(41, 7, "연마초기화", ItemPos.INVEN);
        public static readonly ItemType RESET_CHARISTIC_ITEM = new ItemType(42, 7, "스킬초기화", ItemPos.INVEN);
        public static readonly ItemType RAND_HAIR_ITEM = new ItemType(43, 7, "랜덤헤어", ItemPos.INVEN);
        public static readonly ItemType RAND_FACE_ITEM = new ItemType(44, 7, "랜덤성형", ItemPos.INVEN);
        public static readonly ItemType V_RING = new ItemType(45, 4, "목소리반지", ItemPos.V_RING);
        public static readonly ItemType RESET_MULTI_DUNGEON_ITEM = new ItemType(46, 7, "멀티던전리셋", ItemPos.INVEN);
        public static readonly ItemType SPA_TICKET_ITEM = new ItemType(47, 7, "온천입장권", ItemPos.INVEN);
        public static readonly ItemType SKILL_POINT_ITEM = new ItemType(51, 7, "스킬강화포인트권", ItemPos.INVEN);
        public static readonly ItemType BOX_GACHA_TICKET = new ItemType(52, 7, "박스가챠뽑기권", ItemPos.INVEN);
        public static readonly ItemType HOT_TIME_ITEM = new ItemType(53, 7, "핫타임티켓", ItemPos.INVEN);
        public static readonly ItemType CHANGE_OPTION_ITEM = new ItemType(54, 7, "전체옵션변경권", ItemPos.INVEN);
        public static readonly ItemType CHANGE_SPECIFIC_OPTION = new ItemType(55, 7, "지정옵션변경권", ItemPos.INVEN);
        public static readonly ItemType TRADE_ITEM = new ItemType(56, 7, "거래변경권", ItemPos.INVEN);
        public static readonly ItemType CHANGE_NAME_ITEM = new ItemType(57, 7, "이름변경권", ItemPos.INVEN);
        public static readonly ItemType HP_POTION_ITEM = new ItemType(58, 7, "HP포션아이템", ItemPos.INVEN);
        public static readonly ItemType RAND_ITEM = new ItemType(59, 7, "랜덤아이템", ItemPos.INVEN);
        public static readonly ItemType PACK_BOX_ITEM = new ItemType(60, 7, "패키지아이템", ItemPos.INVEN);
        public static readonly ItemType REPUTE_ITEM = new ItemType(61, 7, "평탄아이템", ItemPos.INVEN);
        public static readonly ItemType PET_WEAPON_EQUIP = new ItemType(62, 5, "펫 장비", ItemPos.INVEN);
        public static readonly ItemType HONOR_POINT_ITEM = new ItemType(63, 7, "명예포인트아이템", ItemPos.INVEN);
        public static readonly ItemType GUILD_CROPS_SEED = new ItemType(64, 7, "길드작물씨앗", ItemPos.INVEN);
        public static readonly ItemType GUILD_CROPS_BEANS = new ItemType(65, 7, "길드작물열매", ItemPos.INVEN);
        public static readonly ItemType GUILD_CROPS_BOOSTER = new ItemType(66, 7, "길드작물촉진제", ItemPos.INVEN);
        public static readonly ItemType BLUE_DIA_ITEM = new ItemType(67, 7, "블루다이아", ItemPos.INVEN);
        public static readonly ItemType COSTUME_COMBINE_ITEM = new ItemType(68, 5, "코스튬합성아이템(무지개천)", ItemPos.INVEN);
        public static readonly ItemType FOOD = new ItemType(69, 7, "음식", ItemPos.INVEN);
        public static readonly ItemType NOT_BREAK_GRIDING_ITEM = new ItemType(70, 7, "연마깨짐방지권", ItemPos.INVEN);
        public static readonly ItemType MARRY_RING = new ItemType(71, 7, "결혼반지", ItemPos.INVEN);
        public static readonly ItemType MARRY_LOVE_POTION = new ItemType(72, 7, "사랑의묘약", ItemPos.INVEN);
        public static readonly ItemType MARRY_OBLIVION_POTION = new ItemType(73, 7, "망각의이슬", ItemPos.INVEN);
        public static readonly ItemType MOTION_ITEM = new ItemType(74, 7, "모션아이템", ItemPos.INVEN);
        public static readonly ItemType CHANGE_MAIN_CHAR = new ItemType(75, 7, "대표캐릭터변경권", ItemPos.INVEN);
        public static readonly ItemType CHANGE_CHAR_GENDER = new ItemType(76, 7, "캐릭터성별변경권", ItemPos.INVEN);

        public readonly byte series_value;
        public readonly ItemPos itemPosType;
        public readonly int weaponRange;
        public readonly int delay_time;

        public ItemType(byte key, byte series_key, string desc, ItemPos itemPosType, int weaponRange = 0, int delay_time = 0) : base(key, desc)
        {
            this.series_value = (byte)series_key;
            this.itemPosType = itemPosType;
            this.weaponRange = weaponRange == 0 ? 0 : (weaponRange + 150);
            this.delay_time = delay_time;
        }

        public static ItemType GetByKey(byte key)
        {
            var data = GetBaseByKey(key);

            if (data != null) return data;
            else return ItemType.NONE;
        }

    }

    public sealed class ItemPos : EnumBaseType<ItemPos, byte, string>
    {
        public static readonly ItemPos NONE = new ItemPos(255, "None", ItemPosGroup.None, false);
        public static readonly ItemPos INVEN = new ItemPos(0, "인벤", ItemPosGroup.INVEN, false);
        public static readonly ItemPos COS_INVEN = new ItemPos(1, "코스튬인벤", ItemPosGroup.COS_INVEN, false);
        public static readonly ItemPos WEAPON = new ItemPos(2, "무기", ItemPosGroup.EQUIP, true);
        public static readonly ItemPos SHIELD = new ItemPos(3, "방패", ItemPosGroup.EQUIP, true);
        public static readonly ItemPos EQUIP_AIDS = new ItemPos(4, "보조무기", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos HELMET = new ItemPos(5, "투구", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos ARMOR = new ItemPos(6, "상의", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos GLOVES = new ItemPos(7, "장갑", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos SHOES = new ItemPos(8, "신발", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos NECKLACE = new ItemPos(9, "목걸이", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos RING = new ItemPos(10, "반지", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos EARRING = new ItemPos(11, "귀걸이", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos C_ARMOR = new ItemPos(12, "코스튬상의", ItemPosGroup.COS_EQUIP, true);
        public static readonly ItemPos C_HELMET = new ItemPos(13, "코스튬모자", ItemPosGroup.COS_EQUIP, true);
        public static readonly ItemPos C_CAPE = new ItemPos(14, "코스튬망토", ItemPosGroup.COS_EQUIP, true);
        public static readonly ItemPos C_PANTS = new ItemPos(15, "코스튬하의", ItemPosGroup.COS_EQUIP, true);
        public static readonly ItemPos HP = new ItemPos(16, "HP포션", ItemPosGroup.EQUIP, false);
        public static readonly ItemPos V_RING = new ItemPos(17, "코스튬반지", ItemPosGroup.EQUIP, true);

        public readonly ItemPosGroup itemPosGroup;
        public readonly bool changeAppear;

        public ItemPos(byte key, string value, ItemPosGroup itemPosGroup, bool changeAppear) : base(key, value)
        {
            this.itemPosGroup = itemPosGroup;
            this.changeAppear = changeAppear;
        }

        public static ItemPos GetByKey(byte key)
        {
            var data = GetBaseByKey(key);

            if (data != null) return data;
            else return ItemPos.NONE;
        }
    }

    public sealed class ResultCode : EnumBaseType<ResultCode, byte, int>//LocalizeKey>
    {
        //private string desc;
        private int msg { get { return Value; } }
        public bool showUserMsg;

        public static readonly ResultCode FAIL = new ResultCode(0, cText.RcodeTxt_000, true);   //FAIL");
        public static readonly ResultCode SUCCESS = new ResultCode(1, cText.RcodeTxt_001, false);   //SUCCESS");
        public static readonly ResultCode ALREADY_EXISTS = new ResultCode(2, cText.RcodeTxt_002, true);   //Duplication Fail");
        public static readonly ResultCode APPVERSION_FAIL = new ResultCode(3, cText.RcodeTxt_003, true);   //APP Version Fail");
        public static readonly ResultCode LOGIN_BAD_USERNAME = new ResultCode(4, cText.RcodeTxt_004, true);   //Login Bad UserName");
        public static readonly ResultCode LOGIN_AUTH_FAIL = new ResultCode(5, cText.RcodeTxt_005, true);   //Login Auth Fail");
        public static readonly ResultCode RETRY_AUTH = new ResultCode(6, cText.RcodeTxt_006, true);   //인증 서버로 재접속");
        public static readonly ResultCode SERVER_IS_BUSY = new ResultCode(7, cText.RcodeTxt_007, true);   //서버가 점검 중입니다.");
        public static readonly ResultCode DUPLICATION_LOGIN = new ResultCode(8, cText.RcodeTxt_008, true);   //중복로그인된 계정입니다.");
        public static readonly ResultCode USER_IS_BANNED = new ResultCode(9, cText.RcodeTxt_009, true);   //User is Banned.");
        public static readonly ResultCode GUEST_LOGIN_FAIL = new ResultCode(10, cText.RcodeTxt_010, true);   //게스트 로그인 실패.");
        public static readonly ResultCode NOT_ENOUGHT_DIA = new ResultCode(11, cText.RcodeTxt_011, true);   //다이아 부족");
        public static readonly ResultCode NOT_ENOUGHT_GOLD = new ResultCode(12, cText.RcodeTxt_012, true);   //엘리 부족");
        public static readonly ResultCode NOT_ENOUGHT_MY_GUILD_POINT = new ResultCode(13, cText.RcodeTxt_013, true);   //길드 포인트 부족");

        public static readonly ResultCode IS_MAX_SIZE = new ResultCode(15, cText.RcodeTxt_015, true);   //확장가능한 최대치");

        public static readonly ResultCode MY_FRIEND_COUNT_IS_FULL = new ResultCode(17, cText.RcodeTxt_017, true);   //친구수 최대");
        public static readonly ResultCode NOT_ENOUGHT_HONOR = new ResultCode(18, cText.RcodeTxt_018, true);   //명예 포인트 부족");
        public static readonly ResultCode NOT_ENOUGHT_COUNT = new ResultCode(19, cText.RcodeTxt_019, true);   //제한횟수 도달");
        public static readonly ResultCode NOT_IN_PROGRESS = new ResultCode(20, cText.RcodeTxt_020, true);   //진행중이 아니다.");
        public static readonly ResultCode NOT_ENOUGHT_MILEAGE = new ResultCode(21, cText.RcodeTxt_021, true);   //마일리지 부족");
        public static readonly ResultCode SHORT_STRING = new ResultCode(22, cText.RcodeTxt_022, true);   //문자열이 너무 짧음.");
        public static readonly ResultCode NOT_EXISTS = new ResultCode(23, cText.RcodeTxt_023, true);   //존재하지 않는다.");    

        public static readonly ResultCode NOT_USED_COMMAND = new ResultCode(25, cText.RcodeTxt_025, true);   //사용할수 없다."); //실패가 아니다.
        public static readonly ResultCode FRIENDS_FRIEND_COUNT_IS_FULL = new ResultCode(26, cText.RcodeTxt_026, true);   //친구의 친구수 최대");
        public static readonly ResultCode ALREADY_FRIEND = new ResultCode(27, cText.RcodeTxt_027, true);   //이미 친구입니다");
        public static readonly ResultCode IAP_IOS_URUS_CRACKER = new ResultCode(28, cText.RcodeTxt_028, true);   //IOS IAP 해킹 시도");
        public static readonly ResultCode IAP_DUPLICATE_RECEIPT = new ResultCode(29, cText.RcodeTxt_029, true);   //IAP 영수증 재사용");
        public static readonly ResultCode SECESSION_DATA_CHOICE = new ResultCode(30, cText.RcodeTxt_030, true);   //탈퇴전 데이터 처리필요");
        public static readonly ResultCode ALREADY_USED_COUPON = new ResultCode(31, cText.RcodeTxt_031, true);   //이미사용한 쿠폰");
        public static readonly ResultCode EXPIRED_COUPON = new ResultCode(32, cText.RcodeTxt_032, true);   //만료된 쿠폰");
        public static readonly ResultCode BAD_ACCOUNT_NAME = new ResultCode(33, cText.RcodeTxt_033, false);   //이름이 잘못됨

        public static readonly ResultCode NOT_SELL_ITEM = new ResultCode(34, cText.RcodeTxt_034, true);   //수령제한(하루한개) 쿠폰");

        public static readonly ResultCode DAILY_TRY_LIMIT = new ResultCode(35, cText.RcodeTxt_035, true);   //쿠폰 일일시도횟수제한");
        public static readonly ResultCode INVALID_COUPON = new ResultCode(36, cText.RcodeTxt_036, true);   //유효하지 않은 쿠폰번호입니다.");
        public static readonly ResultCode DAY_DUNGEON_ALREADY_CLEAR = new ResultCode(37, cText.RcodeTxt_037, true);   //오늘 요일던전을 이미 클리어");
        public static readonly ResultCode NOT_CLEAR_PORTAL_QUEST = new ResultCode(38, cText.RcodeTxt_038, true);   //클리어하지 않은 포탈");
        public static readonly ResultCode NOT_CLEAR_HIDDEN_QUEST = new ResultCode(39, cText.RcodeTxt_039, true);   //클리어 하지 않은 히든퀘스트 포탈");
        public static readonly ResultCode IAP_PRODUCT_CODE_ERROR = new ResultCode(40, cText.RcodeTxt_040, true);   //IAP 상품코드 문제");
        public static readonly ResultCode OVER_DAILY_QUEST_COUNT = new ResultCode(41, cText.RcodeTxt_041, true);   //하루 받을수있는 일일 퀘스트량이 초과하였다.");
        public static readonly ResultCode HOUSE_MAP_COUNT_OVER = new ResultCode(42, cText.RcodeTxt_042, false);   //하우징에 인원제한으로 못들어간다");
        public static readonly ResultCode HOUSE_MAP_SERVER_OVER = new ResultCode(43, cText.RcodeTxt_043, true);   //하우징 이동 시 다른 서버가 꽉차있다."); //서버 이동 실패. 현재 서버에 임시로 집을 만들어서 사용
        public static readonly ResultCode CHARGE_SKILL_ERROR = new ResultCode(44, cText.RcodeTxt_044, true);   //차지스킬 사용 불가");
        public static readonly ResultCode NOTHING_HOUSE = new ResultCode(45, cText.RcodeTxt_045, false);   //집이 없는 사람이다.");

        public static readonly ResultCode WISPERCHAT_FAIL = new ResultCode(49, cText.RcodeTxt_049, true);   //채팅 보내기 실패다.");
        public static readonly ResultCode OVER_SUB_QUEST_COUNT = new ResultCode(50, cText.RcodeTxt_050, true);   //서브 퀘스트가 꽉차 더이상 받을수가 없다.");
        public static readonly ResultCode OVER_DAY_QUEST = new ResultCode(51, cText.RcodeTxt_051, true);   //기간이 지난 퀘스트다.");
        public static readonly ResultCode LEVEL_CUT = new ResultCode(52, cText.RcodeTxt_052, true);   //레벨 제한에 걸림");
        public static readonly ResultCode LEFT_SKILL_COOL_TIME = new ResultCode(53, cText.RcodeTxt_053, true);   //스킬 쿨타임이 지나지 않았다.");
        public static readonly ResultCode NOT_MANA = new ResultCode(54, cText.RcodeTxt_054, false);   //스킬 소모 자원 (MP) 부족");
        public static readonly ResultCode I_AM_SINGLE = new ResultCode(55, cText.RcodeTxt_055, true);   //I'AM SINGLE");
        public static readonly ResultCode OTHER_LOAD_WAIT = new ResultCode(56, cText.RcodeTxt_056, true);   //다른 유저 대기 상태");
        public static readonly ResultCode INVITE_FAIL_NOT_FRIEND = new ResultCode(57, cText.RcodeTxt_057, true);   //친구 요청 실패-친구가 없다.");
        public static readonly ResultCode ACCEPT_INVITE_FAIL_NOT_ROOM = new ResultCode(58, cText.RcodeTxt_058, true);   //초대 수락패킷  실패-방이 없다");
        public static readonly ResultCode ACCEPT_INVITE_FAIL_REQUESTCANCEL = new ResultCode(59, cText.RcodeTxt_059, true);   //초대 수락패킷  실패-요청을 취소한 경우.");
        public static readonly ResultCode PLAYER_ALREADY_DIE = new ResultCode(60, cText.RcodeTxt_060, true);   //이미 죽어 있는 유저가 패킷을 보냈다.");

        public static readonly ResultCode NOTOPEN_PVPZONE = new ResultCode(62, cText.RcodeTxt_062, true);   //열려있는 pvp 존이 없다.");

        public static readonly ResultCode BUY_LIMITED = new ResultCode(63, cText.RcodeTxt_063, false);

        public static readonly ResultCode ALREADY_LINKED_PLATFORM_ACCOUNT = new ResultCode(65, cText.RcodeTxt_065, false);
        public static readonly ResultCode NOT_LIKE_SEASON = new ResultCode(66, cText.RcodeTxt_066, false);   //좋아요 시즌이 없다.");

        public static readonly ResultCode RESULT_CODE_MESSAGE = new ResultCode(67, 0, false); // 서버에서 language id 값을 받음..

        public static readonly ResultCode CHAR_GUILD_NOT_FOUND = new ResultCode(70, cText.RcodeTxt_070, true);   //소속 길드가 없습니다.");
        public static readonly ResultCode GUILD_MEMBER_IS_FULL = new ResultCode(71, cText.RcodeTxt_071, true);   //길드 멤버가 꽉찼습니다. 더 이상 가입 불가");
        public static readonly ResultCode ALREADY_GUILD_MEMBER = new ResultCode(72, cText.RcodeTxt_072, true);   //이미 길드에 소속되어있습니다.");
        public static readonly ResultCode PERMISSION_FAIL = new ResultCode(73, cText.RcodeTxt_073, true);   //권한이 없습니다.");
        public static readonly ResultCode LEFT_COOL_TIME = new ResultCode(74, cText.RcodeTxt_074, true);   //쿨타임이 지나지 않았다.");
        public static readonly ResultCode PROC_GUILD_REWARD_DAY = new ResultCode(75, cText.RcodeTxt_075, true);   //길드 보상 날짜 처리중");

        public static readonly ResultCode OTHER_ALREADY_CHOICE = new ResultCode(76, cText.RcodeTxt_076, true);   //다른사람이 이미 선택함.");

        public static readonly ResultCode INVITE_PARTY_FAIL = new ResultCode(77, cText.RcodeTxt_077, true);   //동행 실패 내용");
        public static readonly ResultCode INVITE_PARTY_ACCEPT_FAIL = new ResultCode(78, cText.RcodeTxt_078, true);   //동행 수락 실패");

        public static readonly ResultCode HOTTIME_ASYNC_FAIL = new ResultCode(79, cText.RcodeTxt_079, true);   //핫타임-싱크에러");

        public static readonly ResultCode ALREADY_START = new ResultCode(80, cText.RcodeTxt_080, true);   //이미 시작함");
        public static readonly ResultCode NOT_FOUND_ACCOUNTKEY_COUPON = new ResultCode(81, cText.RcodeTxt_081, true);   //쿠폰사용-계정이 없습니다.");

        public static readonly ResultCode NOT_EXIST_EMAIL_ACCOUNT = new ResultCode(82, cText.RcodeTxt_082, true);   //존재하지 않는 email");
        public static readonly ResultCode BAD_PASS_ACCOUNT = new ResultCode(83, cText.RcodeTxt_083, true);   //비밀 번호가 잘못됨");
        public static readonly ResultCode LIMIT_EMAIL_ACCOUNT = new ResultCode(84, cText.RcodeTxt_084, true);   //이메일 이름이 잘못됨");    // 5자 미만일수가 없다.
        public static readonly ResultCode ALREADY_LINKED_ACCOUNT = new ResultCode(85, cText.RcodeTxt_085, true);   //이미 연동된 계정");

        public static readonly ResultCode LIMITED_GOODS = new ResultCode(86, cText.RcodeTxt_086, true);   //상품의 살수 있는 갯수를 넘었다.");
        public static readonly ResultCode LEFT_RETURN_TIME = new ResultCode(87, cText.RcodeTxt_087, true);   //귀환해야될 시간이 남았다.");

        public static readonly ResultCode NOT_ENOUGH_INVENTORY = new ResultCode(88, cText.RcodeTxt_088, true);   //인벤토리가 부족 하다.");
        public static readonly ResultCode NOT_ENOUGH_PET_INVENTORY = new ResultCode(89, cText.RcodeTxt_089, true);   //펫 인벤토리가 부족하다.");

        public static readonly ResultCode ALREADY_HP_FULL = new ResultCode(90, cText.RcodeTxt_090, true);   //체력이 FULL입니다.");
        public static readonly ResultCode BINGGO_TIME_OUT = new ResultCode(91, cText.RcodeTxt_091, true);   //빙고 판 시간이 없다");

        public static readonly ResultCode DUNGEON_TIME_OVER = new ResultCode(92, cText.RcodeTxt_092, true);   //던전 시간이 지났다.");
        public static readonly ResultCode DUNGEON_NOT_START_CONDITION = new ResultCode(93, cText.RcodeTxt_093, false);   //던전 시작 조건이 맞지 않는다.");
        public static readonly ResultCode DUNGEON_CREATE_FAIL = new ResultCode(94, cText.RcodeTxt_094, true);   //던전방을 만들다가 실패-필드에재입장필요");

        public static readonly ResultCode RESULT_NOT_JOIN_CHANNEL_CHAT = new ResultCode(95, cText.RcodeTxt_095, true);   //채널 채팅방에 들어가 있지 않다.");
        public static readonly ResultCode MAKE_TIME_OUT = new ResultCode(96, cText.RcodeTxt_096, true);   //제작기간이 지났다.");

        public static readonly ResultCode RESULT_COLLECT_LEFT_TIME = new ResultCode(97, cText.RcodeTxt_097, false);   //채집 시간이 다 안지났는데 보냈다.");

        public static readonly ResultCode RESULT_PROTOCOL_FAIL_OK = new ResultCode(98, cText.RcodeTxt_098, false);   //프로토콜은 성공. 무시됨");
        public static readonly ResultCode RESULT_PROTOCOL_FAIL_ATK_COUNT = new ResultCode(99, cText.RcodeTxt_099, false);   //평타 공격 입력순서가 올바르지 않습니다.");
        public static readonly ResultCode RESULT_PROTOCOL_FAIL = new ResultCode(100, cText.RcodeTxt_100, false);   //프로토콜 FAIL. 무시됨.");

        public static readonly ResultCode RESULT_COLLECT_DIFF_INDEX = new ResultCode(101, cText.RcodeTxt_101, false);
        public static readonly ResultCode NOT_VALUE = new ResultCode(102, cText.RcodeTxt_102, false);

        public static readonly ResultCode IMPOSSIBLE = new ResultCode(103, cText.RcodeTxt_103, false);
        public static readonly ResultCode DAY_DUNGEON_DIFF = new ResultCode(104, cText.RcodeTxt_104, false);       // 요일이 다릅니다.
        public static readonly ResultCode GUILD_CROPS_INFO_CHANGE = new ResultCode(105, cText.RcodeTxt_105, false); // 길드 작물의 정보가 바뀌었습니다. 해당 RCode 반환시 Info 리프레시 해줄 것.
        public static readonly ResultCode CREATE_ACCOUNT_IMPOSSIBLE = new ResultCode(106, cText.RcodeTxt_106, false);
        public static readonly ResultCode AUTH_WAITING = new ResultCode(127, cText.RcodeTxt_127, false);

        public ResultCode(byte key, int msg, bool showUser)
            : base(key, msg)
        {
            this.showUserMsg = showUser;
        }

        public static ReadOnlyCollection<ResultCode> GetValues()
        {
            return GetBaseValues();
        }

        public static ResultCode GetByKey(byte key)
        {
            return GetBaseByKey(key);
        }

        public static ResultCode GetByKey(short key)
        {
            return GetBaseByKey((byte)key);
        }

        public string GetMsg()
        {
            return cText.GetText(Value);
        }

        public override string ToString()
        {
            return string.Concat(
                Key,
                ", ",
                cText.GetText(Value)
            );
        }
    }

    public static class cText
    {
        #region 일반 UI사용

        #region 단위, 재화, 시간

        public const int Unit_0 = 260000;    // "개");
        public const int Unit_1 = 260001;    // "엘리");
        public const int Unit_2 = 260002;    // "다이아");
        public const int Unit_3 = 260003;    // "우정포인트");
        public const int Unit_4 = 260004;    // "마일리지");
        public const int Unit_5 = 260005;    // "일차");
        public const int Unit_6 = 260006;    // "회");
        public const int Unit_7 = 260007;    // "경험치");
        public const int Unit_8 = 260008;    // "일");
        public const int Unit_9 = 260009;    // "시간");

        public const int Unit_10 = 260010;    // "분");
        public const int Unit_11 = 260011;    // "초");
        public const int Unit_12 = 260012;    // "명");
        public const int Unit_13 = 260013;    // "현재");
        public const int Unit_14 = 260014;    // "다음");
        public const int Unit_15 = 260015;    // "점");
        public const int Unit_16 = 260016;    // "위");
        public const int Unit_17 = 260017;    // "킬");

        public const int Unit_20 = 260018;    // "월");
        public const int Unit_21 = 260019;    // "화");
        public const int Unit_22 = 260020;    // "수");
        public const int Unit_23 = 260021;    // "목");
        public const int Unit_24 = 260022;    // "금");
        public const int Unit_25 = 260023;    // "토");
        public const int Unit_26 = 260024;    // "일");

        public const int Unit_27 = 260025;    // "상");
        public const int Unit_28 = 260026;    // "중");
        public const int Unit_29 = 260027;    // "하");

        public const int Unit_30 = 261871;    // "전투력");

        public const int Unit_31 = 262431;    // "활성화 포인트");
        public const int Unit_32 = 262432;    // "스킬 포인트");
        public const int Unit_33 = 263232;         // 블루 다이아
        #endregion 단위, 재화, 시간

        #region 기타(공용)?!

        public const int Common_0 = 260028;    // "몬스터 이름");
        public const int Common_1 = 260029;    // "사용가능");
        public const int Common_2 = 260030;    // "아이템 뽑기");
        public const int Common_3 = 260031;    // "설정안함");
        public const int Common_4 = 260032;    // "펫 뽑기");
        public const int Common_5 = 260033;    // "체력포션");
        public const int Common_6 = 260034;    // "강제보상받기");
        public const int Common_7 = 260035;    // "보상받기");
        public const int Common_8 = 260036;    // "보상완료");
        public const int Common_9 = 260037;    // "남음");

        public const int Common_10 = 260038;    // "기간 종료");
        public const int Common_11 = 260039;    // "등록");
        public const int Common_12 = 260040;    // "수락");
        public const int Common_13 = 260041;    // "완료");
        public const int Common_14 = 260042;    // "없음");
        public const int Common_15 = 260043;    // "이상");
        public const int Common_16 = 260044;    // "선택");
        public const int Common_17 = 260045;    // "생성");
        public const int Common_18 = 260046;    // "세트");
        public const int Common_19 = 260047;    // "효과");

        public const int Common_20 = 260048;    // "해제");
        public const int Common_21 = 260049;    // "장착");
        public const int Common_22 = 260050;    // "보유");
        public const int Common_23 = 260051;    // "확장");
        public const int Common_24 = 260052;    // "불가능");
        public const int Common_25 = 260053;    // "가능");
        public const int Common_26 = 260054;    // "공통");
        public const int Common_27 = 260055;    // "아이템");
        public const int Common_28 = 260056;    // "방어구");

        public const int Common_30 = 260057;    // "재료");
        public const int Common_31 = 260058;    // "새로장착");
        public const int Common_32 = 260059;    // "적용");
        public const int Common_33 = 260060;    // "공격력");
        public const int Common_34 = 260061;    // "방어력");
        public const int Common_35 = 260062;    // "채집");
        public const int Common_36 = 260063;    // "엘리 획득량");
        public const int Common_37 = 260064;    // "아이템 판매시");
        public const int Common_38 = 260065;    // "증가");
        public const int Common_39 = 260066;    // "할인");

        public const int Common_40 = 260067;    // "비용");
        public const int Common_41 = 260068;    // "연마");
        public const int Common_42 = 260069;    // "오류");
        public const int Common_43 = 260070;    // "길드");
        public const int Common_44 = 260071;    // "받기");

        public const int Common_45 = 260072;    // "확인", E_TEXTTYPE.LOCAL);
        public const int Common_46 = 260073;    // "포기");
        public const int Common_48 = 260074;    // "취소", E_TEXTTYPE.LOCAL);
        public const int Common_49 = 260075;    // "장착하기");

        public const int Common_50 = 260076;    // "현재");
        public const int Common_51 = 260077;    // "다음");
        public const int Common_52 = 260078;    // "지역");
        public const int Common_53 = 260079;    // "귓속말");
        public const int Common_54 = 260080;    // "나");
        public const int Common_55 = 260081;    // "시스템"); //시스템 메세지.
        public const int Common_56 = 260082;    // "시스템"); //획득 메세지.

        public const int Common_57 = 260083;    // "펫 티켓");
        public const int Common_58 = 260084;    // "아이템 티켓");
        public const int Common_59 = 260085;    // "바로가기");
        public const int Common_60 = 260086;    // "시나리오");
        public const int Common_61 = 260087;    // "진행맵");
        public const int Common_62 = 260088;    // "매칭하기");
        public const int Common_63 = 260089;    // "취소하기");
        public const int Common_64 = 260090;    // "장착레벨");
        public const int Common_65 = 260091;    // "종류");
        public const int Common_66 = 260092;    // "요구레벨");
        public const int Common_67 = 260093;    // "LV.");
        public const int Common_68 = 260094;    // "튜토리얼");
        public const int Common_69 = 260095;    // "필드오픈");
        public const int Common_70 = 260096;    // "히든");
        public const int Common_71 = 260097;    // "임무");
        public const int Common_72 = 260098;    // "열기");
        public const int Common_73 = 260099;    // "사용");
        public const int Common_74 = 260100;    // "미획득");
        public const int Common_75 = 260101;    // "알림");
        public const int Common_76 = 260102;    // "돌파");
        public const int Common_77 = 260103;    // "진행");
        public const int Common_78 = 260104;    // "반복");
        public const int Common_79 = 260105;    // "연계");
        public const int Common_80 = 260106;    // "퀘스트");
        public const int Common_81 = 260107;    // "오늘의 퀘스트");
        public const int Common_82 = 260108;    // "일일");
        public const int Common_83 = 260109;    // "이벤트");
        public const int Common_84 = 260110;    // "줄");
        public const int Common_85 = 260111;    // "안전지대입니다.");
        public const int Common_86 = 260112;    // "추천 Lv.{0}");
        public const int Common_87 = 260113;    // "{0}의 파티");
        public const int Common_88 = 260114;    // "착용 가능 무기: ");
        public const int Common_89 = 260115;    // "AUTO");
        public const int Common_90 = 260116;    // "레벨제한");
        public const int Common_91 = 260117;    // "완료횟수");
        public const int Common_92 = 260118;    // "일일 완료 횟수");
        public const int Common_93 = 260119;    // "성공");
        public const int Common_94 = 260120;    // "화면을 터치해주세요");
        public const int Common_95 = 250112;    // "자동이동중...");
        public const int Common_96 = 260121;    // "무료");
        public const int Common_97 = 260122;    // "프리미엄");
        public const int Common_98 = 261024;    // "충전");
        public const int Common_99 = 261025;    // "닫기");

        public const int Common_100 = 260123;    // "직업");
        public const int Common_101 = 260124;    // "종류");
        public const int Common_102 = 260125;    // "레벨");
        public const int Common_103 = 260126;    // "성별");
        public const int Common_104 = 260127;    // "내구도");
        public const int Common_105 = 260128;    // "연마");
        public const int Common_106 = 260129;    // "펫");
        public const int Common_107 = 260904;    // "펫 인벤토리");
        public const int Common_108 = 260905;    // "아이템 인벤토리");

        public const int Common_109 = 260947;    // "헤어스타일");
        public const int Common_110 = 260948;    // "얼굴모양");

        public const int Common_111 = 260949;    // "다음");

        public const int Common_112 = 260950;    // "스킬");

        public const int Common_113 = 260951;    // "체력");
        public const int Common_114 = 260952;    // "방어력");
        public const int Common_115 = 260953;    // "공격력");

        public const int Common_116 = 260954;    // "전직");

        public const int Common_117 = 260955;    // "HP");
        public const int Common_118 = 260956;    // "전투력"); // 공격력?
        public const int Common_119 = 260957;    // "마법 방어력");
        public const int Common_120 = 260958;    // "힘");
        public const int Common_121 = 260959;    // "지능");
        public const int Common_122 = 260960;    // "민첩");
        public const int Common_123 = 260961;    // "행운");
        public const int Common_124 = 260962;    // "크리티컬 포인트");
        public const int Common_125 = 260963;    // "크리티컬 데미지");
        public const int Common_126 = 260964;    // "채집 포인트");

        public const int Common_127 = 260965;    // "계정보드");

        public const int Common_128 = 261895;    // "MP");
        public const int Common_129 = 261893;    // "JOBLVEXP");
        public const int Common_130 = 261894;    // "FP");

        public const int Common_200 = 260130;    // "EXP");
        public const int Common_201 = 260131;    // "강화");
        public const int Common_202 = 260132;    // "속성");
        public const int Common_203 = 260133;    // "종류");
        public const int Common_204 = 260134;    // "무기");

        public const int Common_205 = 261026;    // "리셋");
        public const int Common_206 = 261027;    // "모두구매");
        public const int Common_207 = 261028;    // "구매가격");

        public const int Common_208 = 261029;    // "얼굴");
        public const int Common_209 = 261030;    // "헤어");
        public const int Common_210 = 261031;    // "염색");

        public const int Common_211 = 261032;    // "전체");
        public const int Common_212 = 261033;    // "특별");

        public const int Common_213 = 261034;    // "지급");

        public const int Common_214 = 261035;    // "일간");
        public const int Common_215 = 261036;    // "주간");
        public const int Common_216 = 261037;    // "월간");

        public const int Common_217 = 261038;    // "열쇠");
        public const int Common_218 = 261039;    // "버프");
        public const int Common_219 = 261040;    // "염색약");
        public const int Common_220 = 261041;    // "포션");

        public const int Common_221 = 261042;    // "아이템");
        public const int Common_222 = 261043;    // "가방");
        public const int Common_223 = 261044;    // "승급");

        public const int Common_224 = 261045;    // "가방");
        public const int Common_225 = 261046;    // "상세보기");
        public const int Common_226 = 261047;    // "칭호");

        public const int Common_227 = 261048;    // "내구도");
        public const int Common_228 = 261049;    // "연마횟수");
        public const int Common_229 = 261050;    // "진화");
        public const int Common_230 = 261051;    // "초월");

        public const int Common_231 = 261052;    // "획득 아이템");
        public const int Common_232 = 261053;    // "획득 장소");

        public const int Common_233 = 261054;    // "등급");
        public const int Common_234 = 261055;    // "기본 공격력");
        public const int Common_235 = 261056;    // "스킬 대미지");

        public const int Common_236 = 262150;    // "회복");

        public const int Common_237 = 262151;    // "보상 아이템");

        public const int Common_251 = 261057;    // "레벨 상승");

        public const int Common_252 = 261058;    // "삭제");
        public const int Common_253 = 262152;    // "랭킹보기");
        public const int Common_254 = 262153;    // "대화");

        public const int Common_255 = 262154;    // "보유중");

        public const int Common_256 = 262155;    // "경험치 획득량");
        public const int Common_257 = 262156;    // "스킬공격력");
        public const int Common_258 = 262157;    // "레벨강화");

        public const int Common_259 = 262158;    // "실패");
        public const int Common_260 = 262159;    // "기본");

        public const int Common_261 = 261438;    // "관리");
        public const int Common_262 = 261439;    // "검색");
        public const int Common_263 = 261440;    // "탈퇴");

        public const int Common_264 = 261441;    // "보상");

        public const int Common_265 = 261442;    // "가입");
        public const int Common_266 = 261443;    // "자세히");

        public const int Common_300 = 260135;    // "구매");
        public const int Common_301 = 260136;    // "판매");

        public const int Common_401 = 260137;    // "입어보기");
        public const int Common_402 = 260138;    // "해제하기");
        public const int Common_403 = 260139;    // "미리보기");

        public const int Common_500 = 260140;    // "난이도");
        public const int Common_501 = 260141;    // "추천");
        public const int Common_502 = 260142;    // "요구조건");

        public const int Common_503 = 260966;    // "보기");

        public const int Common_521 = 262160;    // "제작 아이템");
        public const int Common_522 = 262161;    // "제료 아이템");

        public const int Common_531 = 262162;    // "정보");
        public const int Common_532 = 262163;    // "장비");
        public const int Common_533 = 262164;    // "콤보");

        public const int Common_541 = 261795;    // "착용중");
        public const int Common_542 = 261796;    // "성별제한");
        public const int Common_543 = 261797;    // "직업제한");
        public const int Common_544 = 261798;    // "레벨제한");

        public const int Common_551 = 261836;    // "HP 포션");

        public const int Common_561 = 261853;    // "자동사냥중...");
        public const int Common_562 = 261854;    // "자동퀘스트중...");

        public const int Common_571 = 261912;    // "마지막 접속 위치:");

        public const int Common_581 = 261915;    // "펫속성");
        public const int Common_582 = 261916;    // "포션 사용 가능 개수");
        public const int Common_583 = 261917;    // "무제한");

        public const int Common_591 = 262001;    // "감소");

        public const int Common_593 = 262061;    // "던전");
        public const int Common_594 = 262062;    // "거래불가");
        public const int Common_595 = 262063;    // "거래가능");
        public const int Common_596 = 262064;    // "염색");

        public const int Common_597 = 262091;    // "연마 수치");

        public const int Common_598 = 262407;         // "제한 없음"

        public const int Common_599 = 262424;         // "잠금"
        public const int Common_600 = 262425;         // "배치"

        public const int Common_601 = 262453;         // "한정"

        public const int Common_602 = 262503;         // "{0} 까지"

        public const int Common_603 = 262534;              // "삭제 대기 중인 캐릭터는 펫을 장착할 수 없습니다."
        public const int Common_604 = 262550;         // 전용
        public const int Common_605 = 262551;        // 타입
        public const int Common_606 = 262552;        // 속성 보너스
        public const int Common_607 = 262553;        // 스킬강화
        public const int Common_608 = 262554;        // 쿨타임
        public const int Common_609 = 262555;        // 소모
        public const int Common_610 = 262556;        // 마나
        public const int Common_611 = 262557;        // 분노

        public const int Common_612 = 262575;             // 이계 밸로스
        public const int Common_613 = 262576;             // 이계 엘리아스
        public const int Common_614 = 262577;             // 이계 용경
        public const int Common_615 = 262578;             // 이계 아오이치
        public const int Common_616 = 262579;             // 이계 엘파

        public const int Common_617 = 262595;            // 공용
        public const int Common_618 = 262600;            // 참가하기
        public const int Common_619 = 263072;            // 옵션 변경

        public const int Common_620 = 172968;            //"한정 {0}회";
        public const int Common_621 = 172969;            //"일간 {0}회";
        public const int Common_622 = 172970;            //"주간 {0}회";
        public const int Common_623 = 172971;            //"월간 {0}회";

        public const int Common_624 = 263668;            // 랭킹 정보가 없습니다.
        public const int Common_625 = 240074;            // 랭킹 시즌이 아닙니다.

        #endregion 기타(공용)

        #region 직업명

        public const int JobName_0 = 260967;    // "전사");
        public const int JobName_1 = 260968;    // "기사");
        public const int JobName_2 = 260969;    // "마법사");
        public const int JobName_3 = 260970;    // "탐험가");

        public const int JobName_4 = 260971;    // "블레이더");
        public const int JobName_5 = 260972;    // "워로드");
        public const int JobName_6 = 260973;    // "템플나이트");
        public const int JobName_7 = 260974;    // "가디언");
        public const int JobName_8 = 260975;    // "소서러");
        public const int JobName_9 = 260976;    // "아티스트");
        public const int JobName_10 = 260977;    // "트레저헌터");
        public const int JobName_11 = 260978;    // "건슬링거");

        public const int JobName_12 = 261059;    // "공통");
        public const int JobName_13 = 261060;    // "백수");

        #endregion

        #region 아이템 타입

        public const int ItemType_0 = 261061;    // "모자");
        public const int ItemType_1 = 261062;    // "상의");
        public const int ItemType_2 = 261063;    // "하의");
        public const int ItemType_3 = 261064;    // "망토");

        public const int ItemType_11 = 261065;    // "한손검");
        public const int ItemType_12 = 261066;    // "둔기");
        public const int ItemType_13 = 261067;    // "너클");
        public const int ItemType_14 = 261068;    // "양손검");
        public const int ItemType_15 = 261069;    // "스피어");
        public const int ItemType_16 = 261070;    // "지팡이");
        public const int ItemType_17 = 261071;    // "단검");
        public const int ItemType_18 = 261072;    // "활");
        public const int ItemType_19 = 261073;    // "석궁");
        public const int ItemType_20 = 261074;    // "쌍검");
        public const int ItemType_21 = 261075;    // "기타");
        public const int ItemType_22 = 261076;    // "총");
        public const int ItemType_23 = 261077;    // "방패");
        public const int ItemType_24 = 261078;    // "보조장비");
        public const int ItemType_25 = 261079;    // "투구");
        public const int ItemType_26 = 261080;    // "갑옷");
        public const int ItemType_27 = 261081;    // "장갑");
        public const int ItemType_28 = 261082;    // "신발");
        public const int ItemType_29 = 261083;    // "목걸이");
        public const int ItemType_30 = 261084;    // "반지");
        public const int ItemType_31 = 261085;    // "귀걸이");
        public const int ItemType_32 = 261086;    // "룬");
        public const int ItemType_33 = 261087;    // "퍼즐");
        public const int ItemType_34 = 261088;    // "속성석");
        public const int ItemType_35 = 261089;    // "기타");
        public const int ItemType_36 = 261090;    // "상자");
        public const int ItemType_37 = 261091;    // "코스튬 염색약");
        public const int ItemType_38 = 261092;    // "코스튬 랜덤 염색약");
        public const int ItemType_39 = 261093;    // "칭호 아이템");
        public const int ItemType_40 = 261094;    // "소셜모션");
        public const int ItemType_41 = 261095;    // "티켓");
        public const int ItemType_42 = 261096;    // "초기화권");
        public const int ItemType_43 = 261097;    // "초기화권");
        public const int ItemType_44 = 261098;    // "초기화권");
        public const int ItemType_45 = 261099;    // "초기화권");
        public const int ItemType_46 = 261100;    // "랜덤 헤어 티켓");
        public const int ItemType_47 = 261101;    // "랜덤 얼굴 티켓");
        public const int ItemType_48 = 261102;    // "악세사리");
        public const int ItemType_49 = 261103;    // "수정구");
        public const int ItemType_50 = 261104;    // "채집물");
        public const int ItemType_51 = 261105;    // "도안");
        public const int ItemType_52 = 261106;    // "머리");
        public const int ItemType_53 = 261107;    // "가구");
        public const int ItemType_54 = 262411;    // 스킬강화포인트");
        public const int ItemType_55 = 262544;    // 모든 무기
        public const int ItemType_56 = 262613;    // 펫장비
        public const int ItemType_74 = 263793;         // 소셜아이템

        public const int ItemType_101 = 261108;    // "체력회복 포션");

        #endregion 아이템 타입

        #region 몬스터 타입

        public const int MonsterType_0 = 261109;    // "데이터 확인");

        public const int MonsterType_1 = 261110;    // "인간");
        public const int MonsterType_2 = 261111;    // "야수");
        public const int MonsterType_3 = 261112;    // "곤충");
        public const int MonsterType_4 = 261113;    // "식물");
        public const int MonsterType_5 = 261114;    // "언데드");
        public const int MonsterType_6 = 261115;    // "기계");
        public const int MonsterType_7 = 261116;    // "요정");
        public const int MonsterType_8 = 261117;    // "악마");

        #endregion

        #region 속성 타입

        public const int AttributeType_0 = 261118;    // "무속성");

        public const int AttributeType_1 = 261119;    // "불");
        public const int AttributeType_2 = 261120;    // "물");
        public const int AttributeType_3 = 261121;    // "바람");
        public const int AttributeType_4 = 261122;    // "땅");
        public const int AttributeType_5 = 261123;    // "어둠");

        #endregion

        #region 성별

        public const int GenderType_0 = 261124;    // "남자");
        public const int GenderType_1 = 261125;    // "여자");

        #endregion

        #region 옵션 타입

        #region 옵션 이름

        public const int OptionType_0 = 261126;    // "최대 생명력");
        public const int OptionType_1 = 261127;    // "최대 마법력");
        public const int OptionType_2 = 261128;    // "최소 공격력");
        public const int OptionType_3 = 261129;    // "최대 공격력");
        public const int OptionType_4 = 261130;    // "모든 스킬 공격력");
        public const int OptionType_5 = 261131;    // "물리 방어력");
        public const int OptionType_6 = 261132;    // "명중률");
        public const int OptionType_7 = 261133;    // "회피율");
        public const int OptionType_8 = 261134;    // "이동 속도");
        public const int OptionType_9 = 261135;    // "점프 파워");
        public const int OptionType_10 = 261136;    // "치유력");
        public const int OptionType_11 = 261137;    // "획득 경험치");
        public const int OptionType_12 = 261138;    // "엘리 획득양");
        public const int OptionType_13 = 261139;    // "공격 성공시 생명력");
        public const int OptionType_14 = 261140;    // "아이템 획득 확률");
        public const int OptionType_15 = 261141;    // "속성 부여");
        public const int OptionType_16 = 261142;    // "속성 스킬 대미지 증가");
        public const int OptionType_17 = 261143;    // "타입 대미지 증가");
        public const int OptionType_18 = 261144;    // "스킬 넉백 증가");
        public const int OptionType_19 = 261145;    // "스턴 효과 부여");
        public const int OptionType_20 = 261146;    // "슬립 효과 부여");
        public const int OptionType_21 = 261147;    // "이동 불가 효과 부여");
        public const int OptionType_22 = 261148;    // "착용 제한 레벨 감소");
        public const int OptionType_23 = 261149;    // "물리 면역");
        public const int OptionType_24 = 261150;    // "마법 면역");
        public const int OptionType_25 = 261151;    // "모든 피해 면역");
        public const int OptionType_26 = 261152;    // "상태이상 면역");
        public const int OptionType_27 = 261153;    // "효과 부여");
        public const int OptionType_28 = 262165;    // "스킬 공격력");
        public const int OptionType_30 = 261714;    // "침묵");
        public const int OptionType_31 = 261715;    // "MP소모량 감소");
        public const int OptionType_32 = 261716;    // "MP회복량 증가");
        public const int OptionType_33 = 261717;    // "분노소모량 감소");
        public const int OptionType_34 = 261718;    // "분노회복량 증가");
        public const int OptionType_35 = 261719;    // "MAX 분노 증가");
        public const int OptionType_36 = 261720;    // "스킬 쿨타임 감소");
        public const int OptionType_37 = 261721;    // "타격가능 개체수 증가");
        public const int OptionType_38 = 261722;    // "특정무기 장착시 공격력 증가");
        public const int OptionType_39 = 261723;    // "도트 타입");
        public const int OptionType_40 = 261724;    // "특정스킬 추가 및 레벨업");
        public const int OptionType_41 = 261725;    // "특정 타입 몬스터에게 스킬추가 데미지");
        public const int OptionType_42 = 261726;    // "특정 속성 몬스터 스킬 추가데미지");
        public const int OptionType_43 = 261727;    // "특정무기 장착시 마법 방어력 증가");
        public const int OptionType_44 = 261728;    // "특정무기 장착시 명중스텟 증가");
        public const int OptionType_45 = 261729;    // "특정무기 장착시 회피스텟 증가");
        public const int OptionType_46 = 261730;    // "특정무기 장착시 치명타율 스텟 증가");
        public const int OptionType_47 = 261731;    // "특정무기 장착시 치명타 데미지 증가");
        public const int OptionType_48 = 261732;    // "크리티컬 포인트 증가");
        public const int OptionType_49 = 261733;    // "가디언-헌신");
        public const int OptionType_50 = 261734;    // "평타 데미지 증가");
        public const int OptionType_51 = 261735;    // "힐스킬 마나소모량 감소");
        public const int OptionType_52 = 261736;    // "힐스킬 쿨타임 감소");
        public const int OptionType_53 = 261737;    // "버프지속시간 증가");
        public const int OptionType_54 = 261738;    // "버프스킬 마나소모량 감소");
        public const int OptionType_55 = 261739;    // "버프스킬 쿨타임 감소");
        public const int OptionType_56 = 261740;    // "평타 타깃수 증가");
        public const int OptionType_57 = 261741;    // "특정 무기 장착시 데미지 증가");
        public const int OptionType_58 = 261742;    // "평타,특정 속성 몬스터에게 추뎀");
        public const int OptionType_59 = 261743;    // "평타,특정 타입 몬스터에게 추뎀");
        public const int OptionType_60 = 261744;    // "평타,확률로 디버프걸기(중독,상처)");
        public const int OptionType_61 = 261745;    // "데미지 감소");
        public const int OptionType_62 = 261746;    // "용기의 오로라(공,방,치명) 증가");
        public const int OptionType_63 = 261747;    // "방어막");
        public const int OptionType_64 = 261748;    // "버서커(방어력감소,평타뎀지증가)");
        public const int OptionType_65 = 261749;    // "방어력 감소");
        public const int OptionType_66 = 261750;    // "몬스터에게 받는 데미지 감소");
        public const int OptionType_67 = 261751;    // "보스몬스터에게 추가데미지(스킬,평타)");
        public const int OptionType_68 = 261752;    // "펫스킬 데미지 증가%(skill.default_dmg 기준)");
        public const int OptionType_69 = 261753;    // "데미지 증가(최종 데미지의 %)");
        public const int OptionType_70 = 262563;         // 장착 시 스킬데미지

        #endregion

        #region 옵션 변경 텍스트

        public const int OptionChangeType_0 = 261154;    // "힘 {0} 증가");
        public const int OptionChangeType_1 = 261155;    // "지능 {0} 증가");
        public const int OptionChangeType_2 = 261156;    // "민첩 {0} 증가");
        public const int OptionChangeType_3 = 261157;    // "행운 {0} 증가");
        public const int OptionChangeType_4 = 261158;    // "최대 생명력 {0} 증가");
        public const int OptionChangeType_5 = 261159;    // "최대 마법력 {0} 증가");
        public const int OptionChangeType_6 = 261160;    // "최소 공격력 {0} 증가");
        public const int OptionChangeType_7 = 261161;    // "최대 공격력 {0} 증가");
        public const int OptionChangeType_8 = 261162;    // "공격력 {0} 증가");
        public const int OptionChangeType_9 = 261163;    // "모든 스킬 공격력 {0} 증가");
        public const int OptionChangeType_10 = 261164;    // "물리 방어력 {0} 증가");
        public const int OptionChangeType_11 = 261165;    // "마법 방어력 {0} 증가");
        public const int OptionChangeType_12 = 261166;    // "명중률 {0} 증가");
        public const int OptionChangeType_13 = 261167;    // "회피율 {0} 증가");
        public const int OptionChangeType_14 = 261168;    // "크리티컬 포인트 {0} 증가");
        public const int OptionChangeType_15 = 261169;    // "크리티컬 데미지 {0} 증가");
        public const int OptionChangeType_16 = 261170;    // "이동 속도 {0} 증가");
        public const int OptionChangeType_17 = 261171;    // "점프 파워 {0} 증가");
        public const int OptionChangeType_18 = 261172;    // "치유력 {0} 증가");
        public const int OptionChangeType_19 = 261173;    // "획득 경험치 {0} 증가");
        public const int OptionChangeType_20 = 261174;    // "엘리 획득양 {0} 증가");
        public const int OptionChangeType_21 = 261175;    // "공격 성공시 생명력 {0}% 증가");
        public const int OptionChangeType_22 = 261176;    // "아이템 획득 확률 {0} 증가");
        public const int OptionChangeType_23 = 261177;    // "{0} 속성 부여");
        public const int OptionChangeType_24 = 261178;    // "{0} 속성 스킬 대미지 {1} 증가");
        public const int OptionChangeType_25 = 261179;    // "{0} 타입 대미지 {1} 증가");
        public const int OptionChangeType_26 = 261180;    // "스킬 넉백 {0} 증가");
        public const int OptionChangeType_27 = 261181;    // "착용 제한 레벨 {0} 감소");
        public const int OptionChangeType_28 = 261182;    // "{0} 효과 부여");
        public const int OptionChangeType_29 = 261754;    // "MP소모량 {0} 감소");
        public const int OptionChangeType_30 = 261755;    // "MP회복량 {0} 증가");
        public const int OptionChangeType_31 = 261756;    // "분노소모량 {0} 감소");
        public const int OptionChangeType_32 = 261757;    // "분노회복량 {0} 증가");
        public const int OptionChangeType_33 = 261758;    // "MAX 분노 {0} 증가");
        public const int OptionChangeType_34 = 261759;    // "스킬 쿨타임 {0} 감소");
        public const int OptionChangeType_35 = 261760;    // "타격가능 개체수 {0} 증가");
        public const int OptionChangeType_36 = 261761;    // "{0} 무기 장착시 공격력 {1} 증가");
        public const int OptionChangeType_37 = 261762;    // "{0} 스킬 추가 및 레벨업");
        public const int OptionChangeType_38 = 261763;    // "{0} 타입 몬스터에게 스킬추가 {1} 데미지");
        public const int OptionChangeType_39 = 261764;    // "{0} 속성 몬스터 스킬 {1} 추가데미지");
        public const int OptionChangeType_40 = 261765;    // "{0} 무기 장착시 마법 방어력 {1} 증가");
        public const int OptionChangeType_41 = 261766;    // "{0} 무기 장착시 명중스텟 {1} 증가");
        public const int OptionChangeType_42 = 261767;    // "{0} 무기 장착시 회피스텟 {1} 증가");
        public const int OptionChangeType_43 = 261768;    // "{0} 무기 장착시 치명타율 스텟 {1} 증가");
        public const int OptionChangeType_44 = 261769;    // "{0} 무기 장착시 치명타 데미지 {1} 증가");
        public const int OptionChangeType_45 = 261770;    // "크리티컬 포인트 {0} 증가");
        public const int OptionChangeType_46 = 261771;    // "평타 데미지 {0} 증가");
        public const int OptionChangeType_47 = 261772;    // "힐스킬 마나소모량 {0} 감소");
        public const int OptionChangeType_48 = 261773;    // "힐스킬 쿨타임 {0} 감소");
        public const int OptionChangeType_49 = 261774;    // "버프지속시간 {0} 증가");
        public const int OptionChangeType_50 = 261775;    // "버프스킬 마나소모량 {0} 감소");
        public const int OptionChangeType_51 = 261776;    // "버프스킬 쿨타임 {0} 감소");
        public const int OptionChangeType_52 = 261777;    // "평타 타깃수 {0} 증가");
        public const int OptionChangeType_53 = 261778;    // "{0} 무기 장착시 데미지 {1} 증가");
        public const int OptionChangeType_54 = 261779;    // "평타,{0} 속성 몬스터에게 {1} 추뎀");
        public const int OptionChangeType_55 = 261780;    // "평타,{0} 타입 몬스터에게 {1} 추뎀");
        public const int OptionChangeType_56 = 261781;    // "평타,확률로 디버프걸기(중독,상처)");
        public const int OptionChangeType_57 = 261782;    // "데미지 {0} 감소");
        public const int OptionChangeType_58 = 261783;    // "용기의 오로라(공,방,치명) {0}% 증가");
        public const int OptionChangeType_59 = 261784;    // "방어막");
        public const int OptionChangeType_60 = 261785;    // "버서커(방어력 {0}% 감소, 평타뎀지 {1}% 증가)");
        public const int OptionChangeType_61 = 261786;    // "방어력 {0} 감소");
        public const int OptionChangeType_62 = 261787;    // "몬스터에게 받는 데미지 {0} 감소");
        public const int OptionChangeType_63 = 261788;    // "보스몬스터에게 {0} 추가데미지(스킬,평타)");
        public const int OptionChangeType_64 = 261789;    // "펫스킬 데미지 {0}% 증가(skill.default_dmg 기준)");
        public const int OptionChangeType_65 = 261790;    // "데미지 {0}% 증가(최종 데미지의 %)");

        #endregion

        #endregion

        #region 하우징 UI에서 사용 

        public const int Housing_0 = 262166;    // "내 꾸미기 점수");
        public const int Housing_1 = 262167;    // "내 좋아요 점수");
        public const int Housing_2 = 262198;    // "집 스킨");
        public const int Housing_3 = 262169;    // "배경 스킨");
        public const int Housing_4 = 262170;    // "미리보기 이미지가 없습니다");
        public const int Housing_5 = 262171;    // "적용중");
        public const int Housing_6 = 262172;    // "미적용");
        public const int Housing_7 = 262173;    // "소지하지 않음");
        public const int Housing_8 = 262174;    // "꾸미기점수 {0}");
        public const int Housing_9 = 262175;    // "설치된 가구");
        public const int Housing_10 = 262176;    // "배치된 펫");
        public const int Housing_11 = 262177;    // "꾸미기 수치 상승");
        public const int Housing_12 = 262178;    // "꾸미기 수치 하락");
        public const int Housing_13 = 262179;    // "층을 더이상 올릴 수 없습니다.");
        public const int Housing_14 = 262180;    // "펫 등급 개수");
        public const int Housing_15 = 262181;    // "판매종료일");
        public const int Housing_16 = 261837;    // "배치중");
        public const int Housing_17 = 261838;    // "미배치");
        public const int Housing_18 = 261839;    // "Upgrade");
        public const int Housing_19 = 262093;    // "가구 획득 하기");
        public const int Housing_20 = 262094;    // "마을의 NPC에게 제작 및 구매가 가능합니다.");
        public const int Housing_21 = 262095;    // "가구 구매");
        public const int Housing_22 = 262096;    // "가구 제작");

        public const int Housing_23 = 262415; // 경험치 획득이 가능합니다.  -> HousingEditingMode 에 적용할것
        public const int Housing_24 = 262416; // 배치하기  -> HousingEditingMode 에 적용할것
        public const int Housing_25 = 262417; // 배치불가 영역  -> HousingEditingMode 에 적용할것
        public const int Housing_26 = 262418; // {0}분 남음  -> HousingEditingMode, HousingFurniturePlacingUIView 에 적용할것

        public const int Housing_30 = 262419; // 업그레이드 가능(편집모드)  -> HousingFurniturePlacingUIView 에 적용할것
        public const int Housing_31 = 262420; // 배치 시 업그레이드 가능  -> HousingFurniturePlacingUIView 에 적용할것
        public const int Housing_32 = 262421; // 경험치 획득 불가  -> HousingFurniturePlacingUIView 에 적용할것

        public const int Housing_33 = 262422; // ( 업그레이드:[0DFF0DFF]{0}점[/c] [0c0f0c0f]▲[/c] ) : 이거 가구 업그레이드 UI 에 있던 건데 잘하면 안 쓸 수도 있어서 보류
        public const int Housing_34 = 262525;      // 경험치 {0}단계
        public const int Housing_35 = 262526;      // 가구 위에 다른 가구가 있어, 옮길 수 없습니다.
        public const int Housing_36 = 263346;        // 가구 타입
        public const int Housing_37 = 263587;        // 외형 변경
        public const int Housing_38 = 263598;        // 꾸미기 점수
        public const int Housing_39 = 263599;        // 조건 완료
        public const int Housing_40 = 263600;        // 업그레이드 재료
        public const int Housing_41 = 263669;      // 좋아요 점수

        public const int Furni_0 = 262182;    // "천장 가구");
        public const int Furni_1 = 262183;    // "커튼");
        public const int Furni_2 = 262184;    // "바닥 가구");
        public const int Furni_3 = 262185;    // "러그");
        public const int Furni_4 = 262186;    // "벽 가구");
        public const int Furni_5 = 262187;    // "가구");

        #endregion

        #region 캐릭터 선택창 사용

        public const int CharacterSelect_0 = 260151;    // "라테일 어딘가");
        public const int CharacterSelect_1 = 260152;    // "길드가 없습니다.");
        public const int CharacterSelect_2 = 260153;    // "대표캐릭터로 선택됨");
        public const int CharacterSelect_3 = 260154;    // "대표캐릭터로 변경");

        public const int CharacterSelect_4 = 260979;    // "캐릭터 선택");
        public const int CharacterSelect_5 = 260980;    // "캐릭터 정보");
        public const int CharacterSelect_6 = 260981;    // "게임 시작");
        public const int CharacterSelect_7 = 260982;    // "캐릭터 삭제");
        public const int CharacterSelect_8 = 260983;    // "캐릭터 생성");
        public const int CharacterSelect_9 = 260984;    // "전직 미리보기");
        public const int CharacterSelect_10 = 260985;    // "캐릭터 생성");
        public const int CharacterSelect_11 = 260986;    // "8자 이내 닉네임");
        public const int CharacterSelect_12 = 262188;    // "터치 시 스킬을 사용합니다.");
        public const int CharacterSelect_13 = 262189;    // "캐릭터 성별");
        public const int CharacterSelect_14 = 262190;    // "직업 선택");
        public const int CharacterSelect_15 = 262191;    // "삭제 대기중");

        public const int CharacterSelect_16 = 260897;         // "이미 존재하는 캐릭터 이름입니다."

        public const int CharacterDesc_1 = 260987;    // "체력과 파괴력을 바탕으로 양손무기를 사용하는 근접전의 전문가입니다.");
        public const int CharacterDesc_2 = 260988;    // "방어력과 균형잡힌 능력치로 안정적인 전투에 특화되어 초반 육성이 쉬운 근거리 전투가입니다.");
        public const int CharacterDesc_3 = 260989;    // "체력은 낮지만 높은 지력과 마나로 강한 공격과 스킬을 사용하는 강력한 딜러입니다.");
        public const int CharacterDesc_4 = 260990;    // "민첩과 행운을 기반한 빠르고 치명적인 공격으로 적을 견제하는 원거리 공격의 전문가입니다.");

        public const int CharacterNameBlank = 260155;    // "이름이 비어있습니다.");
        public const int CharacterNameInvalid = 260156;  // "사용 불가능한 문자가 포함되었습니다.");
        public const int CharacterNameSpace = 260157;    // "이름에 공백문자를 포함시킬 수 없습니다.");
        public const int CharacterNameOverflow = 262192;    // "8자 이하의 이름만 사용할 수 있습니다.");
        public const int CharacterNameShort = 262007;    // "닉네임이 너무 짧습니다. 2자 이상 입력해주세요. ");

        public const int CharacterCustomizing_1 = 262065;    // "한글 8자 이내, 영문 16자 이내");

        #endregion 캐릭터 선택창 사용

        #region 캐릭터 상세보기 창에서 사용

        public const int FriendDetail_0 = 260991;    // "상세보기");
        public const int FriendDetail_1 = 260992;    // "캐릭터선택");
        public const int FriendDetail_2 = 260993;    // "상세 정보");
        public const int FriendDetail_3 = 260994;    // "친구 요청");
        public const int FriendDetail_4 = 260995;    // "권한 변경");
        public const int FriendDetail_5 = 260996;    // "집방문");

        #endregion 캐릭터 상세보기 창에서 사용

        #region 월드맵에서 사용

        public const int WorldMap_0 = 262193;    // "지도");
        public const int WorldMap_1 = 262194;    // "출현 몬스터");
        public const int WorldMap_2 = 262195;    // "맵 정보");
        public const int WorldMap_3 = 262196;    // "파티 정보");

        #endregion

        #region 마을 워프에서 사용

        public const int TownWarp_0 = 262197;    // "마을워프");
        public const int TownWarp_1 = 262198;    // "마을이름");
        public const int TownWarp_2 = 262199;    // "이동가격");
        public const int TownWarp_3 = 262200;    // "이동하기");

        #endregion 마을 워프에서 사용

        #region 캐시 상점에서 사용

        public const int CashShop_0 = 262201;    // "캐시상점");
        public const int CashShop_1 = 262608;         // 상품이 구매되었습니다. 우편함을 확인하여 주세요.
        public const int CashShop_2 = 262609;         // 상품을 구매하는데 실패하였습니다.
        public const int CashShop_3 = 262610;         // 상품을 구매하는데 취소하였습니다.
        public const int CashShop_4 = 262611;         // 다른 상품의 구매가 처리되고 있는 중입니다.
        public const int CashShop_5 = 262612;         // 상품 거래 정보를 찾아오지 못했습니다.
        public const int CashShop_6 = 262614; // 구매시 [FC2FFC2F][f35]{0}%[/f][/c]\n보너스 지급!
        public const int CashShop_7 = 262615; // 다음 보너스\n구매까지\n[FC2FFC2F][f35]{0}[/f][/c] 남음
        public const int CashShop_8 = 263231;        // "블루\n다이아"

        #endregion 캐시 상점에서 사용

        #region 뽑기 상점에서 사용

        public const int GachaShop_0 = 262202;    // "뽑기상점");
        public const int GachaShop_1 = 262203;    // "뽑기");
        public const int GachaShop_2 = 261444;    // "후 무료 뽑기 가능");
        public const int GachaShop_3 = 261445;    // "횟수");

        public const int GachaShop_4 = 260032;    // "펫 뽑기");
        public const int GachaShop_5 = 263180;    // "화려하고 다양한 펫을 뽑아보세요!");
        public const int GachaShop_6 = 260030;    // "아이템 뽑기");
        public const int GachaShop_7 = 263181;    // "장비를 뽑아서 더욱 강력해지세요!");
        public const int GachaShop_8 = 263333;    // "코스튬 뽑기");
        public const int GachaShop_9 = 263334;    // "아름답고 멋있는 코스튬으로 꾸며보세요!");

        #endregion 뽑기 상점에서 사용

        #region PVP 필드 UI에서 사용

        public const int PvpField_0 = 262204;    // "상대방을 처치하고 점수를 지켜내세요.");
        public const int PvpField_1 = 262205;    // " 적을 처치하고 점수를 획득하세요.");

        public const int PvpSeason_0 = 262206;    // "하루 최대 {0}점");
        public const int PvpSeason_1 = 262207;    // "시즌 정보가 없습니다.");
        public const int PvpSeason_2 = 262208;    // "하루 최대 -점");
        public const int PvpSeason_3 = 262209;    // "오늘 획득");

        #endregion

        #region 상점에서 사용

        public const int NPCShop_0 = 262210;    // "상점");
        public const int NPCShop_1 = 262211;    // "합계");
        public const int NPCShop_2 = 262212;    // "모두판매");

        public const int NPCShop_3 = 262102;    // 높은 등급의 아이템입니다.\n판매 후 복구할 수 없습니다. 
        public const int NPCShop_4 = 262103;    // 퍼즐이 장착된 아이템입니다.\n판매 후 복구 할 수 없습니다.

        public const int NPCShop_5 = 262104;    // 높은 등급의 펫입니다.\n판매 후 복구할 수 없습니다.
        public const int NPCShop_6 = 262105;    // 장비가 장착된 펫입니다.\n판매 후 복구할 수 없습니다.

        #endregion

        #region 뷰티샵에서 사용 

        public const int BeautyShop_0 = 262213;    // "코스튬");
        public const int BeautyShop_1 = 262214;    // "코스튬샵");

        public const int BeautyShop_2 = 262215;    // "성형");
        public const int BeautyShop_3 = 262216;    // "성형샵");

        public const int BeautyShop_4 = 262217;    // "소셜");
        public const int BeautyShop_5 = 262218;    // "소셜샵");
        public const int BeautyShop_6 = 262219;    // "춤");

        public const int BeautyShopTemplate_0 = 262220;    // "입어보기");
        public const int BeautyShopTemplate_1 = 262221;    // "세트보기");
        public const int BeautyShopTemplate_2 = 262222;    // "미리보기");
        public const int BeautyShopTemplate_3 = 262223;    // "해제하기");
        public const int BeautyShopTemplate_4 = 261938;    // "성별 조건에 의해 구매할 수 없습니다.");
        public const int BeautyShopTemplate_5 = 261939;    // "직업 조건에 의해 구매할 수 없습니다.");
        public const int BeautyShopTemplate_6 = 262008;    // "판매 종료 {0}일 남음");
        public const int BeautyShopTemplate_7 = 262009;    // "판매 종료 {0}시간 남음");
        public const int BeautyShopTemplate_8 = 262010;    // "판매 종료 {0}분 남음");
        public const int BeautyShopTemplate_9 = 263593;    // 상세정보");

        #endregion 뷰티샵에서 사용

        #region 퀘스트에서 사용

        public const int Quest_0 = 260158;    // "사냥");
        public const int Quest_1 = 260159;    // "수집");
        public const int Quest_2 = 260160;    // "과(와) 대화");
        public const int Quest_3 = 260161;    // "을(를) 방문");
        public const int Quest_4 = 260162;    // "시나리오 진행현황");
        public const int Quest_5 = 260163;    // "퀘스트 진행현황");
        public const int Quest_6 = 260164;    // "퀘스트 완료 대상 npc가 존재하지 않습니다");
        public const int Quest_7 = 262224;    // "서브");
        public const int Quest_8 = 262225;    // "일반");
        public const int Quest_10 = 260165;    // "를(을) 찾아가세요.");
        public const int Quest_11 = 260166;    // "맵을 이동하면 자동으로 완료됩니다.");
        public const int Quest_12 = 260167;    // "남은 시간: {0}일 {1}시간");
        public const int Quest_13 = 261791;    // "보상아이템");
        public const int Quest_14 = 261840;    // "탐색");
        public const int Quest_15 = 261841;    // "제작");
        public const int Quest_16 = 261842;    // "다양한 방법으로 캐릭터를 성장시켜보세요.");
        public const int Quest_17 = 261843;    // "추천사냥터에서 사냥하기");
        public const int Quest_18 = 261844;    // "경험치 던전가기");
        public const int Quest_19 = 261845;    // "핫타임 활용하기");
        public const int Quest_20 = 261846;    // "일일퀘스트 하기");
        public const int Quest_21 = 261847;    // "레벨 부족");
        public const int Quest_22 = 261848;    // "추천 사냥터");
        public const int Quest_23 = 261849;    // "해당 퀘스트를 진행하기 위해서는 던전에 입장해야 합니다.");
        public const int Quest_24 = 261850;    // "벌목");
        public const int Quest_25 = 261851;    // "채광");
        public const int Quest_26 = 261868;    // "낚시");
        public const int Quest_27 = 261884;    // "방문");
        public const int Quest_28 = 261896;    // "일일 퀘스트 완료횟수 [f40]{0}[/f]/{1}");
        public const int Quest_29 = 261897;    // "이벤트 퀘스트 완료횟수 [f40]{0}[/f]/{1}");
        public const int Quest_30 = 261930;    // "반복 수행 횟수 완료");
        public const int Quest_31 = 261931;    // "일일퀘스트 횟수 완료");
        public const int Quest_32 = 262491;         // "퀘스트를 완료하였습니다.\n보상을 수령하세요.");
        public const int Quest_33 = 262492;         // "퀘스트를 완료하였습니다.");
        public const int Quest_34 = 262515;         // 일일퀘스트가 초기화되었습니다.
        public const int Quest_35 = 262536;         // 대사 다시보기
        public const int Quest_36 = 262537;         // 완료대사 보기   (일반적인 퀘스트 대사 미리보기 버튼 텍스트)
        public const int Quest_37 = 262538;         // 퀘스트 대사가 종료되었습니다.
        public const int Quest_38 = 262539;         // 다음대사 보기   (프롤로그 한정 대사 미리보기 버튼 텍스트)
        public const int Quest_39 = 262545;          // 마이룸에서는 던전 퀘스트 바로가기를 진행할 수 없습니다.
        public const int Quest_40 = 262546;          // 베히모스의 뱃속에선 지원하는 기능이 아닙니다.
        public const int Quest_41 = 262547;		 // 발굴
        public const int Quest_42 = 262593;		 // 드릴
        public const int Quest_43 = 263080;          // 퀘스트의 기간이 만료되었습니다.

        #endregion 퀘스트에서 사용

        #region 알림창에서 사용

        public const int Noticfication_0 = 260168;    // "퀘스트 알림");
        public const int Noticfication_1 = 260169;    // "퀘스트가 업데이트 되었습니다.");
        public const int Noticfication_2 = 260170;    // "을(를) 획득했습니다.");
        public const int Noticfication_3 = 260171;    // "레이드 보스 처치!");
        public const int Noticfication_4 = 260172;    // "위 보상이 도착했습니다.");
        public const int Noticfication_5 = 260173;    // "채집에 성공하였습니다.");
        public const int Noticfication_6 = 260174;    // "채집을 시도합니다.");
        public const int Noticfication_7 = 260175;    // "채집에 실패하였습니다.");
        public const int Noticfication_8 = 260176;    // "남은 채집 포인트");
        public const int Noticfication_9 = 260177;    // "채집 포인트");
        public const int Noticfication_10 = 260178;    // " (이)가 시작되었습니다.");
        public const int Noticfication_11 = 260179;    // " 퀘스트가 완료 되었습니다.");
        public const int Noticfication_12 = 261869;    // "필드에서만 계정보드를 열 수 있습니다.");
        public const int Noticfication_13 = 262499;    // "해당 펫 스킬의 무기타입과 장착된 무기 타입이 달라서 전투력이 떨어질 수 있습니다.");
        public const int Noticfication_14 = 262500;    // "해당 무기의 타입과 장착된 펫 스킬의 무기 타입이 달라서 전투력이 떨어질 수 있습니다.");
        public const int Noticfication_15 = 262540; 	// "일회용 삽이 없어 발굴할 수 없습니다. 제작을 통해 삽을 획득하세요."
        public const int Noticfication_16 = 262590; 	// "{0}(이)가 없어 채집할 수 없습니다."


        #endregion 알림창에서 사용

        #region 채팅창에서 사용

        public const int Chatting_0 = 260185;    // "채널변경");
        public const int Chatting_1 = 260186;    // "일반");
        public const int Chatting_2 = 260187;    // "대상입력");
        public const int Chatting_3 = 260188;    // "자기 자신에게 보낼 수 없습니다.");
        public const int Chatting_4 = 262226;    // "파티");
        public const int Chatting_5 = 262227;    // "To.{0}: ");
        public const int Chatting_6 = 262228;    // "채널정보/팁");
        public const int Chatting_7 = 262229;    // "획득정보");
        public const int Chatting_8 = 262230;    // "채팅정보");
        public const int Chatting_9 = 262231;    // "상대를 입력해 주세요.");
        public const int Chatting_10 = 262232;    // "% 보기");
        public const int Chatting_11 = 262233;    // "채팅을 입력하세요");
        public const int Chatting_12 = 262234;    // "입력");
        public const int Chatting_13 = 262235;    // "파티초대");
        public const int Chatting_14 = 262529;    // "블랙리스트 추가 이벤트");
        public const int Chatting_15 = 262530;    // "블랙리스트 해제 이벤트");
        public const int Chatting_16 = 263653;           // 3초내 같은 문장 발송시
        public const int Chatting_17 = 263654;           // 1시간내에 5번 중복시

        #endregion 채팅창에서 사용

        #region 던전창에서 사용

        public const int Dungeon_0 = 260189;    // "입장제한");
        public const int Dungeon_1 = 260190;    // "초 뒤 대기실로 이동합니다.");
        public const int Dungeon_2 = 260191;    // "초 뒤 자동으로 보상이 선택됩니다.");
        public const int Dungeon_3 = 260192;    // "던전 시작");
        public const int Dungeon_4 = 260193;    // "비공개");
        public const int Dungeon_5 = 260194;    // "공개");
        public const int Dungeon_6 = 260195;    // "{0}초 후 보상화면으로 이동합니다.");
        public const int Dungeon_7 = 260196;    // "던전 로딩 중...");
        public const int Dungeon_8 = 260197;    // "다른 유저 대기 중...");
        public const int Dungeon_9 = 260198;    // "[F60FF60F]던전에서는 AUTO를 사용할 수 없습니다.[-]");
        public const int Dungeon_10 = 260199;    // "던전에 입장 할 수 없습니다.");
        public const int Dungeon_11 = 260200;    // "친구가 없습니다.");
        public const int Dungeon_12 = 260201;    // "방이 존재하지 않습니다.");
        public const int Dungeon_13 = 260202;    // "상대가 초대를 취소하였습니다.");
        public const int Dungeon_14 = 260203;    // "보유한 던전 입장 초기화 티켓이 없습니다.");
        public const int Dungeon_15 = 260204;    // "출현 정보");
        public const int Dungeon_16 = 260205;    // "{0}레벨 부터 입장 할 수 있습니다.");
        public const int Dungeon_17 = 260206;    // "횟수충전");
        public const int Dungeon_18 = 260207;    // "{0}층");
        public const int Dungeon_19 = 260208;    // "입장");
        public const int Dungeon_20 = 260209;    // "레벨제한:{0}");

        public const int Dungeon_21 = 262236;    // "남은 몬스터를 모두 처치해야 이동가능합니다.");
        public const int Dungeon_22 = 262237;    // "남은 몬스터를 모두 처치하면 보스 몬스터가 등장합니다.");
        public const int Dungeon_23 = 262238;    // "다음 스테이지로 이동하세요.");
        public const int Dungeon_24 = 262239;    // "보스 몬스터를 처치하세요.");
        public const int Dungeon_25 = 262240;    // "남은 몬스터: {0}/{1}");

        public const int Dungeon_26 = 262241;    // "몬스터 타워로 이동");
        public const int Dungeon_27 = 262242;    // "자원 던전으로 이동");
        public const int Dungeon_28 = 262243;    // "요일 던전으로 이동");
        public const int Dungeon_29 = 262244;    // "다음 층으로");
        public const int Dungeon_30 = 262245;    // "재도전");
        public const int Dungeon_31 = 262246;    // "{0} 던전({1}) 클리어 보상");
        public const int Dungeon_32 = 262247;    // "{0}({1}) 클리어 보상");
        public const int Dungeon_33 = 262248;    // "{0} 클리어 보상");

        public const int Dungeon_34 = 262249;    // "보상을 선택하세요.");
        public const int Dungeon_35 = 262250;    // "대기실로");
        public const int Dungeon_36 = 262251;    // "기본 보상");
        public const int Dungeon_37 = 262252;    // "던전 결과");
        public const int Dungeon_38 = 262253;    // "순위");
        public const int Dungeon_39 = 262254;    // "캐릭터명");
        public const int Dungeon_40 = 262255;    // "누적 데미지");

        public const int Dungeon_41 = 262256;    // "하급");
        public const int Dungeon_42 = 262257;    // "중급");
        public const int Dungeon_43 = 262258;    // "상급");

        public const int Dungeon_44 = 261830;    // "입장 불가");

        public const int Dungeon_45 = 261833;    // "입장 티켓이 부족합니다.");
        public const int Dungeon_46 = 261834;    // "이미 클리어 하신 스테이지입니다.");
        public const int Dungeon_47 = 261835;    // "잠금된 스테이지입니다.");

        public const int Dungeon_48 = 261879;    // "모든 몬스터를 처치하세요!");
        public const int Dungeon_49 = 261880;    // "{0}동안 던전에서 살아남으세요!!");
        public const int Dungeon_50 = 261881;    // "{0}안에 모든 몬스터를 처치하세요!!");
        public const int Dungeon_51 = 261882;    // "잠시 후 몬스터가 등장합니다.");
        public const int Dungeon_52 = 261883;    // "잠시 후 보스 몬스터가 등장합니다!");
        public const int Dungeon_53 = 261885;    // "새로운 포탈이 열렸습니다.다음스테이지로 이동합니다.\n계속하려면
        public const int Dungeon_54 = 261887;    // "제한시간");

        public const int Dungeon_55 = 261928;    // "사용 가능 포션");
        public const int Dungeon_56 = 262036;    // "도전횟수를 {0}회 충전합니다.");

        public const int Dungeon_57 = 262039;    // "설정");
        public const int Dungeon_58 = 262040;    // "나가기");

        public const int Dungeon_59 = 262057;    // "도전횟수");
        public const int Dungeon_60 = 262058;    // "초기화");
        public const int Dungeon_61 = 262059;    // "캐시를 소모해 던전의 입장 횟수를 초기화하시겠습니까?");
        public const int Dungeon_62 = 263588;         // 금일 초기화 모두 사용

        public const int Dungeon_63 = 263622;        // 던전 정보가 갱신되었습니다.

        #region 던전타입 선택창 사용

        //미리 생성.
        public const int DungeonTypeSelect_00 = 260210;    // "던전");

        public const int DungeonTypeSelect_10 = 260211;    // "요일던전");
        public const int DungeonTypeSelect_11 = 260212;    // "멀티던전");
        public const int DungeonTypeSelect_12 = 260213;    // "몬스터타워");
        public const int DungeonTypeSelect_13 = 260214;    // "자원던전");
        public const int DungeonTypeSelect_14 = 260215;    // "자원던전");
        public const int DungeonTypeSelect_15 = 260216;    // "자원던전");
        public const int DungeonTypeSelect_16 = 260217;    // "자원던전");
        public const int DungeonTypeSelect_17 = 260218;    // "자원던전");
        public const int DungeonTypeSelect_18 = 260219;    // "자원던전");

        public const int DungeonTypeSelect_20 = 260220;    // "매일 성장에 필요한 재료아이템을 획득할 수 있습니다.");
        public const int DungeonTypeSelect_21 = 260221;    // "친구와 함께 즐거운 던전 탐험을!\n 지금 입장하세요.");
        public const int DungeonTypeSelect_22 = 260222;    // "자신의 한계를 시험해보세요.");
        public const int DungeonTypeSelect_23 = 260223;    // "대량의 경험치와 엘리를 획득해보세요.");
        public const int DungeonTypeSelect_24 = 260224;    // "설명두줄");
        public const int DungeonTypeSelect_25 = 260225;    // "설명두줄");
        public const int DungeonTypeSelect_26 = 260226;    // "설명두줄");
        public const int DungeonTypeSelect_27 = 260227;    // "설명두줄");
        public const int DungeonTypeSelect_28 = 260228;    // "설명두줄");


        #endregion 던전타입 선택창 사용

        #region 일일 던전

        public const int DungeonDaily_00 = 260229;    // "완료 보상");
        public const int DungeonDaily_01 = 260230;    // "게임 시작");
        public const int DungeonDaily_02 = 260231;    // "횟수 초기화");
        public const int DungeonDaily_03 = 260232;    // "{0} 클리어 시 오픈됩니다.");
        public const int DungeonDaily_04 = 260233;    // "도전횟수");

        #endregion 일일 던전

        #region 멀티던전

        public const int DungeonMulti_00 = 260234;    // "쉬움");
        public const int DungeonMulti_01 = 260235;    // "어려움");
        public const int DungeonMulti_02 = 260236;    // "보상목록");
        public const int DungeonMulti_03 = 260237;    // "방 생성");
        public const int DungeonMulti_04 = 260238;    // "빠른 입장");
        public const int DungeonMulti_05 = 260239;    // "던전 클리어 시 아래 보상을 하나 수령할 수 있습니다.");
        public const int DungeonMulti_06 = 263791;    // "제한 없음");

        #endregion

        #region 자원던전

        public const int DungeonGoods_00 = 260240;    // "엘리던전");
        public const int DungeonGoods_01 = 260241;    // "경험치던전");

        #endregion

        #region 몬스터타워

        public const int MonsterTower_00 = 262259;    // "도전 실패");
        public const int MonsterTower_01 = 262260;    // "도전 성공");
        public const int MonsterTower_02 = 262505;         // "다음 층이 없습니다.\n 업데이트를 기다려 주세요."

        #endregion


        #endregion 던전창에서 사용

        #region 친구 창에서 사용

        public const int Friend_0 = 260242;    // "오프라인");
        public const int Friend_1 = 260243;    // "시간 전 접속");
        public const int Friend_2 = 260244;    // "일 전 접속");
        public const int Friend_3 = 260245;    // "친구의 친구목록이 가득찼습니다");
        public const int Friend_4 = 260246;    // "친구(캐릭터) 가 없습니다");
        public const int Friend_5 = 260247;    // "이미 요청을 보냈거나 친구입니다");
        public const int Friend_6 = 260248;    // "친구목록이 가득찼습니다");
        public const int Friend_7 = 260249;    // "친구 요청 실패");
        public const int Friend_10 = 260250;    // "일");
        public const int Friend_11 = 260251;    // "시간");
        public const int Friend_12 = 260252;    // "분");
        public const int Friend_13 = 260253;    // "전");
        public const int Friend_14 = 260254;    // "방금");
        public const int Friend_15 = 260255;    // "데이터를 받지 못했습니다");
        public const int Friend_16 = 260256;    // "우정 포인트를 보낼 친구가 없습니다.");
        #endregion 친구 창에서 사용

        #region 길드 창에서 사용

        public const int Guild_0 = 260257;    // "배경 선택");
        public const int Guild_1 = 260258;    // "문양 선택");
        public const int Guild_2 = 260259;    // "색깔 선택");
        public const int Guild_3 = 260260;    // "추천 길드");
        public const int Guild_4 = 260261;    // "검색된 길드");
        public const int Guild_5 = 260262;    // "공지사항이 없습니다.");
        public const int Guild_6 = 260263;    // "중립");
        public const int Guild_7 = 260264;    // "혼돈");
        public const int Guild_8 = 260265;    // "질서");
        public const int Guild_9 = 260266;    // "변경");
        public const int Guild_10 = 260267;    // "길드를 생성할 수 없습니다.");
        public const int Guild_11 = 260268;    // "길드를 만드시겠습니까?");
        public const int Guild_12 = 260269;    // "길드원을 추방하시겠습니까?");
        public const int Guild_13 = 260270;    // "게시글 보기");
        public const int Guild_14 = 260271;    // "길드원 전체");
        public const int Guild_15 = 260272;    // "직위가 낮아 할 수 없습니다.");
        public const int Guild_16 = 260273;    // "길드원");
        public const int Guild_17 = 260274;    // "길드장");
        public const int Guild_18 = 260275;    // "부 길드장");
        public const int Guild_19 = 260276;    // "길드 이름이 너무 짧습니다.");
        public const int Guild_20 = 260277;    // "길드마크 설정");
        public const int Guild_21 = 260278;    // "길드 소개말입니다. 최대 32자 입력가능합니다.");
        public const int Guild_22 = 260279;    // "엠블럼을 변경 하시겠습니까?");
        public const int Guild_23 = 260280;    // "신청이 완료되었습니다!");
        public const int Guild_24 = 260281;    // "길드에 가입신청 하시겠습니까?");
        public const int Guild_25 = 260282;    // "조건을 충족하지 않아 보상을 받을 수 없습니다.");
        public const int Guild_26 = 260283;    // "길드 포인트가 지급되었습니다!");
        public const int Guild_27 = 260284;    // "변경할 권한을 선택하세요.(현재: ");
        public const int Guild_28 = 260285;    // " 님에게 다음 권한을 부여합니다.\n확인 시 취소가 불가합니다.");
        public const int Guild_29 = 260286;    // "공지사항");
        public const int Guild_30 = 260287;    // "동일한 권한으로 변경할 수 없습니다.");
        public const int Guild_31 = 260288;    // "최대 임명 횟수를 초과했습니다.");
        public const int Guild_32 = 260289;    // "부길드장으로 임명 되었습니다.");
        public const int Guild_33 = 260290;    // "부길드장 해임 하였습니다.");
        public const int Guild_34 = 260291;    // "가입을 승인하였습니다.");
        public const int Guild_35 = 260292;    // "가입을 거절하였습니다.");
        public const int Guild_36 = 260293;    // "길드에서 추방당했습니다.");
        public const int Guild_37 = 260294;    // "[f00ff00f]필요길드레벨 : {0}[/c]\n길드레벨이 부족하여 습득할 수 없습니다.");
        public const int Guild_38 = 260295;    // "누적 기부 포인트 보상을 받았습니다.");
        public const int Guild_39 = 260296;    // "누적 기부 포인트 보상이 없습니다.");
        public const int Guild_40 = 260898;    // "존재하지 않는 길드입니다.");
        public const int Guild_41 = 260899;    // "출석체크");
        public const int Guild_42 = 260900;    // "※ 출석체크 보상은 다음날 우편을 통해 지급됩니다.");
        public const int Guild_43 = 260901;    // "누적 길드 포인트");
        public const int Guild_44 = 260902;    // "※ 일일단위로 초기화 됩니다.");       /// 이거 변경됨
        public const int Guild_45 = 260903;    // "기부한 포인트");                      /// 이거도
        public const int Guild_46 = 262261;    // "공지를 수정하시겠습니까?");
        public const int Guild_47 = 262262;    // "입력한 글이 너무 짧습니다.");
        public const int Guild_48 = 262263;    // "검색할 길드 이름을 입력하세요.");
        public const int Guild_49 = 262264;    // "최대 길드원 인원이 초과하였습니다.");
        public const int Guild_50 = 262265;    // "한글 64자, 영문 128자까지 입력 가능합니다.");
        public const int Guild_51 = 262266;    // "권한 변경 완료");
        public const int Guild_52 = 262267;    // "님이 길드장으로 임명되었습니다!");
        public const int Guild_53 = 261446;    // "게시판");
        public const int Guild_54 = 261447;    // "길드\n하우스");
        public const int Guild_55 = 261448;    // "내길드");
        public const int Guild_56 = 261449;    // "인원수");
        public const int Guild_57 = 261450;    // "길드타입");
        public const int Guild_58 = 261451;    // "오늘 출석");
        public const int Guild_59 = 261452;    // "보상보기");
        public const int Guild_60 = 261453;    // "출석 완료");
        public const int Guild_61 = 261454;    // "오늘 누적");
        public const int Guild_62 = 261455;    // "기부한 포인트");
        public const int Guild_63 = 261456;    // "랭킹");
        public const int Guild_64 = 261457;    // "레벨순");
        public const int Guild_65 = 261458;    // "최근 접속순");
        public const int Guild_66 = 261459;    // "공지사항");
        public const int Guild_67 = 261460;    // "글쓰기");
        public const int Guild_68 = 261461;    // "길드 검색");
        public const int Guild_69 = 261462;    // "길드검색\n텍스트 입니다.");
        public const int Guild_70 = 261463;    // "검색하기");
        public const int Guild_71 = 261464;    // "추천 길드\n텍스트 입니다.");
        public const int Guild_72 = 261465;    // "추천길드 보기");
        public const int Guild_73 = 261466;    // "길드 생성");
        public const int Guild_74 = 261467;    // "길드 생성\n텍스트 입니다.");
        public const int Guild_75 = 261468;    // "생성하기");
        public const int Guild_76 = 261469;    // "길드명");
        public const int Guild_77 = 261470;    // "길드정보");
        public const int Guild_78 = 261471;    // "혼돈 성향 설명 텍스트 입니다.");
        public const int Guild_79 = 261472;    // "중립 성향 설명 텍스트 입니다.");
        public const int Guild_80 = 261473;    // "질서 성향 설명 텍스트 입니다.");
        public const int Guild_81 = 261474;    // "길드 생성");
        public const int Guild_82 = 261475;    // "외형 설정");
        public const int Guild_83 = 261476;    // "아이콘 모양 선택");
        public const int Guild_84 = 261477;    // "아이콘 색상 선택");
        public const int Guild_85 = 261478;    // "아이콘 설정");
        public const int Guild_86 = 261479;    // "외형 모양 선택");
        public const int Guild_87 = 261480;    // "외형 색상 선택");
        public const int Guild_88 = 261888;    // "길드 이름이 너무 깁니다.");
        public const int Guild_89 = 261889;    // "길드 문양을 완성해주세요.");
        public const int Guild_90 = 261890;    // "길드 소개가 너무 길거나 짧습니다.");
        public const int Guild_91 = 261913;    // "길드장은 남은 길드원이 없어야 탈되 가능합니다.");
        public const int Guild_92 = 261918;    // "누적 보상을 받을 수 없습니다.");
        public const int Guild_93 = 261929;    // "길드 문양을 변경해주세요.");
        public const int Guild_94 = 261940;    // "현재 적용된 효과가 없습니다.");
        public const int Guild_95 = 261941;    // "정보");
        public const int Guild_96 = 261942;    // "길드원");
        public const int Guild_97 = 261943;    // "퀘스트");
        public const int Guild_98 = 261944;    // "스킬");
        public const int Guild_99 = 261945;    // "게시판");
        public const int Guild_100 = 261946;    // "관리");
        public const int Guild_101 = 261947;    // "검색");
        public const int Guild_102 = 261948;    // "탈퇴");
        public const int Guild_103 = 261949;    // "출석");
        public const int Guild_104 = 261950;    // "받기");
        public const int Guild_105 = 261951;    // "다음 보상");
        public const int Guild_106 = 261952;    // "보유한 길드 포인트");
        public const int Guild_107 = 261953;    // "접속중 입니다.");
        public const int Guild_108 = 261954;    // "오프라인 {0}일 전 접속");
        public const int Guild_109 = 261955;    // "임무");
        public const int Guild_110 = 261956;    // "보상");
        public const int Guild_111 = 261957;    // "강화");
        public const int Guild_112 = 261958;    // "가입 관리");
        public const int Guild_113 = 261959;    // "승인");
        public const int Guild_114 = 261960;    // "거절");
        public const int Guild_115 = 261961;    // "출석 보상목록");
        public const int Guild_116 = 261962;    // "확인");
        public const int Guild_117 = 261963;    // "누적 포인트 보상목록");
        public const int Guild_118 = 261964;    // "길드 포인트 랭킹");
        public const int Guild_119 = 261965;    // "작성완료");
        public const int Guild_120 = 261966;    // "취소");
        public const int Guild_121 = 261967;    // "가입 현황");
        public const int Guild_122 = 261968;    // "가입 현황탭 설명입니다.");
        public const int Guild_123 = 261969;    // "신청 길드 보기");
        public const int Guild_124 = 261970;    // "선택");
        public const int Guild_125 = 261971;    // "길드 이름을 입력하세요.");
        public const int Guild_126 = 261972;    // "생성");
        public const int Guild_127 = 261973;    // "가입");
        public const int Guild_128 = 261974;    // "아직 창설된 길드가 없습니다.\n길드를 창설 해보세요!");
        public const int Guild_129 = 261975;    // "길드 창설하기");
        public const int Guild_130 = 261976;    // "자세히");
        public const int Guild_131 = 261977;    // "길드 가입 현황");
        public const int Guild_132 = 261978;    // "가입취소");
        public const int Guild_133 = 261987;    // "해산");
        public const int Guild_134 = 261988;    // "소개글이 입력되지 않았습니다.");
        public const int Guild_135 = 261989;    // "공지사항");
        public const int Guild_136 = 261991;    // "길드 출석보상을 받았습니다.");
        public const int Guild_137 = 262011;    // "몬스터 {0}마리 사냥");
        public const int Guild_138 = 262012;    // "채집 {0}회 수행");
        public const int Guild_139 = 262013;    // "PVP {0}회 수행");
        public const int Guild_140 = 262041;    // "확인을 누르면 다이아가 차감되며, 엠블럼이 변경됩니다.");
        public const int Guild_141 = 262042;    // "입력한 글이 너무 깁니다.");
        public const int Guild_142 = 262043;    // "길드 소개말은 한글 32자, 영문 64자까지 입력 가능합니다.");
        public const int Guild_143 = 262044;    // "정말로 게시글을 삭제하시겠습니까?");
        public const int Guild_144 = 262045;    // "해당 길드 스킬을 찍을 경우 GP가 차감됩니다.");
        public const int Guild_145 = 262052;    // "공지사항은 한글 32자, 영문 64자까지 입력 가능합니다.");
        public const int Guild_146 = 262053;    // "길드를 정말 해산 하시겠습니까?\n탈퇴후 24시간 동안 가입이 불가능합니다.");
        public const int Guild_147 = 262409;         // "이미 가입 신청한 길드입니다."
        public const int Guild_148 = 262477;         // "길드 탈퇴 후 24시간 동안 길드 가입이 불가능합니다."
        public const int Guild_149 = 262478;         // "길드 탈퇴 후 24시간 동안 길드 생성이 불가능합니다."
        public const int Guild_150 = 262520;         // "길드마스터의 권한을 위임 받습니다.\n위임 시 마스터의 권한을 받게 됩니다.\n위임 시 취소할 수 없습니다."
        public const int Guild_151 = 262521;         // "길드 마스터가 변경되었습니다."
        public const int Guild_152 = 262522;         // "{0}년 {1}월 {2}일 출석체크 완료"


        #endregion 길드 창에서 사용

        #region 아이템 UI에서 사용

        public const int Item_1 = 260297;    // "한손검");
        public const int Item_2 = 260298;    // "둔기");
        public const int Item_3 = 260299;    // "너클");
        public const int Item_4 = 260300;    // "양손검");
        public const int Item_5 = 260301;    // "스피어");
        public const int Item_6 = 260302;    // "지팡이");
        public const int Item_7 = 260303;    // "단검");
        public const int Item_8 = 260304;    // "활");
        public const int Item_9 = 260305;    // "석궁");
        public const int Item_10 = 260306;    // "쌍검");
        public const int Item_11 = 260307;    // "기타");
        public const int Item_12 = 260308;    // "총");

        public const int Item_20 = 260309;    // "강화할 장비를 선택하세요.");
        public const int Item_21 = 260310;    // "승급할 장비를 선택하세요.");
        public const int Item_22 = 260311;    // "연마할 장비를 선택하세요.");
        public const int Item_23 = 262268;    // "염색할 코스튬을 선택하세요.");
        public const int Item_24 = 262269;    // "강화 재료를 추가하세요.");
        public const int Item_25 = 262270;    // "염색하기");
        public const int Item_26 = 261876;    // "연마 성공 확률: [F00FF00F]{0}%[/c]");
        public const int Item_27 = 261877;    // "연마 실패 시 내구도가 하락할 수 있습니다.\n연마에 실패하더라도 장비의 성능은 증가됩니다.");
        public const int Item_28 = 262066;    // "[F00FF00F]내구도가 0인 상태에서 연마실패시 장비는 파괴됩니다.[/c]");
        public const int Item_29 = 262067;    // "장비의 내구도가 0입니다.\n연마실패시 장비가 파괴됩니다.\n진행하시겠습니까?");

        public const int Item_30 = 261873;    // " 후\n무료 확장가능");
        public const int Item_31 = 261874;    // "확장가능!!");

        public const int Item_33 = 262068;    // "연마하기");
        public const int Item_34 = 262069;    // "장비가 파괴되었습니다.");

        public const int Item_35 = 261875;    // "소모품");

        public const int Item_36 = 262070;    // "염색 불가능한 코스튬입니다.");
        public const int Item_37 = 262502;    // "※ 해당 빙고는 시즌 아이템입니다.\n빙고판에서 등록할 수 있습니다.");
        public const int Item_38 = 262506;         // 상점으로 이동
        public const int Item_39 = 262507;         // 보유 중인 염색약이 없습니다.\n 상점에서 염색약을 획득할 수 있습니다.

        public const int Item_40 = 262516;       //연마 최대치에 도달
        public const int Item_41 = 262562;         // 재료가 부족합니다.

        public const int Item_42 = 263081;      // 합성할 코스튬들을 선택하세요.
        public const int Item_43 = 263083;      // 합성하기
        public const int Item_44 = 263084;      // 합성 시 재료로 사용된 코스튬은 사라집니다.\n진행하시겠습니까?
        public const int Item_45 = 263085;      // {0}은(는)\n{1}성으로 합성이 불가능합니다.\n다른 코스튬을 추가하여\n랜덤 합성을 진행할 수 있습니다.
        public const int Item_46 = 263086;      // {0}성 코스튬은 동일 코스튬으로\n합성이 불가능합니다.\n다른 코스튬을 추가하여\n랜덤 합성을 진행할 수 있습니다.
        public const int Item_47 = 263087;      // {0}은(는) 최대 {1}개까지만 사용할 수 있습니다.        
        public const int Item_48 = 263710;      // 합성 시 5성 랜덤 코스튬 1개를 획득합니다.\n그대로 진행하시겠습니까? 
        public const int Item_49 = 269380;      // 이미 장착한 헤어입니다.
        public const int Item_50 = 269381;      // 이미 장착한 얼굴입니다.

        #endregion 아이템 UI에서 사용

        #region 펫 창에서 사용

        public const int Pet_0 = 260312;    // "합성펫");
        public const int Pet_1 = 260313;    // "강화펫");
        public const int Pet_2 = 260314;    // "강화 성공확률");
        public const int Pet_3 = 260315;    // "누적 강화성공률");
        public const int Pet_4 = 260316;    // "속성효과");
        public const int Pet_5 = 260317;    // "바람속성에게");
        public const int Pet_6 = 260318;    // "불속성에게");
        public const int Pet_7 = 260319;    // "땅속성에게");
        public const int Pet_8 = 260320;    // "물속성에게");
        public const int Pet_9 = 260321;    // "모든속성에게");

        public const int Pet_10 = 260322;    // "추가데미지");
        public const int Pet_11 = 260323;    // "룬 효과");
        public const int Pet_12 = 260324;    // "세트 효과");
        public const int Pet_13 = 260325;    // "초월펫");
        public const int Pet_14 = 260326;    // "승급펫");

        public const int Pet_15 = 262271;    // "펫정보");

        public const int Pet_16 = 262272;    // "강화 재료를 추가하세요.");
        public const int Pet_17 = 262273;    // "강화할 펫을 선택해 주세요.");

        public const int Pet_18 = 262274;    // "레벨 강화할 펫을 선택해 주세요.");

        public const int Pet_19 = 262275;    // "진화할 펫을 선택해 주세요.");
        public const int Pet_20 = 262276;    // "진화 재료를 추가하세요.");

        public const int Pet_21 = 262277;    // "초월할 펫을 선택해 주세요.");
        public const int Pet_22 = 262278;    // "초월 재료를 추가하세요.");

        public const int Pet_23 = 261852;    // "콤보 적용 시");

        public const int Pet_24 = 262098;    // 합성
        public const int Pet_25 = 262558;         // 에게 50% 스킬 공격력 증가
        public const int Pet_26 = 262559;         // 몬스터 타입 보너스
        public const int Pet_27 = 262560;         // 형 몬스터에게
        public const int Pet_28 = 262561;         // % 스킬 공격력 증가

        #endregion 펫 창에서 사용

        #region 제작 창에서 사용

        public const int Craft_0 = 260327;    // "부터 제작이 가능합니다.");
        public const int Craft_1 = 260328;    // "제작 가능 횟수가 초과하였습니다.");

        public const int Craft_2 = 262279;    // "제작");
        public const int Craft_3 = 262280;    // "제작재료를 확인하세요.");

        public const int Craft_4 = 261907;    // "경고!\n\n제작 시 사용된 모든 재료가\n소실되며 복구할 수 없습니다.");

        public const int Craft_5 = 261992;    // "판매종료");
        public const int Craft_6 = 261993;    // "기간 초과");

        public const int Craft_7 = 262046;    // "제작가능");
        public const int Craft_8 = 262047;    // "레벨부족");
        public const int Craft_9 = 262048;    // "재료부족");
        public const int Craft_10 = 262049;    // "레벨부족, 재료부족");

        public const int Craft_11 = 262594;     // "펫 장비"
        public const int Craft_12 = 263703;     // 레벨이 부족하여 제작할 수 없습니다.

        #endregion 제작 창에서 사용

        #region PushBarUI에서 사용

        public const int PushBar_0 = 260329;    // "인벤토리 부족");
        public const int PushBar_1 = 260330;    // "인벤토리 부족");
        public const int PushBar_2 = 260331;    // "업적 완료");
        public const int PushBar_3 = 260332;    // "퀘스트 완료");
        public const int PushBar_4 = 260333;    // "칸 남았습니다.");
        public const int PushBar_5 = 260334;    // "인벤토리가 가득 찼습니다.");
        public const int PushBar_6 = 260335;    // "퀘스트를 찾지 못했습니다.");
        public const int PushBar_7 = 260997;    // "직업레벨업!");
        public const int PushBar_8 = 260998;    // "직업레벨이 상승하였습니다.\n트리를 확인해보세요.");
        public const int PushBar_9 = 260999;    // "칭호 획득");
        public const int PushBar_10 = 261000;    // "찾을 수 없는 칭호입니다.");
        public const int PushBar_11 = 261001;    // "{0}님께서 파티에 초대하였습니다.");
        public const int PushBar_12 = 261872;    // "해당 유저를 블루투스로 발견하였습니다.");
        public const int PushBar_13 = 262014;    // "길드 퀘스트 완료!");
        public const int PushBar_14 = 262015;    // "길드 퀘스트를 완료하였습니다.\n보상을 수령하세요.");
        public const int PushBar_15 = 262016;    // "길드 가입신청 요청");
        public const int PushBar_16 = 262017;    // " 길드 가입신청이 도착했습니다.\n확인해주세요.");
        public const int PushBar_17 = 262412;         // "하우징 업데이트"
        public const int PushBar_18 = 262413;         // "정보가 업데이트 되었습니다.\n확인해주세요."
        public const int PushBar_19 = 263340;         // 판매 완료
        public const int PushBar_20 = 263230;         // 물건이 판매됐습니다.
        #endregion

        #region 창고에서 사용
        public const int Storage_0 = 260336;    // "보관하기");
        public const int Storage_1 = 260337;    // "꺼내기");
        public const int Storage_2 = 262281;    // "창고");

        public const int Inventory_0 = 262282;    // "이동순");
        public const int Inventory_1 = 262283;    // "타입순");
        public const int Inventory_2 = 262284;    // "획득순");
        public const int Inventory_3 = 262285;    // "등급순");
        public const int Inventory_4 = 262286;    // "장착레벨순");
        public const int Inventory_5 = 262287;    // "레벨순");
        #endregion

        #region 몬스터 도감에서 사용

        public const int Mob_0 = 260338;    // "현재 적용중인 옵션이 없습니다.");
        public const int Mob_1 = 260339;    // "다이아로 몬스터 카드를 강제 등록합니다.\n강제 등록 시 100% 성공합니다.");
        public const int Mob_2 = 260340;    // "몬스터 카드를 등록합니다.(확률 {0}%)\n등록 시도 시 몬스터 카드가 소멸됩니다.");

        public const int Mob_3 = 262288;    // "몬스터 도감");
        public const int Mob_4 = 262289;    // "기본 정보");
        public const int Mob_5 = 262290;    // "출현 지역");
        public const int Mob_6 = 262291;    // "등록 아이템");
        public const int Mob_7 = 262292;    // "카드 효과");

        public const int Mob_8 = 262071;    // "지역별 보기");
        public const int Mob_9 = 262072;    // "옵션별 보기");

        public const int Mob_10 = 262073;    // "힘");
        public const int Mob_11 = 262074;    // "지능");
        public const int Mob_12 = 262075;    // "민첩");
        public const int Mob_13 = 262076;    // "행운");
        public const int Mob_14 = 262077;    // "체력");
        public const int Mob_15 = 262078;    // "최소 공격력");
        public const int Mob_16 = 262079;    // "최대 공격력");
        public const int Mob_17 = 262080;    // "모든 공격력");
        public const int Mob_18 = 262081;    // "스킬 공격력");
        public const int Mob_19 = 262082;    // "물리 방어력");
        public const int Mob_20 = 262083;    // "마법 방어력");
        public const int Mob_21 = 262084;    // "크리티컬 포인트");
        public const int Mob_22 = 262085;    // "크리티컬 데미지");
        public const int Mob_23 = 262504;      // "마나");


        #endregion

        #region 펫 도감에서 사용

        public const int PetBook_0 = 262479;    // "펫 기본 정보");
        public const int PetBook_1 = 262480;    // "추가 정보");
        public const int PetBook_2 = 262295;    // "추가대미지(무기)");
        public const int PetBook_3 = 262296;    // "추가대미지(타입)");
        public const int PetBook_4 = 261799;    // "제작가능");

        #endregion 펫 도감에서 사용

        #region 빙고에서 사용
        public const int Bingo_0 = 260341;    // "모든 빙고를 완료하면\n{1} 아이템 지급!");
        public const int Bingo_1 = 260342;    // "다음 시즌 빙고가 없습니다.");
        public const int Bingo_2 = 260343;    // "다음 시즌 빙고까지 {1}시간 남았습니다.");
        public const int Bingo_3 = 260344;    // "이미 지난 시즌 빙고입니다.");

        public const int Bingo_4 = 262297;    // "전체완료 보너스");

        public const int Bingo_5 = 262298;    // "빙고보상 획득");
        public const int Bingo_6 = 262299;    // "전체완료 보너스");

        public const int Bingo_7 = 261481;    // "빙고 완료까지");
        public const int Bingo_8 = 261482;    // "일 남았습니다.");
        public const int Bingo_9 = 262408;         // "필요 아이템의 개수가 부족합니다."
        #endregion

        #region 평타 강화 트리에서 사용
        public const int AtkEnchant_0 = 260345;    // "추가대미지");
        public const int AtkEnchant_1 = 260346;    // "추가 범위");
        public const int AtkEnchant_2 = 260347;    // "추가 타겟");
        public const int AtkEnchant_3 = 260348;    // "추가 분노 획득");
        public const int AtkEnchant_4 = 260349;    // "추가 피버수치 획득");
        public const int AtkEnchant_5 = 260350;    // "확률로");
        public const int AtkEnchant_6 = 260351;    // "자신에게");
        public const int AtkEnchant_7 = 260352;    // "상대에게");
        public const int AtkEnchant_8 = 260353;    // "발동");
        public const int AtkEnchant_9 = 262300;    // "{0}타");
        public const int AtkEnchant_10 = 262301;    // "Job Lv.\n{0}");
        public const int AtkEnchant_11 = 262302;    // "평타강화트리");
        #endregion

        #region NPC 관련에서 사용
        public const int Npc_0 = 260354;    // "일반");
        public const int Npc_1 = 260355;    // "장비상인");
        public const int Npc_2 = 260356;    // "창고");
        public const int Npc_3 = 260357;    // "체력회복");
        public const int Npc_4 = 260358;    // "제작상인");
        public const int Npc_5 = 260359;    // "포탈이동");
        #endregion

        #region 접속완료보상에서 사용
        public const int ConnectReward_0 = 260360;    // "접속완료보상");
        public const int ConnectReward_1 = 260361;    // "완주보상");
        public const int ConnectReward_2 = 260362;    // "{0}분 후 가능");
        public const int ConnectReward_3 = 260906;    // "완주 보상이 우편으로 도착하였습니다.");
        public const int ConnectReward_4 = 260907;    // "접속유지 보상이 우편으로 도착하였습니다.");
        public const int ConnectReward_5 = 262438;    // "{0}회차");
        #endregion 접속완료보상에서 사용

        #region 휴식보상에서 사용
        public const int RestReward_0 = 260908;    // "휴식보상");
        public const int RestReward_1 = 260909;    // "오늘 사용가능 휴식 포인트");
        public const int RestReward_2 = 260910;    // "휴식보상 받기");
        public const int RestReward_3 = 260911;    // "휴식보상을 받았습니다.");
        #endregion 휴식보상에서 사용

        #region 메일에서 사용
        public const int Mail_0 = 260912;    // "우편을 받았습니다");
        public const int Mail_1 = 260913;    // "인벤토리가 가득 차 다음 항목을 받지 못했습니다");
        public const int Mail_2 = 261792;    // "우편함");
        public const int Mail_3 = 261793;    // "모두받기");
        public const int Mail_4 = 261794;    // "선물");
        #endregion

        #region 배틀필드 UI 사용

        public const int BattleField_0 = 260914;    // "업적");
        public const int BattleField_1 = 260915;    // "퀘스트");
        public const int BattleField_2 = 260916;    // "빙고");
        public const int BattleField_3 = 260917;    // "사전");
        public const int BattleField_4 = 260918;    // "귀환");
        public const int BattleField_5 = 260919;    // "퀘스트");
        public const int BattleField_6 = 260920;    // "아이템");
        public const int BattleField_7 = 260921;    // "펫");
        public const int BattleField_8 = 260922;    // "우편");
        public const int BattleField_9 = 260923;    // "AUTO");
        public const int BattleField_10 = 260924;    // "꾸미기");
        public const int BattleField_11 = 260925;    // "뽑기");
        public const int BattleField_12 = 260926;    // "캐시상점");
        public const int BattleField_13 = 260927;    // "나가기");
        public const int BattleField_14 = 260928;    // "업그레이드");
        public const int BattleField_15 = 260929;    // "편집모드");
        public const int BattleField_16 = 260930;    // "판매하기");
        public const int BattleField_17 = 260931;    // "설정");
        public const int BattleField_18 = 260932;    // "던전");
        public const int BattleField_19 = 260933;    // "PVP");
        public const int BattleField_20 = 260934;    // "평타강화");
        public const int BattleField_21 = 260935;    // "특성");
        public const int BattleField_22 = 260936;    // "길드");
        public const int BattleField_23 = 260937;    // "친구");
        public const int BattleField_24 = 260938;    // "하우징");
        public const int BattleField_25 = 262304;    // "블루팀");
        public const int BattleField_26 = 262305;    // "레드팀");
        public const int BattleField_27 = 262306;    // "이동취소");
        public const int BattleField_28 = 261483;    // "OPEN!");
        public const int BattleField_29 = 261919;    // "미니맵");
        public const int BattleField_30 = 261920;    // "월드맵");
        public const int BattleField_31 = 261921;    // "칭호");
        public const int BattleField_32 = 261922;    // "계정보드");
        public const int BattleField_33 = 261927;    // "펫 룸");
        public const int BattleField_34 = 262099;    // "가구배치");
        public const int BattleField_35 = 262100;    // "확장하기");
        public const int BattleField_36 = 261979;    // "NPC 이동");
        public const int BattleField_37 = 262136;    // "달성"
        public const int BattleField_38 = 262137;    // "일일미션"
        public const int BattleField_39 = 262138;    // "커뮤니티"
        public const int BattleField_40 = 262139;    // "스킬"
        public const int BattleField_41 = 262140;    // "스킬트리"
        public const int BattleField_42 = 262141;    // "스킬배치"
        public const int BattleField_43 = 262145;    // "전투"
        public const int BattleField_44 = 262423;    // "랭킹"
        public const int BattleField_45 = 262439;    // "패키지상점"
        public const int BattleField_46 = 262524;    // "마이룸 업그레이드" ???
        public const int BattleField_47 = 262564;    // 배히모스 던전을? 나가시겠습니까?
        public const int BattleField_48 = 262566;    // 베히모스 뱃속 종료까지 {0}초 남았습니다.
        public const int BattleField_49 = 263350;         // 거래소

        #endregion

        #region 업적에서 사용
        public const int Achievement_0 = 261002;    // "업적");
        public const int Achievement_1 = 261003;    // "주간");
        public const int Achievement_2 = 261004;    // "월간");
        public const int Achievement_3 = 261005;    // "일반");
        public const int Achievement_4 = 261006;    // "업적보상을 받았습니다.");
        #endregion

        #region 출석에서 사용
        public const int Attendance_0 = 261007;    // "출석");
        public const int Attendance_1 = 261008;    // "이벤트기간 {0}.{1}~{2}.{3}");
        public const int Attendance_2 = 261009;    // "{0}월~");
        public const int Attendance_3 = 261010;    // "출석보상");
        public const int Attendance_4 = 261011;    // "이벤트!");
        public const int Attendance_5 = 261012;    // "{0}월 한달 동안 매일매일 접속 시 해당\n아이템을 얻을 수 있습니다.");
        public const int Attendance_6 = 261013;    // "출석보상을 받았습니다.");
        public const int Attendance_7 = 261014;    // "보상아이템이 우편함으로 지급되었습니다.");
        #endregion

        #region 도감에서 사용
        public const int BookSelect_0 = 261015;    // "도감");
        public const int BookSelect_1 = 261016;    // "펫도감");
        public const int BookSelect_2 = 261017;    // "소유한 펫이나 획득 가능한 펫 정보를 볼 수 있습니다.");
        public const int BookSelect_3 = 261018;    // "몬스터도감");
        public const int BookSelect_4 = 261019;    // "몬트서카드를 등록해서 캐릭터를 성장시키세요.");
        public const int BookSelect_5 = 261020;    // "가구도감");
        public const int BookSelect_6 = 261021;    // "다양한 가구 정보를 확인할 수 있습니다.");
        public const int BookSelect_7 = 261022;    // "아이템도감");
        public const int BookSelect_8 = 261023;    // "모든 아이템의 기능과 획득 경로를 확인합니다.");
        public const int BookSelect_9 = 261909;    // "7레벨 이후에 가능합니다.");
        public const int BookSelect_10 = 262092;	// "뽑기상점에서 획득 할 수 있습니다.");

        public const int BookSelect_11 = 262454;	// "모든 지역 드랍됩니다.";
        public const int BookSelect_12 = 262455;	// "필드 보스 드랍됩니다.";
        public const int BookSelect_13 = 262456;	// "에픽 보스 드랍됩니다.";
        public const int BookSelect_14 = 262457;	// "벨로스 지역 드랍됩니다.";
        public const int BookSelect_15 = 262458;	// "엘리아스 지역 드랍됩니다.";
        public const int BookSelect_16 = 262459;	// "용경 지역 드랍됩니다.";
        public const int BookSelect_17 = 262460;	// "아오이치 지역 드랍됩니다.";
        public const int BookSelect_18 = 262461;	// "엘파 지역 드랍됩니다.";
        public const int BookSelect_19 = 262462;	// "퀘스트를 통해 획득합니다.";
        public const int BookSelect_20 = 262463;	// "제작을 통해 획득합니다.";
        public const int BookSelect_21 = 262464;	// "일반 상점에서 획득합니다.";
        public const int BookSelect_22 = 262465;	// "행운의 보물상자에서 획득합니다.";
        public const int BookSelect_23 = 262466;	// "요일 던전에서 획득합니다.";
        public const int BookSelect_24 = 262481;	// "멀티던전에서 획득합니다.";
        public const int BookSelect_25 = 262482;	// "꾸미기 상점에서 구매할 수 있습니다.";
        public const int BookSelect_26 = 262483;	// "캐시상점에서 구매할 수 있습니다.";
        public const int BookSelect_27 = 262484;	// "이벤트를 통해 획득할 수 있습니다.";
        public const int BookSelect_28 = 263073;         // 이계 벨로스 지역 드랍됩니다.
        public const int BookSelect_29 = 263074;         // 이계 엘리아스 지역 드랍됩니다.
        public const int BookSelect_30 = 263075;         // 이계 용경 지역 드랍 됩니다.
        public const int BookSelect_31 = 263076;         // 이계 아오이치 지역 드랍됩니다.
        public const int BookSelect_32 = 263077;         // 이계 엘파 지역 드랍됩니다.
        public const int BookSelect_33 = 263078;         // 베스 지역 드랍됩니다.

        public const int BookSelect_34 = 263329;	// "코스튬도감");
        public const int BookSelect_35 = 263330;    // "코스튬을 미리 입어보고 나에게\n잘 어울리는지 확인해 보세요.");

        public const int BookSelect_36 = 261574;    // 옵션 :
        public const int BookSelect_37 = 261575;    // 랜덤 {0} 개
        public const int BookSelect_38 = 261577;    // 확정 {0} 개
        public const int BookSelect_39 = 263557;    // 공용 작물에서 획득할 수 있습니다
        public const int BookSelect_40 = 263558;    // 혼돈 작물에서 획득할 수 있습니다.
        public const int BookSelect_41 = 263559;    // 중립 작물에서 획득할 수 있습니다
        public const int BookSelect_42 = 263560;    // 질서 작물에서 획득할 수 있습니다
        public const int BookSelect_43 = 263898;    // 채집에서 획득할 수 있습니다. 
        public const int BookSelect_44 = 263899;    // 벌목에서 획득할 수 있습니다.
        public const int BookSelect_45 = 263900;    // 낚시에서 획득할 수 있습니다.
        public const int BookSelect_46 = 263901;    // 채광에서 획득할 수 있습니다.
        #endregion

        #region 필드 핫타임에서 사용
        public const int FieldHotTime_0 = 262307;    // "필드 핫타임");
        public const int FieldHotTime_1 = 262308;    // "핫타임 시작");
        public const int FieldHotTime_2 = 262309;    // "[000F000F]필드핫타임을 [F00FF00F]{0}분 [000F000F]충전하시겠습니까?");
        public const int FieldHotTime_3 = 262310;    // "핫타임이 시작되었습니다. 필드에서 보상을 {0}배로 획득할 수 있습니다.");
        public const int FieldHotTime_4 = 262311;    // "핫타임이 종료되었습니다.");
        public const int FieldHotTime_5 = 262312;    // "24시 기점");
        public const int FieldHotTime_6 = 262313;    // "핫타임이 충전되었습니다.");
        #endregion

        #region 특성에서 사용
        public const int Characteristic_0 = 262314;    // "보유포인트");
        public const int Characteristic_1 = 262315;    // "사용포인트");
        public const int Characteristic_2 = 262316;    // "초기화");
        public const int Characteristic_3 = 262317;    // "특성 상세정보");
        public const int Characteristic_4 = 262318;    // "현재 레벨에서는 효과가 없습니다.");// 특성레벨0 일때 효과 텍스트
        public const int Characteristic_5 = 262319;    // "Lv.{0}에 습득 가능");
        public const int Characteristic_6 = 262320;    // "선행특성필요");
        public const int Characteristic_7 = 262321;    // "현재 습득할 수 없는 특성입니다");
        public const int Characteristic_8 = 262322;    // "Max 상태의 특성입니다.");
        public const int Characteristic_9 = 262323;    // "특성포인트가 부족합니다.");
        public const int Characteristic_10 = 262324;    // "포인트");
        public const int Characteristic_11 = 262325;    // "변경");
        public const int Characteristic_12 = 262326;    // "선행특성");
        public const int Characteristic_13 = 262327;    // "선행특성이 없습니다.");// 선행특성이 존재하지 않을 경우(레벨제한만 걸릴경우)

        #region 특성페이지
        public const int CharacteristicPage_0 = 262328;    // "공격");
        public const int CharacteristicPage_1 = 262329;    // "방어");
        public const int CharacteristicPage_2 = 262330;    // "속성");
        #endregion

        #endregion

        #region 계정보드에서 사용
        public const int AccountBoard_0 = 262331;    // "\"{0}\" 을 찍으시겠습니까?");
        public const int AccountBoard_1 = 262332;    // "소모포인트: ");
        public const int AccountBoard_2 = 262333;    // "명예포인트가 부족하여 습득하지 못했습니다.");
        public const int AccountBoard_3 = 262334;    // "포인트 획득하기");
        public const int AccountBoard_4 = 262335;    // "\"{0}\" 을 습득하였습니다.");
        public const int AccountBoard_5 = 262336;    // "명예 포인트");
        public const int AccountBoard_6 = 262337;    // "배우기");
        #endregion

        #region 전직에서 사용

        public const int JobAdvance_0 = 262338;    // "전직선택");
        public const int JobAdvance_1 = 262339;    // "전직 후 캐릭터는 되돌릴 수 없습니다.");
        public const int JobAdvance_2 = 262340;    // "전직하기");
        public const int JobAdvance_3 = 262341;    // "터치 시 스킬을 사용합니다.");

        public const int JobAdvance_10 = 99000;          // 워로드 기계적인 강화를 통한 육체의 힘을 극대화시킨 존재.강력한 공격력을 가졌지만 방어력이 취약합니다.
        public const int JobAdvance_11 = 99001;          // 블레이더 유전자 연구를 통해 모든 인체의 능력을 한계까지 끌어올린 존재.이도류를 휘두르며 광전사가 된 상태에서도 충분히 힘을 발휘한다.
        public const int JobAdvance_12 = 99002;          // 템플나이트 성성스러운 힘을 바탕으로 전투의 앞에서는 강력한 방어를. 뒤에서는 보조를 하며 전투를 승리로 이끄는 기사.
        public const int JobAdvance_13 = 99003;         // 가디언 기사의 신분이지만 공격적인 전사를	추구하며 생긴 두가지 조화를 가져온	특수한 유형
        public const int JobAdvance_14 = 99004;          // 소서러 마녀와 마법사에게 전해지는 특별한 시험을 통과하여 대마법사의 경지에 다다른 소서러. 모든 속성을 거침없이 다룬다.
        public const int JobAdvance_15 = 99005;         // 아티스트 성스러운 여연주로 아군들의 능력을 극대화 시키는 연주가. 마력을 노래에 담는 비법으로	아군을 보조한다.
        public const int JobAdvance_16 = 99006;          // 트레져헌터 모험에 필요한 모든 것을 겸비한 능력자로써, 여러 방면에서	자신의 특화 능력을 빛낸다.
        public const int JobAdvance_17 = 99007;			// 아티스트 총만 있으면 무서울 것이 없는 이들.	쌍권총의 시대를 만들기 위해	이들이 여행을 시작했다.

        #endregion

        #region npc자동이동에서 사용

        public const int NpcAuto_0 = 262342;    // "NPC 자동이동");
        public const int NpcAuto_1 = 262343;    // "NPC 이름");
        public const int NpcAuto_2 = 262344;    // "NPC 속성");
        public const int NpcAuto_3 = 262106;    // "NPC 찾기"
        public const int NpcAuto_4 = 262107;    // NPC와 대화를 통해 다양한 퀘스트를 획득할 수 있습니다.
        public const int NpcAuto_5 = 262108;    // 물건을 사고 팔 수 있는 NPC입니다.
        public const int NpcAuto_6 = 262109;    // 창고를 관리해주는 NPC 입니다.
        public const int NpcAuto_7 = 262110;    // 체력을 회복합니다.
        public const int NpcAuto_8 = 262111;    // 여러가지 아이템과 전투 필요 물품을 제작할 수 있습니다.
        public const int NpcAuto_9 = 262112;    // 다른 마을로 이동할 수 있습니다.
        public const int NpcAuto_10 = 262113;   // 집 꾸미기와 관리를 할 수 있습니다.
        public const int NpcAuto_11 = 262114;   // 더 높은 단계로 가기 위한 전직을 진행할 수 있습니다.
        public const int NpcAuto_12 = 262115;   // 하우징 좋아요 랭킹을 확인할 수 있습니다.
        public const int NpcAuto_13 = 262116;   // 제작 이벤트 참여에서 다양한 보상을 받을 수 있습니다.

        #endregion

        #region 파티에서 사용

        public const int PartyCommon_0 = 262345;    // "위치 : {0}");
        public const int PartyCommon_1 = 262346;    // "{0}님에게 이동 중 입니다...");
        public const int PartyCommon_2 = 262347;    // "파티{0}"); // 기본 파티명
        public const int PartyCommon_3 = 262348;    // "초대하기");
        public const int PartyCommon_4 = 262349;    // "참가");
        public const int PartyCommon_5 = 262350;    // "파티홍보 시 파티명으로 홍보됩니다.");
        public const int PartyCommon_6 = 262351;    // "파티생성");
        public const int PartyCommon_7 = 262352;    // "파티보기");
        public const int PartyCommon_8 = 262353;    // "파티를 생성하세요.");

        #region 파티정보

        public const int PartyDetail_0 = 262354;    // "파티정보");
        public const int PartyDetail_1 = 262355;    // "파티해산");
        public const int PartyDetail_2 = 262356;    // "파티명 변경");
        public const int PartyDetail_3 = 262357;    // "홍보");
        public const int PartyDetail_4 = 262358;    // "파티탈퇴");
        public const int PartyDetail_5 = 262359;    // "홍보중...");

        #endregion

        #region 파티초대

        public const int PartyInvite_0 = 262360;    // "파티초대");
        public const int PartyInvite_1 = 262361;    // "주변");

        #endregion

        #region 파티찾기

        public const int PartyNearby_0 = 262362;    // "파티찾기");
        public const int PartyNearby_1 = 262363;    // "파티장");
        public const int PartyNearby_2 = 262364;    // "파티명");
        public const int PartyNearby_3 = 262365;    // "인원");

        #endregion

        #endregion

        #region 부활에서 사용

        public const int ReBirth_0 = 262366;    // "캐릭터가 사망하였습니다.");
        public const int ReBirth_1 = 262367;    // "즉시 부활");
        public const int ReBirth_2 = 262368;    // "아이템 사용");
        public const int ReBirth_3 = 262369;    // "마을이동");
        public const int ReBirth_4 = 262370;    // "파티부활");
        public const int ReBirth_5 = 262548;         // 베히모스에서 부활할 때 부활 버튼에 표시해줄 내용
        public const int ReBirth_6 = 262549;         // "{0}초 뒤 마을로 자동이동합니다."); 의 베히모스 버전. (0)초 뒤 ~~ 합니다.

        #endregion

        #region 연마 초기화에서 사용

        public const int ResetGriding_0 = 262371;    // "초기화 할 아이템을 선택하세요.");
        public const int ResetGriding_1 = 262372;    // "연마 수치와 내구도를 초기화합니다.");
        public const int ResetGriding_2 = 262373;    // "현재 연마");
        public const int ResetGriding_3 = 262374;    // "현재 내구도");

        #endregion

        #region 소셜모션에서 사용

        public const int SocialMotion_0 = 262375;    // "나의 퀵슬롯");
        public const int SocialMotion_1 = 262376;    // "(보유 {0}/{1})");
        public const int SocialMotion_2 = 262377;    // "퀵슬롯 편집");
        public const int SocialMotion_3 = 262378;    // "모두보기");
        public const int SocialMotion_4 = 262379;    // "퀵슬롯 보기");
        public const int SocialMotion_5 = 262380;    // "상호작용");
        public const int SocialMotion_6 = 262381;    // "교체할 소셜 모션을 선택하세요");

        // 슬롯 이름 8개 : 240111~240118
        public const int SocialMotion_11 = 262541;    // "최근 사용한 소셜모션"
        public const int SocialMotion_12 = 262542;    // "보유하지 않은 소셜 모션입니다. 꾸미기 상점에서 구매할 수 있습니다. \n 꾸미기 상점으로 이동하시겠습니까?"
        public const int SocialMotion_13 = 262543;    // "{0}으로 등록하여 사용이 가능합니다."

        public const int SocialMotion_21 = 262382;    // "빈슬롯");
        public const int SocialMotion_22 = 262383;    // "미보유");
        public const int SocialMotion_23 = 262384;    // "사용불가");
        public const int SocialMotion_24 = 262385;    // "Wait!");
        public const int SocialMotion_25 = 262386;    // "Touch!");
        public const int SocialMotion_26 = 263794;         // ""보유 중인 폭죽이 없습니다. 폭죽을 구매해볼까요?"");

        #endregion

        #region IntroUI
        public const int IntroUI_000 = 261561;    // "위 내용에 동의합니다.", E_TEXTTYPE.LOCAL);
        public const int IntroUI_001 = 261562;    // "다른 계정 로그인", E_TEXTTYPE.LOCAL);
        public const int IntroUI_002 = 262120;    // "서버 변경"
        public const int IntroUI_003 = 262501;    // "네트워크가 안정적인 환경일 경우 더욱 빠르게 다운로드 받을 수 있습니다."
        #endregion

        #region 설정에서 사용

        public const int Setting_0 = 261800;    // "게임설정");
        public const int Setting_1 = 261801;    // "전투");
        public const int Setting_2 = 261802;    // "고객지원");
        public const int Setting_3 = 261803;    // "계정");

        public const int Setting_4 = 261804;    // "배경음");
        public const int Setting_5 = 261805;    // "효과음");
        public const int Setting_6 = 261806;    // "성우목소리");
        public const int Setting_7 = 261807;    // "푸시알림");
        public const int Setting_8 = 261808;    // "진동");
        public const int Setting_9 = 261809;    // "화면자동꺼짐");
        public const int Setting_10 = 261810;    // "던전초대");

        public const int Setting_11 = 261811;    // "타유저 데미지");
        public const int Setting_12 = 261812;    // "타유저 이펙트");
        public const int Setting_13 = 261813;    // "조작키");
        public const int Setting_14 = 261814;    // "포션 자동사용");

        public const int Setting_15 = 261815;    // "약관, 운영정책");
        public const int Setting_16 = 261816;    // "FAQ");
        public const int Setting_17 = 261817;    // "공식카페로 이동");
        public const int Setting_18 = 261818;    // "추가 다운로드");
        public const int Setting_19 = 261819;    // "현재버전");
        public const int Setting_20 = 261820;    // "최신버전");
        public const int Setting_21 = 261821;    // "쿠폰입력");

        public const int Setting_22 = 261822;    // "계정 연동");
        public const int Setting_23 = 261823;    // "구글");
        public const int Setting_24 = 261824;    // "페이스북");
        public const int Setting_25 = 261825;    // "이메일");
        public const int Setting_26 = 261826;    // "계정관리");
        public const int Setting_27 = 261827;    // "로그아웃");
        public const int Setting_28 = 261828;    // "계정삭제");

        public const int Setting_29 = 261829;    // "캐릭터 선택화면");

        public const int Setting_30 = 262018;    // "푸시알림이 {0} 상태로 변경되었습니다.");
        public const int Setting_31 = 262019;    // "게임 진동이 {0} 상태로 변경되었습니다.");
        public const int Setting_32 = 262020;    // "야간 진동 가능이 {0} 상태로 변경되었습니다.");
        public const int Setting_33 = 262021;    // "타유저 데미지 표시가 {0} 상태로 변경되었습니다.");
        public const int Setting_34 = 262022;    // "타유저 이펙트 표시가 {0} 상태로 변경되었습니다.");
        public const int Setting_35 = 262023;    // "조작키 고정 여부가 {0} 상태로 변경되었습니다.");
        public const int Setting_36 = 262024;    // "블루투스 탐색기능이 {0} 상태로 변경되었습니다.");
        public const int Setting_37 = 262025;    // "블루투스 검색대상 등록이 {0} 상태로 변경되었습니다.");
        public const int Setting_38 = 262026;    // "플레이어 이펙트 퀄리티가 {0} 상태로 변경되었습니다.");
        public const int Setting_39 = 262027;    // "계정이 연동되었습니다.");
        public const int Setting_40 = 262028;    // "이메일 계정이 연동되었습니다.");

        public const int Setting_41 = 262029;    // "상");
        public const int Setting_42 = 262030;    // "중");
        public const int Setting_43 = 262031;    // "하");

        public const int Setting_44 = 262032;    // "전투 연출 효과가 {0} 상태로 변경되었습니다.");

        public const int Setting_45 = 262033;    // "항상 적용");
        public const int Setting_46 = 262034;    // "일부 적용");
        public const int Setting_47 = 262035;    // "적용 안함");

        public const int Setting_48 = 262050;    // "쿠폰번호를 입력해주세요.");

        public const int Setting_49 = 262054;    // "자동 포션 사용이 {0} 상태로 변경되었습니다.");

        public const int Setting_50 = 262101;    // "경고 : 계정 연동이 되지 않은 경우 계정이 삭제됩니다."

        public const int Setting_51 = 262126;    // "게임센터 계정이 연동되었습니다."

        public const int Setting_52 = 262427;         // "{0}로 변경되었습니다."

        public const int Setting_53 = 262490;    // "전투 시 카메라 흔들림이 {0}로 변경되었습니다.");

        public const int Setting_54 = 262510;         // "서버 획득 알림이 {0} 상태로 변경되었습니다."

        public const int Setting_55 = 262527;         // "포션 자동 사용이 {0} 상태로 변경되었습니다."

        public const int Setting_56 = 262535;         // "가이드 튜토리얼이 {0} 상태가 되었습니다."

        public const int Setting_57 = 262574;        // 자동사냥 보스 피하기가 {0} 상태로 변경되었습니다.
        public const int Setting_58 = 263328; // "회원중심"
        public const int Setting_59 = 269407;   // "외곽선 효과가 {0}상태로 변경되었습니다."

        #endregion

        #region 이벤트게시판
        public const int Event_0 = 262127;       // "보상 현황"
        public const int Event_1 = 262128;       // "다음 보상까지"
        public const int Event_2 = 262129;       // "선물 받기"
        public const int Event_3 = 262130;       // "받을 수 있는 선물"
        public const int Event_4 = 262131;       // "{0}개 남음"
        public const int Event_5 = 262132;       // "전체 달성"
        public const int Event_6 = 262133;       // "티켓 이벤트!"
        public const int Event_7 = 262146;            // "보상이벤트"
        public const int Event_8 = 320009;            // "펫 티켓"    // 박스가챠 최종 보상용...
        public const int Event_9 = 320010;            // "아이템 티켓" // 박스가챠 최종 보상용..
        public const int Event_10 = 262519;          //현재 진행 중인 이벤트가 없습니다.
        #endregion

        #region 오늘의 미션
        public const int Daily_0 = 262147;            // "오늘의 미션"
        public const int Daily_1 = 262148;            // "클리어 보상 받기"
        public const int Daily_2 = 262149;            // "전체 달성"
        #endregion

        #region 패키지 상점
        public const int Package_0 = 262440;   // "패키지상점"
        public const int Package_1 = 262441;   // "일일패키지"
        public const int Package_2 = 262442;   // "시나리오"
        public const int Package_3 = 262443;   // "레벨달성"
        public const int Package_4 = 262444;   // "패키지"
        public const int Package_5 = 262445;   // "일일 다이아 패키지 상품을 구매합니다."
        public const int Package_6 = 262446;   // "시나리오 패키지 상품을 구매합니다."
        public const int Package_7 = 262447;   // "레벨달성 패키지 상품을 구매합니다."
        public const int Package_8 = 262448;   // "{0} 패키지 상품을 구매합니다."
        public const int Package_9 = 262449;   // "수령완료"
        public const int Package_10 = 262469;   // "상품을 구매하는데 실패하였습니다."
        public const int Package_11 = 262470;   // "상품을 구매하는데 취소하였습니다."
        public const int Package_12 = 262471;   // "구매 가능 한도를 초과하였습니다."
        public const int Package_13 = 262472;   // "일일패키지 상품이 구매되었습니다."
        public const int Package_14 = 262473;   // "{0}일차 보상을 받았습니다."
        public const int Package_15 = 262474;   // "시나리오 패키지 상품이 구매되었습니다."
        public const int Package_16 = 262475;   // "레벨달성 패키지 상품이 구매되었습니다."
        public const int Package_17 = 262476;   // "미지급되었던 상품이 지급되었습니다."
        public const int Package_18 = 262485;   // "상품을 구매하였습니다."
        public const int Package_19 = 262486;   // "즉시지급"
        public const int Package_20 = 262487;  // "{0}레벨 달성"
        public const int Package_21 = 262488;   // "보상이 지급되었습니다. 우편함을 확인하여 주세요."
        public const int Package_22 = 262508;   // "※청약철회는 구매일로부터 7일 이내 가능합니다. (단, 사용했을 경우 청약철회 불가) / 법정대리인 동의 없이 미성년자 명의의 결제수단을 통해 결제한 경우 취소할 수 없습니다. [FF0FFF0F][자세히보기][/c]"
        public const int Package_23 = 262509;   // "청약철회 안내"
        #endregion

        #region OptionChangerUI

        public const int OptionChangerUI_0 = 262619; //"{0} 옵션이 무작위로 변경됩니다."
        public const int OptionChangerUI_1 = 262620; //"{0} 의 변경할 옵션을 선택해주세요."
        public const int OptionChangerUI_2 = 262621; //"\"{0}\"의 모든 옵션을 랜덤하게 변경합니다."
        public const int OptionChangerUI_3 = 262622; //"주의! 변경 후에는 되돌릴 수 없습니다."
        public const int OptionChangerUI_4 = 262623; //"\"{0}\"의 \"{1}\"을 랜덤하게 변경합니다."
        public const int OptionChangerUI_5 = 262624; //"주의! 변경 후에는 되돌릴 수 없습니다."
        public const int OptionChangerUI_6 = 262625; //"옵션을 모두 리셋합니다."

        #endregion

        public const int TouchPopup000 = 260363;    // "플레이어 정보를 가져오는데 실패했습니다.");
        #endregion 일반 UI사용

        #region 공용팝업 문구

        public const int PopupText_0 = 260364;    // "친구요청을 보냈습니다.");
        public const int PopupText_1 = 260365;    // "친구목록이 가득찼습니다");
        public const int PopupText_2 = 260366;    // "대기실 정보가 잘못되었습니다.");
        public const int PopupText_3 = 260367;    // "등록 성공!");
        public const int PopupText_4 = 260368;    // "등록 실패!");
        public const int PopupText_5 = 260369;    // "퀘스트 완료");
        public const int PopupText_6 = 260370;    // "체력이 이미 가득 차 있습니다.");
        public const int PopupText_7 = 260371;    // "초기화 되었습니다.");
        public const int PopupText_8 = 260372;    // "구매가 완료되었습니다.");
        public const int PopupText_9 = 260373;    // "인벤토리가 꽉 찼습니다.");

        public const int PopupText_10 = 260374;    // "개의 아이템이 메일로 발송됩니다.");
        public const int PopupText_11 = 260375;    // "님을 초대하였습니다.");
        public const int PopupText_12 = 260376;    // "알 수 없는 이유로\n초대에 실패하였습니다.");
        public const int PopupText_13 = 260377;    // "우정 포인트 보냈습니다");
        public const int PopupText_14 = 260378;    // "모두 요청을 보냈습니다");
        public const int PopupText_15 = 260379;    // "더 이상 선택할 수 없습니다.");
        public const int PopupText_16 = 260380;    // "상자를 열어 아이템을 받았습니다.");
        public const int PopupText_17 = 260381;    // "인벤토리가 꽉 차 우편으로 발송됩니다.");
        public const int PopupText_18 = 260382;    // "장착되었습니다.");
        public const int PopupText_19 = 260383;    // "추출되었습니다.");

        public const int PopupText_20 = 260384;    // "판매가 완료되었습니다.");
        public const int PopupText_21 = 260385;    // "길드를 정말 탈퇴하시겠습니까?\n탈퇴후 24시간동안 가입이 불가능합니다.");
        public const int PopupText_22 = 260386;    // "{0}님이 {1}에서 초대했습니다.\n입장하시겠습니까?");
        public const int PopupText_23 = 260387;    // "정말 귀환하시겠습니까?");
        public const int PopupText_24 = 260388;    // "귀환 가능시간이 아닙니다.\n 엘리를 사용하여 귀환하시겠습니까?");
        public const int PopupText_25 = 260389;    // "퇴장하시겠습니까?");
        public const int PopupText_26 = 260390;    // "아이템보상을 받으시겠습니까?");
        public const int PopupText_27 = 260391;    // "다이아가 부족합니다. 충전하시겠습니까?");
        public const int PopupText_28 = 260392;    // "해당 아이템을 빙고에 등록하시겠습니까?\n등록된 아이템은 사라집니다.");
        public const int PopupText_29 = 260393;    // "빙고 등록에 필요한 아이템이 부족합니다.\n강제 등록하시겠습니까?");

        public const int PopupText_30 = 260394;    // "해당 몬스터 도감을 등록하시겠습니까?\n등록에 사용된 아이템은 사라집니다.\n확률30%");
        public const int PopupText_31 = 260395;    // "아이템이 모자라 등록할 수 없습니다.\n(현재 ");
        public const int PopupText_32 = 260396;    // " 개 보유)");
        public const int PopupText_33 = 260397;    // "퀘스트를 포기하시겠습니까?");
        public const int PopupText_34 = 260398;    // "마을로 부활하시겠습니까?");
        public const int PopupText_35 = 260399;    // "정말 부활하시겠습니까?");
        public const int PopupText_36 = 260400;    // "다이아가 부족합니다.");
        public const int PopupText_37 = 260401;    // "엘리가 부족합니다.\n상점으로 이동하시겠습니까?");
        public const int PopupText_38 = 260402;    // "체력을 회복하시겠습니까?");
        public const int PopupText_39 = 260403;    // "해당 타운포탈로 이동하시겠습니까?");

        public const int PopupText_40 = 260404;    // "을(를) 친구목록에서\n삭제하시겠습니까?");
        public const int PopupText_41 = 260405;    // "길드성향을 '");
        public const int PopupText_42 = 260406;    // "'(으)로 만드시겠습니까?");
        public const int PopupText_43 = 260407;    // "염색 하시겠습니까?");
        public const int PopupText_44 = 260408;    // "아이템을 삭제하시겠습니까?");
        public const int PopupText_45 = 260409;    // "열쇠가 부족합니다.");
        public const int PopupText_46 = 260410;    // "등록하시겠습니까?");
        public const int PopupText_47 = 260411;    // "명예포인트가 부족합니다.");
        public const int PopupText_48 = 260412;    // "선택한 항목을 모두 판매합니다.");
        public const int PopupText_49 = 260413;    // "해당 펫을 장착하시겠습니까?");

        public const int PopupText_50 = 260414;    // "대상을 찾을 수 없습니다.");
        public const int PopupText_51 = 260415;    // "자기 자신을 대상으로 할 수 없습니다.");
        public const int PopupText_52 = 260416;    // "엘리가 부족합니다.");
        public const int PopupText_53 = 260417;    // "소속된 길드가 없습니다.");
        public const int PopupText_54 = 260418;    // "출석이 확인되었습니다.");
        public const int PopupText_55 = 260419;    // "보상이 우편으로 발송되었습니다.");
        public const int PopupText_56 = 260420;    // "가 부족합니다.");
        public const int PopupText_57 = 260421;    // "보상을 받았습니다.");
        public const int PopupText_58 = 260422;    // "인벤토리가 부족합니다.");
        public const int PopupText_59 = 260423;    // "특성은 레벨 30부터 설정할 수 있습니다.");

        public const int PopupText_60 = 260424;    // "포탈을 이용하기 위해선\n필드 오픈 임무를 먼저 완료해야합니다.");
        public const int PopupText_61 = 260425;    // "히든 포탈 미션을 시작하시겠습니까?");
        public const int PopupText_62 = 260426;    // "이미 진행중인 히든 포탈 미션이 존재합니다.\n기존 퀘스트를 포기하고 진행하시겠습니까?");
        public const int PopupText_63 = 260427;    // "아직 준비중입니다.");
        public const int PopupText_64 = 260428;    // "이미 요청을 보냈습니다.");
        public const int PopupText_65 = 260429;    // "자동 이동/사냥 모드에서는 UI를 열 수 없습니다.");

        public const int PopupText_66 = 260430;    // "다이아가 부족합니다.");
        public const int PopupText_67 = 260431;    // "등록 횟수가 최대치입니다. 더 이상 등록할 수 없습니다.");
        public const int PopupText_68 = 260432;    // "카드를 등록하시겠습니까?\n(확률 ");
        public const int PopupText_69 = 260433;    // "자동 이동/사냥 모드가 종료됩니다.");
        public const int PopupText_70 = 260434;    // "채집 포인트가 부족합니다.");

        public const int PopupText_71 = 260435;    // "유저가 접속 중이 아닙니다.");
        public const int PopupText_72 = 260436;    // "유저의 캐릭터를 찾을 수 않습니다.");
        public const int PopupText_73 = 260437;    // "유저의 정보를 찾을 수 없습니다.");
        public const int PopupText_74 = 260438;    // "해당 유저는 지금 초대할 수 없습니다.");
        public const int PopupText_75 = 260439;    // "해당 유저는 던전에 입장 할 수 없습니다.");
        public const int PopupText_76 = 260440;    // "해당 슬롯에 이미 초대한 유저가 있습니다.");
        public const int PopupText_77 = 260441;    // "이미 초대 중인 친구입니다.");
        public const int PopupText_78 = 260442;    // "이미 던전에 있는 친구입니다.");
        public const int PopupText_79 = 260443;    // "현재 입장 할 수 없습니다.");
        public const int PopupText_80 = 260444;    // "입장을 위한 퀘스트를 진행하시겠습니까?");
        public const int PopupText_81 = 260445;    // "은 던전에서 진행되는 퀘스트 입니다.\n던전 빠른 매칭을 진행하시겠습니까?");
        public const int PopupText_82 = 260446;    // "업데이트 예정입니다.");

        public const int PopupText_83 = 260447;    // "창고가 가득 찼습니다.\n창고 5칸을 확장하시겠습니까?");
        public const int PopupText_84 = 260448;    // "펫 인벤토리가 부족합니다.");
        public const int PopupText_85 = 260449;    // "창고를 확장하시겠습니까?\n확장 칸수: 5칸");
        public const int PopupText_86 = 260450;    // "창고가 확장되었습니다.");
        public const int PopupText_87 = 260451;    // "더 이상 창고를 확장할 수 없습니다.");
        public const int PopupText_88 = 260452;    // "더 이상 퀘스트 받을 수 없습니다.");
        public const int PopupText_89 = 260453;    // "특성을 초기화합니다.");
        public const int PopupText_90 = 260454;    // "특성이 성공적으로 변경되었습니다.");
        public const int PopupText_91 = 260455;    // "{0}를(을) 배우시겠습니까?\n특성포인트{1}이 소모됩니다.");
        public const int PopupText_92 = 260456;    // "{0}초 뒤 마을로 자동이동합니다.");
        public const int PopupText_93 = 260457;    // "부활 후 5초간 무적입니다. ({0})");
        public const int PopupText_94 = 260458;    // "부활 아이템이 부족합니다.");
        public const int PopupText_95 = 260459;    // "즉시 부활 (Lv.5 미만 무료)");
        public const int PopupText_96 = 260460;    // "{0}를(을) 사용하였습니다.");
        public const int PopupText_97 = 260461;    // "효과: {0}");
        public const int PopupText_98 = 260462;    // "입장횟수가 {0}회 남아있습니다.\n입장횟수를 모두 소진 후 사용해 주세요.");
        public const int PopupText_99 = 260463;    // "오늘의 던전입장횟수를 초기화합니다.\n[f00ff00f]※남아있는 횟수가 0일때만 사용가능[/c]\n\n[f44]현재 입장가능 횟수: {0}[/f]");

        public const int PopupText_100 = 260464;    // "보상획득");
        public const int PopupText_101 = 260465;    // "착용 중인 아이템은 창고에 보관할 수 없습니다.");
        public const int PopupText_102 = 260466;    // "잠금 상태인 아이템은 창고에 보관할 수 없습니다.");
        public const int PopupText_103 = 260467;    // "연마에 성공한 아이템은 창고에 보관할 수 없습니다.");
        public const int PopupText_104 = 260468;    // "연마에 성공한 아이템은 창고에 보관할 수 없습니다.");
        public const int PopupText_105 = 260469;    // "연마 초기화 완료");
        public const int PopupText_106 = 260470;    // "{0}의 연마 횟수와 내구도가 초기화 되었습니다.");
        public const int PopupText_107 = 260471;    // "[ffffffff]랭킹보상[/c]\n{0}점");
        public const int PopupText_108 = 260472;    // "[ffffffff]점수보상[/c]\n{0}점");
        public const int PopupText_109 = 260473;    // "이미 전직한 캐릭터입니다.");
        public const int PopupText_110 = 260474;    // "전직을 하기 위한 레벨이 부족합니다.");
        public const int PopupText_111 = 260475;    // "착용 중인 펫은 창고에 보관할 수 없습니다.");
        public const int PopupText_112 = 260476;    // "잠금 상태인 펫은 창고에 보관할 수 없습니다.");
        public const int PopupText_113 = 260477;    // "{0}를(을) {1}로 전직합니다.\n[f00ff00f]※전직 후 캐릭터는 되돌릴 수 없습니다.[/c]");
        public const int PopupText_114 = 260478;    // "전직 시 캐릭터 특성이 초기화됩니다.");
        public const int PopupText_115 = 260479;    // "전직이 완료되었습니다. '{0}' 전직 보상이 우편으로 지급되었습니다.");
        public const int PopupText_116 = 260480;    // "변경된 특성트리와 스테이터스를 확인해보세요!");
        public const int PopupText_117 = 260481;    // "퀘스트 수행 레벨이 부족합니다.");
        public const int PopupText_118 = 260482;    // "스킬 쿨타임을 확인해 주세요.");
        public const int PopupText_119 = 260483;    // "해당 상품을 구매하시겠습니까?");
        public const int PopupText_120 = 260484;    // "우정 포인트가 부족합니다.");
        public const int PopupText_121 = 260485;    // "아이템을 사용하여 부활하시겠습니까?");
        public const int PopupText_122 = 260486;    // "초기화 티켓이 없습니다.");
        public const int PopupText_123 = 260487;    // "마일리지를 획득하였습니다.");
        public const int PopupText_124 = 261990;    // "초기화할 특성이 없습니다.");

        public const int PopupText_201 = 260488;    // "잠금된 아이템은 삭제할 수 없습니다.");
        public const int PopupText_202 = 260489;    // "장착한 아이템은 삭제할 수 없습니다.");
        public const int PopupText_203 = 260490;    // "아이템을 삭제하시겠습니까?");
        public const int PopupText_204 = 260491;    // "잠금된 펫은 삭제할 수 없습니다.");
        public const int PopupText_205 = 260492;    // "장착중인 펫입니다.");
        public const int PopupText_206 = 260493;    // "펫을 삭제하시겠습니까?");
        public const int PopupText_207 = 260494;    // "해당 직업 아이템이 아닙니다");
        public const int PopupText_208 = 260495;    // "해당 성별 아이템이 아닙니다.");
        public const int PopupText_209 = 260496;    // "레벨 이상 착용할 수 있습니다.");
        public const int PopupText_210 = 261870;    // "장착 중인 펫은 진화(초월)할 수 없습니다.\n바로 장착 해제 하고 대상을 등록하시겠습니까?");

        public const int PopupText_299 = 260497;    // "칭호를 획득 실패하였습니다.");
        public const int PopupText_300 = 260498;    // "칭호를 획득하시겠습니까?");
        public const int PopupText_301 = 260499;    // "칭호를 획득하였습니다.");
        public const int PopupText_302 = 260500;    // "횟수 초기화 완료");
        public const int PopupText_303 = 260501;    // "던전 도전 가능 횟수가 초기화되었습니다.");
        public const int PopupText_304 = 260502;    // "스킬트리 초기화 완료");
        public const int PopupText_305 = 260503;    // "스킬트리가 초기화되었습니다.");

        public const int PopupText_306 = 260504;    // "헤어 변경 미리보기");
        public const int PopupText_307 = 260505;    // "외형 변경 미리보기");
        public const int PopupText_308 = 260506;    // "랜덤으로 {0}를(을) 변경합니다.\n확인 시 변경권 1장을 소모합니다.");
        public const int PopupText_309 = 260507;    // "외형");
        public const int PopupText_310 = 260508;    // "헤어");
        public const int PopupText_311 = 260509;    // "변경 취소");
        public const int PopupText_312 = 260510;    // "변경 확정");
        public const int PopupText_313 = 260511;    // "취소 시 변경권은 사라집니다.");
        public const int PopupText_314 = 260512;    // "외형 랜덤 변경 완료");
        public const int PopupText_315 = 260513;    // "캐릭터의 얼굴 외형이 변경되었습니다.");
        public const int PopupText_316 = 260514;    // "헤어 랜덤 변경 완료");
        public const int PopupText_317 = 260515;    // "캐릭터의 헤어가 변경되었습니다.");
        public const int PopupText_318 = 262387;    // "랜덤 변경");

        public const int PopupText_351 = 262388;    // "파티에 가입되었습니다.");
        public const int PopupText_352 = 262389;    // "파티를 탈퇴하였습니다.");
        public const int PopupText_353 = 262390;    // "파티에서 추방되었습니다.");
        public const int PopupText_354 = 262391;    // "대상이 파티초대를 받을 수 없는 상태입니다.");
        public const int PopupText_355 = 262392;    // "파티 상태에서는 할 수 없습니다.");
        public const int PopupText_356 = 262393;    // "너무 많은 채팅을 입력하였습니다. 잠시만 기다려주세요.");
        public const int PopupText_357 = 262394;    // "대상이 이미 파티중입니다.");
        public const int PopupText_358 = 262395;    // "이미 초대중입니다.");
        public const int PopupText_359 = 262396;    // "이미 파티가 가득 찼습니다.");
        public const int PopupText_360 = 262397;    // "대상이 필드에 있지 않습니다.");
        public const int PopupText_361 = 262398;    // "해당 파티를 찾을 수 없습니다.");
        public const int PopupText_362 = 262399;    // "대상이 싱글모드 중입니다.");
        public const int PopupText_363 = 262400;    // "대상이 접속중이지 않습니다.");
        public const int PopupText_364 = 262401;    // "대상이 튜토리얼 중입니다.");
        public const int PopupText_365 = 262402;    // "대상이 초대를 받을 수 없는 상태입니다.");

        public const int PopupText_366 = 261855;    // "엘리가 부족합니다.\n캐시상점으로 이동해\n엘리를 구매할 수 있습니다.");
        public const int PopupText_367 = 261856;    // "다이아가 부족합니다.\n캐시상점으로 이동해\n다이아를 구매할 수 있습니다.");
        public const int PopupText_368 = 261857;    // "열쇠가 부족합니다.\n캐시상점으로 이동해\n열쇠를 구매할 수 있습니다.");
        public const int PopupText_369 = 261858;    // "엘리 또는 마일리지가 부족합니다.\n캐시상점으로 이동해\n엘리와 마일리지를 구매할 수 있습니다.");
        public const int PopupText_370 = 261859;    // "캐시상점으로");
        public const int PopupText_371 = 261860;    // "열 수 있는 보물상자가 없습니다.");

        public const int PopupText_372 = 261861;    // "엘리 부족");
        public const int PopupText_373 = 261862;    // "다양한 방법으로 엘리를 획득하세요.");
        public const int PopupText_374 = 261863;    // "추천 사냥터에서 사냥하기");
        public const int PopupText_375 = 261864;    // "장비 정리하기");
        public const int PopupText_376 = 261865;    // "충전하기");
        public const int PopupText_377 = 261866;    // "엘리던전 참여하기");
        public const int PopupText_378 = 261867;    // "바로가기");

        public const int PopupText_391 = 261542;    // "시나리오 퀘스트 : [집 구매]를\n완료해야 합니다.");

        public const int PopupText_395 = 261878;    // "착용 가능한 직업이 아닙니다.");
        public const int PopupText_396 = 261886;    // "마나(분노)가 부족하여 스킬을 사용할 수 없습니다.");

        public const int PopupText_400 = 261898;    // "RAID BOSS 출현!");
        public const int PopupText_401 = 261899;    // "레이드 보스를 처치하고 보상을 획득하세요.");
        public const int PopupText_402 = 261900;    // "EPIC BOSS 출현!");
        public const int PopupText_403 = 261901;    // "친구들과 함께 처치하세요!");
        public const int PopupText_404 = 261902;    // "경험치 획득량 {0}%증가");
        public const int PopupText_405 = 261903;    // "엘리 획득량 {0}%증가");
        public const int PopupText_406 = 261904;    // "펫 추가 드랍률 {0}%증가");
        public const int PopupText_407 = 261905;    // "아이템 추가 드랍률 {0}%증가");

        public const int PopupText_421 = 261932;    // "현재 업적 보상을 받을 수 없는 상태입니다.");
        public const int PopupText_422 = 261933;    // "출석체크를 하기 위해서는 게임을 재접속 해야합니다."); // 안씀

        public const int PopupText_431 = 261994;    // "추출하지 않을 경우 퍼즐은 파괴됩니다.");
        public const int PopupText_432 = 261995;    // "퍼즐을 장착 중이지 않습니다.");
        public const int PopupText_433 = 261996;    // "장비 옵션이 존재하지 않습니다.");
        public const int PopupText_434 = 261997;    // "옵션이 사라집니다.");
        public const int PopupText_435 = 261998;    // "위 옵션중 랜덤으로 한 개가 적용됩니다.");
        public const int PopupText_436 = 262002;    // "장착 시 전투력 {0}");
        public const int PopupText_437 = 262003;    // "장착 시 공격력 {0}");
        public const int PopupText_438 = 262004;    // "{0}을 장착하였습니다.");
        public const int PopupText_439 = 262005;    // "{0}이 적용되었습니다.");
        public const int PopupText_440 = 262006;    // "공격력이 {0}{1} 하였습니다."); // {0}:수치 // {1}:증가/감소
        public const int PopupText_441 = 262051;    // "이미 존재하는 길드 이름입니다.");
        public const int PopupText_442 = 262060;    // "티켓이 없어 대기실로 갈 수 없습니다.");
        public const int PopupText_443 = 262000;    // "사용가능한 포션 개수를 초과했습니다.");
        public const int PopupText_444 = 262086;    // "초대 받는 사람의 티켓이 부족하여 초대에 실패했습니다.");

        public const int PopupText_451 = 262087;    // "캐릭터 직업이 조건에 맞지 않습니다.");
        public const int PopupText_452 = 262088;    // "캐릭터 성별이 조건에 맞지 않습니다.");
        public const int PopupText_453 = 262089;    // "캐릭터 레벨이 조건에 맞지 않습니다.");

        public const int PopupText_454 = 262121;         // "박스 가챠 이벤트 보상을 획득하였습니다."
        public const int PopupText_455 = 262122;         // "박스 가챠 이벤트 전체 보상을 획득하였습니다."

        public const int PopupText_456 = 262125;         // "달성이벤트 보상을 획득하였습니다."

        public const int PopupText_457 = 262405;         // "일일미션 보상을 획득하였습니다."
        public const int PopupText_458 = 262414;         // "스킬강화포인트를 획득했습니다."

        public const int PopupText_459 = 262426;         // "{0} 상태인 펫은 사용할 수 없습니다."

        public const int PopupText_469 = 262467;   // "하루동안 띄우지 않습니다." (먼가 이상한데 좋은 문장으로 수정 부탁드려요)

        public const int PopupText_470 = 262428;   // "[F00FF00F]상점보다 낮은 가격에 판매됩니다.\n판매 시 복구 할 수 없습니다.[/c]"
        public const int PopupText_471 = 262429;   // "{0}엘리를 획득했습니다."
        public const int PopupText_472 = 262468;        // "입력할 수 있는 글자수를 초과했습니다." (로컬)

        public const int PopupText_473 = 262513;        // "해당 지역에서는 자동 이동을 할 수 없습니다."
        public const int PopupText_474 = 262514;        // "해당 지역에서는 자동 사냥을 할 수 없습니다."

        public const int PopupText_475 = 262517;        // "잠금했습니다. 기기전환시 잠금이 해제될 수 있습니다."
        public const int PopupText_476 = 262518;        // "잠금 해제했습니다."
        public const int PopupText_477 = 262565;        // 부활하여 베히모스 뱃속 처음으로 이동합니다.

        public const int PopupText_481 = 262571;         // "소셜모션 진행 중에는 외형을 변경할 수 없습니다."
        public const int PopupText_482 = 262591;         // "핫타임 티켓을 사용합니다.\n\n충전되는 이용시간"
        public const int PopupText_483 = 262592;         // "핫타임 추가 이용 시간";


        public const int PopupText_484 = 262601;              // 장착 중인 펫은 판매할 수 없습니다.
        public const int PopupText_485 = 262602;              // 차단할 대상을 입력하세요.
        public const int PopupText_486 = 262603;              // 가입된 길드가 없습니다. 길드에 가입해보세요!
        public const int PopupText_487 = 262604;              // 가입된 파티가 없습니다. 파티에 가입해보세요!
        public const int PopupText_488 = 262605;              // 이미 차단한 유저입니다!
        public const int PopupText_489 = 262606;              // {0}의 채팅을 차단합니다.(최대 10자 메모)
        public const int PopupText_490 = 262607;              // 지금은 다시 요청 할 수 없습니다!

        public const int PopupText_491 = 262616;             // 선택한 펫이 무기를 장착 중입니다. 판매하시겠습니까?
        public const int PopupText_492 = 262617;             // 판매 항목 중에 무기를 장착한 펫이 있습니다. 전부 판매하시겠습니까?
        public const int PopupText_493 = 262618;             // 선택한 펫이 무기를 장착 중입니다. 버리시겠습니까?
        public const int PopupText_494 = 263391;             // 블루다이아가 부족합니다.
        public const int PopupText_495 = 263392;             // 즐겨찾기 목록이 가득찼습니다.
        public const int PopupText_496 = 263393;             // 즐겨찾기 목록에서 제거되었습니다.
        public const int PopupText_497 = 263408;                  // 거래소 마을에서만 열 수 있다는 내용의 팝업.

        public const int PopupText_498 = 263419;                 // 거래소 아이템 등록시 수량을 0개로 세팅한 채 등록하려고 할 때 안내 팝업
        public const int PopupText_499 = 263420;                 // 거래소 아이템 등록시 가격을 0개로 세팅한 채 등록하려고 할 때 안내 팝업
        public const int PopupText_500 = 263421;                 // 거래소 아이템 등록시 총 가격이 21억을 넘긴 채 등록하려고 할 때 안내 팝업

        public const int PopupText_501 = 263816;             // 웨딩 드레스는 마을에서만 구매하실 수 있습니다.

        public const int PopupText_510 = 263896;					// 거래불가 아이템이 포함되어 있습니다. 합성 후, 결과물이 '거래불가'상태가 됩니다. 합성을 계속 하시겠습니까?
        public const int PopupText_511 = 263918;                 // "이미 보유중인 칭호입니다.");

        #endregion 공용팝업 문구

        #region 튜토리얼 알람 문구
        // 프롤로그 튜토리얼
        public const int TutorialText_0 = 166000;    // "좌측 버튼을 이용해 캐릭터를 목표지점까지 이동시켜보세요.");
        public const int TutorialText_1 = 166001;    // "점프 버튼을 2번 누르거나 계속 누르고 있으면 2단 점프가 가능합니다. 2단 점프를 해보세요!");
        public const int TutorialText_2 = 166002;    // "우측의 공격버튼으로 몬스터를 공격할 수 있습니다.");
        public const int TutorialText_3 = 166003;    // "스킬을 사용해서 몬스터에게 강력한 공격을 해보세요!");
        public const int TutorialText_4 = 166004;    // "포탈 위로 올라가 방향키를 위로 밀어보세요.");
        public const int TutorialText_5 = 166005;    // "마신을 쓰러뜨려보자.");
        public const int TutorialText_6 = 166006;    // "스킬을 명중시켜 보자.");
        public const int TutorialText_7 = 166007;    // "2단점프를 해보자.");
                                                     // 인게임 튜토리얼
        public const int TutorialText_8 = 166008;    // "NPC AUTO 버튼을 사용해서 자동으로 이동해볼까요?");
        public const int TutorialText_9 = 166009;    // "화살표 방향으로 드래그 하여 원하는 NPC를 찾으세요");
        public const int TutorialText_10 = 166010;    // "이동을 원하는 NPC를 확인하고 바로가기 버튼을 눌러보세요.");
        public const int TutorialText_11 = 166011;    // "우측 상단의 메뉴 확장 버튼을 눌러 목록을 열어보세요");
        public const int TutorialText_12 = 166012;    // "캐릭터 버튼을 눌러 보세요.");
        public const int TutorialText_13 = 166013;    // "내가방 버튼을 눌러 인벤토리를 확인해 보세요.");
        public const int TutorialText_14 = 166014;    // "장착할 아이템을 터치해보세요.");
        public const int TutorialText_15 = 166015;    // "좌측 하단의 장착 버튼을 눌러서 아이템을 착용해 보세요,");
        public const int TutorialText_16 = 166016;    // "뒤로가기 버튼을 눌러 게임으로 돌아가 보세요.");
        public const int TutorialText_17 = 166017;    // "NPC 앞에서 화면을 꾹 누르면서 위쪽으로 밀면 대화를 진행할 수 있습니다.");
        public const int TutorialText_18 = 166018;    // "세로에게 말을 걸어보세요.");
        public const int TutorialText_20 = 166019;    // "상점으로 들어가보세요");
        public const int TutorialText_21 = 166020;    // "구매 버튼을 눌러보세요. 첫 회는 무료로 제공합니다.");
        public const int TutorialText_22 = 166021;    // "멋진 펫을 뽑았네요! 그럼 이제 돌아가 볼까요?");
        public const int TutorialText_23 = 166022;    // "장착할 펫을 터치해 보세요.");
        public const int TutorialText_24 = 166023;    // "펫을 장착할 슬롯을 선택하세요.");
        public const int TutorialText_25 = 166024;    // "새로운 스킬을 획득했습니다! 아세스에게 보고하세요.");
        public const int TutorialText_26 = 166025;    // "퀘스트 이동을 눌러서 자동으로 퀘스트를 수행해 보세요.");
        public const int TutorialText_27 = 166026;    // "필드에서는 NPC AUTO가 AUTO로 변합니다. 자동사냥을 멈추고 싶으면 버튼을 눌러보세요.");
        public const int TutorialText_28 = 166027;    // "튜토리얼 필드에서 벗어났습니다.");
        public const int TutorialText_29 = 166028;    // "퀘스트 완료를 위해 아세스를 찾아가세요.");
        public const int TutorialText_30 = 166029;    // "튜토리얼을 완료하지 못해\n새로운 캐릭터를 생성할 수 없습니다.");
        public const int TutorialText_31 = 166030;    // "강화 버튼을 눌러보세요.");
        public const int TutorialText_32 = 166031;    // "강화할 무기를 선택하세요.");
        public const int TutorialText_33 = 166032;    // "다른 장비를 사용해서 무기를 강화할 수 있습니다.");
        public const int TutorialText_34 = 166033;    // "이제 무기를 강화해 보세요!");
        public const int TutorialText_35 = 166034;    // "레벨 버튼을 눌러보세요.");
        public const int TutorialText_36 = 166035;    // "강화시킬 펫을 선택하세요.");
        public const int TutorialText_37 = 166036;    // "다른 펫을 재료로 선택해서 펫을 강화시킬 수 있습니다.");
        public const int TutorialText_38 = 166037;    // "이제 펫을 강화시켜 보세요!");
        public const int TutorialText_39 = 166038;    // "요일 던전은 요일마다 다른 특별한 재료를 얻을 수 있는 던전입니다.");
        public const int TutorialText_40 = 166039;    // "요일 던전은 상중하의 3단계로 구성되고, 매일 각 1회 도전 가능합니다.");
        public const int TutorialText_41 = 166040;    // "전투 버튼을 눌러 보세요.");
        public const int TutorialText_42 = 166041;    // "무지개 낚시터로 이동하세요.");
        public const int TutorialText_43 = 166042;    // "낚시터 앞에서 화면을 위로 밀어서 낚시를 할 수 있습니다.");
        public const int TutorialText_44 = 166043;    // "낚시나 다른 채집을 통해서 얻은 재료로 다양한 아이템을 제작할 수 있습니다.");
        public const int TutorialText_45 = 166044;    // "좌측하단의 장착 버튼을 눌러서 펫을 착용할 수 있습니다.");
        public const int TutorialText_46 = 166045;    // "비어있는 슬롯을 눌러서 펫을 착용해 보세요.");
        public const int TutorialText_47 = 166046;    // "던전 아이콘을 눌러 보세요.");
        public const int TutorialText_48 = 166047;    // "왼쪽 퀘스트 알림창에서 AUTO 버튼을 누르면 퀘스트를 수행하러 자동으로 움직입니다.");
        public const int TutorialText_49 = 166048;    // "좌측의 이동 버튼을 밀면서 점프를 하면 언덕을 넘어갈 수 있습니다.");
        public const int TutorialText_50 = 166049;    // "앞쪽에서 어둠의 기운이 느껴진다. 쫓아가 보자.");
        public const int TutorialText_51 = 166050;    // "튜토리얼 도중에는 내용과 관련없는 UI는 비활성화 됩니다.");
        public const int TutorialText_52 = 166051;    // "펫가방 버튼을 눌러 인벤토리를 확인해 보세요.");
        public const int TutorialText_53 = 166052;    // "자동 이동을 지원하지 않는 퀘스트입니다.");
        public const int TutorialText_54 = 166053;    // "펫 뽑기 메뉴로 이동해 주세요.");
        public const int TutorialText_55 = 250078;    // "튜토리얼과 관련이 적은 NPC와는 대화할 수 없습니다.");
        public const int TutorialText_56 = 250079;    // "튜토리얼과 관련이 적은 지역으로 이동할 수 없습니다.");
        public const int TutorialText_57 = 250080;    // "튜토리얼과 관련이 적은 이벤트는 진행할 수 없습니다.");
        public const int TutorialText_58 = 250081;    // "아세스에게 말을 걸어보세요.");
        public const int TutorialText_59 = 250082;    // "신규 콘텐츠 오픈!!");
        public const int TutorialText_60 = 250083;    // "하우징 아이콘을 눌러 보세요.");
        public const int TutorialText_61 = 250084;    // "이리스 & 사라스바티에게 말을 걸어보세요.");
        public const int TutorialText_62 = 250105;    // "LV.{0}에 오픈 됩니다.");
        public const int TutorialText_63 = 260946;    // "축하합니다^^ 튜토리얼을 완료 하였습니다.(보상 우편 지급)");
        public const int TutorialText_64 = 166055;    // 스킬트리 아이콘을 눌러보세요
        public const int TutorialText_65 = 166057;    // 스킬 활성화 버튼을 눌러보세요
        public const int TutorialText_66 = 166056;    // 스킬 아이콘을 터치하세요
        public const int TutorialText_67 = 166058;    // 스킬 업그레이드 버튼을 눌러보세요
        public const int TutorialText_68 = 166059;    // 뒤로가세요
        public const int TutorialText_69 = 166063;    // 스킬배치 아이콘을 눌러보세요
        public const int TutorialText_70 = 166065;    // 배치할 스킬을 선택하세요
        public const int TutorialText_71 = 166066;    // 스킬 배치하기 버튼을 눌러주세요
        public const int TutorialText_72 = 166067;    // 원하는 위치를 선택해 누르면 스킬이 등록됩니다
        public const int TutorialText_73 = 261436;    // 낚시를 취소하려면 오른쪽으로 움직이세요
        public const int TutorialText_74 = 250106;    // 파티가 열렷어요
        public const int TutorialText_75 = 250085;    // 친구가 열렷어요
        public const int TutorialText_76 = 250086;    // 업적이 열렷어요
        public const int TutorialText_77 = 250087;    // 펫북이 열렷어요
        public const int TutorialText_78 = 250088;    // 몬북이 열렷어요
        public const int TutorialText_79 = 250089;    // 아북이 열렷어요
        public const int TutorialText_80 = 250090;    // 던전이 열렷어요
        public const int TutorialText_81 = 250092;    // 피빕이 열렷어요
        public const int TutorialText_82 = 250093;    // 길드가 열렷어요
        public const int TutorialText_83 = 250025;    // 파티 생성을 눌러보아요
        public const int TutorialText_84 = 250107;    // 파티 초대 할수잇어요
        public const int TutorialText_85 = 250108;    // 파티 이름 바꿀수잇어요
        public const int TutorialText_86 = 250109;    // 파티 홍보할 수 잇어요
        public const int TutorialText_87 = 250110;    // 파티 탈퇴할 수 잇어요
        public const int TutorialText_88 = 250004;    // 업적 탭을 보세요
        public const int TutorialText_89 = 250005;    // 업적이 이런게 잇어영
        public const int TutorialText_90 = 250006;    // 업적 보상은 이런거에요
        public const int TutorialText_91 = 250007;    // 업적 받기를 누르면 보상이 와요
        public const int TutorialText_92 = 250011;    // 펫 도감 등급이 있어요
        public const int TutorialText_93 = 250012;    // 펫 도감 타입이 있어요
        public const int TutorialText_94 = 250016;    // 몬스터 도감 등록할 수 있어요
        public const int TutorialText_95 = 250017;    // 몬스터 도감 효과는 요거에요
        public const int TutorialText_96 = 250020;    // 아이템 도감 아이템이 있어요
        public const int TutorialText_97 = 250021;    // 아이템 도감 제작 할 수 잇어요
        public const int TutorialText_98 = 250022;    // 아이템 도감 획득장소에요
        public const int TutorialText_99 = 250030;    // 요일 던전 이에요
        public const int TutorialText_100 = 250031;   // 요일 던전 시작버튼 이에요
        public const int TutorialText_101 = 250034;   // 멀티 던전 이에요
        public const int TutorialText_102 = 250035;   // 멀티 던전 시작버튼 이에요
        public const int TutorialText_103 = 250037;   // 무한의 탑 이에요
        public const int TutorialText_104 = 250038;   // 무한의 탑 인거시에요
        public const int TutorialText_105 = 250046;   // 피빕 랭킹 이에요
        public const int TutorialText_106 = 250047;   // 피빕 랭킹 점수에요
        public const int TutorialText_107 = 250048;   // 피빕 명예포인트 에요
        public const int TutorialText_108 = 250049;   // 피빕 게임 시작은 이 버튼을 누르세요
        public const int TutorialText_109 = 250053;   // 길드 검색 이에요
        public const int TutorialText_110 = 250054;   // 길드 추천 이에요
        public const int TutorialText_111 = 250055;   // 길드 생성 할 수 있어요
        public const int TutorialText_112 = 250058;   // 퀘스트 탭이에요
        public const int TutorialText_113 = 250059;   // 일일퀘 목록이에요
        public const int TutorialText_114 = 250060;   // 일일퀘 내용은 이래요
        public const int TutorialText_115 = 250061;   // 일일퀘 시작은 이거 누르면데요
        public const int TutorialText_116 = 250064;   // 아이템승급 리스트에요
        public const int TutorialText_117 = 250065;   // 아이템승급 대상은 여기다 꽂아요
        public const int TutorialText_118 = 250066;   // 아이템승급 버튼은 이거 누르세요
        public const int TutorialText_119 = 250068;   // 아이템연마 리스트는 이거에요
        public const int TutorialText_120 = 250069;   // 아이템연마 대상은 여깃어요
        public const int TutorialText_121 = 250070;   // 아이템연마 버튼은 이거 누르세요
        public const int TutorialText_122 = 250072;   // 펫스킬강화 리스트에요
        public const int TutorialText_123 = 250073;   // 펫스킬강화 대상은 여기다 꽂아요
        public const int TutorialText_124 = 250075;   // 펫스킬강화 재료는 아무거나 하세요
        public const int TutorialText_125 = 250076;   // 펫스킬강화 재료는 여기다 와요
        public const int TutorialText_126 = 250077;   // 펫스킬강화 버튼은 이거 누르면데요
        public const int TutorialText_127 = 250095;   // 펫진화 리스트는 여깄어요
        public const int TutorialText_128 = 250096;   // 펫진화 대상은 여기다 꽂혀요
        public const int TutorialText_129 = 250097;   // 펫진화 재료는 여기 보면데요
        public const int TutorialText_130 = 250098;   // 펫진화 버튼은 여깄어요
        public const int TutorialText_131 = 250100;   // 펫초월 리스트는 이거에요
        public const int TutorialText_132 = 250101;   // 펫초월 대상은 여기다 꽂혀요
        public const int TutorialText_133 = 250102;   // 펫초월 재료는 여기다 꽂혀요
        public const int TutorialText_134 = 250103;   // 펫초월 버튼은 이거 누르면 되요
        public const int TutorialText_135 = 250042;   // 뷰티샵 착용중인 아이템 리스트에요
        public const int TutorialText_136 = 250043;   // 뷰티샵 미리보기 버튼이에요
        public const int TutorialText_137 = 250044;   // 뷰티샵 구매는 이 버튼을 누르세요
        public const int TutorialText_138 = 250151;   // 핫타임 시간이 흘러요
        public const int TutorialText_139 = 250152;   // 핫타임 시작은 이거 누르세요
        public const int TutorialText_140 = 250155;   // 레벨업 방법에는 다음과 같은 방법이 있어요
        public const int TutorialText_141 = 250156;   // 레벨업 방법중에서 이게 추천합니다
        public const int TutorialText_142 = 250114;   // 하우징 UI를 봐주세요
        public const int TutorialText_143 = 250115;   // 하우징 메뉴 UI를 봐주세요
        public const int TutorialText_144 = 250118;   // 하우징 상세보기 탭 메뉴입니다
        public const int TutorialText_145 = 250119;   // 하우징 상세보기 정보 입니다
        public const int TutorialText_146 = 250120;   // 하우징 상세보기 집 입니다
        public const int TutorialText_147 = 250122;   // 하우징 상세보기 층 가구 입니다
        public const int TutorialText_148 = 250123;   // 하우징 상세보기 층 펫 입니다
        public const int TutorialText_149 = 250125;   // 하우징 상세보기 스킨 탭 메뉴 입니다
        public const int TutorialText_150 = 250126;   // 하우징 상세보기 스킨 리스트 입니다
        public const int TutorialText_151 = 250127;   // 하우징 상세보기 스킨 정보 입니다
        public const int TutorialText_152 = 250129;   // 하우징 업그레이드 리스트 입니다
        public const int TutorialText_153 = 250130;   // 하우징 업그레이드 정보 입니다
        public const int TutorialText_154 = 250131;   // 하우징 업그레이드 재료 입니다
        public const int TutorialText_155 = 250132;   // 하우징 업그레이드 등급 입니다
        public const int TutorialText_156 = 250133;   // 하우징 업그레이드 등급 이에요
        public const int TutorialText_157 = 250134;   // 하우징 업그레이드 버튼 입니다
        public const int TutorialText_158 = 240147;   // 하우징 ? 설명 이미지 텍스트 0-1
        public const int TutorialText_159 = 240148;   // 하우징 ? 설명 이미지 텍스트 0-2
        public const int TutorialText_160 = 240149;   // 하우징 ? 설명 이미지 텍스트 0-3
        public const int TutorialText_161 = 240150;   // 하우징 ? 설명 이미지 텍스트 0-4
        public const int TutorialText_162 = 240151;   // 하우징 ? 설명 이미지 텍스트 0-5
        public const int TutorialText_163 = 240152;   // 하우징 ? 설명 이미지 텍스트 0-6
        public const int TutorialText_164 = 240153;   // 하우징 ? 설명 이미지 텍스트 0-7
        public const int TutorialText_165 = 240154;   // 하우징 ? 설명 이미지 텍스트 0-8
        public const int TutorialText_166 = 240131;   // 하우징 ? 설명 이미지 텍스트 1-1
        public const int TutorialText_167 = 240133;   // 하우징 ? 설명 이미지 텍스트 1-2
        public const int TutorialText_168 = 240134;   // 하우징 ? 설명 이미지 텍스트 1-3
        public const int TutorialText_169 = 240135;   // 하우징 ? 설명 이미지 텍스트 1-4
        public const int TutorialText_170 = 240136;   // 하우징 ? 설명 이미지 텍스트 1-5
        public const int TutorialText_171 = 250167;
        public const int TutorialText_172 = 250168;
        public const int TutorialText_173 = 250169;

        #endregion 튜토리얼 알람 문구

        #region Result UI에서 사용

        public const int Result_0 = 260516;    // "보물상자 결과");
        public const int Result_1 = 260517;    // "인벤토리 공간이 부족해 보상이 우편으로 도착하였습니다.");

        #endregion

        #region 캐릭터정보UI에서 사용
        public const int CharacterInfo_0 = 261980;    // "캐릭터");
        public const int CharacterInfo_1 = 261981;    // "JOB EXP");
        public const int CharacterInfo_2 = 261982;    // "캐릭터 능력치");
        public const int CharacterInfo_3 = 261983;    // "계정\n보드");
        public const int CharacterInfo_4 = 262037;    // "모든 특성이 초기화되었습니다.\n특성 포인트를 재분배 할 수 있습니다.");
        #endregion

        #region ETC

        public const int Etc_0 = 261484;    // "이 장비는 세트 장비가 아닙니다.");
        public const int Etc_1 = 261485;    // "{0}일 {1}시간 후 종료");
        public const int Etc_2 = 261486;    // "더 이상 경험치 획득 불가");
        public const int Etc_3 = 261487;    // "연마 성공!");
        public const int Etc_4 = 261488;    // "연마 실패!");
        public const int Etc_5 = 261489;    // "차원 포탈");
        public const int Etc_6 = 261490;    // "도전 횟수가 충전 되었습니다.");
        public const int Etc_7 = 261491;    // "블루투스를 활성화하면 배터리가 빠르게 소모됩니다.");
        public const int Etc_8 = 261492;    // "게임을 종료하시겠습니까?");
        public const int Etc_9 = 261493;    // "일일 친구 초대 횟수가 초과되었습니다.");
        public const int Etc_10 = 261494;    // "권한이 없습니다! Google 설정 > Nearby 에서 Nearby 사용을 허용해 주세요.");
        public const int Etc_11 = 261495;    // "연결 실패! 블루투스 활성화에 실패하였습니다. 인터넷 연결을 확인해주세요.");
        public const int Etc_12 = 261496;    // "타유저 터치");
        public const int Etc_13 = 261497;    // "소셜모션 아이템 등록 완료");
        public const int Etc_14 = 261498;    // "소셜모션 아이템 등록 실패");
        public const int Etc_15 = 261499;    // "추출에는 다이아가 소모됩니다. 추출하시겠습니까?");
        public const int Etc_16 = 261500;    // "퍼즐을 장착하시겠습니까?\n");
        public const int Etc_17 = 261501;    // "\n(기존에 장착된 퍼즐은 제거됩니다.)");
        public const int Etc_18 = 261502;    // "아이템을 등록하시겠습니까?\n등록한 아이템은 돌려받을 수 없습니다.");
        public const int Etc_19 = 261503;    // "우정포인트를 받았습니다.");
        public const int Etc_20 = 261504;    // "등록성공");
        public const int Etc_21 = 261505;    // "카드등록에 성공하였습니다.");
        public const int Etc_22 = 261506;    // "등록실패");
        public const int Etc_23 = 261507;    // "카드등록에 실패하였습니다.");
        public const int Etc_24 = 261508;    // "등록실패\n몬스터 도감을 재설정 합니다.");
        public const int Etc_25 = 261509;    // "이 스킬을 배우시겠습니까?\n[f20](스킬은 언제든지 변경 가능합니다.)[/f]");
        public const int Etc_26 = 261510;    // "가 부족하여 배울 수 없습니다.");
        public const int Etc_27 = 261511;    // "전체 부활하시겠습니까?");
        public const int Etc_28 = 261512;    // "다이아가 모자라 구매할 수 없습니다.");
        public const int Etc_29 = 261513;    // "구입하신 상품을 바로 장착하시겠습니까?");
        public const int Etc_30 = 261514;    // "대표캐릭터를 삭제할 수 없습니다.");
        public const int Etc_31 = 261515;    // "생성한지 24시간 이상 지난 캐릭터만 삭제 가능합니다.");
        public const int Etc_32 = 261516;    // "펫을 장착한 캐릭터는 삭제할 수 없습니다.");
        public const int Etc_33 = 261517;    // "길드에 가입되어 있는 캐릭터는 삭제할 수 없습니다.");
        public const int Etc_34 = 261518;    // "펫을 장학한 캐릭터는 삭제할 수 없습니다.");
        public const int Etc_35 = 261519;    // "캐릭터를 삭제하시겠습니까?\n");
        public const int Etc_36 = 261520;    // "[f28]※ 캐릭터는 24시간 후 부터 삭제 할 수 있으며 취소할 수 있습니다.\n삭제 후에는 모든 정보가 사라집니다.[/ f]");
        public const int Etc_37 = 261521;    // "정말 캐릭터를 삭제하시겠습니까?");
        public const int Etc_38 = 261522;    // "데이터가 잘못 입력되었습니다.");
        public const int Etc_39 = 261523;    // "명에게 친구요청을 보냈습니다.");
        public const int Etc_40 = 261524;    // "모두에게 친구 신청한 상태입니다.");
        public const int Etc_41 = 261525;    // "이미 검색된 유저입니다.");
        public const int Etc_42 = 261526;    // "캐릭터 이름을 입력해 주십시오");
        public const int Etc_43 = 261527;    // "염색이 완료되었습니다.");
        public const int Etc_44 = 261528;    // "염색에 성공하셨습니다.");
        public const int Etc_45 = 261529;    // "칭호가 등록되었습니다.");
        public const int Etc_46 = 261530;    // "칭호가 해제되었습니다.");
        public const int Etc_47 = 261531;    // "인벤토리를 확장하시겠습니까?");
        public const int Etc_48 = 261532;    // "제작할 아이템을 선택해 주세요.");
        public const int Etc_49 = 261533;    // "재료 또는 엘리가 부족합니다.");
        public const int Etc_50 = 261534;    // "을(를) 제작하였습니다.");
        public const int Etc_51 = 261535;    // "선택한 항목이 없습니다.");
        public const int Etc_52 = 261536;    // "이미 장착한 종류의 펫입니다.");
        public const int Etc_53 = 261537;    // "보유한 엘리가 부족합니다.");
        public const int Etc_54 = 261538;    // "준비중입니다.");
        public const int Etc_55 = 261539;    // "가까운 친구 추가 보상이 선물함으로 지급되었습니다!");
        public const int Etc_56 = 261540;    // "가까운 친구 추가");
        public const int Etc_57 = 261541;    // "명 보상이 선물함으로 지급되었습니다!");
        public const int Etc_58 = 261831;    // "집에서 나가 라테일 월드로 돌아갑니다.");
        public const int Etc_59 = 261832;    // "필드로 돌아갑니다. 게임에서 획득한 점수는 초기화됩니다.");
        public const int Etc_60 = 261909;    // "경고!\n선택한 재료가 퍼즐 또는 무기를\n장착 중입니다.\n재료로 사용할 경우 복구할 수 없습니다.");
        public const int Etc_61 = 261910;    // "선택한 재료가 높은 등급의 아이템입니다.\n재료로 사용할 경우 복구할 수 없습니다.");
        public const int Etc_62 = 261923;    // "몬스터에게 피격 당해 {0}이(가) 취소되었습니다.");
        public const int Etc_63 = 261924;    // "{0}을(를) 완료했습니다!\n");
        public const int Etc_64 = 261925;    // "{0} 중...");
        public const int Etc_65 = 262038;    // "가까운 마을로 귀환합니다.");
        public const int Etc_66 = 262097;    // "해당 NPC를 찾을 수 없습니다.");
        public const int Etc_67 = 262430;    // "튜토리얼 중에는 뒤로가기를 할 수 없습니다."
        public const int Etc_68 = 262528;    // "강화되어있는 아이템입니다.\n재료로 사용할 경우 복구할 수 없습니다."
        public const int Etc_69 = 262567;         // "비밀번호 변경에 성공했습니다."
        public const int Etc_70 = 262568;         // 비밀번호가 일치하지 않습니다.    => 잘못된 패스워드입니다.
        public const int Etc_71 = 262569;         // 이메일 주소가 존재하지 않습니다    => 잘못된 이메일입니다.
        public const int Etc_72 = 262570;         // 알 수 없는 이유로 비밀번호 변경에 실패했습니다.
        public const int Etc_73 = 262572;         // 같은 비밀번호로는 변경이 불가능합니다.
        public const int Etc_74 = 262573;        // 해당 펫은 재료로 사용할 수 없습니다.
        public const int Etc_75 = 262596;         // 각인
        public const int Etc_76 = 262597;         // 교체
        public const int Etc_77 = 262598;         // 펫등급보다 높은 등급의 무기는 각인할 수 없습니다.
        public const int Etc_78 = 262599;         // 한 번 각인? 장착? 시킨 아이템은 다시 뺄 수 없습니다. 정말로 하시겠습니까?
        public const int Etc_79 = 263233;        // 거래 가격은 블루다이아 1개 이상이어야 합니다.
        public const int Etc_80 = 263234;        // 판매된 물품이 없습니다.
        public const int Etc_81 = 263235;        // 등록된 물품이 없습니다.
        public const int Etc_82 = 263236;        // 물품 등록을 취소하시겠습니까?
        public const int Etc_83 = 263335;            // 등록 대기
        public const int Etc_84 = 263336;            // 판매 취소
        public const int Etc_85 = 263337;            // 등록 만료
        public const int Etc_86 = 263347;            // 이미 구매요청을 보낸 후 응답을 받기 전이라 새로 구매 요청할 수 없는 경우 띄울 내용
        public const int Etc_87 = 263348;            // 이미 등록요청을 보낸 후 응답을 받기 전이라 새로 등록 요청할 수 없는 경우 띄울 내용
        public const int Etc_88 = 263349;            // 거래소에서 등록한 물품이 10개(최대)가 되었는데, 또 등록하려고 할때 할 수 없다는 알림 내용
        public const int Etc_89 = 263404;            // 카테고리를 선택하세요.
        public const int Etc_90 = 263405;            // 등록된 물품이 없습니다.
        public const int Etc_91 = 263406;            // 거래소에서 해당 그룹에 등록된 상품이 없는 경우 표시할 내용
        public const int Etc_92 = 263407;            // 거래소 검색 시 단어는 1단어 이상이어야함을 표시할 때 쓸 내용
        public const int Etc_93 = 263437;                // 최고가
        public const int Etc_94 = 263438;                // 최저가
        public const int Etc_95 = 263670;                // 같은 계열의 스킬입니다.
        public const int Etc_96 = 263830;           // 결혼식장에서 나가시겠습니까?

        #endregion ETC

        #region NETWORK

        #region CONNECTION
        public const int SFSConnection_000 = 260518;    // "인증 서버에 접속할 수 없습니다.\n인터넷 연결 상태를 확인해주세요.", E_TEXTTYPE.LOCAL);
        public const int SFSConnection_001 = 260519;    // "게임 서버에 접속할 수 없습니다.\n인터넷 연결 상태를 확인해주세요.", E_TEXTTYPE.LOCAL);
        public const int SFSConnection_002 = 260520;    // "인증 서버에 접속할 수 없습니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnection_003 = 260521;    // "게임 서버에 접속할 수 없습니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnection_004 = 261543;    // "서버 정보를 가져오는데 실패했습니다.", E_TEXTTYPE.LOCAL);
        #endregion

        #region CONNECTION_LOST
        public const int SFSConnectionLost_000 = 260522;    // "서버와의 연결이 끊어졌습니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnectionLost_001 = 260523;    // "서버로부터 차단 당해 연결이 끊어졌습니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnectionLost_002 = 260524;    // "장시간 입력이 없어 서버로부터 연결이 끊어졌습니다.\n재접속을 시도합니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnectionLost_003 = 260525;    // "장시간 입력이 없어 서버로부터 연결이 끊어졌습니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnectionLost_004 = 260526;    // "서버로부터 추방 당했습니다.", E_TEXTTYPE.LOCAL);
        public const int SFSConnectionLost_005 = 260527;    // "서버와의 연결이 끊어졌습니다..", E_TEXTTYPE.LOCAL);
        #endregion

        #region RESOURCE_CHECK            
        public const int DBChk_000 = 261544;    // "패치 데이터를 다운받습니다. 진행하시겠습니까?\nLTE 환경에선 추가 데이터 요금이\n발생할 수 있습니다.", E_TEXTTYPE.LOCAL);
        public const int DBChk_001 = 261545;    // "패치 진행 중에 문제가 발생했습니다. 다시 시도하시겠습니까?", E_TEXTTYPE.LOCAL);
        public const int DBChk_002 = 261546;    // "기기의 게임 데이터를 확인하고 있습니다.", E_TEXTTYPE.LOCAL);
        public const int DBChk_003 = 261547;    // "기기의 게임 데이터를 확인하는 중에 문제가 발생했습니다. 다시 시도하시겠습니까?", E_TEXTTYPE.LOCAL);
        public const int DBChk_004 = 261548;    // "게임 데이터를 다운로드하고 있습니다.", E_TEXTTYPE.LOCAL);
        public const int DBChk_005 = 261549;    // "게임 데이터를 다운로드 하는 중에 문제가 발생했습니다. 다시 시도하시겠습니까?", E_TEXTTYPE.LOCAL);
        public const int DBChk_006 = 261550;    // "게임 데이터를 다운로드하고 있습니다..", E_TEXTTYPE.LOCAL);
        public const int DBChk_007 = 261551;    // "다운로드 받은 게임 데이터를 확인하는 중에 문제가 발생했습니다. 다시 시도하시겠습니까?", E_TEXTTYPE.LOCAL);
        public const int DBChk_008 = 261552;    // "게임 데이터를 불러오고 있습니다.", E_TEXTTYPE.LOCAL);
        public const int DBChk_009 = 261553;    // "게임 데이터를 불러오는 중에 문제가 발생했습니다. 다시 시도하시겠습니까?");

        public const int AssetChk_000 = 261554;    // "패치 내역을 받아오는데 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int AssetChk_001 = 261555;    // "패치 진행 과정에서 문제가 발생했습니다.\n중복된 값입니다.", E_TEXTTYPE.LOCAL);
        public const int AssetChk_002 = 261556;    // "패치 진행 과정에서 문제가 발생했습니다.\n호환되지 않는 값입니다.", E_TEXTTYPE.LOCAL);

        public const int AssetBundleStartDownload = 261557;    // "애셋번들을 받고있습니다.", E_TEXTTYPE.LOCAL);
        public const int DataConfirm_0 = 263983; // {0}MB 의 추가 데이터를 다운로드 받습니다.\n계속하시겠습니까?
        public const int DataConfirm_1 = 263984; // {0}MB 의 추가 리소스를 다운로드 받습니다.\n계속하시겠습니까?
        public const int DataConfirm_2 = 263985; // (주의 : Wi-Fi이 환경이 아닌경우 추가 데이터 요금이 부과될 수 있습니다.)
        #endregion

        #region RESPONSE_CHECK
        public const int ResponseChk_000 = 260535;    // "서버로부터 응답이 지연되고있습니다.\n서버에 재접속하겠습니까?", E_TEXTTYPE.LOCAL);
        public const int ResponseChk_001 = 260536;    // "네트워크 상태가 불안정합니다.\n네트워크를 확인해 주세요.", E_TEXTTYPE.LOCAL);
        public const int ResponseChk_002 = 260537;    // "네트워크 상태가 변경되었습니다.\n네트워크를 확인해 주세요.", E_TEXTTYPE.LOCAL);
        public const int ResponseChk_003 = 260538;    // "서버와 연결되어 있지 않습니다.\n네트워크 상태를 확인해주세요.", E_TEXTTYPE.LOCAL);
        #endregion

        #region ACCOUNT_LINK
        public const int AccountLink_000 = 260539;    // "계정 연동 타입을 찾을 수 없습니다.", E_TEXTTYPE.LOCAL);
        public const int AccountLink_001 = 260540;    // "구글플레이 접속에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int AccountLink_002 = 260541;    // "구글플레이 인증 토큰을 가져오는데 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int AccountLink_003 = 261558;    // "기기에 저장된 계정은 데이터 연동이\n되어있지 않아 사라지게 됩니다.\n계속하시겠습니까?", E_TEXTTYPE.LOCAL);
        public const int AccountLink_004 = 261559;    // "게스트 계정을 생성합니다. 추후 계정 연동을 통해 계정을 보호할 수 있습니다.", E_TEXTTYPE.LOCAL);
        public const int AccountLink_005 = 261560;    // "기기에 저장된 계정은 게스트 계정입니다. 계정을 보호하려면 게임 환경설정에서 계정 연동을 해주세요.", E_TEXTTYPE.LOCAL);
        public const int AccountLink_006 = 262451;         // 구글 플레이 서비스가 없음.
        public const int AccountLink_007 = 262452;         // 이메일 로그인은 신규 계정 생성을 지원하지 않습니다. 계정을 새로 생성하실 경우 다른 계정 연동 방식을 선택해주세요.
                                                           /// <summary>
                                                           /// 게스트 계정 로그인 시 앱 삭제, 디바이스 변경 등으로
                                                           ///정보가 손실될 수 있습니다. 안전한 계정보호를 위해
                                                           ///계정 연동을 추천합니다.
                                                           ///게스트 계정으로 로그인하시겠습니까?
                                                           /// </summary>
        public const int AccountLink_008 = 262489;         // 
        public const int AccountLink_009 = 262493;   //환경설정에서 계정 연동 후 시도해주세요.

        #endregion

        #region NETWORK_ETC
        public const int NetworkETC_000 = 260542;    // "패킷 정보를 읽을 수 없습니다.", E_TEXTTYPE.LOCAL);
        public const int NetworkETC_001 = 260543;    // "인증 서버 접속 과정에서\n네트워크 초기화에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int NetworkETC_002 = 260544;    // "인증 서버 로그인 과정에서\n네트워크 초기화에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int NetworkETC_003 = 260545;    // "게임 서버 접속 과정에서\n네트워크 초기화에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int NetworkETC_004 = 260546;    // "게임 서버 로그인 과정에서\n네트워크 초기화에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int NetworkETC_005 = 260547;    // "서버 이동 접속 과정에서\n네트워크 초기화에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int NetworkETC_006 = 260548;    // "서버 이동 로그인 과정에서\n네트워크 초기화에 실패했습니다.", E_TEXTTYPE.LOCAL);            
        public const int NetworkETC_007 = 260549;    // "푸시 서비스 등록이 지연되고있습니다.\n초기 화면으로 돌아가시겠습니까?", E_TEXTTYPE.LOCAL);
        #endregion

        #region PROTOCOL
        public const int ProtocolTxt_001 = 260550;    // "게임 서버 정보를 가져오는데 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int ProtocolTxt_003 = 260551;    // "리소스 버전 정보를 가져오는데 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int ProtocolTxt_008 = 260552;    // "지금은 캐릭터를 삭제 할 수 없습니다.");
        public const int ProtocolTxt_009 = 260553;    // "지금은 캐릭터를 삭제 할 수 없습니다.");
        public const int ProtocolTxt_010 = 260554;    // "지금은 캐릭터 삭제를 취소할 수 없습니다.");
        public const int ProtocolTxt_011 = 260555;    // "지금은 유저 정보를 가져올 수 없습니다.");
        public const int ProtocolTxt_012 = 260556;    // "지금은 캐릭터 목록을 가져올 수 없습니다.");
        public const int ProtocolTxt_013 = 260557;    // "지금은 캐릭터를 생성 할 수 없습니다.");
        public const int ProtocolTxt_016 = 260558;    // "게임 맵에 입장 할 수 없습니다.");
        public const int ProtocolTxt_027 = 260559;    // "지금은 친구를 검색할 수 없습니다.");
        public const int ProtocolTxt_028 = 260560;    // "이미 친구 요청을 하였습니다.");
        public const int ProtocolTxt_029 = 260561;    // "지금은 친구 목록을 가져올 수 없습니다.");
        public const int ProtocolTxt_030 = 260562;    // "지금은 친구 수락 또는 거절이 불가능합니다.");
        public const int ProtocolTxt_031 = 260563;    // "지금은 친구 정보를 가져올 수 없습니다.");
        public const int ProtocolTxt_032 = 260564;    // "해당 이름의 친구 정보를 가져올 수 없습니다.");
        public const int ProtocolTxt_033 = 260565;    // "지금은 친구 목록을 가져올 수 없습니다.");
        public const int ProtocolTxt_034 = 260566;    // "지금은 친구를 삭제할 수 없습니다.");
        public const int ProtocolTxt_035 = 260567;    // "랜덤 친구 목록을 가져올 수 없습니다.");
        public const int ProtocolTxt_036 = 260568;    // "지금은 우정 포인트를 보낼 수 없습니다.");
        public const int ProtocolTxt_037 = 263237;    // "Facebook 게시 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_038 = 263238;    // "Line 친구 친구 코드 등록에 실패했습니다.");
        public const int ProtocolTxt_039 = 263239;    // "Line 친구 초대 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_040 = 263240;    // "Line 친구 초대 보상을 받을 수 없습니다.");
        public const int ProtocolTxt_041 = 263241;    // "Facebook 게시 보상을 받을 수 없습니다.");
        public const int ProtocolTxt_044 = 260569;    // "난투 친구 랭킹 목록을 가져올 수 없습니다.");
        public const int ProtocolTxt_045 = 260570;    // "난투 길드 랭킹 목록을 가져올 수 없습니다.");
        public const int ProtocolTxt_064 = 260571;    // "일일 퀘스트 클리어 정보를 가져올 수 없습니다.");
        public const int ProtocolTxt_065 = 260572;    // "지금은 퀘스트를 포기 할 수 없습니다.");
        public const int ProtocolTxt_066 = 260573;    // "지금은 퀘스트 보상을 받을 수 없습니다.");
        public const int ProtocolTxt_067 = 260574;    // "지금은 퀘스트 수락을 할 수 없습니다.");
        public const int ProtocolTxt_068 = 260575;    // "유저 일일 정산 정보를 가져올 수 없습니다.");
        public const int ProtocolTxt_069 = 262512;         // "캐릭터 일일 정산 정보를 가져올 수 없습니다."
        public const int ProtocolTxt_076 = 260576;    // "지금은 업적 보상을 받을 수 없습니다.");
        public const int ProtocolTxt_086 = 260577;    // "지금은 뷰티샵 상품을 구매할 수 없습니다.");
        public const int ProtocolTxt_089 = 260578;    // "지금은 아이템 인벤토리 확장을 할 수 없습니다.");
        public const int ProtocolTxt_090 = 260579;    // "지금은 펫 인벤토리 확장을 할 수 없습니다.");
        public const int ProtocolTxt_102 = 260580;    // "지금은 이벤트 던전을 시작할 수 없습니다.");
        public const int ProtocolTxt_103 = 260581;    // "이벤트 던전 종료에 실패했습니다.");
        public const int ProtocolTxt_109 = 260582;    // "지금은 창고 확장을 할 수 없습니다.");
        public const int ProtocolTxt_120 = 260583;    // "채팅 채널 입장에 실패했습니다.");
        public const int ProtocolTxt_121 = 260584;    // "지금은 채널에 채팅을 보낼 수 없습니다.");
        public const int ProtocolTxt_123 = 260585;    // "지금은 귓속말을 보낼 수 없습니다.");
        public const int ProtocolTxt_124 = 260586;    // "지금은 귓속말을 받을 수 없습니다.");
        public const int ProtocolTxt_125 = 260587;    // "지금은 채팅을 보낼 수 없습니다.");
        public const int ProtocolTxt_132 = 260588;    // "132", E_TEXTTYPE.LOCAL);
        public const int ProtocolTxt_144 = 260589;    // "지금은 친구를 초대할 수 없습니다.");
        public const int ProtocolTxt_146 = 260590;    // "던전 스테이지 로딩 정보를 보낼 수 없습니다.");
        public const int ProtocolTxt_148 = 260591;    // "지금은 초대를 수락할 수 없습니다.");
        public const int ProtocolTxt_150 = 260592;    // "스테이지 로딩을 시작 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_153 = 260593;    // "AI 상태 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_154 = 260594;    // "던전 초대 정보를 받는데 실패했습니다.");
        public const int ProtocolTxt_155 = 260595;    // "초대 거절 요청을 보내는데 실패했습니다.");
        public const int ProtocolTxt_157 = 260596;    // "지금은 던전에 입장할 수 없습니다.");
        public const int ProtocolTxt_158 = 260597;    // "초대 취소 요청을 보내는데 실패했습니다.");
        public const int ProtocolTxt_165 = 260598;    // "다음 스테이지 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_166 = 260599;    // "던전 보상 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_172 = 260600;    // "출석 보상 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_208 = 260601;    // "난투 시즌 시간 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_210 = 260602;    // "난투 시즌 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_222 = 260603;    // "추천 길드 목록을 받을 수 없습니다.");
        public const int ProtocolTxt_228 = 260604;    // "길드를 생성할 수 없습니다.");
        public const int ProtocolTxt_220 = 260605;    // "길드 정보를 요청할 수 없습니다.");
        public const int ProtocolTxt_221 = 260606;    // "길드원 정보를 찾을 수 없습니다.");
        public const int ProtocolTxt_223 = 260607;    // "길드 검색에 실패했습니다.");
        public const int ProtocolTxt_224 = 260608;    // "길드 정보를 찾을 수 없습니다.");
        public const int ProtocolTxt_225 = 261891;    // "길드 가입 신청 리스트를 요청할 수 없습니다.");
        public const int ProtocolTxt_226 = 260609;    // "길드 가입에 실패했습니다.");
        public const int ProtocolTxt_227 = 261892;    // "길드 가입 요청 취소를 실패했습니다.");
        public const int ProtocolTxt_229 = 260610;    // "길드 소개글을 변경할 수 없습니다.");
        public const int ProtocolTxt_230 = 260611;    // "길드 엠블렘을 변경할 수 없습니다.");
        public const int ProtocolTxt_231 = 260612;    // "길드 가입 신청 목록을 받는데 실패했습니다.");
        public const int ProtocolTxt_232 = 260613;    // "길드 가입 신청을 처리할 수 없습니다.");
        public const int ProtocolTxt_233 = 260614;    // "길드 운영진 임명에 실패했습니다.");
        public const int ProtocolTxt_234 = 260615;    // "길드 마스터 위임에 실패했습니다.");
        public const int ProtocolTxt_235 = 262523;    // "길드 마스터 권한을 가져오는데 실패했습니다.");
        public const int ProtocolTxt_236 = 260616;    // "길드 추방에 실패했습니다.");
        public const int ProtocolTxt_237 = 260617;    // "길드 탈퇴에 실패했습니다.");
        public const int ProtocolTxt_238 = 260618;    // "길드 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_239 = 260619;    // "길드 채팅을 받을 수 없습니다.");
        public const int ProtocolTxt_240 = 260620;    // "길드 공지사항을 변경할 수 없습니다.");
        public const int ProtocolTxt_241 = 260621;    // "게시판 조회에 실패했습니다.");
        public const int ProtocolTxt_242 = 260622;    // "글 작성에 실패했습니다.");
        public const int ProtocolTxt_243 = 260623;    // "출석 정보를 받을 수 없습니다.");
        public const int ProtocolTxt_248 = 260624;    // "글 삭제에 실패했습니다.");
        public const int ProtocolTxt_244 = 260625;    // "출석 요청에 실패했습니다.");
        public const int ProtocolTxt_245 = 260626;    // "출석 보상을 받을 수 없습니다.");
        public const int ProtocolTxt_251 = 260627;    // "길드 스킬 레벨업에 실패했습니다.");
        public const int ProtocolTxt_252 = 260628;    // "길드 퀘스트 완료에 실패했습니다.");
        public const int ProtocolTxt_253 = 260629;    // "기부 보상을 받을 수 없습니다.");
        public const int ProtocolTxt_255 = 260630;    // "길드 스킬 목록을 받아오는데 실패했습니다.");
        public const int ProtocolTxt_301 = 260631;    // "플레이어 입장 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_302 = 260632;    // "플레이어 퇴장 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_303 = 260633;    // "플레이어 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_304 = 260634;    // "몬스터 데미지 정보를 보내는데 실패하였습니다.");
        public const int ProtocolTxt_305 = 260635;    // "플레이어 데미지 정보를 보내는데 실패했습니다.");
        public const int ProtocolTxt_306 = 260636;    // "플레이어 캐스팅 정보가 잘못되었습니다.");
        public const int ProtocolTxt_308 = 260637;    // "몬스터 캐스팅 정보가 잘못되었습니다.");
        public const int ProtocolTxt_310 = 260638;    // "펫 인벤토리 리스트를 받을 수 없습니다.");
        public const int ProtocolTxt_311 = 260639;    // "펫 장착을 실패했습니다.");
        public const int ProtocolTxt_313 = 260640;    // "펫 장착 해제를 실패했습니다.");
        public const int ProtocolTxt_314 = 260641;    // "펫 스킬 강화를 실패했습니다.");
        public const int ProtocolTxt_316 = 260642;    // "펫 초월을 실패했습니다.");
        public const int ProtocolTxt_323 = 260643;    // "펫 도감 리스트를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_325 = 260644;    // "아이템 강화를 실패했습니다.");
        public const int ProtocolTxt_326 = 260645;    // "아이템 승급을 실패했습니다.");
        public const int ProtocolTxt_327 = 260646;    // "퍼즐 장착을 실패했습니다.");
        public const int ProtocolTxt_328 = 260647;    // "퍼즐 추출을 실패했습니다.");
        public const int ProtocolTxt_329 = 260648;    // "아이템 장착을 실패했습니다.");
        public const int ProtocolTxt_330 = 260649;    // "아이템 해제를 실패했습니다.");
        public const int ProtocolTxt_331 = 260650;    // "포션 사용을 실패했습니다.");
        public const int ProtocolTxt_332 = 260651;    // "아이템 판매를 실패했습니다.");
        public const int ProtocolTxt_333 = 260652;    // "아이템 삭제를 실패했습니다.");
        public const int ProtocolTxt_336 = 260653;    // "아이템 구매를 실패했습니다.");
        public const int ProtocolTxt_335 = 260654;    // "열쇠 사용을 실패했습니다.");
        public const int ProtocolTxt_337 = 260655;    // "아이템 연마를 실패했습니다.");
        public const int ProtocolTxt_338 = 260656;    // "펫 판매를 실패했습니다.");
        public const int ProtocolTxt_339 = 260657;    // "펫 삭제를 실패했습니다.");
        public const int ProtocolTxt_340 = 260658;    // "펫 도감 등록에 실패했습니다.");
        public const int ProtocolTxt_341 = 260659;    // "타유저 체력정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_342 = 260660;    // "아이템 사용을 실패했습니다.");
        public const int ProtocolTxt_343 = 260661;    // "외형 변경에 실패했습니다.");
        public const int ProtocolTxt_344 = 262494;    // "타유저 아이템 장착을 받아오는데 실패했습니다.");
        public const int ProtocolTxt_345 = 262495;    // "타유저 아이템 해제를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_346 = 262496;         // "타유저 펫 장착 처리에 실패"
        public const int ProtocolTxt_347 = 262497;         // "타유저 펫 해제 처리에 실패"
        public const int ProtocolTxt_350 = 260662;    // "플레이어 좌표값 요청을 실패했습니다.");
        public const int ProtocolTxt_351 = 260663;    // "필드 이동을 실패했습니다.");
        public const int ProtocolTxt_352 = 260664;    // "패시브 스킬 활성화에 실패했습니다.");
        public const int ProtocolTxt_353 = 260665;    // "패시브 스킬 습득에 실패했습니다.");
        public const int ProtocolTxt_360 = 260666;    // "우편 목록을 받아오는데 실패했습니다.");
        public const int ProtocolTxt_361 = 260667;    // "우편 받기 요청을 실패했습니다.");
        public const int ProtocolTxt_362 = 260668;    // "우정포인트 받기 요청을 실패했습니다.");
        public const int ProtocolTxt_363 = 260669;    // "티켓 받기를 실패했습니다.");
        public const int ProtocolTxt_365 = 260670;    // "모두 받기 요청을 실패했습니다.");
        public const int ProtocolTxt_367 = 260671;    // "플레이어 좌표값을 받아오는데 실패했습니다.");
        public const int ProtocolTxt_368 = 260672;    // "구매 요청을 실패했습니다.");
        public const int ProtocolTxt_369 = 260673;    // "구매 제한 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_370 = 260674;    // "플레이어 부활 요청을 실패했습니다.");
        public const int ProtocolTxt_371 = 260675;    // "제자리 부활 요청을 실패했습니다.");
        public const int ProtocolTxt_372 = 260676;    // "체력 회복 요청을 실패했습니다.");
        public const int ProtocolTxt_373 = 260677;    // "귀환 요청을 실패했습니다.");
        public const int ProtocolTxt_374 = 260678;    // "몬스터 AI정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_375 = 260679;    // "몬스터 도감 목록 요청을 실패했습니다.");
        public const int ProtocolTxt_376 = 260680;    // "몬스터 도감 등록 요청을 실패했습니다.");
        public const int ProtocolTxt_378 = 260681;    // "타운 포탈 사용 요청을 실패했습니다.");
        public const int ProtocolTxt_379 = 260682;    // "계정보드 등록 요청을 실패했습니다.");
        public const int ProtocolTxt_380 = 260683;    // "포탈 사용 요청을 실패했습니다.");
        public const int ProtocolTxt_390 = 260684;    // "빙고 등록 요청을 실패했습니다.");
        public const int ProtocolTxt_391 = 260685;    // "빙고 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_392 = 260686;    // "빙고 보상 요청을 실패했습니다.");
        public const int ProtocolTxt_395 = 260687;    // "멀티 던전 대기실 생성 요청을 실패했습니다.");
        public const int ProtocolTxt_396 = 260688;    // "멀티 던전 대기실 입장 요청을 실패했습니다.");
        public const int ProtocolTxt_397 = 260689;    // "멀티 던전 대기실 퇴장 요청을 실패했습니다.");
        public const int ProtocolTxt_398 = 260690;    // "멀티 던전 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_399 = 260691;    // "멀티 던전 스테이지 이동 요청을 실패했습니다.");
        public const int ProtocolTxt_401 = 260692;    // "멀티 던전 타유저 입장 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_402 = 260693;    // "멀티 던전 타유저 퇴장 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_403 = 260694;    // "멀티 던전 타유저 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_404 = 260695;    // "멀티 던전 대기실 설정에 실패했습니다.");
        public const int ProtocolTxt_406 = 260696;    // "멀티 던전 부활 요청을 실패했습니다.");
        public const int ProtocolTxt_408 = 260697;    // "멀티 던전 보상 선택 요청을 실패했습니다.");
        public const int ProtocolTxt_415 = 260698;    // "채집 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_416 = 260699;    // "채집 완료 요청을 실패했습니다.");
        public const int ProtocolTxt_418 = 261581;    // "제작 이벤트 목록 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_419 = 260700;    // "제작 요청을 실패했습니다.");
        public const int ProtocolTxt_435 = 260701;    // "몬스터 AI 처리에 실패했습니다.");
        public const int ProtocolTxt_436 = 260702;    // "몬스터 AI 정보 전송에 실패했습니다.");
        public const int ProtocolTxt_439 = 260703;    // "던전 나가기 요청을 실패했습니다.");
        public const int ProtocolTxt_440 = 260704;    // "몬스터 타워 정보 불러오기를 실패했습니다.");
        public const int ProtocolTxt_441 = 260705;    // "몬스터 타워 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_442 = 260706;    // "몬스터 타워 나가기 요청을 실패했습니다.");
        public const int ProtocolTxt_443 = 260707;    // "요일 던전 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_444 = 260708;    // "요일 던전 나가기 요청을 실패했습니다.");
        public const int ProtocolTxt_445 = 260709;    // "요일 던전 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_446 = 260710;    // "하우징 구매 요청을 실패했습니다.");
        public const int ProtocolTxt_447 = 260711;    // "하우징 입장 요청을 실패했습니다.");
        public const int ProtocolTxt_448 = 260712;    // "하우징 나가기 요청을 실패했습니다.");
        public const int ProtocolTxt_449 = 0;    // "");
        public const int ProtocolTxt_450 = 260713;    // "하우징 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_451 = 260714;    // "가구 목록 요청을 실패했습니다.");
        public const int ProtocolTxt_452 = 260715;    // "가구 배치요청을 실패했습니다.");
        public const int ProtocolTxt_453 = 260716;    // "가구 업그레이드 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_456 = 260717;    // "가구 업드레이드 완료 요청을 실패했습니다.");
        public const int ProtocolTxt_457 = 260718;    // "하우징 펫 배치 요청을 실패했습니다.");
        public const int ProtocolTxt_458 = 260719;    // "하우징 펫 경험치 획득 요청을 실패했습니다.");
        public const int ProtocolTxt_459 = 260720;    // "하우징 펫 꺼내기 요청을 실패했습니다.");
        public const int ProtocolTxt_460 = 260721;    // "하우스 업그레이드 시작 요청을 실패했습니다. ")r;
        public const int ProtocolTxt_461 = 260722;    // "하우스 좋아요 요청을 실패했습니다.");
        public const int ProtocolTxt_462 = 260723;    // "하우스 랭킹 리스트 요청을 실패했습니다.");
        public const int ProtocolTxt_464 = 260724;    // "추천 하우스 랭킹 리스트 요청을 실패했습니다.");
        public const int ProtocolTxt_465 = 260725;    // "가구 팔기 요청을 실패했습니다.");
        public const int ProtocolTxt_466 = 260726;    // "하우징 물품 구매 요청을 실패했습니다.");
        public const int ProtocolTxt_467 = 260727;    // "하우스 스킨 선택 요청을 실패했습니다.");
        public const int ProtocolTxt_468 = 260728;    // "스킨 리스트 요청을 실패했습니다.");
        public const int ProtocolTxt_469 = 260729;    // "하우스 확장 요청을 실패했습니다.");
        public const int ProtocolTxt_470 = 260730;    // "칭호 변경 요청을 실패했습니다.");
        public const int ProtocolTxt_471 = 260731;    // "칭호 리스트 요청을 실패했습니다.");
        public const int ProtocolTxt_475 = 260732;    // "하우징 재배 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_479 = 260733;    // "하우스 업그레이드 완료 요청을 실패했습니다.");
        public const int ProtocolTxt_482 = 260734;    // "하우징 내부 이동 요청을 실패했습니다.");
        public const int ProtocolTxt_510 = 260735;    // "PVP 전투 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_511 = 260736;    // "다음 튜토리얼 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_516 = 260737;    // "난투 종료에 실패했습니다.");
        public const int ProtocolTxt_517 = 260738;    // "PVP 부활 요청을 실패했습니다.");
        public const int ProtocolTxt_518 = 260739;    // "PVP 나가기 요청을 실패했습니다.");
        public const int ProtocolTxt_519 = 260740;    // "PVP 타유저 나가기 정보를 찾을 수 없습니다.");
        public const int ProtocolTxt_521 = 260741;    // "특성 배우기 요청을 실패했습니다.");
        public const int ProtocolTxt_522 = 260742;    // "특성 초기화 요청을 실패했습니다.");
        public const int ProtocolTxt_524 = 260743;    // "PVP 타유저 입장 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_525 = 260744;    // "PVP 타유저 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_530 = 260745;    // "펫 LV업 요청을 실패했습니다.");
        public const int ProtocolTxt_531 = 260746;    // "히든 포탈 퀘스트 수락 요청을 실패했습니다.");
        public const int ProtocolTxt_532 = 260747;    // "히든 포탈 퀘스트 포기 요청을 실패했습니다.");
        public const int ProtocolTxt_550 = 260748;    // "펫 진화 요청을 실패했습니다.");
        public const int ProtocolTxt_551 = 260749;    // "펫 무기 장착 요청을 실패했습니다.");
        public const int ProtocolTxt_560 = 260750;    // "랭킹 보상 요청을 실패했습니다.");
        public const int ProtocolTxt_561 = 260752;    // "PVP 랭킹 보상 목록을 요청하는데 실패했습니다.");
        public const int ProtocolTxt_571 = 260751;    // "시스템 메세지 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_572 = 260811;    // "캐릭터 상태 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_570 = 260753;    // "아이템 획득 메시지 요청을 실패했습니다.");
        public const int ProtocolTxt_573 = 260754;    // "차지 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_574 = 260755;    // "차지 중지 요청을 실패했습니다.");
        public const int ProtocolTxt_575 = 260756;    // "창고 아이템 넣기 요청을 실패했습니다.");
        public const int ProtocolTxt_577 = 260757;    // "창고 아이템 꺼내기 요청을 실패했습니다.");
        public const int ProtocolTxt_576 = 260758;    // "창고 펫 넣기 요청을 실패했습니다.");
        public const int ProtocolTxt_578 = 260759;    // "창고 펫 꺼내기 요청을 실패했습니다.");
        public const int ProtocolTxt_579 = 260760;    // "창고 목록 요청을 실패했습니다.");
        public const int ProtocolTxt_580 = 260761;    // "캐릭터 전직 요청을 실패했습니다.");
        public const int ProtocolTxt_590 = 260762;    // "계정 연동 요청을 실패했습니다.");
        public const int ProtocolTxt_591 = 260763;    // "이메일 패스워드 변경 요청을 실패했습니다.");
        public const int ProtocolTxt_595 = 260764;    // "소셜 모션 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_596 = 260765;    // "협력형 소셜 모션 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_597 = 260766;    // "소셜 모션 목록 요청을 실패했습니다.");
        public const int ProtocolTxt_600 = 260767;    // "하우징 타유저 입장 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_601 = 260768;    // "하우징 타유저 나가기 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_602 = 260769;    // "하우징 타유저 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_603 = 262090;    // "하우징 가구 인벤토리 확장 요청을 실패했습니다.");
        public const int ProtocolTxt_620 = 260770;    // "요일 던전 재시작 요청을 실패했습니다.");
        public const int ProtocolTxt_621 = 260771;    // "몬스터 타워 재시작 요청을 실패했습니다.");
        public const int ProtocolTxt_809 = 260772;    // "자원 던전 재시작 요청을 실패했습니다.");
        public const int ProtocolTxt_750 = 260773;    // "파티 생성 요청을 실패했습니다.");
        public const int ProtocolTxt_751 = 260774;    // "파티 초대 전송에 실패했습니다.");
        public const int ProtocolTxt_752 = 260775;    // "파티 수락 요청을 실패했습니다.");
        public const int ProtocolTxt_753 = 260776;    // "새로운 파티원 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_754 = 260777;    // "파티 탈퇴 요청을 실패했습니다.(1)");
        public const int ProtocolTxt_755 = 260778;    // "파티 탈퇴 요청을 실패했습니다.(2)");
        public const int ProtocolTxt_756 = 260779;    // "파티 이름 바꾸기 요청을 실패했습니다.");
        public const int ProtocolTxt_757 = 260780;    // "파티 홍보하기 요청을 실패했습니다.(1)");
        public const int ProtocolTxt_758 = 260781;    // "파티 홍보하기 요청을 실패했습니다.(2)");
        public const int ProtocolTxt_759 = 260782;    // "파티 초대 정보를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_760 = 260783;    // "파티원 추방 요청을 실패했습니다.(1)");
        public const int ProtocolTxt_761 = 260784;    // "파티원 추방 요청을 실패했습니다.(2)");
        public const int ProtocolTxt_762 = 260785;    // "파티장 변경 요청을 실패했습니다.");
        public const int ProtocolTxt_763 = 260786;    // "파티원 채널 이동 요청을 실패했습니다.");
        public const int ProtocolTxt_764 = 260787;    // "파티 채팅 메시지를 받아오는데 실패했습니다.");
        public const int ProtocolTxt_765 = 260788;    // "파티원 위치 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_766 = 260789;    // "파티원 따라가기 요청을 실패했습니다.");
        public const int ProtocolTxt_770 = 260790;    // "하우징 펫 배치 변경 요청을 실패했습니다.");
        public const int ProtocolTxt_775 = 260791;    // "맵 나가기 요청을 실패했습니다.");
        public const int ProtocolTxt_776 = 260792;    // "친구 수락 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_777 = 260793;    // "길드원 가입 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_778 = 260794;    // "퀘스트 보상 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_779 = 263705;        // "던전 홍보 요청에 실패했습니다."
        public const int ProtocolTxt_780 = 263706;		  // "멀티 던전 입장 요청에 실패했습니다."
        public const int ProtocolTxt_781 = 260795;    // "멀티 던전 보상 목록 요청을 실패했습니다.");
        public const int ProtocolTxt_785 = 260796;    // "핫타임 사용 요청을 실패했습니다.");
        public const int ProtocolTxt_790 = 260797;    // "파티원 정보 변경을 받아오는데 실패했습니다.");
        public const int ProtocolTxt_795 = 260798;    // "휴식 보상 요청을 실패했습니다.");
        public const int ProtocolTxt_796 = 260799;    // "접속 유지 보상 받기 요청을 실패했습니다.(1)");
        public const int ProtocolTxt_797 = 260800;    // "접속 유지 보상 받기 요청을 실패했습니다.(2)");
        public const int ProtocolTxt_800 = 260801;    // "자원 던전 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_801 = 260802;    // "자원 던전 입장 횟수 초기화 요청을 실패했습니다.");
        public const int ProtocolTxt_802 = 260803;    // "자원 던전 종료 요청을 실패했습니다.");
        public const int ProtocolTxt_803 = 260804;    // "자원 던전 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_805 = 262531;    // "상품 구매정보요청에 실패했습니다.");
        public const int ProtocolTxt_806 = 262532;    // "상품 구매요청에 실패했습니다.");
        public const int ProtocolTxt_807 = 262533;    // "일일 다이아 상품 수락 요청에 실패했습니다.");
        public const int ProtocolTxt_811 = 260805;    // "제작 횟수 제한 정보 요청을 실패했습니다.");
        public const int ProtocolTxt_820 = 260939;    // "블루투스 친구추가 요청에 실패했습니다.");
        public const int ProtocolTxt_821 = 260940;    // "블루투스 친구 리스트 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_822 = 260941;    // "친구 리스트 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_812 = 260942;    // "친구 삭제 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_824 = 261926;    // "길드 포인트 랭킹을 요청하는데 실패했습니다.");
        public const int ProtocolTxt_826 = 261911;    // "타 유저 칭호 변경 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_832 = 261984;    // "낚시 시작 요청을 실패했습니다.");
        public const int ProtocolTxt_833 = 261985;    // "낚시 종료 요청을 실패했습니다.");
        public const int ProtocolTxt_831 = 261986;    // "타 유저 채집 성공 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_834 = 261999;    // "낚시 취소 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_835 = 262055;    // "타 유저 낚시 성공 정보를 가져오는데 실패했습니다.");
        public const int ProtocolTxt_836 = 262056;    // "멀티던전 입장권 구매에 실패했습니다.");
        public const int ProtocolTxt_837 = 262117;         // 이벤트 리스트를 가져오는데 실패했습니다.
        public const int ProtocolTxt_838 = 262118;         // 달성 이벤트 보상을 받지 못했습니다.
        public const int ProtocolTxt_839 = 262119;         // 내 달성 이벤트 정보를 가져오기 못했습니다.
        public const int ProtocolTxt_841 = 262123;         // "박스 이벤트 정보를 가져오는데 실패했습니다."
        public const int ProtocolTxt_842 = 262124;         // "박스 뽑기 요청을 실패했습니다."
        public const int ProtocolTxt_843 = 262142;         // "오라 스킬 활성화 요청을 실패했습니다."
        public const int ProtocolTxt_844 = 262143;         // "엘리 스킬 활성화 요청을 실패했습니다."
        public const int ProtocolTxt_845 = 262144;         // "일일미션 보상받기를 실패했습니다."
        public const int ProtocolTxt_846 = 262406;         // "전투력 랭킹 리스트를 가져오는데 실패했습니다."
        public const int ProtocolTxt_847 = 262433;         // "스킬트리 활성화 요청을 실패했습니다."
        public const int ProtocolTxt_848 = 262434;         // "스킬트리 레벨업 요청을 실패했습니다."
        public const int ProtocolTxt_849 = 262435;         // "스킬트리 초기화 요청을 실패했습니다."
        public const int ProtocolTxt_850 = 262436;         // "스킬트리 장착 요청을 실패했습니다."
        public const int ProtocolTxt_851 = 262437;         // "스킬트리 장착해제 요청을 실패했습니다."
        public const int ProtocolTxt_880 = 263082;         // "코스튬 합성 요청을 실패했습니다."
        public const int ProtocolTxt_881 = 900014;         // "이벤트 보상 요청을 실패했습니다."

        public const int ProtocolTxt_900 = 263758;         // "결혼 정보 요청을 실패했습니다."
        public const int ProtocolTxt_901 = 263759;         // "결혼 프로포즈 요청을 실패했습니다."
        public const int ProtocolTxt_902 = 263760;         // "결혼 프로포즈 응답을 가져오는데 실패하였습니다."
        public const int ProtocolTxt_903 = 263761;         // "약혼 파기 요청을 실패했습니다."
        public const int ProtocolTxt_904 = 263762;         // "결혼 정보 갱신 정보를 가져오는데 실패했습니다."
        public const int ProtocolTxt_905 = 263763;         // "결혼식장 생성에 실패하였습니다.."
        public const int ProtocolTxt_906 = 263764;         // "결혼식장 입장에 실패하였습니다.."
        public const int ProtocolTxt_907 = 263765;         // "결혼식장 퇴장에 실패하였습니다.."
        public const int ProtocolTxt_908 = 263766;         // "결혼식장의 다른 유저들 정보를 받아오는데실패하였습니다.."
        public const int ProtocolTxt_909 = 263767;         // "유저를 결혼식장에 초대하는데 실패하였습니다.."
        public const int ProtocolTxt_910 = 263768;         // "결혼식장 초대 수락에 실패하였습니다.."
        public const int ProtocolTxt_911 = 263769;         // "결혼식장 진입에 실패하였습니다."
        public const int ProtocolTxt_912 = 263770;         // "결혼식장 진입(서버이동)에 실패하였습니다."
        public const int ProtocolTxt_913 = 263829;         // "결혼식장 준비에 실패하였습니다."
        public const int ProtocolTxt_914 = 263828;         // "결혼식장 시작에 실패하였습니다."
        public const int ProtocolTxt_915 = 263827;         // "결혼식장 진행에 실패하였습니다."
        public const int ProtocolTxt_916 = 263826;         // "결혼식장 퇴장에 실패하였습니다."
        public const int ProtocolTxt_917 = 263825;         // "결혼식장 부케던지기에 실패하였습니다."

        public const int ProtocolTxt_920 = 263824;         // "결혼 반지 강화에 실패하였습니다."

        public const int ProtocolTxt_922 = 263823;         // "이혼 요청에 실패하였습니다."
        public const int ProtocolTxt_924 = 263822;         // "이혼 요청 응답에 실패하였습니다."
        public const int ProtocolTxt_926 = 263821;         // "강제 이혼 요청에 실패하였습니다."
        public const int ProtocolTxt_927 = 263914;              // "반지 승급 재료를 등록하는데에 실패하였습니다."
        public const int ProtocolTxt_928 = 263915;              // "반지를 승급하는데 실패하였습니다."
        public const int ProtocolTxt_930 = 263916;         // "서버에서 메세지를 받아오는데 실패하였습니다."

        public const int ProtocolTxt_950 = 263820;          // 재구매 아이템 리스트 요청을 실패했습니다.
        public const int ProtocolTxt_951 = 263819;          // 아이템 재구매 요청을 실패했습니다.
        public const int ProtocolTxt_952 = 263818;          // 재구매 펫 리스트 요청을 실패했습니다.
        public const int ProtocolTxt_953 = 263817;          // 펫 재구매 요청을 실패했습니다.

        public const int ProtocolTxt_1012 = 263771;       // "공동 구매 정보를 받아오는데 실패하였습니다."
        public const int ProtocolTxt_1013 = 263772;       // "공동 구매 요청을 실패했습니다."
        public const int ProtocolTxt_1014 = 263773;       // "공동 구매 정산 정보를 받아오는데 실패하였습니다."

        #endregion

        #region 스킬트리

        public const int SkillTree_0 = 262410; //""스킬을 활성화하였습니다.\n습득한 스킬을 배치할 수 있습니다.";
        public const int SkillTree_1 = 502000; // "스킬 공격력 {0}\n[분노 {1}] [타겟 {2}명] [쿨타임 {3}초]"
        public const int SkillTree_2 = 502001;// "스킬 공격력 {0}\n[마나 {1}] [타겟 {2}명] [쿨타임 {3}초]"
        public const int SkillTree_3 = 502002;// "{0}\n[분노 {1}] [지속시간 {2}초] [쿨타임 {3}초]"
        public const int SkillTree_4 = 502003;// "{0}\n[마나 {1}] [지속시간 {2}초] [쿨타임 {3}초]"
        public const int SkillTree_5 = 502004;// "{0}, {1}\n[분노 {2}] [지속시간 {3}초] [쿨타임 {4}초]"
        public const int SkillTree_6 = 502005;//  "{0}, {1}\n[마나 {2}] [지속시간 {3}초] [쿨타임 {4}초]"
        public const int SkillTree_7 = 502006;// "\n[분노 {0}] [지속시간 {1}초] [쿨타임 {2}초]"
        public const int SkillTree_8 = 502007;// "\n[마나 {0}] [지속시간 {1}초] [쿨타임 {2}초]"
        public const int SkillTree_9 = 502008;// "{0} 사용 시 {1} {2}\n[분노 {3}] [쿨타임 {4}초] " // {2} == 감소or증가
        public const int SkillTree_10 = 502009;///"{0} 사용 시 {1} {2}\n[마나 {3}] [쿨타임 {4}초] " // {2} == 감소or증가
        public const int SkillTree_11 = 502010;//   "[오라]"
        public const int SkillTree_12 = 502011;// "{0} 사용 시 {1} {2}, {3} {4}\n[분노 {5}] [쿨타임 {6}초] " {2},{4} == 감소or증가
        public const int SkillTree_13 = 502012;// "{0} 사용 시 {1} {2}, {3} {4}\n[마나 {5}] [쿨타임 {6}초] " {2},{4} == 감소or증가
        public const int SkillTree_14 = 502013;// 감소
        public const int SkillTree_15 = 502014;// 증가
        public const int SkillTree_16 = 269393;// "[배율 {0}%]"
        public const int SkillTree_17 = 269394;// " [위협수치 {0}%]"
        public const int SkillTree_18 = 269395;// " [도발수치 {0}%]"

        #endregion

        #region clientProtocl

        public const int ProtocolTxt_10000 = 260806;    // "인증 서버 접속 요청을 실패했습니다.");
        public const int ProtocolTxt_10001 = 260807;    // "게임 서버 접속 요청을 실패했습니다.");
        public const int ProtocolTxt_10002 = 260808;    // "인증 서버 로그인 요청을 실패했습니다.");
        public const int ProtocolTxt_10003 = 260809;    // "게임 서버 로그인 요청을 실패했습니다.");
        public const int ProtocolTxt_10004 = 260943;    // "인증 서버 연결이 끊어졌습니다.");
        public const int ProtocolTxt_10005 = 260944;    // "게임 서버 연결이 끊어졌습니다.");
        public const int ProtocolTxt_10006 = 260945;    // "푸시 서버 초기화에 실패했습니다.");

        public const int ProtocolTxt_10015 = 260810;    // "맵 이벤트 처리 요청을 실패했습니다.");
        #endregion

        #region ResultCode ErrorPopup
        public const int RcodeTxt_000 = 260812;    // "비정상적인 동작으로 인하여 접속을 종료합니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_001 = 260813;    // "성공", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_002 = 260814;    // "중복된 값입니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_003 = 260815;    // "업데이트가 필요합니다. 다운로드 페이지로 이동하시겠습니까?", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_004 = 260816;    // "플레이어 정보를 찾을 수 없습니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_005 = 260817;    // "계정 인증에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_006 = 260818;    // "인증 서버 접속에 실패했습니다.\n다시 시도해주세요.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_007 = 260819;    // "서버가 점검중입니다.\n네이버 카페로 이동합니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_008 = 260820;    // "중복 접속이 감지되었습니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_009 = 260821;    // "접속이 차단된 계정입니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_010 = 260822;    // "게스트 계정 생성에 실패했습니다.", E_TEXTTYPE.LOCAL);
        public const int RcodeTxt_011 = 260823;    // "다이아가 부족합니다.");
        public const int RcodeTxt_012 = 260824;    // "엘리가 부족합니다.");
        public const int RcodeTxt_013 = 260825;    // "길드 포인트가 부족합니다.");

        public const int RcodeTxt_015 = 260826;    // "확장 가능한 칸이 이미 최대치입니다.");
        public const int RcodeTxt_017 = 260827;    // "수락 가능한 친구 수가 이미 최대치입니다.");

        public const int RcodeTxt_018 = 260828;    // "명예 포인트가 부족합니다.");
        public const int RcodeTxt_019 = 260829;    // "수행 가능 횟수가 부족합니다.");
        public const int RcodeTxt_020 = 260830;    // "진행 중이 아닙니다.");
        public const int RcodeTxt_021 = 260831;    // "마일리지가 부족합니다.");
        public const int RcodeTxt_022 = 260832;    // "문자열 길이가 너무 짧습니다.");
        public const int RcodeTxt_023 = 260833;    // "존재하지 않습니다.");

        public const int RcodeTxt_025 = 260834;    // "사용이 불가능합니다.");
        public const int RcodeTxt_026 = 260835;    // "대상의 친구창이 이미 최대치입니다.");
        public const int RcodeTxt_027 = 260836;    // "이미 친구입니다.");
        public const int RcodeTxt_028 = 260837;    // "비정상적인 행위가 감지되었습니다.");
        public const int RcodeTxt_029 = 260838;    // "중복된 IAP입니다.");
        public const int RcodeTxt_030 = 260839;    // "탈퇴 처리가 진행중입니다.");

        public const int RcodeTxt_031 = 260840;    // "이미 사용된 쿠폰입니다.");
        public const int RcodeTxt_032 = 260841;    // "기간이 만료된 쿠폰입니다.");
        public const int RcodeTxt_033 = 260842;    // 이름이 잘못됨

        public const int RcodeTxt_034 = 262511;         // "아이템의 갯수가 변경되어 처리할 수 없습니다.");

        public const int RcodeTxt_035 = 260844;    // "하루 사용 제한이 초과되었습니다.");
        public const int RcodeTxt_036 = 260845;    // "유효하지 않은 쿠폰번호입니다.");
        public const int RcodeTxt_037 = 260846;    // "이미 수행 완료한 던전입니다.");
        public const int RcodeTxt_038 = 260847;    // "포탈 퀘스트를 클리어해야합니다.");
        public const int RcodeTxt_039 = 260848;    // "숨겨진 포탈 퀘스트를 클리어해야합니다.");
        public const int RcodeTxt_040 = 260849;    // "상품정보가 잘못되었습니다.");
        public const int RcodeTxt_041 = 260850;    // "이미 오늘 수행가능한 일일퀘스트를 모두 수락했습니다.");
        public const int RcodeTxt_042 = 260851;    // "하우징 채널 접속 인원이 가득찼습니다.");
        public const int RcodeTxt_043 = 260852;    // "서버가 포화 상태라 하우징 입장이 불가능합니다.");
        public const int RcodeTxt_044 = 260853;    // "차지 스킬 사용이 불가능합니다.");
        public const int RcodeTxt_045 = 240144;    // "집이 없는 유저입니다.");

        public const int RcodeTxt_049 = 260854;    // "귓속말 대상이 접속 중이지 않습니다.");

        public const int RcodeTxt_050 = 260855;    // "수행 중인 서브 퀘스트가 이미 최대치입니다.");
        public const int RcodeTxt_051 = 260856;    // "수행 기간이 만료된 퀘스트입니다.");
        public const int RcodeTxt_052 = 260857;    // "레벨이 부족합니다.");
        public const int RcodeTxt_053 = 260858;    // "스킬 쿨타임입니다.");
        public const int RcodeTxt_054 = 260859;    // "스킬 소모 자원이 부족합니다.");
        public const int RcodeTxt_055 = 260860;    // "혼자하기 모드입니다.");
        public const int RcodeTxt_056 = 260861;    // "다른 유저를 기다리고 있습니다.");
        public const int RcodeTxt_057 = 260862;    // "유저를 찾을 수 없습니다.");
        public const int RcodeTxt_058 = 260863;    // "방을 찾을 수 없습니다.");
        public const int RcodeTxt_059 = 260864;    // "방장이 초대를 취소했습니다.");

        public const int RcodeTxt_060 = 260865;    // "이미 죽은 상태입니다.");

        public const int RcodeTxt_062 = 260866;    // "현재 PVP시즌이 진행중이지 않습니다.");
        public const int RcodeTxt_063 = 262450;    // "가능한 횟수를 초과하였습니다."

        public const int RcodeTxt_065 = 262498;         // "이미 연동되어 있는 계정입니다.\n다른 계정을 이용해 주세요."
        public const int RcodeTxt_066 = 260867;    // "현재 좋아요 시즌을 진행중이지 않습니다.");

        public const int RcodeTxt_070 = 260868;    // "길드가 존재하지 않습니다.");
        public const int RcodeTxt_071 = 260869;    // "길드 멤버가 가득찼습니다.");
        public const int RcodeTxt_072 = 260870;    // "이미 가입한 길드가 존재합니다.");
        public const int RcodeTxt_073 = 260871;    // "권한이 부족합니다.");
        public const int RcodeTxt_074 = 260871;    // "쿨타임 입니다.");
        public const int RcodeTxt_075 = 260872;    // "길드 보상 날짜를 설정중입니다.");
        public const int RcodeTxt_076 = 260873;    // "이미 다른 유저가 선택했습니다.");
        public const int RcodeTxt_077 = 260874;    // "파티 수행에 실패했습니다.");
        public const int RcodeTxt_078 = 260875;    // "파티 수락에 실패했습니다.");
        public const int RcodeTxt_079 = 260876;    // "핫타임이 서버와 다릅니다.");
        public const int RcodeTxt_080 = 261906;    // "이미 시작되었습니다.");

        public const int RcodeTxt_081 = 260877;    // "쿠폰을 적용할 계정이 존재하지 않습니다.");
        public const int RcodeTxt_082 = 260878;    // "이메일 아이디가 존재하지 않습니다.");
        public const int RcodeTxt_083 = 260879;    // "비밀번호가 일치하지 않습니다.");
        public const int RcodeTxt_084 = 260880;    // "이메일 아이디가 잘못되었습니다.");
        public const int RcodeTxt_085 = 260881;    // "이미 연동되어 있는 계정입니다.\n다른 계정을 이용해 주세요.");
        public const int RcodeTxt_086 = 260882;    // "더 이상 구매가 불가능합니다.");
        public const int RcodeTxt_087 = 260883;    // "아직 귀환이 불가능합니다.");
        public const int RcodeTxt_088 = 260884;    // "아이템 가방이 가득 찼습니다.");
        public const int RcodeTxt_089 = 260885;    // "펫 가방이 가득 찼습니다.");

        public const int RcodeTxt_090 = 260886;    // "이미 체력이 가득찼습니다.");
        public const int RcodeTxt_091 = 260887;    // "빙고 시즌이 존재하지않습니다.");
        public const int RcodeTxt_092 = 260888;    // "던전 시간이 초과되었습니다.");
        public const int RcodeTxt_093 = 260889;    // "던전 시작 조건을 충족시키지 못했습니다.");
        public const int RcodeTxt_094 = 260890;    // "던전 생성에 실패했습니다.");
        public const int RcodeTxt_095 = 260891;    // "채널 채팅방 입장에 실패했습니다.");
        public const int RcodeTxt_096 = 260892;    // "제작 기간이 아닙니다.");

        public const int RcodeTxt_097 = 260893;    // "아직은 채집을 시도할 수 없습니다.");
        public const int RcodeTxt_098 = 260894;    // "네트워크 처리가 잘못되었습니다.");
        public const int RcodeTxt_099 = 260895;    // "일반 공격 순서가 잘못되었습니다.");
        public const int RcodeTxt_100 = 260896;    // "네트워크 처리에 실패했습니다.");

        public const int RcodeTxt_101 = 262134;    // "채집 인덱스 번호가 다르다."
        public const int RcodeTxt_102 = 262135;    // "값이 부족"
        public const int RcodeTxt_103 = 262580;         // "입장이 불가능한 상태입니다."
        public const int RcodeTxt_104 = 262581;         // "요일이 다릅니다."
        public const int RcodeTxt_106 = 263441;         //TODO: 수정 필요 "계정 생성이 불가능한 서버임"
        public const int RcodeTxt_127 = 263079;         // "인증 대기중입니다."
        public const int RcodeTxt_105 = 263442;         // "길드 작물의 정보가 바뀌었습니다."
        #endregion
        #endregion

        #region GameEvent
        public const int GameEvent_0 = 900012; // "{0} 다이아 충전"
        public const int GameEvent_1 = 900013; // "{0} 다이아 소비"
        public const int GameEvent_2 = 900003; // "남은 기간 : {0}시간 미만"
        public const int GameEvent_3 = 900015; // "남은 기간 : {0}일 {1}시간"
        public const int GameEvent_4 = 900006; // "다이아 충전"
        public const int GameEvent_5 = 900004; // "다이아를 충전하고 보상을 받아가세요~!"
        public const int GameEvent_6 = 900007; // "다이아 소비"
        public const int GameEvent_7 = 900005; // "다이아를 소비하고 보상을 받아가세요~!"
        #endregion

        #region HotPackage
        public const int HotPackage_0 = 263528; //"{0}일";
        public const int HotPackage_1 = 263529; //"상시";
        #endregion

        #region Sell
        public const int SellPopup_0 = 263779; //"선택목록 [{0}]"
        #endregion

        #region Duel
        public const int Duel_0 = 269397; //"{0}님이 결투를 신청하셨습니다.\n수락하시겠습니까?"/
        #endregion

        public static string GetText(int id)
        {
            var findData = DataBaseManager.Instance.GetDataBase(DataBaseType.LANGUAGE).GetData(id);

            return findData == null ? "해당 ID가 언어테이블에 없음" : findData.NAME;
        }
    }

    public sealed class GiftType : EnumBaseType<GiftType, byte, string>
    {
        public static readonly GiftType NONE = new GiftType((byte)Gift_Type.NONE, "NONE");
        public static readonly GiftType ITEMS = new GiftType((byte)Gift_Type.ITEMS, "아이템(복수형)");
        public static readonly GiftType ITEM = new GiftType((byte)Gift_Type.ITEM, "아이템(단수형)");
        public static readonly GiftType PETS = new GiftType((byte)Gift_Type.PETS, "펫(복수형)");
        public static readonly GiftType PET = new GiftType((byte)Gift_Type.PET, "펫(단수형)");
        public static readonly GiftType HP_POTION = new GiftType((byte)Gift_Type.HP_POTION, "HP포션");
        public static readonly GiftType DIA = new GiftType((byte)Gift_Type.DIA, "다이아");
        public static readonly GiftType MILEAGE = new GiftType((byte)Gift_Type.MILEAGE, "마일리지");
        public static readonly GiftType GOLD = new GiftType((byte)Gift_Type.GOLD, "골드");
        public static readonly GiftType BUDDY_POINT = new GiftType((byte)Gift_Type.BUDDY_POINT, "우정포인트");
        public static readonly GiftType PET_TICKET = new GiftType((byte)Gift_Type.PET_TICKET, "펫 티켓");
        public static readonly GiftType ITEM_TICKET = new GiftType((byte)Gift_Type.ITEM_TICKET, "아이템 티켓");
        public static readonly GiftType FURNI_ITEM = new GiftType((byte)Gift_Type.FURNI_ITEM, "가구 아이템");
        public static readonly GiftType HOUSE_SKIN = new GiftType((byte)Gift_Type.HOUSE_SKIN, "하우스 스킨");
        public static readonly GiftType HOUSE_BG_SKIN = new GiftType((byte)Gift_Type.HOUSE_BG_SKIN, "하우스 배경 스킨");
        public static readonly GiftType COSTUME = new GiftType((byte)Gift_Type.COSTUME, "코스튬");
        public static readonly GiftType BUFF = new GiftType((byte)Gift_Type.BUFF, "버프");
        public static readonly GiftType USE_ITEM = new GiftType((byte)Gift_Type.USE_ITEM, "사용 아이템");
        public static readonly GiftType CHOICE_PET = new GiftType((byte)Gift_Type.CHOICE_PET, "선택 펫 티켓");
        public static readonly GiftType CHOICE_ITEM = new GiftType((byte)Gift_Type.CHOICE_ITEM, "선택 아이템 티켓");
        public static readonly GiftType REAL_CASH = new GiftType((byte)Gift_Type.REAL_CASH, "현금 다이아");  // 임시 키값.
        public static readonly GiftType SKILL_ENCHANTPOINT = new GiftType((byte)Gift_Type.SKILL_ENCHANTPOINT, "스킬강화포인트");
        public static readonly GiftType EVERYDAY_GOODS = new GiftType((byte)Gift_Type.EVERYDAY_GOODS, "매일 지급 상품");
        public static readonly GiftType PAY_MAIN_QUEST_GOODS = new GiftType((byte)Gift_Type.PAY_MAIN_QUEST_GOODS, "유료 메인 퀘스트 상품");
        public static readonly GiftType PAY_LEVEL_GOODS = new GiftType((byte)Gift_Type.PAY_LEVEL_GOODS, "유료 레벨 달성 상품");
        public static readonly GiftType PAY_INVEN_EXPAND = new GiftType((byte)Gift_Type.PAY_INVEN_EXPAND, "인벤 최대 확장 상품");
        public static readonly GiftType BLUE_DIA = new GiftType((byte)Gift_Type.BLUE_DIA, "블루 다이아");
        public static readonly GiftType GUILD_COIN = new GiftType((byte)Gift_Type.GUILD_COIN, "길드코인");
        public static readonly GiftType GUILD_POINT = new GiftType((byte)Gift_Type.GUILD_POINT, "길드포인트");

        public static readonly GiftType EXP = new GiftType((byte)Gift_Type.EXP, "경험치");  // 임시 키값.

        public GiftType(byte key, string desc)
            : base(key, desc)
        {
        }

        public static GiftType GetGiftType(int key)
        {
            if (key == GiftType.NONE.Key)
                return NONE;
            else if (key == GiftType.ITEM.Key)
                return ITEM;
            else if (key == GiftType.PETS.Key)
                return PETS;
            else if (key == GiftType.PET.Key)
                return PET;
            else if (key == GiftType.HP_POTION.Key)
                return HP_POTION;
            else if (key == GiftType.DIA.Key)
                return DIA;
            else if (key == GiftType.MILEAGE.Key)
                return MILEAGE;
            else if (key == GiftType.GOLD.Key)
                return GOLD;
            else if (key == GiftType.BUDDY_POINT.Key)
                return BUDDY_POINT;
            else if (key == GiftType.PET_TICKET.Key)
                return PET_TICKET;
            else if (key == GiftType.ITEM_TICKET.Key)
                return ITEM_TICKET;
            else if (key == GiftType.FURNI_ITEM.Key)
                return FURNI_ITEM;
            else if (key == GiftType.HOUSE_SKIN.Key)
                return HOUSE_SKIN;
            else if (key == GiftType.HOUSE_BG_SKIN.Key)
                return HOUSE_BG_SKIN;
            else if (key == GiftType.COSTUME.Key)
                return COSTUME;
            else if (key == GiftType.BUFF.Key)
                return BUFF;
            else if (key == GiftType.USE_ITEM.Key)
                return USE_ITEM;
            else if (key == GiftType.CHOICE_PET.Key)
                return CHOICE_PET;
            else if (key == GiftType.CHOICE_ITEM.Key)
                return CHOICE_ITEM;
            else if (key == GiftType.EXP.Key)
                return EXP;
            else if (key == GiftType.SKILL_ENCHANTPOINT.Key)
                return SKILL_ENCHANTPOINT;
            else if (key == GiftType.REAL_CASH.Key)
                return REAL_CASH;
            else if (key == GiftType.BLUE_DIA.Key)
                return BLUE_DIA;
            else if (key == GiftType.GUILD_COIN.Key)
                return GUILD_COIN;
            else if (key == GiftType.GUILD_POINT.Key)
                return GUILD_POINT;
            else
                return NONE;
        }
    }

    public sealed class DataUnit : EnumBaseType<DataUnit, byte, string>
    {
        public static readonly DataUnit BOOLEAN = new DataUnit(0, "BOOL");
        public static readonly DataUnit INTEGER = new DataUnit(1, "정수");
        public static readonly DataUnit PERCENT = new DataUnit(2, "백분율");
        public static readonly DataUnit PERMILLE = new DataUnit(3, "천분율");
        public static readonly DataUnit STRING = new DataUnit(4, "문자열");

        public DataUnit(byte key, string desc)
            : base(key, desc)
        {
        }

        public static ReadOnlyCollection<DataUnit> GetValues()
        {
            return GetBaseValues();
        }

        public static DataUnit GetByKey(byte key)
        {
            return GetBaseByKey(key);
        }

        public bool IsInteger
        {
            get { return this == DataUnit.INTEGER || this == DataUnit.PERCENT || this == DataUnit.PERMILLE; }
        }
    }

    public sealed class BasisType : EnumBaseType<BasisType>
    {
        public static BasisType POTION_COOL_TIME = new BasisType(0, "물약사용 쿨타임(millSec)", DataUnit.INTEGER, true);
        public static BasisType BASIS_ITEM_ID = new BasisType(1, "아이템코드", DataUnit.INTEGER, true);
        public static BasisType JOB_DEF_WEAPON = new BasisType(2, "직업별 기본무기", DataUnit.INTEGER, true);
        public static BasisType JOBGROUP_DEF_ARMOR = new BasisType(3, "직업그룹별 기본방어구", DataUnit.INTEGER, true);
        public static BasisType KNIGHT_DEF_SHIELD = new BasisType(4, "기사기본방패", DataUnit.INTEGER, false);
        public static BasisType CHAR_SLOT_BUY_DIA = new BasisType(5, "캐릭터 슬롯 구입다이아", DataUnit.INTEGER, false);
        public static BasisType STORAGE_DEFAULT_SLOT = new BasisType(6, "창고 기본&확장 슬롯수", DataUnit.INTEGER, false);
        public static BasisType STORAGE_MAX_SLOT = new BasisType(7, "창고 최대 슬롯수", DataUnit.INTEGER, false);
        public static BasisType STORAGE_EXPAND_DIA = new BasisType(8, "창고확장 다이아", DataUnit.INTEGER, false);
        public static BasisType INVEN_DEFAULT_SLOT = new BasisType(9, "인벤 기본 슬롯수", DataUnit.INTEGER, false);
        public static BasisType INVEN_MAX_SLOT = new BasisType(10, "인벤 최대 슬롯수", DataUnit.INTEGER, false);
        public static BasisType INVEN_EXPAND_DIA = new BasisType(11, "인벤확장 다이아(10분당)", DataUnit.INTEGER, false);
        public static BasisType INVEN_EXPAND_MILLISEC = new BasisType(12, "인벤확장 단위시간(millisec)", DataUnit.INTEGER, false);
        public static BasisType PET_INVEN_DEFAULT_SLOT = new BasisType(13, "펫인벤 기본 슬롯수", DataUnit.INTEGER, false);
        public static BasisType PET_INVEN_MAX_SLOT = new BasisType(14, "펫인벤 최대 슬롯수", DataUnit.INTEGER, false);
        public static BasisType PET_INVEN_EXPAND_DIA = new BasisType(15, "펫인벤확장 다이아(15분당)", DataUnit.INTEGER, false);
        public static BasisType PET_INVEN_EXPAND_MILLISEC = new BasisType(16, "펫인벤확장 단위시간(millisec)", DataUnit.INTEGER, false);
        public static BasisType NEW_USER_PROTECT_LEVEL = new BasisType(17, "신규유저 보호 레벨", DataUnit.INTEGER, false);
        public static BasisType DEFINE_RETURN_TIME = new BasisType(18, "귀환 대기 시간", DataUnit.INTEGER, false);
        public static BasisType DEFINE_RETURN_PRICE = new BasisType(19, "귀환 기본 가격", DataUnit.INTEGER, false);
        public static BasisType DEFINE_REBIRTH_PRICE = new BasisType(20, "제자리 부활 기본 가격", DataUnit.INTEGER, false);
        public static BasisType QUEST_START_ID = new BasisType(21, "시작 메인 퀘스트 ID", DataUnit.INTEGER, false);
        public static BasisType DEFINE_DUNGEON_REBIRTH_PRICE = new BasisType(22, "던전 제자리 부활 가격", DataUnit.INTEGER, false);
        public static BasisType DEFINE_DUNGEON_REBIRTH_ALL_PRICE = new BasisType(23, "던전 제자리 부활(모두) 가격", DataUnit.INTEGER, false);
        public static BasisType GUILD_CREATE_GOLD = new BasisType(24, "길드 창설 필요 골드", DataUnit.INTEGER, false);
        public static BasisType GUILD_REQUEST_LIMIT = new BasisType(25, "길드 가입요청 최대횟수", DataUnit.INTEGER, false);
        public static BasisType GUILD_MEMBER_LIMIT = new BasisType(26, "길드원 제한", DataUnit.INTEGER, false);
        public static BasisType GUILD_PART_MASTER_LIMIT = new BasisType(27, "부길마 인원제한", DataUnit.INTEGER, false);
        public static BasisType GUILD_CHANGE_EMBLEM_DIA = new BasisType(28, "길드 엠블럼 변경 다이아", DataUnit.INTEGER, false);
        public static BasisType GUILD_MASTER_NOCONNECTION_DAYS = new BasisType(29, "길드 마스터 권한 획득 미접속 일수", DataUnit.INTEGER, false);
        public static BasisType GUILD_SKILL_INIT_DIA = new BasisType(30, "길드 스킬 초기화 다이아", DataUnit.INTEGER, false);

        public static BasisType HOUSE_BUY_LEVEL_LIMIT = new BasisType(31, "하우스 구매 레벨 제한", DataUnit.INTEGER, false);
        public static BasisType HOUSE_FIRST_MAP_INDEX = new BasisType(32, "하우스 처음 맵 인덱스", DataUnit.INTEGER, false);
        public static BasisType HOUSE_BUY_GOLD = new BasisType(33, "하우스 초기 구입 비용", DataUnit.INTEGER, false);
        public static BasisType HOUSE_TOTAL_FURNI_COUNT = new BasisType(34, "하우스에 최대 가구 배치갯수", DataUnit.INTEGER, false);
        public static BasisType FURNI_INVEN_DEFAULT_SLOT = new BasisType(35, "가구 인벤 기본 슬롯수", DataUnit.INTEGER, false);
        public static BasisType FURNI_INVEN_MAX_SLOT = new BasisType(36, "가구 인벤 최대 슬롯수", DataUnit.INTEGER, false);
        public static BasisType FURNI_INVEN_EXPAND_DIA = new BasisType(37, "가구 인벤확장 다이아(개당)", DataUnit.INTEGER, false);

        public static BasisType DAY_DUNGEON_MAX_TICKET = new BasisType(38, "요일던전 하루 입장 가능 횟수", DataUnit.INTEGER, false);
        public static BasisType FURNI_UPGRADE_USE_DIA = new BasisType(39, "가구 업그레이드 다이아 갯수 당 차감 초", DataUnit.INTEGER, false);
        public static BasisType INFINITE_TOWER_MAX_TICKET = new BasisType(40, "무한의 탑 입장 열쇠 최대 개수", DataUnit.INTEGER, false);
        public static BasisType EVENT_DUNGEON_COOLTIME = new BasisType(41, "이벤트 던전 생성 쿨타임 (millesec)", DataUnit.INTEGER, false);
        public static BasisType HOUSE_PET_GET_EXP_PER_MIN = new BasisType(42, "몇분마다 경험치 획득가능(10분간격으로 설정. 초단위)", DataUnit.INTEGER, false);
        public static BasisType HOUSE_LIKE_COUNT_LIMIT = new BasisType(43, "일주일에 좋아요 최대 갯수", DataUnit.INTEGER, false);
        public static BasisType GUILD_POINT_SHOP_BUY_COUNT = new BasisType(44, "길드 포인트샵 최대 구매횟수(기본3)", DataUnit.INTEGER, false);
        public static BasisType PVP_TAG_RANK_NEED_PLAY_COUNT = new BasisType(45, "pvp 태그 랭킹 등록 필요 플레이횟수", DataUnit.INTEGER, false);
        public static BasisType REVIVAL_BUFF_ID = new BasisType(46, "부활버프 ID", DataUnit.INTEGER, false);
        public static BasisType REVIVAL_BUFF_TIME = new BasisType(47, "부활버프 TIME 밀리세컨드", DataUnit.INTEGER, false);
        public static BasisType PET_TUTORIAL_DEFAULT_DROPID = new BasisType(48, "펫 뽑기 튜토리얼 시 드랍 리스트 ID", DataUnit.INTEGER, false);
        public static BasisType DEFINE_PVP_REVIVAL_DIA = new BasisType(49, "PVP 미리 부활 비용", DataUnit.INTEGER, false);
        public static BasisType CHARACTERISTIC_INIT_DIA = new BasisType(50, "특성 초기화 소비 다이아 갯수", DataUnit.INTEGER, false);
        public static BasisType CHARACTERISTIC_GET_LEVEL = new BasisType(51, "처음으로 특성이 생기는 레벨", DataUnit.INTEGER, false);
        public static BasisType MANA_REGENERATE_TIME = new BasisType(52, "마나 리젠시간,밀리세컨 1/1000초", DataUnit.INTEGER, false);
        public static BasisType T_QUEST_START_ID = new BasisType(53, "튜토리얼 스타트 ID", DataUnit.INTEGER, false);
        public static BasisType EVENT_DUNGEON_RATE = new BasisType(54, "이벤트던전 생성 확률(천분률)", DataUnit.INTEGER, false);
        public static BasisType EVENT_DUNGEON_TIER_LEVEL = new BasisType(55, "이벤트던전 구간 레벨단위", DataUnit.INTEGER, false);
        public static BasisType EVENT_DUNGEON_LIVE_TIME = new BasisType(56, "이벤트던전 포탈 지속시간", DataUnit.INTEGER, false);
        public static BasisType HOUSE_EXTEND_DEFAULT_VALUE = new BasisType(57, "하우스 확장 기본 가격 수치값", DataUnit.INTEGER, false);
        public static BasisType HOUSE_EXTEND_STEP_VALUE = new BasisType(58, "하우스 확장 단계별 증가값", DataUnit.INTEGER, false);
        public static BasisType TUTORIAL_WEAPON_INCHANT_ID = new BasisType(59, "튜토,무기강화 재료ID", DataUnit.INTEGER, false);
        public static BasisType TUTORIAL_PET_INCHANT_ID = new BasisType(60, "튜토,펫강화 재료ID", DataUnit.INTEGER, false);
        public static BasisType TUTORIAL_PETBOOK_ID = new BasisType(61, "튜토, 몬스터도감 재료ID", DataUnit.INTEGER, false);
        public static BasisType TUTORIAL_PETBOOK_COUNT = new BasisType(62, "튜토, 몬스터도감 재료수량", DataUnit.INTEGER, false);
        public static BasisType TUTORIAL_PET_DIA = new BasisType(63, "튜토, 펫뽑기 다이아 갯수", DataUnit.INTEGER, false);
        public static BasisType HOUSE_LIKE_FARM_BUDDY_POINT = new BasisType(64, "하우스, 재배-버디 포인트", DataUnit.INTEGER, false);
        public static BasisType STORAGE_EXPAND_INCREAD_SLOT = new BasisType(65, "증가시 늘어나는 칸수", DataUnit.INTEGER, false);
        public static BasisType STORAGE_EXPAND_INCREAD_DIA_1 = new BasisType(66, "{1}번 확장시 마다 다이아{2}개 증가", DataUnit.INTEGER, false);
        public static BasisType STORAGE_EXPAND_INCREAD_DIA_2 = new BasisType(67, "{1}번 확장시 마다 다이아{2}개 증가", DataUnit.INTEGER, false);
        public static BasisType DEFAULT_START_JOB_QUEST = new BasisType(68, "전직 후 받는 첫번째 퀘스트", DataUnit.INTEGER, false);
        public static BasisType DAILY_QUEST_MAX_GET_COUNT = new BasisType(69, "일일 퀘스트 가질수 있는 최대 갯수", DataUnit.INTEGER, false);

        public static BasisType PARTY_PLAYER_NUM_PER_EXP_RATE = new BasisType(70, "동행수에 따른 경험치 증가량", DataUnit.INTEGER, false);
        public static BasisType PARTY_PLAYER_NUM_PER_GOLD_RATE = new BasisType(71, "동행수에 따른 골드 증가량", DataUnit.INTEGER, false);
        public static BasisType PARTY_LOW_LEVEL_MONSTER_PER_RATE = new BasisType(72, "레벨차에 경험치 감소량(저렙)", DataUnit.INTEGER, false);// 180을 넣었다면 5레벨까지는 그대로 6레벨부터 18%씩 감소..
        public static BasisType PARTY_HIGH_LEVEL_MONSTER_PER_RATE = new BasisType(73, "레벨차에 경험치 감소량(저렙)", DataUnit.INTEGER, false);// 10을 넣었다면 1레벨당 감소량 1퍼센트씩 감소.. 

        public static BasisType FURNI_EXPAND_INCREAD_SLOT = new BasisType(74, "가구 인벤 늘어나는 칸수", DataUnit.INTEGER, false);

        public static BasisType EG_DUNGEON_PAY = new BasisType(75, "경험치&골드 유료사용비(횟수에 * 된다.)", DataUnit.INTEGER, false);

        public static BasisType REST_REWARD_POINT_MAX = new BasisType(76, "휴식보상-포인트 맥스값", DataUnit.INTEGER, false);
        public static BasisType REST_REWARD_POINT_MIN = new BasisType(77, "휴식보상-최소 사용 포인트값", DataUnit.INTEGER, false);
        public static BasisType REST_REWARD_DAY_POINT_MAX = new BasisType(78, "휴식보상-오늘 얻는 최대 포인트값", DataUnit.INTEGER, false);

        public static BasisType CONNECT_REWARD_GET_MIN = new BasisType(79, "접속보상-몇분마다 주사위 횟수를 얻나?(sec)", DataUnit.INTEGER, false);

        public static BasisType CONNECT_REWARD_FREE_DICE_MAX_COUNT = new BasisType(80, "접속보상-하루 최대 무료 횟수", DataUnit.INTEGER, false);
        public static BasisType CONNECT_REWARD_DICE_MAX = new BasisType(81, "접속보상-주사위 최대값", DataUnit.INTEGER, false);
        public static BasisType CONNECT_REWARD_DICE_MIN = new BasisType(82, "접속보상-주사위 최소값", DataUnit.INTEGER, false);

        public static BasisType CONNECT_REWARD_DIA_DICE_MAX_COUNT = new BasisType(83, "접속보상-유료 주사위 최대 횟수", DataUnit.INTEGER, false);
        public static BasisType REWARD_DICE_DEFAULT_PRICE = new BasisType(84, "접속보상-유료 주사위 기본 가격", DataUnit.INTEGER, false);
        public static BasisType CONNECT_REWARD_DICE_RATE_PRICE = new BasisType(85, "접속보상-유료 주사위 증가 가격", DataUnit.INTEGER, false);

        public static BasisType EG_DUNGEON_TICKET_COUNT = new BasisType(86, "경험치,골드 던전 일일 입장 횟수", DataUnit.INTEGER, false);
        public static BasisType HOTTIME_FREE_CHARGE_MILESEC = new BasisType(87, "핫타임-무료 추가 시간(Milesec)", DataUnit.INTEGER, false);
        public static BasisType HOTTIME_DIA_CHARGE_MILESEC = new BasisType(88, "핫타임-유료 추가 시간(Milesec)", DataUnit.INTEGER, false);
        public static BasisType HOTTIME_DIA_MAX_COUNT = new BasisType(89, "핫타임-최대 유료 사용 횟수", DataUnit.INTEGER, false);
        public static BasisType HOTTIME_DIA_DEFAULT_PRICE = new BasisType(90, "핫타임-유료 기본 가격", DataUnit.INTEGER, false);
        public static BasisType HOTTIME_DIA_RATE_PRICE = new BasisType(91, "핫타임-유료 증가 가격", DataUnit.INTEGER, false);
        public static BasisType MULTI_DUNGEON_BONUS_CNT = new BasisType(92, "멂티던전 보상 보너스 하루 갯수", DataUnit.INTEGER, false);
        public static BasisType MULTI_DUNGEON_BONUS_PER = new BasisType(93, "멂티던전 보상 보너스 추가 % (골드 & 경험치)", DataUnit.INTEGER, false);
        public static BasisType EG_DUNGEON_BUY_TICKET = new BasisType(94, "경험치&골드 던전 입장권 하루 구매횟수", DataUnit.INTEGER, false);
        public static BasisType TUTORIAL_FINISH_PET_TICKET = new BasisType(95, "튜토, 완료 지급 펫티켓(드랍리스트 아이디)", DataUnit.INTEGER, false);
        public static BasisType BLUETOOTH_DIA_MAX_COUONT = new BasisType(96, "블루투스 친구-다이아 최대 받는 횟수", DataUnit.INTEGER, false);
        public static BasisType BLUETOOTH_GET_DIA = new BasisType(97, "블루투스 친구-다이아 값", DataUnit.INTEGER, false);
        public static BasisType DEFINE_HOTTIME_GET_EXP_RATE = new BasisType(98, "핫타임 추가경험치(천분률)", DataUnit.INTEGER, false);
        public static BasisType DEFINE_HOTTIME_GET_ELLY_RATE = new BasisType(99, "핫타임 추가 엘리(천분률)", DataUnit.INTEGER, false);
        public static BasisType DEFINE_HOTTIME_GET_DROP_PET_RATE = new BasisType(100, "핫타임 펫 추가 드랍률(천분률)", DataUnit.INTEGER, false);
        public static BasisType DEFINE_HOTTIME_GET_DROP_ITME_RATE = new BasisType(101, "핫타임 아이템 추가드랍률(천분률)", DataUnit.INTEGER, false);


        public static BasisType FREE_PRIMIUM_PET_COOLTIME = new BasisType(102, "무료 고급펫 뽑기 쿨타임(mill)", DataUnit.INTEGER, false);
        public static BasisType FREE_PRIMIUM_ITEM_COOLTIME = new BasisType(103, "무료 고급아이템 뽑기 쿨타임(mill)", DataUnit.INTEGER, false);
        public static BasisType FREE_NORMAL_PET_COUNT = new BasisType(104, "무료 일반펫 뽑기 하루 횟수", DataUnit.INTEGER, false);
        public static BasisType FREE_NORMAL_PET_COOLTIME = new BasisType(105, "무료  일반펫 뽑기 쿨타임(mill)", DataUnit.INTEGER, false);
        public static BasisType FREE_NORMAL_ITEM_COUNT = new BasisType(106, "무료 일반아이템 뽑기 하루 횟수", DataUnit.INTEGER, false);
        public static BasisType FREE_NORMAL_ITEM_COOLTIME = new BasisType(107, "무료  일반아이템 뽑기 쿨타임(mill)", DataUnit.INTEGER, false);
        public static BasisType POWER_RANK_LIMIT_POION = new BasisType(108, "전투력 랭킹 최소값", DataUnit.INTEGER, false);

        public static BasisType BUY_HOUSE_GIFT_1 = new BasisType(109, "집들이 선물1", DataUnit.INTEGER, false);
        public static BasisType BUY_HOUSE_GIFT_2 = new BasisType(110, "집들이 선물2", DataUnit.INTEGER, false);
        public static BasisType BUY_HOUSE_GIFT_3 = new BasisType(111, "집들이 선물3", DataUnit.INTEGER, false);
        public static BasisType MULTI_DUNGEON_TICKET = new BasisType(112, "멀티던전 하루 입장 횟수", DataUnit.INTEGER, false);
        public static BasisType MULTI_DUNGEON_TICKET_DIA = new BasisType(113, "멀티던전 입장권 구매 다이아 갯수", DataUnit.INTEGER, false);

        public static BasisType MONSTER_BOOK_VERSION = new BasisType(114, "몬스터 도감 버전", DataUnit.INTEGER, false);
        public static BasisType TOWN_SELL_PRICE = new BasisType(115, "마을 물건 팔기(천분율)", DataUnit.INTEGER, false);

        public static BasisType CASH_MAINQUEST_COUNT = new BasisType(116, "현금-메인퀘스트보상 계정당 횟수", DataUnit.INTEGER, false);

        public static BasisType CHAR_MAX_LEVEL = new BasisType(117, "캐릭터 맥스레벨", DataUnit.INTEGER, false);

        public static BasisType MARKET_ELLY_FEES = new BasisType(118, "거래소 등록 수수료(엘리, 고정)", DataUnit.INTEGER, false);
        public static BasisType MARKET_CAL_FEES = new BasisType(119, "거래소 정산 수수료(블루 다이아,%)", DataUnit.INTEGER, false);
        public static BasisType MARKET_LIVE_TIME = new BasisType(120, "거래소 만료시간", DataUnit.INTEGER, false);
        public static BasisType LINE_FRIEND_CODE_LEVEL_LIMIT = new BasisType(121, "라인 친구 보상 레벨 제한", DataUnit.INTEGER, false);
        public static BasisType MARKET_LEVEL_LIMIT = new BasisType(122, "거래소-레벨제한", DataUnit.INTEGER, false);
        public static BasisType PET_EQUIPMENT_LEVEL_LIMIT = new BasisType(123, "펫 장비 장착 탭 레벨 제한", DataUnit.INTEGER, false);
        public static BasisType FOOD_ITEM_COOL_TIME = new BasisType(124, "음식 쿨타임", DataUnit.INTEGER, false);
        public static BasisType POTION_USE_TYPE = new BasisType(125, "물약 사용 타입", DataUnit.INTEGER, false); // 0인경우 한국, 1인경우 그 외

        // 126은 현재 사용하지 않습니다.
        public static BasisType POTION_ADD_VALUE = new BasisType(126, "물약 체력 올려주는 수치+", DataUnit.INTEGER, false);
        public static BasisType LOCAL_PRICE_CURRENCY_CODE = new BasisType(127, "local_price 항목의 통화코드", DataUnit.INTEGER, false);
        public static BasisType CROP_UI_ENABLE = new BasisType(128, "작물 UI 활성화(true 인 경우에 활성화)", DataUnit.INTEGER, false);

        public static BasisType CHARGE_SKILL_ACTIVE_FLAG = new BasisType(129, "차지 스킬 기능 활성화 여부", DataUnit.INTEGER, false);
        public static BasisType PVP_LOCK = new BasisType(130, "PVP 버튼 활성화 여부", DataUnit.INTEGER, false); // 1인 경우 PVP 오픈, 0인경우 닫힘
        public static BasisType CHANGE_JOB_GIFT_TYPE = new BasisType(131, "전직 선물 타입", DataUnit.INTEGER, false);
        public static BasisType CHANGE_JOB_ITEM_ID = new BasisType(132, "전직 선물 id", DataUnit.INTEGER, false);
        public static BasisType MARRY_LEVEL_LIMIT = new BasisType(133, "결혼 레벨제한", DataUnit.INTEGER, false);
        public static BasisType MARRY_BOUQUET_REWARD = new BasisType(134, "결혼식 부케 보상", DataUnit.INTEGER, false);

        public static BasisType MARKET_BUY_LIMIT = new BasisType(135, "거래소 구매 제한", DataUnit.INTEGER, false);

        public readonly DataUnit Data_Unit;
        public readonly bool HasDetail;

        public BasisType(int key, string value, DataUnit dataUnit, bool hasDetail)
            : base(key, value)
        {
            this.Data_Unit = dataUnit;
            this.HasDetail = hasDetail;
        }

        public static BasisType GetByKey(int key)
        {
            return GetBaseByKey(key);
        }

        public bool IsInteger
        {
            get { return Data_Unit.IsInteger; }
        }

        //public bool GetBool(int seq = -1)
        //{
        //    if (Data_Unit != DataUnit.BOOLEAN)
        //        return false;

        //    if (BasisDataManager.Instance == null)
        //        return false;

        //    return BasisDataManager.Instance.GetBool(this);
        //}

        //public string GetString(int seq = -1)
        //{
        //    if (Data_Unit != DataUnit.STRING)
        //        return string.Empty;

        //    if (BasisDataManager.Instance == null)
        //        return string.Empty;

        //    return BasisDataManager.Instance.GetString(this);
        //}

        //public int GetInt(int seq = -1)
        //{
        //    if (Data_Unit != DataUnit.INTEGER && Data_Unit != DataUnit.PERCENT)
        //        return 0;

        //    if (BasisDataManager.Instance == null)
        //        return 0;

        //    return BasisDataManager.Instance.GetInt(this);
        //}

        //public float GetFloat(int seq = -1)
        //{
        //    float output = 0;

        //    if (Data_Unit != DataUnit.PERCENT && Data_Unit != DataUnit.PERMILLE)
        //        return output;

        //    if (BasisDataManager.Instance == null)
        //        return output;

        //    output = BasisDataManager.Instance.GetInt(this);

        //    return output * (Data_Unit == DataUnit.PERCENT ? 0.01f : 0.001f);
        //}

        //public static int GetItemID(E_BasisItem basisItem)
        //{
        //    return BASIS_ITEM_ID.GetInt((int)basisItem);
        //}
    }

}
