﻿using Sfs2X;
using Sfs2X.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace HMTool.Latale
{
    //라테일 코드들
    public static class Utills
    {
        /// <summary>
        ///     특별한 맵 인덱스 드랍 처리.
        /// </summary>
        /// <param name="Idx">50000 이상의 mapDropID</param>
        /// <returns></returns>
        public static string GetSpecialDropMapText(object Idx)
        {
            string text = String.Empty;

            switch (Idx)
            {
                case 50000:
                    text = cText.GetText(cText.BookSelect_11);
                    break;
                case 50001:
                    text = cText.GetText(cText.BookSelect_12);
                    break;
                case 50002:
                    text = cText.GetText(cText.BookSelect_13);
                    break;
                case 50003:
                    text = cText.GetText(cText.BookSelect_14);
                    break;
                case 50004:
                    text = cText.GetText(cText.BookSelect_15);
                    break;
                case 50005:
                    text = cText.GetText(cText.BookSelect_16);
                    break;
                case 50006:
                    text = cText.GetText(cText.BookSelect_17);
                    break;
                case 50007:
                    text = cText.GetText(cText.BookSelect_18);
                    break;
                case 50008:
                    text = cText.GetText(cText.BookSelect_19);
                    break;
                case 50009:
                    text = cText.GetText(cText.BookSelect_20);
                    break;
                case 50010:
                    text = cText.GetText(cText.BookSelect_21);
                    break;
                case 50011:
                    text = cText.GetText(cText.BookSelect_22);
                    break;
                case 50012:
                    text = cText.GetText(cText.BookSelect_23);
                    break;
                case 50013:
                    text = cText.GetText(cText.BookSelect_24);
                    break;
                case 50014:
                    text = cText.GetText(cText.BookSelect_25);
                    break;
                case 50015:
                    text = cText.GetText(cText.BookSelect_26);
                    break;
                case 50016:
                    text = cText.GetText(cText.BookSelect_27);
                    break;
                case 50017:
                    text = cText.GetText(cText.BookSelect_28);
                    break;
                case 50018:
                    text = cText.GetText(cText.BookSelect_29);
                    break;
                case 50019:
                    text = cText.GetText(cText.BookSelect_30);
                    break;
                case 50020:
                    text = cText.GetText(cText.BookSelect_31);
                    break;
                case 50021:
                    text = cText.GetText(cText.BookSelect_32);
                    break;
                case 50022:
                    text = cText.GetText(cText.BookSelect_33);
                    break;
                case 50023:
                    text = cText.GetText(cText.BookSelect_39);
                    break;
                case 50024:
                    text = cText.GetText(cText.BookSelect_40);
                    break;
                case 50025:
                    text = cText.GetText(cText.BookSelect_41);
                    break;
                case 50026:
                    text = cText.GetText(cText.BookSelect_42);
                    break;
                case 50028:
                    text = cText.GetText(cText.BookSelect_43);
                    break;
                case 50029:
                    text = cText.GetText(cText.BookSelect_44);
                    break;
                case 50030:
                    text = cText.GetText(cText.BookSelect_45);
                    break;
                case 50031:
                    text = cText.GetText(cText.BookSelect_46);
                    break;
            }
            return text;
        }

        public static bool IsNumber(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            foreach (char ch in str)
            {
                if (!System.Char.IsDigit(ch))
                    return false;
            }
            return true;
        }

        public static ByteArray GetEncrypt(string s, SmartFox sfs2x)
        {
            AesEncrypter encrypter = new AesEncrypter(sfs2x.SessionToken);
            return new ByteArray(encrypter.Encrypt(Encoding.UTF8.GetBytes(s)));
        }
        public static string GetDecrypt(ByteArray s, SmartFox sfs2x)
        {
            AesEncrypter encrypter = new AesEncrypter(sfs2x.SessionToken);
            return Encoding.UTF8.GetString(encrypter.Decrypt(s.Bytes));
        }

        //C#의 Datatime을 Java에 맞게
        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalMilliseconds);
        }

        public static GCFreeListEnumerable<T> Iter<T>(this List<T> list)
        {
            return new GCFreeListEnumerable<T>(list);
        }

        public struct GCFreeListEnumerable<T>
        {
            private List<T> list;
            public GCFreeListEnumerable(List<T> aList)
            {
                list = aList;
            }
            public GCFreeListIterator<T> GetEnumerator()
            {
                return new GCFreeListIterator<T>(list);
            }
        }

        public struct GCFreeListIterator<T>
        {
            private int index;
            private List<T> list;

            public GCFreeListIterator(List<T> aList)
            {
                list = aList;
                index = -1;
            }

            public T Current
            {
                get { return list[index]; }
            }

            public bool MoveNext()
            {
                return ++index < list.Count;
            }
        }

        public static T ToEnum<T>(this int value)
            where T : struct, IConvertible, IFormattable, IComparable
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        public static T ToEnum<T>(this string value)
            where T : struct, IConvertible, IFormattable, IComparable
        {
            return (T)Enum.ToObject(typeof(T), value);
        }
    }
}
