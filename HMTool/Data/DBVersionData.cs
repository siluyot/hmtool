﻿using HMTool.Data.Base;
using HMTool.Latale;

namespace HMTool.Data
{
    public class DBVersionData : ReadData
    {
        public byte data_type;
        public short data_version;
        public int server_only;

        public override int ID => 0;
        public override string NAME => "";

        public DataBaseType Data_Type
        {
            get { return (DataBaseType)data_type; }
        }
    }

    public class LAVersion
    {
        public short data_version;
        public string aes_key;
        public string aes_iv;
        public string aes_secret_key;
    }
}
