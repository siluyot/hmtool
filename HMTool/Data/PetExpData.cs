﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class PetExpData : ReadViewData
    {
        public int id;
        public byte pet_group_id;
        public int pet_level;
        public short exppoint;

        public override int ID => id;
        public override string NAME => string.Format("{0} {1} {2}", id, pet_level, exppoint);

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "pet_group_id", typeof(byte)),
                new Tuple<string, Type>( "pet_level", typeof(int)),
                new Tuple<string, Type>( "exppoint", typeof(short)),
            };
        }
    }
}
