﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class JobDropData : ReadViewData
    {
        public int id;
        public byte job;
        public short level_start;
        public short level_end;
        public int drop_list_id1;
        public int drop_list_id2;
        public int drop_list_id3;
        public int drop_list_id4;
        public int drop_list_id5;
        public int drop_weight1;
        public int drop_weight2;
        public int drop_weight3;
        public int drop_weight4;
        public int drop_weight5;
        public int gender;

        public int Total_Weight
        {
            get
            {
                return drop_weight1 + drop_weight2 + drop_weight3 + drop_weight4;
            }
        }

        public int[] Drop_list_id
        {
            get
            {
                return new int[]
                {
                    drop_list_id1,
                    drop_list_id2,
                    drop_list_id3,
                    drop_list_id4,
                    drop_list_id5,
                };
            }
        }

        public override int ID { get => id; }
        public override string NAME { get => string.Format("{0} {1}~{2}", (JobBaseType)job, level_start, level_end); }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "job", typeof(byte)),
                new Tuple<string, Type>( "level_start", typeof(short)),
                new Tuple<string, Type>( "level_end", typeof(short)),
                new Tuple<string, Type>( "drop_list_id1", typeof(int)),
                new Tuple<string, Type>( "drop_list_id2", typeof(int)),
                new Tuple<string, Type>( "drop_list_id3", typeof(int)),
                new Tuple<string, Type>( "drop_list_id4", typeof(int)),
                new Tuple<string, Type>( "drop_weight1", typeof(int)),
                new Tuple<string, Type>( "drop_weight2", typeof(int)),
                new Tuple<string, Type>( "drop_weight3", typeof(int)),
                new Tuple<string, Type>( "drop_weight4", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            byte job = row.Field<byte>("job");
            short level_start = row.Field<short>("level_start");
            short level_end = row.Field<short>("level_end");
            
            return list.Find(v => 
            {
                JobDropData jobDropData = v as JobDropData;
                return jobDropData.id == id && jobDropData.job == job &&
                       jobDropData.level_start == level_start && jobDropData.level_end == level_end;
            });
        }
    }
}
