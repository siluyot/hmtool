﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class MonsterDropListData : ReadViewData
    {
        public int drop_list_id;
        public string desc;
        public byte object_type;
        public int object_id;
        public int weight;
        public short no_drop_level;

        public string ObjectType;

        public override int ID { get => drop_list_id; }
        public override string NAME { get => desc; }

        public override Tuple<string, Type>[] GetColumns()
        {
            ObjectType = ((DropType)object_type).ToString();

            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "drop_list_id", typeof(int)),
                new Tuple<string, Type>( "desc", typeof(string)),
                new Tuple<string, Type>( "ObjectType", typeof(string)),
                new Tuple<string, Type>( "object_id", typeof(int)),
                new Tuple<string, Type>( "weight", typeof(int)),
                new Tuple<string, Type>( "no_drop_level", typeof(short)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("drop_list_id");
            string name = row.Field<string>("desc");

            return list.Find(v =>
            {
                MonsterDropListData monsterDropListData = v as MonsterDropListData;
                return v.ID == id && v.NAME == name;
            });
        }
    }
}
