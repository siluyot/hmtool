﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class ItemRandOptionData : ReadViewData
    {
        public int item_rand_option_id;
        public int option_id;
        public int option_fix;
        public int option_fix_count;

        public override int ID { get => item_rand_option_id; }
        public override string NAME { get => string.Format("{0} {1}", option_id, option_fix); }
        
        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "item_rand_option_id", typeof(int)),
                new Tuple<string, Type>( "option_id", typeof(int)),
                new Tuple<string, Type>( "option_fix", typeof(int)),
                new Tuple<string, Type>( "option_fix_count", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("item_rand_option_id");
            return list.Find(v => v.ID == id);
        }
    }
}
