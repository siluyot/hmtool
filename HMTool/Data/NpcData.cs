﻿using HMTool.Data.Base;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class NpcData : ReadViewData
    {
        public int No;
        public string Npcname;
        public int NpcName_id;

        /// <summary>
        /// 초기 대화 ID
        /// </summary>
        public int BaseTalkID;
        /// <summary>
        /// 상세 대화 ID
        /// </summary>
        public int MainTalkID;
        /// <summary>
        /// 퀘스트 대화 ID
        /// </summary>
        public int QuestTalkID;

        public int EventType1; 
        public int TypeID1;

        public int EventType2;   
        public int TypeID2;

        public int EventType3;     
        public int TypeID3;

        public int EventType4;    
        public int TypeID4;

        public string icon;
        public int sound_id;
        
        public NPCEventType NPCEventType1
        {
            get { return (NPCEventType)EventType1; }
        }

        public NPCEventType NPCEventType2
        {
            get { return (NPCEventType)EventType2; }
        }

        public NPCEventType NPCEventType3
        {
            get { return (NPCEventType)EventType3; }
        }

        public NPCEventType NPCEventType4
        {
            get { return (NPCEventType)EventType4; }
        }

        public override int ID { get => No; }
        public override string NAME { get => Npcname; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "No", typeof(int)),
                new Tuple<string, Type>( "Npcname", typeof(string)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("No");
            return list.Find(v => v.ID == id);
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, ID, NAME),
            };
        }
    }
}
