﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class OptionData : ReadViewData
    {
        public int option_id;
        public string desc;
        public int desc_id;
        public byte option_fix;
        public short option_type;
        public int option_default_value;
        public short option_rand_value;
        public short option_value_rate;
        public int option_value_extra;
        public byte option_unit;
        public byte buff_exercise_type;
        public int buff_prob;
        public int pet_equip_weight;
        
        public OptionType OptionType;
        public BuffExeType BuffExeType;
        public float option_power_value;

        public override int ID => option_id;
        public override string NAME => desc;

        public void Init()
        {
            this.OptionType = (OptionType)option_type;
            this.BuffExeType = (BuffExeType)buff_exercise_type; 
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
          {
                new Tuple<string, Type>( "option_id", typeof(int)),
                new Tuple<string, Type>( "desc", typeof(string)),
                new Tuple<string, Type>( "OptionType", typeof(string)),
                new Tuple<string, Type>( "option_default_value", typeof(int)),
                new Tuple<string, Type>( "option_rand_value", typeof(short)),
                new Tuple<string, Type>( "option_value_rate", typeof(short)),
                new Tuple<string, Type>( "option_value_extra", typeof(int)),                
          };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("option_id");
            return list.Find(v => v.ID == id);
        }        
    }
}
