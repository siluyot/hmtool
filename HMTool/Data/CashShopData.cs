﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class CashShopData : ReadViewData
    {
        public int id;
        public string title;
        public string content;
        public int desc_id;
        public int goods_explain_id;
        public byte gender;
        public byte job;
        public byte goods_type;
        public byte shop_tab;
        public byte tab_order;
        public byte pay_type;
        public int pay;
        public int mileage;
        public int add_currency;
        public byte limit_count;
        public byte limit_day_type;
        public byte limit_id;
        public byte sale;
        public byte state;
        public int drop_list_id;
        public int drop_value;
        public string timeout;
        public int price;
        public float local_price;
        public float usd_price;
        public string dia_shop_code;
        public string google_product_id;
        public string apple_product_id;
        public string tstore_product_id;
        public byte visable;
        public byte first_day_type;
        public byte first_pos;
        public short first_rate;
        public byte packageBuyType;
        public int duration_day;
        public int realcash_goods_guroup_id;
        public string icon;
        public byte bonus_object_type;
        public int bonus_drop_id;
        public int bonus_object_size;
        public int event_drop_list_id;
        public int event_drop_value;
        public int pay_back_event_value;

        public DateTime Timeout { get { return DateTime.Parse(timeout); } }
        public GoodsTypes GoodsType { get { return (GoodsTypes)goods_type; } }
        
        public override int ID { get => id; }
        public override string NAME { get => title; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "title", typeof(string)),
                new Tuple<string, Type>( "content", typeof(string)),
                new Tuple<string, Type>( "job", typeof(byte)),
                new Tuple<string, Type>( "pay", typeof(int)),
                new Tuple<string, Type>( "price", typeof(int)),
                new Tuple<string, Type>( "drop_list_id", typeof(int)),
                new Tuple<string, Type>( "drop_value", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }

        public bool IsGachaData()
        {
            switch (GoodsType)
            {
                case GoodsTypes.PET_1:
                case GoodsTypes.PET_10_1:
                case GoodsTypes.PET_10_BUDDY:
                case GoodsTypes.ITEM_1:
                case GoodsTypes.RUNE_PUZZLE_1:
                case GoodsTypes.ITEM_10_1:
                case GoodsTypes.PET_1_JOB:
                case GoodsTypes.PET_10_1_JOB:
                case GoodsTypes.COSTUME_ITEM_1:
                case GoodsTypes.COSTUME_ITEM_10:
                case GoodsTypes.COSTUME_ITEM_10_1:
                case GoodsTypes.FREE_PRIMIUM_PET:
                case GoodsTypes.FREE_PRIMIUM_ITEM:
                case GoodsTypes.FREE_PET:
                case GoodsTypes.FREE_ITEM:
                    return true;
            }
            return false;
        }
    }
}
