﻿using HMTool.Latale;
using Sfs2X.Entities.Data;

namespace HMTool.Data
{
    public class GiftData
    {
        public long seq;
        public int uid;
        public int whois_id;
        public int contents_id;
        public byte gift_type;
        public int gift;
        public byte gift_option;
        public int item_count;
        public long insert_dt;
        public int lid;

        public string contents;
        public string senderName;

        public GiftData(ISFSObject sfsObject)
        {
            seq = sfsObject.GetLong("1");
            uid = sfsObject.GetInt("2");
            whois_id = sfsObject.GetInt("3");
            contents_id = sfsObject.GetInt("4");
            gift_type = sfsObject.GetByte("5");
            gift = sfsObject.GetInt("6");
            gift_option = sfsObject.GetByte("7");
            item_count = sfsObject.GetInt("8");
            insert_dt = sfsObject.GetLong("9");
            lid = sfsObject.GetInt("10");

            contents = contents_id == 0 ? sfsObject.GetUtfString("0") : contents_id.ToString();
            senderName = whois_id == 0 ? sfsObject.GetUtfString("11") : senderName = whois_id.ToString();
        }
    }

    public class BuddyPointGiftData
    {
        public long seq;
        public int uid;
        public int contents_id;
        public string senderName;
        public int item_count;
        public long insert_dt;
        public byte gift_type;

        public BuddyPointGiftData(ISFSObject sfsObject)
        {
            this.seq = sfsObject.GetLong("1");
            this.uid = sfsObject.GetInt("2");
            this.contents_id = sfsObject.GetInt("3");
            this.senderName = sfsObject.GetUtfString("4");
            this.item_count = sfsObject.GetShort("5");
            this.insert_dt = sfsObject.GetLong("6");
            this.gift_type = GiftType.BUDDY_POINT.Key;
        }
    }

    public class TicketGiftData
    {
        public long seq;
        public int uid;
        public int whois_id;
        public int contents_id;
        public byte gift_type;
        public int gift;
        public string contents;
        public long insert_dt;
        public string senderName;

        public TicketGiftData(ISFSObject sfsObject)
        {
            this.seq = sfsObject.GetLong("1");
            this.uid = sfsObject.GetInt("2");
            this.whois_id = sfsObject.GetInt("3");
            this.contents_id = sfsObject.GetInt("4");
            this.gift_type = sfsObject.GetByte("5");
            this.gift = sfsObject.GetInt("6");
            this.contents = sfsObject.GetUtfString("0");
            this.insert_dt = sfsObject.GetLong("9");

            contents = contents_id == 0 ? sfsObject.GetUtfString("0") : contents_id.ToString();
            senderName = whois_id == 0 ? sfsObject.GetUtfString("11") : senderName = whois_id.ToString();
        }
    }
}
