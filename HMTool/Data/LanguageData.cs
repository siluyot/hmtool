﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class LanguageData : ReadViewData
    {
        public int id;
        public string korean;
        public string english;
        public string taiwan;
        public string thailand;
        public string china;
        public string japanese;        
        public int category;

        public override int ID { get => id; }
        public override string NAME { get => korean.ToString(); }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "korean", typeof(string)),
                new Tuple<string, Type>( "english", typeof(string)),
                new Tuple<string, Type>( "taiwan", typeof(string)),
                new Tuple<string, Type>( "thailand", typeof(string)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }
    }
}
