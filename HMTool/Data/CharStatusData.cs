﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class CharStatusData : ReadViewData
    {
        public byte job;
        public short level;
        public int hp;
        public int energy;
        public int str;
        public int inte;
        public int dex;
        public int luck;
        public int accuracy;
        public int dodge;
        public int cri_per;
        public int cri_damage_per;
        public int skill_point;
        public int skill_up_point;
        public int exppoint;
        public int rest_max_exp;
        public int rest_max_elly;
        public int total_power;
        public int base_hp_potion;

        public override int ID { get => job; }
        public override string NAME { get => string.Format("{0} {1}", (JobBaseType)job, level); }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
           {
                new Tuple<string, Type>( "job",  typeof(string) ),
                new Tuple<string, Type>( "level", typeof(int)),
                new Tuple<string, Type>( "exppoint", typeof(int)),
                new Tuple<string, Type>( "total_power", typeof(int)),
           };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int job = int.Parse(row.Field<string>("job"));
            int level = row.Field<int>("level");

            return list.Find(v =>
            {
                CharStatusData charStatusData = v as CharStatusData;
                return charStatusData.job == job && charStatusData.level == level;
            });
        }
    }
}
