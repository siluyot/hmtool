﻿using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView;
using HMTool.WindowView.ViewModel;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HMTool.Data
{
    public class CharPetData : ViewData
    {
        public long seq;
        public int uid;
        public int owner_cid;
        public short pet_id;
        public short Pet_Exp;
        public short skill_id;
        public short Skill_Exp;
        public byte Skill_Dmg_Value;    
        public byte Attr_Stone;
        public short Item_Id;
        public short Item_Attack_Value;
        public short Item_Option_Index;
        public byte Item_Value;
        public byte flag;
        public byte bInsertFlag;
        public int cnt;
        public bool bUpdate;
        public PetBookData settingPet;
        public bool isModify = false;

        //public string name;
        public int grade;
        public int PetLevel = 1;
        public int PetSkillLevel = 1;

        public override int ID => pet_id;
        public override string NAME => settingPet?.pet_name;
        
        public void SetUserPetInvenData(ISFSObject sfsObj)
        {
            this.seq = sfsObj.GetLong("1");
            this.owner_cid = sfsObj.GetInt("2");
            this.pet_id = sfsObj.GetShort("3");
            this.skill_id = sfsObj.GetShort("4");
            this.Skill_Exp = sfsObj.GetShort("5");
            this.Pet_Exp = sfsObj.GetShort("6");
            this.Attr_Stone = sfsObj.GetByte("7");

            this.Item_Id = sfsObj.GetShort("m");
            this.Item_Option_Index = sfsObj.GetShort("j");
            this.Item_Value = sfsObj.GetByte("k");
            this.Item_Attack_Value = sfsObj.GetShort("l");

            this.Skill_Dmg_Value = sfsObj.GetByte("s");
            this.cnt = sfsObj.GetInt("c");

            this.flag = sfsObj.GetByte("f");

            this.settingPet = DataBaseManager.Instance.GetDataBase(DataBaseType.PET_BOOK).GetData(pet_id) as PetBookData;
            if (settingPet != null)
            {
                this.name = settingPet.pet_name;
                this.grade = settingPet.pet_grade;

                var expDatabase = DataBaseManager.Instance.GetDataBase(DataBaseType.PET_EXP) as PetExpDataBase;
                var level = expDatabase.GetPetLevelByExp(Pet_Exp, settingPet.pet_exp_id);
                this.PetLevel = level;

                var skillDatabase = DataBaseManager.Instance.GetDataBase(DataBaseType.SKILL) as SkillDataBase;
                var skill = skillDatabase.GetData(skill_id) as SkillData;
                this.PetSkillLevel = skill.skill_level;
            }
            else this.name = "존재하지 않는 데이터";
        }

        public void ApplyChange()
        {
            var expDatabase = DataBaseManager.Instance.GetDataBase(DataBaseType.PET_EXP) as PetExpDataBase;

            var findData = expDatabase.GetPetExpData(PetLevel, settingPet.pet_exp_id);
            if (findData == null)
            {
                var findList = expDatabase.GetDataList().FindAll(v =>
                {
                    PetExpData data = v as PetExpData;
                    return data.pet_group_id == settingPet.pet_exp_id;
                });
                var lastData = findList.Last() as PetExpData;

                this.Pet_Exp = lastData.exppoint;
                this.PetLevel = lastData.pet_level;
            }
            else this.Pet_Exp = findData.exppoint;

            var skillDatabase = DataBaseManager.Instance.GetDataBase(DataBaseType.SKILL) as SkillDataBase;

            var findSkill = skillDatabase.GetData(skill_id) as SkillData;
            if (findSkill != null)
            {
                SkillData firstSkill = null;

                int skillGroup_id = findSkill.skill_group;
                if (skillGroup_id != 0)
                {
                    var findGroupList = skillDatabase.GetDataList().FindAll(v => (v as SkillData).skill_group == skillGroup_id);
                    firstSkill = findGroupList.First() as SkillData;
                }

                var curSkill = firstSkill;
                if (curSkill != null)
                {
                    for (int i = 0; i < PetSkillLevel; i++)
                    {
                        if (curSkill.next_upgrade_skill == 0)
                            break;

                        curSkill = skillDatabase.GetData(curSkill.next_upgrade_skill) as SkillData;
                    }

                    this.skill_id = (short)curSkill.skill_id;
                    this.PetSkillLevel = curSkill.skill_level;
                }
            }

            //지엠툴에서 최대로 127까지만 넣어줌
            if (Skill_Dmg_Value > 127) Skill_Dmg_Value = 127;
        }

        public ISFSObject ToSFSObject()
        {
            ISFSObject data = SFSObject.NewInstance();
            data.PutLong("1", this.seq);
            data.PutInt("2", this.owner_cid);
            data.PutShort("3", this.pet_id);
            data.PutShort("4", this.skill_id);
            data.PutShort("5", this.Skill_Exp);
            data.PutShort("6", this.Pet_Exp);
            data.PutByte("7", this.Attr_Stone);
            data.PutShort("m", this.Item_Id);
            data.PutShort("j", this.Item_Option_Index);
            data.PutByte("k", this.Item_Value);
            data.PutShort("l", this.Item_Attack_Value);

            data.PutByte("s", this.Skill_Dmg_Value);
            data.PutInt("c", this.cnt);

            data.PutByte("f", this.flag);

            return data;
        }
        
        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, settingPet.ID, settingPet.NAME, settingPet.pet_grade),
            };
        }

        public override Tuple<string, Type>[] GetColumns()
        {           
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "seq", typeof(long)),
                new Tuple<string, Type>( "pet_id", typeof(short)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "grade", typeof(int)),
                new Tuple<string, Type>( "PetLevel", typeof(int)),
                new Tuple<string, Type>( "Pet_Exp", typeof(short)),
                new Tuple<string, Type>( "skill_id", typeof(short)),
                new Tuple<string, Type>( "PetSkillLevel", typeof(int)),
                new Tuple<string, Type>( "Skill_Exp", typeof(short)),
                new Tuple<string, Type>( "Skill_Dmg_Value", typeof(byte)),
                new Tuple<string, Type>( "Attr_Stone", typeof(byte)),
                new Tuple<string, Type>( "Item_Id", typeof(short)),
                new Tuple<string, Type>( "Item_Attack_Value", typeof(short)),
                new Tuple<string, Type>( "Item_Option_Index", typeof(short)),
                new Tuple<string, Type>( "Item_Value", typeof(byte)),
                new Tuple<string, Type>( "owner_cid", typeof(int)),                
            };
        }

        public Tuple<string, TemplateBox>[] GetEditColumns()
        {
            int maxLevel = 80;
            object[] petLevel = new object[maxLevel];
            for (int i = 0; i < maxLevel; i++)
            {
                petLevel[i] = i + 1;
            }

            int maxSkillLevel = 40;
            object[] petSkillLevel = new object[maxSkillLevel];
            for (int i = 0; i < maxSkillLevel; i++)
            {
                petSkillLevel[i] = i + 1;
            }

            return new Tuple<string, TemplateBox>[]
            {
                new Tuple<string, TemplateBox>( "PetLevel",            new ComboTemplateBox(petLevel)),                
                new Tuple<string, TemplateBox>( "PetSkillLevel",            new ComboTemplateBox(petSkillLevel)),                
                new Tuple<string, TemplateBox>( "Skill_Dmg_Value",      new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Attr_Stone",           new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Item_Id",              new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Item_Attack_Value",    new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Item_Option_Index",    new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Item_Value",           new TextTemplateBox()),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            long seq = row.Field<long>("seq");
            return list.Find(v => (v as CharPetData).seq == seq);
        }
    }
}
