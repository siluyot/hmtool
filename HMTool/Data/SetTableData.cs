﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class SetTableData : ReadViewData
    {
        public int set_id;
        public string set_name;
        public int set_name_id;
        public int set_type;
        public int set_count;
        public int set_option_index;
        public int set_option_value;
        public int set_motion_id;
        public string set_motion_file;
        public int event_field_type;
        public int event_field_id;
        public int event_desc;
        public int event_desc_id;
        public int event_clear_type;
        public int event_clear_value;
        public int event_drop_list_id;

        public override int ID { get => set_id; }
        public override string NAME { get => set_name; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "set_id", typeof(int)),
                new Tuple<string, Type>( "set_name", typeof(string)),
                new Tuple<string, Type>( "set_type", typeof(int)),
                new Tuple<string, Type>( "set_count", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("set_id");
            return list.Find(v => v.ID == id);
        }
    }
}
