﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class MonsterDropData : ReadViewData
    {
        public int drop_id;
        public string desc;
        public int object_type;
        public int drop_list_id;
        public int weight;
        public int no_drop_level;

        public string ObjectType;

        public override int ID { get => drop_id; }
        public override string NAME { get => desc; }


        public override Tuple<string, Type>[] GetColumns()
        {
            ObjectType = ((DropType)object_type).ToString();

            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "drop_id", typeof(int)),
                new Tuple<string, Type>( "desc", typeof(string)),
                new Tuple<string, Type>( "ObjectType", typeof(string)),
                new Tuple<string, Type>( "drop_list_id", typeof(int)),                
                new Tuple<string, Type>( "weight", typeof(int)),
                new Tuple<string, Type>( "no_drop_level", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("drop_id");
            string name = row.Field<string>("desc");

            return list.Find(v =>
            {
                MonsterDropData monsterDropListData = v as MonsterDropData;
                return v.ID == id && v.NAME == name;
            });
        }
    }
}
