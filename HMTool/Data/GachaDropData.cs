﻿using HMTool.Data.Base;
using HMTool.Latale;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class GachaDropData : ViewData
    {
        //public int id;
        //public string name;
        public int dropId;
        public int additionalDropId1;
        public int additionalDropId2;
        public bool jobDrop;
        public int order;

        public GachaDropData() { }

        public GachaDropData(CashShopData cashShopData)
        {
            //지엠툴의 jobDrop = true; 스위치문 그대로 가져옴
            if (IsJobDrop(cashShopData))
                jobDrop = true;
            
            id = cashShopData.id;
            name = cashShopData.title;
            order = cashShopData.shop_tab;

            dropId = cashShopData.drop_list_id;
            additionalDropId1 = cashShopData.drop_value;
        }

        public override int ID { get => id; }
        public override string NAME { get => name; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "dropId", typeof(int)),
                new Tuple<string, Type>( "additionalDropId1", typeof(int)),
                new Tuple<string, Type>( "additionalDropId2", typeof(int)),
                new Tuple<string, Type>( "jobDrop", typeof(bool)),
                new Tuple<string, Type>( "order", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }

        public bool IsJobDrop(CashShopData cashShopData)
        {
            switch (cashShopData.GoodsType)
            {
                case GoodsTypes.CASH_POP_SHOP:
                case GoodsTypes.ITEM_1:
                case GoodsTypes.PACKAGE:
                case GoodsTypes.PACKAGE_GOODS:
                case GoodsTypes.PET_10_1:
                case GoodsTypes.PET_10_1_JOB:
                case GoodsTypes.PET_10_BUDDY:
                case GoodsTypes.POTION:
                case GoodsTypes.RUNE_PUZZLE_1:
                case GoodsTypes.SOME_ITEM:
                    return true;
            }
            return false;
        }
    }
}
