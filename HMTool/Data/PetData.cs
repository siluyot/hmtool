﻿using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class PetBookData : ReadViewData, IMailData
    {
        public int pet_id;
        public short pet_book_id;
        public string pet_name;
        public int pet_name_id;
        public int pet_group_id;
        public byte pet_job;
        public short pet_grade;
        public int effect_group_id;
        public string bundle_group_id;
        public int sound_group_id;
        //private int pet_memory_index;
        public int monster_id;
        public short pet_size;
        public byte max_lv;
        public short skill_id;
        public byte default_attr;
        public int default_attack;
        public int default_attack_max;
        public int default_defen;
        public int default_defen_max;
        public byte weapon_type;
        public byte weapon_type_add_type;
        public int weapon_type_add_dmg;
        public int weapon_type_add_dmg_power;
        public byte weapon_type_1;
        public int weapon_type_add_dmg_1;
        public int weapon_type_add_dmg_power_1;
        public int weapon_custom;
        public byte monster_type;
        public byte monster_type_add_type;
        public int monster_type_add_dmg;
        public short option_index1;
        public short option_index2;
        public byte rune_default_count;
        public byte rune_rand_count;
        public byte rune_unlock_count;
        public byte rune_prob1;
        public byte rune_prob2_over;
        public int next_combo_pet_id;
        public int next_combo_damage;
        public short next_combo_option;
        public int upgrade_pet_id;
        public int evolve_pet_id1;
        public int evolve_pet_id2;
        public int evolve_pet_id3;
        public int evolve_pet_id4;
        public int evolve_pet_id5;
        public int drop_id;
        public int sell_gold;
        public int upgrade_exp;
        public int pet_exp;
        public int pet_exp_id;
        public byte not_consumer;
        public int set_table_id;
        public byte visable_flag;
        public int add_power;
        public byte remain_log;
        public int m_group_id;
        public int m_min_pay;
        public int m_max_pay;
        public int drop_map_index1;
        public int drop_map_index2;
        public int drop_map_index3;
        public int drop_map_index4;
        public int drop_map_index5;

        public override int ID { get => pet_id; }
        public override string NAME { get => pet_name; }

        public GiftType GiftType { get => GiftType.PET; }
        public int MaxCount { get => 1; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "pet_id", typeof(int)),
                new Tuple<string, Type>( "pet_name", typeof(string)),
                new Tuple<string, Type>( "pet_grade", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("pet_id");
            return list.Find(v => v.ID == id);
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, ID, NAME, pet_grade),
            };
        }
    }
}
