﻿using HMTool.Data.Base;
using Sfs2X.Entities.Data;
using System.Text;

namespace HMTool.Data
{
    public class FindUserData
    {
        public enum LOGINTYPE
        {
            INPUT = 1,
            LINE,
            FACEBOOK,
            GOOGLE_PLUS,
            GAME_CENTER,
            EMAIL,
            GAMEDREAMER,
            GUEST_LOGIN
        }

        public int uid;
        public string searchName;
        public LOGINTYPE login_Type;

        public string Account_Key { get; set; }
        public string Account_Pass { get; set; }

        public string Account_ID { get; set; }
        public string Email_Password { get; set; }

        public int dia_free;
        public int dia;
        public int connectedServer;
        public byte tutorialState;

        public FindCharacter[] findCharacters = null;
        public FindCharacter currentCharacter = null;

        public FindUserData(SFSObject resObj)
        {
            string key_searchName = "1";
            string key_userInfo = "2";
            string key_charList = "3";

            searchName = resObj.GetUtfString(key_searchName);

            var userInfo = resObj.GetSFSObject(key_userInfo);
            if (userInfo != null)
            {
                uid = userInfo.GetInt("1");
                Account_Key = userInfo.GetUtfString("2");
                Account_Pass = userInfo.GetUtfString("19");
                dia_free = userInfo.GetInt("3");
                dia = userInfo.GetInt("4");

                connectedServer = userInfo.GetInt("11");

                if (userInfo.ContainsKey("29"))
                {
                    ISFSArray sfsArray = userInfo.GetSFSArray("29");
                    if (sfsArray.Count > 0)
                    {
                        var authObj = sfsArray.GetSFSObject(0);

                        Account_ID = authObj.GetUtfString("1");
                        login_Type = (LOGINTYPE)authObj.GetByte("2");
                        //accountKey = authObj.GetUtfString("3");
                        if (authObj.ContainsKey("4"))
                        {
                            Email_Password = authObj.GetUtfString("4");
                        }
                    }
                }

                tutorialState = userInfo.GetByte("30");
            }

            var charList = resObj.GetSFSArray(key_charList);
            if (charList != null)
            {
                StringBuilder sb = new StringBuilder();
                findCharacters = new FindCharacter[charList.Count];

                for (int i = 0; i < charList.Count; i++)
                {
                    ReadData data = new FindCharacter();
                    FindCharacter findCharacter = data.GetReadData<FindCharacter>(charList.GetSFSObject(i), sb);

                    findCharacters[i] = findCharacter;

                    if (searchName == findCharacter.name)
                        currentCharacter = findCharacter;
                }

                if (sb.ToString() != string.Empty)
                    CustomMessageBox.Show(sb.ToString());
            }


            /* SFSObject data = SFSObject.newInstance();
    data.putInt("1", this.uid);
    data.putUtfString("2", this.account_key);
    data.putUtfString("19", this.pass);
    data.putInt("3", this.dia_free);
    data.putInt("4", this.dia);
    data.putInt("5", this.buddy_point);
    data.putByte("6", this.store_type.getValue());
    data.putByte("7", this.slot_size);
    data.putShort("8", this.storage_size);
    data.putLong("9", this.insert_dt.getTime());
    data.putLong("10", this.login_dt.getTime());
    data.putInt("11", this.connectedServer);
    data.putInt("12", this.connectedCid);
    if (this.block_dt != null)
    {
      data.putLong("13", this.block_dt.getTime());
      data.putUtfString("14", this.block_reason);
      data.putUtfString("15", this.block_remainTime);
    }
    data.putInt("m", this.mileage);
    data.putInt("h", this.honor_point);
    data.putInt("16", this.infi_point);
    data.putByte("17", this.grade);
    data.putInt("18", this.connect_time);
    
    data.putInt("20", this.connect_day_dt);
    data.putInt("21", this.day_connect_time);
    data.putInt("22", this.reward_connect_time);
    data.putByte("23", this.connect_pos);
    data.putInt("24", this.connect_cycle_cnt);
    data.putByte("25", this.dia_dice_cnt);
    data.putShort("26", this.rest_point);
    data.putShort("27", this.rest_day_limit);
    data.putInt("28", this.linked_login);
    if (this.authData != null)
    {
      SFSArray sfsArray = new SFSArray();
      for (AuthData r : this.authData) {
        sfsArray.addSFSObject(r.getSFS());
      }
      data.putSFSArray("29", sfsArray);
    }
    data.putByte("30", this.tutorial_state);
    data.putInt("rc", this.real_cash);
    
    data.putLong("cut", this.connectedUpdateTime);
    if (this.everyGoods != null) {
      data.putSFSObject("ev", this.everyGoods.toSFS());
    }
    data.putShort("31", this.infi_floor);
    if (this.create_ip != null) {
      data.putUtfString("32", this.create_ip);
    } else {
      data.putUtfString("32", "");
    }*/
        }
    }

    public class FindCharacter : ReadData
    {
        public int uid;
        public int cid;
        public string name;
        public byte deleted;

        public override int ID => uid;
        public override string NAME => name;
    }

    public class CharDetailInfo
    {
        public int cid;
        public int uid;
        public string name;
        public byte job;
        public byte gender;
        public short level;
        public int exppoint;
        public int gold;
        public short inven_count;
        public short title_id;
        public int honor_point;
        public int hp_potion;
        public long hp_potion_dt;
        public int tutorial_finish;
        public byte deleted;
        public long deleted_dt;
        public int quest_main_index;
        public int quest_main_state;
        public int map_index;
        public int day_dungeon_count;
        public int guild_id;
        public int job_exp;
        public int collect_point;
        public int exp_gold_dungeon_cnt;
        public int hot_time;
        public int power_score_rank;
        public int power_score;
        public int skill_point;
        public int skill_add_point;
        public short pay_main_quest;
        public short pay_level;
        public short pay_package;

        public CharDetailInfo(ISFSObject sfsObject)
        {
            cid = sfsObject.GetInt("1");
            uid = sfsObject.GetInt("2");
            name = sfsObject.GetUtfString("3");
            job = sfsObject.GetByte("4");
            gender = sfsObject.GetByte("5");
            level = sfsObject.GetShort("6");
            exppoint = sfsObject.GetInt("7");
            gold = sfsObject.GetInt("8");
            inven_count = sfsObject.GetShort("9");

            honor_point = sfsObject.GetInt("12");
            hp_potion = sfsObject.GetInt("13");
            hp_potion_dt = sfsObject.GetLong("14");
            deleted_dt = sfsObject.GetLong("15");
            tutorial_finish = sfsObject.GetInt("16");
            quest_main_index = sfsObject.GetInt("17");
            quest_main_state = sfsObject.GetInt("18");
            map_index = sfsObject.GetInt("19");
            day_dungeon_count = sfsObject.GetInt("20");
            guild_id = sfsObject.GetInt("21");
            job_exp = sfsObject.GetInt("22");
            collect_point = sfsObject.GetInt("23");
            exp_gold_dungeon_cnt = sfsObject.GetInt("24");
            hot_time = sfsObject.GetInt("25");
            power_score_rank = sfsObject.GetInt("26");
            power_score = sfsObject.GetInt("27");
            title_id = sfsObject.GetShort("28");
            skill_point = sfsObject.GetInt("29");
            skill_add_point = sfsObject.GetInt("30");
            pay_main_quest = sfsObject.GetShort("31");
            pay_level = sfsObject.GetShort("32");

            if (sfsObject.ContainsKey("33"))
            {
                pay_package = sfsObject.GetShort("33");
            }
        }
    }
}
