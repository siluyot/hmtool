﻿using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class MakeData : ReadViewData
    {
        public int make_id;
        public string make_name;
        public int make_name_id;
        public int make_type;
        public int quest_complete_id;
        public int npc_id;
        public short level_limit;
        public byte job_limit;
        public byte time_type;
        public MakeTimeType TimeType
        {
            get { return (MakeTimeType)time_type; }
        }
        public string start_time;
        public string end_time;
        public int limit_count;
        public int limit_id;
        public int consume_gold;
        public int consume_mileage;

        public byte package_type_0;
        public int item_id_0;
        public int item_size_0;

        public byte package_type_1;
        public int item_id_1;
        public int item_size_1;

        public byte package_type_2;
        public int item_id_2;
        public int item_size_2;

        public byte package_type_3;
        public int item_id_3;
        public int item_size_3;

        public byte package_type_4;
        public int item_id_4;
        public int item_size_4;
        
        public int furni_item_id;
        public byte object_type;
        public int object_id;
        public int object_size;
        public byte tab;
        public byte deleted;
        public int line_reward_count;

        public MakeMaterialData[] MakeMaterials { get; set; }

        public override int ID { get => make_id; }
        public override string NAME { get => make_name; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "make_id", typeof(int)),
                new Tuple<string, Type>( "make_name", typeof(string)),
                new Tuple<string, Type>( "npc_id", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("make_id");
            return list.Find(v => v.ID == id);
        }

        private Tuple<ItemData, int>[] MakeDataMaterialToItems(MakeData makeData)
        {
            //제작을 하기 위한 재료아이템들 + 요구 수량
            List<Tuple<ItemData, int>> itemTuple = new List<Tuple<ItemData, int>>();

            var materials = makeData.MakeMaterials;
            for (int i = 0; i < materials.Length; i++)
            {
                var material = materials[i];

                if (material != null)
                {
                    if (material.IsGroup)
                    {
                        for (int j = 0; j < material.itemIDs.Length; j++)
                        {
                            var itemID = material.itemIDs[j];
                            var findItem = FindItemData(itemID);

                            itemTuple.Add(new Tuple<ItemData, int>(findItem as ItemData, material.requiredCount));
                        }
                    }
                    else
                    {
                        var itemID = material.itemID;
                        var findItem = FindItemData(itemID);

                        itemTuple.Add(new Tuple<ItemData, int>(findItem as ItemData, material.requiredCount));
                    }
                }
            }
            return itemTuple.ToArray();
        }

        private ItemData FindItemData(int id)
        {
            var findItem = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM).GetData(id);
            return findItem as ItemData;
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            var dataList = new List<DataBoxItem>
            {
                //자기자신(제작데이터) 추가
                new DataBoxItem(this, ID, NAME),
            };

            //이후 재료데이터 추가
            var materials = MakeDataMaterialToItems(this);
            foreach (var material in materials)
            {
                var item = material.Item1;
                var count = material.Item2;

                string newName = string.Format("[{0} {1} 재료] {2}", make_id, make_name, item.NAME);
                
                var newMaterialData = new DataBoxItem(item, item.item_id, newName, item.item_grade, count);
                dataList.Add(newMaterialData);
            }

            return dataList.ToArray();
        }
    }

    public class MakeMaterialData
    {
        public readonly bool IsGroup;
        public readonly int itemID;
        public readonly int[] itemIDs;
        public readonly int requiredCount;
        public readonly int groupImageID;
        public readonly int nameID;
        //패키지 상품일때 타입
        public readonly GiftType giftType;

        public MakeMaterialData(int itemID, int requiredCount, int nameID, GiftType giftType)
        {
            IsGroup = false;
            this.itemID = itemID;
            this.requiredCount = requiredCount;
            groupImageID = 0;
            this.nameID = nameID;
            this.giftType = giftType;
        }

        public MakeMaterialData(int[] itemIDs, int requiredCount, int groupImageID, int nameID)
        {
            IsGroup = true;
            this.itemIDs = itemIDs;
            this.requiredCount = requiredCount;
            this.groupImageID = groupImageID;
            this.nameID = nameID;
            this.giftType = GiftType.NONE;
        }
    }

    public class MaterialInfo
    {
        public int itemID;
        public int size;
        public int package_type;
        //public int package_id;

        public bool IsColumnValid { get { return itemID > 0; } }
        public bool UseGroupMaterialPool { get { return package_type == 1; } }

        public MaterialInfo(int itemID, int size, int package_type)
        {
            this.itemID = itemID;
            this.size = size;
            this.package_type = package_type;
        }
    }
}
