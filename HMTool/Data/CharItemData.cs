﻿using HMTool.Custom;
using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView;
using HMTool.WindowView.ViewModel;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class CharItemData : ViewData
    {
        public long item_no;
        public int cid;
        public int uid;
        public int item_id;
        public short size;
        public ItemPos item_pos;
        public short Item_Exppoint;

        [BaseDetail(DataBaseType.ITEM, "puzzle")]
        public short puzzle;

        [BaseDetail(DataBaseType.OPTION_DATA, "Option_Index")]
        public short Option_Index1;
        public byte value1;
        [BaseDetail(DataBaseType.OPTION_DATA, "Option_Index")]
        public short Option_Index2;
        public byte value2;
        [BaseDetail(DataBaseType.OPTION_DATA, "Option_Index")]
        public short Option_Index3;
        public byte value3;
        [BaseDetail(DataBaseType.OPTION_DATA, "Option_Index")]
        public short Option_Index4;
        public byte value4;

        public byte Grinding_Add;
        public byte Grinding_Cnt;
        public byte flag;
        public long insert_dt;
        public ItemType itemType;
        public ItemData settingItem;
        public int iUpdateSize;
        public byte byUpdateType;
        public byte bInsertFlag;

        public string pos;
        public string type;
        public byte grade;
        //public string name;
        public short ItemLevel = 1;

        public override int ID => item_id;
        public override string NAME => settingItem?.name;

        public void SetCharItemData(int uid, int cid, ISFSObject sfsObj)
        {
            this.cid = cid;
            this.uid = uid;
            this.item_no = sfsObj.GetLong("1");
            this.item_id = sfsObj.GetInt("2");
            this.size = sfsObj.GetShort("3");

            this.item_pos = ItemPos.GetByKey(sfsObj.GetByte("4"));
            this.Item_Exppoint = sfsObj.GetShort("5");
            this.puzzle = sfsObj.GetShort("6");

            this.Option_Index1 = sfsObj.GetShort("7");
            this.value1 = sfsObj.GetByte("8");
            this.Option_Index2 = sfsObj.GetShort("9");
            this.value2 = sfsObj.GetByte("10");
            this.Option_Index3 = sfsObj.GetShort("11");
            this.value3 = sfsObj.GetByte("12");
            this.Option_Index4 = sfsObj.GetShort("13");
            this.value4 = sfsObj.GetByte("14");
            this.Grinding_Add = sfsObj.GetByte("15");
            this.Grinding_Cnt = sfsObj.GetByte("16");
            this.flag = sfsObj.GetByte("17");

            this.pos = item_pos?.itemPosGroup.ToString();

            this.settingItem = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM).GetData(item_id) as ItemData;
            if (settingItem != null)
            {
                this.itemType = settingItem.GetItemType;

                this.type = itemType?.Value.ToString();
                this.grade = settingItem.item_grade;
                this.name = settingItem.name;

                if (Item_Exppoint > 0 && settingItem.upgrade_exp > 0)
                {
                    var level = Item_Exppoint / settingItem.upgrade_exp;
                    ItemLevel = (short)(level > 10 ? 10 : level);
                }
            }
            else this.name = "존재하지 않는 데이터";
        }

        public void ApplyChange()
        {
            Item_Exppoint = (short)(ItemLevel * settingItem.upgrade_exp);
        }

        public ISFSObject ToSFSObject()
        {
            ISFSObject data = SFSObject.NewInstance();

            data.PutLong("1", this.item_no);
            data.PutInt("2", this.item_id);
            data.PutShort("3", this.size);
            data.PutByte("4", this.item_pos.Key);                        
            data.PutShort("5", Item_Exppoint);
            data.PutShort("6", this.puzzle);
            data.PutShort("7", this.Option_Index1);
            data.PutByte("8", this.value1);
            data.PutShort("9", this.Option_Index2);
            data.PutByte("10", this.value2);
            data.PutShort("11", this.Option_Index3);
            data.PutByte("12", this.value3);
            data.PutShort("13", this.Option_Index4);
            data.PutByte("14", this.value4);
            data.PutByte("15", this.Grinding_Add);
            data.PutByte("16", this.Grinding_Cnt);
            data.PutByte("17", this.flag);

            return data;
        }

        public Tuple<string, TemplateBox>[] GetEditColumns()
        {
            object[] itemExp = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            object[] grindingCnt = { 0, 1, 2, 3, 4, 5 };

            int grindingMax = 99;
            object[] grindingAdd = new object[grindingMax];
            for (int i = 0; i < grindingMax; i++)
            {
                grindingAdd[i] = i + 1;
            }
            
            return new Tuple<string, TemplateBox>[]
            {                
                new Tuple<string, TemplateBox>( "ItemLevel",    new ComboTemplateBox(itemExp)),                
                new Tuple<string, TemplateBox>( "Grinding_Add",     new ComboTemplateBox(grindingAdd)),
                new Tuple<string, TemplateBox>( "Grinding_Cnt",     new ComboTemplateBox(grindingCnt)),
                new Tuple<string, TemplateBox>( "Option_Index1",    new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "value1",           new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Option_Index2",    new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "value2",           new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Option_Index3",    new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "value3",           new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "Option_Index4",    new TextTemplateBox()),
                new Tuple<string, TemplateBox>( "value4",           new TextTemplateBox()),
            };
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, settingItem.ID, settingItem.NAME, settingItem.item_grade, size),
            };
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "item_no", typeof(long)),
                new Tuple<string, Type>( "pos", typeof(string)),
                new Tuple<string, Type>( "type", typeof(string)),
                new Tuple<string, Type>( "item_id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "size", typeof(short)),
                new Tuple<string, Type>( "ItemLevel", typeof(short)),
                new Tuple<string, Type>( "grade", typeof(byte)),                                
                new Tuple<string, Type>( "Item_Exppoint", typeof(short)),
                new Tuple<string, Type>( "Grinding_Add", typeof(byte)),
                new Tuple<string, Type>( "Grinding_Cnt", typeof(byte)),
                new Tuple<string, Type>( "Option_Index1", typeof(short)),
                new Tuple<string, Type>( "value1", typeof(byte)),
                new Tuple<string, Type>( "Option_Index2", typeof(short)),
                new Tuple<string, Type>( "value2", typeof(byte)),
                new Tuple<string, Type>( "Option_Index3", typeof(short)),
                new Tuple<string, Type>( "value3", typeof(byte)),
                new Tuple<string, Type>( "Option_Index4", typeof(short)),
                new Tuple<string, Type>( "value4", typeof(byte)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            long no = row.Field<long>("item_no");
            return list.Find(v => (v as CharItemData).item_no == no);
        }

    }
}
