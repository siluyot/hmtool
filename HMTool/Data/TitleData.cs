﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class TitleData : ReadViewData
    {
        public int title_id;
        public string title_name;
        public int title_name_id;
        public string title_desc;
        public int title_desc_id;
        public int title_type;
        public int title_mem_index;
        public int title_limit_value;
        public int title_option_type1;
        public int title_option_type2;
        public int title_option_type3;
        public int title_grade;

        public override int ID => title_id;
        public override string NAME => title_name;

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("title_id");
            return list.Find(v => v.ID == id);
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
           {
                new Tuple<string, Type>( "title_id", typeof(int)),
                new Tuple<string, Type>( "title_name", typeof(string)),
                new Tuple<string, Type>( "title_desc", typeof(string)),
                new Tuple<string, Type>( "title_option_type1", typeof(int)),
                new Tuple<string, Type>( "title_option_type2", typeof(int)),
                new Tuple<string, Type>( "title_option_type3", typeof(int)),
                new Tuple<string, Type>( "title_grade", typeof(int)),
           };
        }
    }
}
