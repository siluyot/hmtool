﻿using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class BinggoPanData : ReadViewData
    {
        public enum ItemType
        {
            Reward,
            Collect,
        }

        public int id;
        public int group_id;
        public DateTime start_date;
        public DateTime end_date;
        public int binggo_x;
        public int binggo_y;
        public int collect_type;
        public int collect_id;
        public int collect_size;
        public int cash;
        public int reward_type;
        public int reward_id;
        public int reward_size;

        public string name;
        public ItemType itemType; // 빙고 수집용 아이템인지 보상 아이템인지
        public int size;

        public override int ID { get => itemType == ItemType.Collect ? collect_id : reward_id; }
        public override string NAME { get => name; }

        public GiftType GetGiftType()
        {
            if (itemType == ItemType.Collect)
            {
                // 기본적으로는 아이템이나 펫 같은 다른 타입이 될수도 있으므로 추가 작업이 필요할 수 있음.
                if (collect_type.ToEnum<Gift_Type>() == Gift_Type.ITEM)
                    return GiftType.ITEM;
            }
            else
            {
                Gift_Type rewardGiftType = reward_type.ToEnum<Gift_Type>();
                switch (rewardGiftType)
                {
                    case Gift_Type.ITEM:
                        return GiftType.ITEM;

                    case Gift_Type.DIA:
                        return GiftType.DIA;

                    case Gift_Type.PET_TICKET:
                        return GiftType.PET_TICKET;

                    case Gift_Type.ITEM_TICKET:
                        return GiftType.ITEM_TICKET;
                }
            }
            return GiftType.NONE;
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "size", typeof(int)),
                new Tuple<string, Type>( "itemType", typeof(string)),
                new Tuple<string, Type>( "start_date", typeof(DateTime)),
                new Tuple<string, Type>( "end_date", typeof(DateTime)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }
        
        public override DataBoxItem[] GetDataBoxItem()
        {
            int grade = 1;
            if (itemType == ItemType.Collect)
            {
                ItemData itemData = DataBaseManager.Instance.GetDataBase(DataBaseType.ITEM).GetData(ID) as ItemData;
                grade = itemData.item_grade;
            }
            return new MailData(MailTab.None, ID, NAME, grade, GetGiftType(), size).GetDataBoxItem();
        }
    }
}
