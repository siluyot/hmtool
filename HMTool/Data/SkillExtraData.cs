﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class SkillExtraData : ReadViewData
    {
        public int skill_extra_id;

        public int attack_count_0;
        public int attack_hit_rate_0;
        public int area_top_0;
        public int area_bottom_0;
        public int area_left_0;
        public int area_right_0;
        public int hit_count_0;
        public int hit_tick_0;

        public int attack_count_1;
        public int attack_hit_rate_1;
        public int area_top_1;
        public int area_bottom_1;
        public int area_left_1;
        public int area_right_1;
        public int hit_count_1;
        public int hit_tick_1;

        public int attack_count_2;
        public int attack_hit_rate_2;
        public int area_top_2;
        public int area_bottom_2;
        public int area_left_2;
        public int area_right_2;
        public int hit_count_2;
        public int hit_tick_2;

        public int effect_group_id1;
        public int effect_group_id2;

        public int area_type_0;
        public int area_type_1;
        public int area_type_2;

        public override int ID { get => skill_extra_id; }
        public override string NAME { get => string.Format("Type 0:{0} 1:{1} 2:{2}", area_type_0, area_type_1, area_type_2); }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "skill_extra_id", typeof(int)),
                new Tuple<string, Type>( "area_type_0", typeof(int)),
                new Tuple<string, Type>( "area_type_1", typeof(int)),
                new Tuple<string, Type>( "area_type_2", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("skill_extra_id");
            return list.Find(v => v.ID == id);
        } 
    }
}
