﻿using HMTool.Network;
using Sfs2X.Entities.Data;
using System;

namespace HMTool.Data
{
    public class RequestData
    {
        public Protocol protocol;
        public SFSObject sfsObject;
        public Action<bool, SFSObject> callback;

        public RequestData(Protocol protocol, SFSObject sfsObject = null, Action<bool, SFSObject> callback = null)
        {
            this.protocol = protocol;
            this.sfsObject = sfsObject ?? SFSObject.NewInstance();
            this.callback = callback;
        }
    }
}
