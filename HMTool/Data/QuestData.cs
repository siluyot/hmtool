﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class QuestData : ReadViewData
    {
        public int quest_id;
        public int quest_type;
        public int chapter_id;
        public int name_id;
        public int quest_desc_id;
        public int daily_group_id;
        public int daily_max_count;
        public int period_max_count;
        public int relay_seq;
        public DateTime start_date;
        public DateTime end_date;
        public int quest_limit_lv;
        public int quest_title_count;
        public int request_map_index;
        public int request_npc_id;
        public int request_desc_start_id;
        public int progress_desc_id;
        public int complete_map_index;
        public int complete_npc_id;
        public int complete_desc_start_id;

        public int quest_progress_map_index;
        public int quest_complete_type_01;
        public int quest_complete_type_id_01;
        public int quest_complete_count_01;
        public int quest_complete_prob_01;
        public int quest_complete_desc_id_01;

        public int quest_progress_map_index_02;
        public int quest_complete_type_02;
        public int quest_complete_type_id_02;
        public int quest_complete_count_02;
        public int quest_complete_prob_02;
        public int quest_complete_desc_id_02;

        public int next_quest_id;
        public int reward_exp;
        public int reward_gold;

        public int reward_type_1;
        public int reward_id_1;
        public int reward_count_1;

        public int reward_type_2;
        public int reward_id_2;
        public int reward_count_2;

        public int reward_type_3;
        public int reward_id_3;
        public int reward_count_3;

        public int quest_weight;

        public override int ID { get => quest_id; }
        public override string NAME { get => name_id.ToString(); }


        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "quest_id", typeof(int)),
                new Tuple<string, Type>( "quest_type", typeof(int)),
                new Tuple<string, Type>( "chapter_id", typeof(int)),
                new Tuple<string, Type>( "daily_max_count", typeof(int)),
                new Tuple<string, Type>( "period_max_count", typeof(int)),
                new Tuple<string, Type>( "start_date", typeof(DateTime)),
                new Tuple<string, Type>( "end_date", typeof(DateTime)),
                new Tuple<string, Type>( "quest_limit_lv", typeof(int)),
                new Tuple<string, Type>( "quest_title_count", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("quest_id");
            return list.Find(v => v.ID == id);
        }
    }
}
