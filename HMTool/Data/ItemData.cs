﻿using HMTool.Custom;
using HMTool.Data.Base;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class ItemData : ReadViewData, IMailData
    {
        public int item_id;
        public short group_id;
        public string icon;
        public short item_appear;

        [ColumnDetailParameter("drop_list", "item_type")]
        public byte item_type;

        public byte item_job;
        public byte item_grade;
        public byte item_gender;
        public short level;
        public string name;

        [BaseDetail(DataBaseType.LANGUAGE, "name, description")]
        public int name_id;
        [BaseDetail(DataBaseType.LANGUAGE, "name, description")]
        public int description_id;

        public int max_count;

        [DropListDetail("drop_list")]
        public int item_value_min;  
        [ColumnDetailParameter("drop_list", "item_value_max")]
        public int item_value_max;

        [BaseDetail(DataBaseType.OPTION_DATA, "puzzle_option")]
        public int puzzle_option_id;

        [ItemRandOptionDetail("item_rand_option")]
        public int item_rand_option_id;

        public byte item_rand_option_count;
        public int sell_gold;
        public int upgrade_exp;
        public int upgrade_stuff_exp;

        [BaseDetail(DataBaseType.ITEM, "item_upgrade_id")]        
        public int item_upgrade_id;

        [BaseDetail(DataBaseType.ITEM, "item_upgrade_consume_id")]        
        public int item_upgrade_consume_id;

        [SetTableDetail("set_table")]
        public int set_table_id;

        public int grinding_durability;
        public byte grinding_max_count;
        public short grinding_add;
        public byte belong_item;
        public byte enable_sell;
        public byte enable_dismantle;
        public byte enable_storage;
        public byte enable_trade;
        public int add_power;
        public byte remain_log;
        public byte visable_flag;
        public int m_group_id;
        public int m_min_pay;
        public int m_max_pay;

        [MapInfoDetail("drop_map_index")]
        public int drop_map_index1;
        [MapInfoDetail("drop_map_index")]
        public int drop_map_index2;
        [MapInfoDetail("drop_map_index")]
        public int drop_map_index3;
        [MapInfoDetail("drop_map_index")]
        public int drop_map_index4;
        [MapInfoDetail("drop_map_index")]
        public int drop_map_index5;

        public byte is_soul;
        public int book_category;
        public int book_set_id;
        public int book_score_min;
        public int book_score_max;

        public override int ID { get => item_id; }
        public override string NAME { get => name; }

        public GiftType GiftType { get => GiftType.ITEM; }
        public int MaxCount { get => max_count; }
        
        public ItemType GetItemType { get => ItemType.GetByKey(item_type); }
        public string Item_Type = string.Empty;

        public override Tuple<string, Type>[] GetColumns()
        {
            if (GetItemType != null)
                Item_Type = GetItemType.Value.ToString();

            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "item_id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "item_grade", typeof(int)),
                new Tuple<string, Type>( "level", typeof(int)),
                new Tuple<string, Type>( "Item_Type", typeof(string)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("item_id");
            string name = row.Field<string>("name");

            return list.Find(v => v.ID == id);
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, ID, NAME, item_grade),
            };
        }
    }
}
