﻿using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class MailData : ViewData, IMailData
    {
        public long seq;
        public MailTab mailTab;
        //public int id;
        //public string name;
        public string type;
        public int count;
        public int grade;
        public GiftType giftType;
        public string leftTime;
        
        private int maxCount;

        public override int ID => id;
        public override string NAME => name;

        public GiftType GiftType => giftType;
        public int MaxCount => maxCount;

        public MailData()
        {

        }

        public MailData(MailTab mailTab, int id, string name, int grade, GiftType giftType, int count, int maxCount = int.MaxValue, string leftTime = "None")
        {
            this.mailTab = mailTab;
            this.id = id;
            this.name = name;
            this.count = count;
            this.grade = grade;
            this.giftType = giftType;
            this.leftTime = leftTime;

            this.type = giftType.Value;
            this.maxCount = maxCount;
        }

        public MailData(BuddyPointGiftData buddyPointGiftData)
        {
            this.id = 0;
            this.name = "우정포인트";
            this.grade = 1;

            this.seq = buddyPointGiftData.seq;
            this.mailTab = MailTab.Buddy;
            this.giftType = GiftType.GetGiftType(buddyPointGiftData.gift_type);
            this.count = buddyPointGiftData.item_count;

            this.leftTime = GetLeftTime(buddyPointGiftData.insert_dt);

            this.type = giftType.Value;
            this.maxCount = int.MaxValue;
        }

        public MailData(TicketGiftData ticketGiftData)
        {
            this.id = ticketGiftData.gift;
            this.name = "티켓아이템";
            this.grade = 1;

            this.seq = ticketGiftData.seq;
            this.mailTab = MailTab.Ticket;
            this.giftType = GiftType.GetGiftType(ticketGiftData.gift_type);
            this.count = 1;

            this.leftTime = GetLeftTime(ticketGiftData.insert_dt);

            this.type = giftType.Value;
            this.maxCount = 1;
        }

        public MailData(GiftData giftData)
        {
            this.seq = giftData.seq;
            this.mailTab = MailTab.Gift;

            this.count = giftData.item_count;
            this.giftType = GiftType.GetGiftType(giftData.gift_type);
            this.leftTime = GetLeftTime(giftData.insert_dt);
            this.type = giftType.Value;

            List<ICustomData> dataList = null;
            if (giftType == GiftType.ITEM)
            {
                dataList = DataBaseManager.Instance.GetDataList(DataBaseType.ITEM);

                var findData = dataList.Find(v => v.ID == giftData.gift) as ItemData;
                this.id = findData.item_id;
                this.name = findData.name;
                this.grade = findData.item_grade;
                this.maxCount = findData.max_count;
            }
            else if (giftType == GiftType.PET)
            {
                dataList = DataBaseManager.Instance.GetDataList(DataBaseType.PET_BOOK);

                var findData = dataList.Find(v => v.ID == giftData.gift) as PetBookData;
                this.id = findData.pet_id;
                this.name = findData.pet_name;
                this.grade = findData.pet_grade;
                this.maxCount = 1;
            }
            else
            {
                this.id = 0;
                this.name = giftType.Value;
                this.grade = 1;
                this.maxCount = int.MaxValue;
            }

        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            long seq = row.Field<long>("seq");
            int id = row.Field<int>("id");
            string name = row.Field<string>("name");
            int count = row.Field<int>("count");

            return list.Find(v =>
            {
                var data = v as MailData;
                return
                data.seq == seq &&
                data.id == id &&
                data.name == name &&
                data.count == count;
            });
        }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
           {
                new Tuple<string, Type>( "seq", typeof(long)),
                new Tuple<string, Type>( "mailTab", typeof(string)),
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "type", typeof(string)),
                new Tuple<string, Type>( "grade", typeof(int)),
                new Tuple<string, Type>( "count", typeof(int)),
                new Tuple<string, Type>( "leftTime", typeof(string)),
           };
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, id, name, grade, count),
            };
        }

        //자바 -> c#
        public string GetLeftTime(long dt)
        {
            DateTime elapsedTime = new DateTime(dt * 10000);
            TimeSpan elapsedSpan = new TimeSpan(elapsedTime.Ticks);

            return string.Format("{0}일 {1}시 {2}분 {3}초", elapsedSpan.Days, elapsedSpan.Hours, elapsedSpan.Minutes, elapsedSpan.Seconds);
        }
    }
}
