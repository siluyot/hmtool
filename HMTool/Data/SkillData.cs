﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class SkillData : ReadViewData
    {
        public int skill_id;
        public int skill_type;
        public int skill_sub_type;
        public int skill_target_type;
        public int resource;
        public int effect_group_id;
        public int sound_id;
        public string name;
        public int name_id;
        public int description_id;
        public int skill_series;
        public int skill_group;
        public int skill_attr;
        public int skill_attr_rate;
        public int item_job;
        public int skill_level;
        public int need_level;
        public int skill_atk_type;
        public int weapon_limit;
        public int skill_dam_default;
        public int skill_dam_value;
        public int skill_dam_value_rate;
        public int skill_dam_prob_id;
        public int skill_attack_rate;
        public int tick_time;
        public int total_attack_count;
        public int dul_target_count;
        public int skill_control;
        public int invin_start_time;
        public int invin_duration;
        public int casting_time;
        public int monster_limit;
        public int cooldown_time;
        public int need_mp;
        public int duration;
        public int pvp_rate;
        public int skill_buff_id1;
        public int buff_value1;
        public int skill_buff_id2;
        public int buff_value2;
        public int buff_prob;
        public int usage_type;
        public int movespeed_x;
        public int movespeed_y;
        public int start_delay;
        public int move_duration;
        public int skill_extra_id;
        public int area_type;
        public int area_top;
        public int area_bottom;
        public int area_left;
        public int area_right;
        public int hit_count;
        public int hit_tick;
        public int aggro;
        public int knockback_X;
        public int knockback_Y;
        public int need_upgrade_exp;
        public int next_upgrade_skill;
        public int skill_set_value;
        public int consume_mana;
        public int consume_fury;
        public int consume_elly;
        public int add_power;

        public override int ID { get => skill_id; }
        public override string NAME { get => name; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "skill_id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
                new Tuple<string, Type>( "skill_type", typeof(int)),
                new Tuple<string, Type>( "next_upgrade_skill", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("skill_id");
            return list.Find(v => v.ID == id);
        }
    }
}
