﻿using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace HMTool.Data.Base
{
    public class ViewData : IViewData
    {
        public int id;
        public string name;

        public virtual int ID { get => id; }
        public virtual string NAME { get => name; }

        public virtual DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, ID, NAME),
            };
        }

        /// <summary>
        /// DataRow를 리스트에서 찾아서 ICustomData로 리턴
        /// </summary>
        /// <returns></returns>
        public virtual ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }

        /// <summary>
        /// 필드이름과 타입으로 구성된 간단한 Tuple
        /// new Tuple<string, Type>( "변수명", typeof(변수타입))
        /// </summary>
        /// <returns></returns>
        public virtual Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
           {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "name", typeof(string)),
           };
        }

        /// <summary>
        /// 모든 필드들의 이름과 타입으로 구성된 Tuple
        /// </summary>
        /// <returns></returns>
        public Tuple<string, Type>[] GetAllColumns()
        {
            FieldInfo[] fields = this.GetType().GetFields();

            List<Tuple<string, Type>> columnList = new List<Tuple<string, Type>>();
            foreach (var info in fields)
            {
                columnList.Add(new Tuple<string, Type>(info.Name, info.FieldType));
            }
            return columnList.ToArray();
        }
    }
}
