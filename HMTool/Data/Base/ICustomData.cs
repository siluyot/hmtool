﻿namespace HMTool.Data.Base
{
    /// <summary>
    /// 데이터 기본 단위
    /// </summary>
    public interface ICustomData
    {
        int ID { get; }
        string NAME { get; }
    }
}
