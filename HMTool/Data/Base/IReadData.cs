﻿using Sfs2X.Entities.Data;
using System.Data.SQLite;
using System.Text;

namespace HMTool.Data.Base
{
    /// <summary>
    /// 데이터를 읽어올 수 있을 경우.
    /// </summary>
    public interface IReadData : ICustomData
    {
        T GetReadData<T>(SQLiteDataReader rdr, StringBuilder sb) where T : new();
        T GetReadData<T>(ISFSObject sfsObj, StringBuilder sb) where T : new();
    }
}
