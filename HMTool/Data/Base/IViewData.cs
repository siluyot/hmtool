﻿using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data.Base
{
    /// <summary>
    /// 데이터를 보여주어야 할 경우.
    /// 주로 View에 표시하기 위함.
    /// </summary>
    public interface IViewData : ICustomData
    {
        Tuple<string, Type>[] GetColumns();
        Tuple<string, Type>[] GetAllColumns();

        DataBoxItem[] GetDataBoxItem();
        ICustomData DataRowToData(List<ICustomData> list, DataRow row);
    }
}
