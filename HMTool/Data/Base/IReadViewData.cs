﻿namespace HMTool.Data.Base
{
    /// <summary>
    /// 데이터를 읽기도 하고, 보여주기도 해야 할 경우.
    /// 주로 DB 데이터
    /// </summary>
    public interface IReadViewData : IReadData, IViewData
    {
    }
}
