﻿using HMTool.WindowView.ViewModel;
using Sfs2X.Entities.Data;
using System;
using System.Data.SQLite;
using System.Reflection;
using System.Text;

namespace HMTool.Data.Base
{
    public abstract class ReadData : IReadData
    {
        public abstract int ID { get; }
        public abstract string NAME { get; }

        public virtual DataBoxItem[] GetDataBoxItem()
        {
            return new DataBoxItem[]
            {
                new DataBoxItem(this, ID, NAME),
            };
        }

        /// <summary>
        /// DB 컬럼 이름으로 T 타입의 필드이름을 찾아서 읽어온다.
        /// </summary>
        public T GetReadData<T>(SQLiteDataReader rdr, StringBuilder sb) where T : new()
        {
            T newData = new T();

            for (int i = 0; i < rdr.FieldCount; i++)
            {
                string rdrName = rdr.GetName(i);    //컬럼 이름
                object rdrValue = rdr.GetValue(i);  //해당 데이터

                FieldInfo field = newData.GetType().GetField(rdrName);
                if (field != null)
                {
                    Type fieldType = field.FieldType;
                    field.SetValue(newData, Convert.ChangeType(rdrValue, fieldType));
                }
                else
                {
                    string msg = string.Format("[DB READ ERROR]\n{0} is not have {1}", typeof(T).Name, rdrName);
                    if (!sb.ToString().Contains(msg))
                    {
                        sb.AppendLine(msg);
                    }                
                }
            }
            return newData;
        }

        /// <summary>
        /// 받아온 데이터와 필드가 순서대로 매칭될때 사용 가능
        /// </summary>
        public T GetReadData<T>(ISFSObject sfsObj, StringBuilder sb) where T : new()
        {            
            T newData = new T();

            FieldInfo[] fields = newData.GetType().GetFields();
            string[] keys = sfsObj.GetKeys();

            for (int i = 0; i < fields.Length; i++)
            {
                SFSDataWrapper wrapper = sfsObj.GetData(keys[i]);

                object value = wrapper.Data;
                Type valueType = wrapper.Data.GetType();

                try
                {
                    fields[i].SetValue(newData, Convert.ChangeType(value, valueType));
                }
                catch(Exception e)
                {

                    string msg = string.Format("[SFSObj READ ERROR] {0} - {1}\n{2}", typeof(T).Name, fields[i].Name, e.Message);
                    if (!sb.ToString().Contains(msg))
                    {
                        sb.AppendLine(msg);
                    }
                }
                
            }
            return newData;
        }
    }
}
