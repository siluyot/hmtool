﻿using HMTool.Latale;

namespace HMTool.Data.Base
{
    /// <summary>
    /// 이 타입이 메일 보내기가 가능한 경우.
    /// </summary>
    public interface IMailData : ICustomData
    {
        int MaxCount { get; }
        GiftType GiftType { get; }
    }
}
