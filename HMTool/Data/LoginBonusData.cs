﻿using HMTool.Data.Base;
using HMTool.DB;
using HMTool.Latale;
using HMTool.WindowView.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class LoginBonusData : ReadViewData
    {
        public int id;
        public int group_id;
        public int bonus_type;
        public byte day;
        public int gift_type;
        public int gift;
        public int gift_option;
        public int item_count;
        public int vip_grade;
        public int effect;
        public int lid;

        public override int ID => id;
        public override string NAME => Gift_Name;

        public string Gift_Type = string.Empty;
        public string Gift_Name = string.Empty;

        private ICustomData data;

        public override Tuple<string, Type>[] GetColumns()
        {
            var type = GiftType.GetGiftType(gift_type);
            Gift_Type = type.Value;

            //기프트 변환
            if (type == GiftType.ITEM)
                data = DataBaseManager.Instance.GetDataBase(Latale.DataBaseType.ITEM).GetData(gift);
            else if (type == GiftType.PET)
                data = DataBaseManager.Instance.GetDataBase(Latale.DataBaseType.PET_BOOK).GetData(gift);

            Gift_Name = data != null ? data.NAME : gift.ToString();

            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "id", typeof(int)),
                new Tuple<string, Type>( "group_id", typeof(int)),
                new Tuple<string, Type>( "day", typeof(byte)),
                new Tuple<string, Type>( "Gift_Type", typeof(string)),
                new Tuple<string, Type>( "Gift_Name", typeof(string)),
                new Tuple<string, Type>( "item_count", typeof(int)),
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("id");
            return list.Find(v => v.ID == id);
        }

        public override DataBoxItem[] GetDataBoxItem()
        {
            if (data != null && data is IViewData)
                return (data as IViewData).GetDataBoxItem();
            
            return new DataBoxItem[]
            {
                new DataBoxItem(this, gift, Gift_Type, 1, item_count),
            };
        }
    }
}
