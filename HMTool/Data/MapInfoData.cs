﻿using HMTool.Data.Base;
using System;
using System.Collections.Generic;
using System.Data;

namespace HMTool.Data
{
    public class MapInfoData : ReadViewData
    {
        public int No;
        public int Map;
        public string Name;
        public int lid;
        public int type;
        public int width;
        public int height;
        public int base_x;
        public int base_y;
        public int return_map_index;
        public string recommend_level;
        public int map_resource;
        public int map_pvp;
        public bool isFieldMap;

        public override int ID { get => No; }
        public override string NAME { get => Name; }

        public override Tuple<string, Type>[] GetColumns()
        {
            return new Tuple<string, Type>[]
            {
                new Tuple<string, Type>( "No", typeof(int)),
                new Tuple<string, Type>( "Map", typeof(int)),
                new Tuple<string, Type>( "Name", typeof(string)),                
            };
        }

        public override ICustomData DataRowToData(List<ICustomData> list, DataRow row)
        {
            int id = row.Field<int>("no");
            return list.Find(v => v.ID == id);
        }
    }
}
